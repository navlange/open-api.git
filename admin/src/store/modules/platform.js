import { get as get_platform_info } from '@/platform/api/info'
import { Message } from 'element-ui'

const state = {
	platform_info: {}
}

const mutations = {
	SET_PLATFORM_INFO: (state, platform_info) => {
		state.platform_info = platform_info
	}
}

const actions = {
	get_info({ commit }) {
		return new Promise((resolve, reject) => {
			get_platform_info().then(response => {
				if (response.errorCode === 200) {
					const { data } = response
					commit('SET_PLATFORM_INFO', data)
					resolve()
				} else {
					Message({
						message: response.errorMsg,
						type: 'error',
						duration: 5 * 1000
					})
					reject(response.errorCode)
				}
			}).catch(error => {
				console.log(error)
				reject(error)
			})
		})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}

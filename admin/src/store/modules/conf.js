import { init as get_init } from '@/platform/api/conf'
import { Message } from 'element-ui'

const state = {
	alias: {}
}

const mutations = {
	SET_ALIAS: (state, alias) => {
		state.alias = alias
	}
}

const actions = {
	// user login
	get_init({ commit }) {
		return new Promise((resolve, reject) => {
			get_init().then(response => {
				if (response.errorCode === 200) {
					const { data } = response
					commit('SET_ALIAS', data.alias)
					resolve()
				} else {
					Message({
						message: response.errorMsg,
						type: 'error',
						duration: 5 * 1000
					})
					reject(response.errorCode)
				}
			}).catch(error => {
				console.log(error)
				reject(error)
			})
		})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}

import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function query(query) {
	return request({
		url: openapi_url('/app/install/query'),
		method: 'get',
		params: query
	})
}

export function list(query) {
	return request({
		url: openapi_url('/app/install/list'),
		method: 'get',
		params: query
	})
}

export function check(query) {
	return request({
		url: openapi_url('/app/install/check'),
		method: 'post',
		params: query
	})
}

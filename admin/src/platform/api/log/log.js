import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function loginSuccess(query) {
    return request({
        url: openapi_url('/platform/log/loginSuccess'),
        method: 'post',
        params: query
    })
}

export function list(query) {
    return request({
        url: openapi_url('/platform/log/list'),
        method: 'get',
        params: query
    })
}

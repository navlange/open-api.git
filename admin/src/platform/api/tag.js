import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function fetchList(query) {
  return request({
    url: openapi_url('/tag/list'),
    method: 'get',
    params: query
  })
}

export function ofScene(query) {
  return request({
    url: openapi_url('/tag/ofScene'),
    method: 'get',
    params: query
  })
}

export function create(query) {
  return request({
    url: openapi_url('/tag/create'),
    method: 'post',
    params: query
  })
}

export function deleteTag(query) {
  return request({
    url: openapi_url('/tag/delete'),
    method: 'post',
    params: query
  })
}

import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function ofStore(query) {
  return request({
    url: openapi_url('/printer/ofStore'),
    method: 'get',
    params: query
  })
}

export function create(query) {
  return request({
    url: openapi_url('/printer/create'),
    method: 'post',
    params: query
  })
}

export function deletePrinter(query) {
  return request({
    url: openapi_url('/printer/delete'),
    method: 'post',
    params: query
  })
}

export function updatePrinter(query) {
  return request({
    url: openapi_url('/printer/update'),
    method: 'post',
    params: query
  })
}

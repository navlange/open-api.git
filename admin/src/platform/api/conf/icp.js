import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function fetch(query) {
	return request({
		url: openapi_url('/platform/icp/get'),
		method: 'get',
		params: query
	})
}

export function update(query) {
	return request({
		url: openapi_url('/platform/icp/update'),
		method: 'post',
		params: query
	})
}

import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function get(query) {
	return request({
		url: openapi_url('/platform/os/get'),
		method: 'get',
		params: query
	})
}

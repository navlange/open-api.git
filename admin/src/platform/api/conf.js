import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function init(query) {
	return request({
		url: openapi_url('/conf/init/get'),
		method: 'get',
		params: query
	})
}

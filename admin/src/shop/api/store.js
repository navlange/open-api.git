import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function queryList(query) {
	return request({
		url: openapi_url('/store/list'),
		method: 'get',
		params: query
	})
}

export function fetch(query) {
	return request({
		url: openapi_url('/store/get'),
		method: 'get',
		params: query
	})
}

export function createStore(query) {
	return request({
		url: openapi_url('/store/create'),
		method: 'post',
		params: query
	})
}

export function importStore(query) {
	return request({
		url: openapi_url('/store/import'),
		method: 'post',
		params: query
	})
}

export function update(query, new_data) {
	return request({
		url: openapi_url('/store/update'),
		method: 'post',
		params: query,
		data: new_data,
		transformRequest: [
			function (data) {
				var ret = ''
				for (var it in data) {
					ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
				}
				ret = ret.substring(0, ret.lastIndexOf('&'))
				return ret
			}
		]
	})
}

export function create(query, new_data) {
	return request({
		url: openapi_url('/store/create'),
		method: 'post',
		params: query,
		data: new_data,
		transformRequest: [
			function (data) {
				var ret = ''
				for (var it in data) {
					ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
				}
				ret = ret.substring(0, ret.lastIndexOf('&'))
				return ret
			}
		]
	})
}

export function deleteStore(query) {
	return request({
		url: openapi_url('/store/delete'),
		method: 'post',
		params: query
	})
}

export function setOrder(query) {
	return request({
		url: openapi_url('/store/setOrder'),
		method: 'post',
		params: query
	})
}

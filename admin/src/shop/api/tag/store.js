import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function ofStore(query) {
	return request({
		url: openapi_url('/store/tag/ofStore'),
		method: 'get',
		params: query
	})
}

export function set(query) {
	return request({
		url: openapi_url('/store/tag/set'),
		method: 'post',
		params: query
	})
}

import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function bindUser(query) {
	return request({
		url: openapi_url('/store/auth/bindUser'),
		method: 'post',
		params: query
	})
}

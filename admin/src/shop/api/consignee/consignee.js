import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function get(query) {
	return request({
		url: openapi_url('/store/consignee/get'),
		method: 'get',
		params: query
	})
}

export function set(query) {
	return request({
		url: openapi_url('/store/consignee/set'),
		method: 'post',
		params: query
	})
}

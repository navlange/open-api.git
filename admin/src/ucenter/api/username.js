import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function update(query) {
    return request({
        url: openapi_url('/user/username/update'),
        method: 'post',
        params: query
    })
}

import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function list(query) {
	return request({
		url: openapi_url('/fans/list'),
		method: 'get',
		params: query
	})
}

export function ofUser(query) {
	return request({
		url: openapi_url('/fans/ofUser'),
		method: 'get',
		params: query
	})
}

export function deleteFans(query) {
	return request({
		url: openapi_url('/fans/delete'),
		method: 'post',
		params: query
	})
}

import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function fetch(query) {
	return request({
		url: openapi_url('/user/get'),
		method: 'get',
		params: query
	})
}

export function fetchByMobile(query) {
	return request({
		url: openapi_url('/user/getByMobile'),
		method: 'get',
		params: query
	})
}

export function createUser(query) {
	return request({
		url: openapi_url('/user/create'),
		method: 'post',
		params: query
	})
}

export function updateUser(query) {
	return request({
		url: openapi_url('/user/update'),
		method: 'post',
		params: query
	})
}

export function fetchList(query) {
	return request({
		url: openapi_url('/user/list'),
		method: 'get',
		params: query
	})
}

export function deleteUser(query) {
	return request({
		url: openapi_url('/user/delete'),
		method: 'post',
		params: query
	})
}

export function getIdcard(query) {
	return request({
		url: openapi_url('/user/idcard/get'),
		method: 'get',
		params: query
	})
}

import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function login(data) {
  return request({
    url: openapi_url('/auth/admin/login'),
    method: 'post',
    data
  })
}

export function getInfo(token) {
  // return new Promise(function(resolve, reject) {
  //   resolve({ data: { roles: ['admin'] }})
  // })
  return request({
    url: openapi_url('/auth/admin/role'),
    method: 'get',
    params: { token }
  })
}

export function logout(token) {
  return request({
    url: openapi_url('/auth/logout'),
    method: 'post',
    params: { token }
  })
}

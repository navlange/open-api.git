import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function ofProj(query) {
  return request({
    url: openapi_url('/clerk/list'),
    method: 'get',
    params: query
  })
}

export function fetch(query) {
  return request({
    url: openapi_url('/clerk/get'),
    method: 'get',
    params: query
  })
}

export function update(query) {
  return request({
    url: openapi_url('/clerk/update'),
    method: 'post',
    params: query
  })
}

export function create(query) {
  return request({
    url: openapi_url('/clerk/create'),
    method: 'post',
    params: query
  })
}

export function deleteClerk(query) {
  return request({
    url: openapi_url('/clerk/delete'),
    method: 'post',
    params: query
  })
}

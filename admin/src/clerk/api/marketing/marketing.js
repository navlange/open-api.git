import request from '@/utils/request'
import { smartore_url } from '@/utils/url'

export function fetch(query) {
  return request({
    url: smartore_url('/clerk/marketing/get'),
    method: 'get',
    params: query
  })
}

export function update(query) {
  return request({
    url: smartore_url('/clerk/marketing/update'),
    method: 'post',
    params: query
  })
}

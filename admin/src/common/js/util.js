export function isObj(object) {
	return object && typeof (object) === 'object' && Object.prototype.toString.call(object).toLowerCase() === '[object object]'
}

export function isJSON(str) {
	try {
		const obj = JSON.parse(str)
		return !!obj && typeof obj === 'object'
	} catch (e) {
		// console.log(e)
	}
	return false
}

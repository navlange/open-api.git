import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function fetch(query) {
	return request({
		url: openapi_url('/pay/wx/conf/get'),
		method: 'get',
		params: query
	})
}

export function set(query) {
	return request({
		url: openapi_url('/pay/wx/conf/set'),
		method: 'post',
		params: query
	})
}

export function setProfitsharing(query) {
	return request({
		url: openapi_url('/pay/wx/conf/setProfitsharing'),
		method: 'post',
		params: query
	})
}

export function gainPlatformCerts(query) {
	return request({
		url: openapi_url('/pay/wx/conf/gainPlatformCerts'),
		method: 'get',
		params: query
	})
}

import request from '@/utils/request'
import { openapi_url } from '@/utils/url'

export function bindAccount(query) {
	return request({
		url: openapi_url('/pay/withdraw/bindAccount'),
		method: 'post',
		params: query
	})
}

export function fetchList(query) {
	return request({
		url: openapi_url('/pay/withdraw/list'),
		method: 'get',
		params: query
	})
}

export function acceptWithdraw(query) {
	return request({
		url: openapi_url('/pay/withdraw/accept'),
		method: 'post',
		params: query
	})
}

export function deleteWithdraw(query) {
	return request({
		url: openapi_url('/pay/withdraw/delete'),
		method: 'post',
		params: query
	})
}

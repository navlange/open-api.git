<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2023-02-27 19:46:28
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	/** 公告 */
	$router->group(['prefix' => 'test'], function () use ($router) {
		// Activity
		$router->get('cert/getPublicKey', [
			'as' => 'getCertPublicKey', 'uses' => '\App\Http\Controllers\Test\CertController@getPublicKey'
		]);
	});

});

<?php
/*
 * @Author: lokei
 * @Date: 2022-08-02 18:52:31
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-04 08:23:47
 * @Description: 
 */

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	/**应用 */
	$router->group(['prefix' => 'app/install'], function () use ($router) {
		$router->get('query', [
			'as' => 'queryInstall', 'uses' => '\App\Http\Controllers\App\InstallController@query'
		]);
		$router->get('list', [
			'as' => 'listInstall', 'uses' => '\App\Http\Controllers\App\InstallController@list'
		]);
		$router->post('check', [
			'as' => 'checkInstall', 'uses' => '\App\Http\Controllers\App\InstallController@check'
		]);
	});
	
});

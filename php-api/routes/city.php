<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:13:01
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	$router->group(['prefix' => 'city'], function () use ($router) {
		// 城市小区
		$router->get('list', [
			'as' => 'cityList', 'uses' => '\App\Http\Controllers\City\CityController@list'
		]);
		$router->get('get', [
			'as' => 'getCity', 'uses' => '\App\Http\Controllers\City\CityController@get'
		]);
		$router->post('create', [
			'as' => 'createCity', 'uses' => '\App\Http\Controllers\City\CityController@create'
		]);
		$router->post('update', [
			'as' => 'updateCity', 'uses' => '\App\Http\Controllers\City\CityController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteCity', 'uses' => '\App\Http\Controllers\City\CityController@delete'
		]);

		// 省份
		$router->get('prov/list', [
			'as' => 'provList', 'uses' => '\App\Http\Controllers\City\ProvController@list'
		]);
		$router->get('prov/get', [
			'as' => 'getProv', 'uses' => '\App\Http\Controllers\City\ProvController@get'
		]);
		$router->post('prov/create', [
			'as' => 'createProv', 'uses' => '\App\Http\Controllers\City\ProvController@create'
		]);
		$router->post('prov/update', [
			'as' => 'updateProv', 'uses' => '\App\Http\Controllers\City\ProvController@update'
		]);
		$router->post('prov/delete', [
			'as' => 'deleteProv', 'uses' => '\App\Http\Controllers\City\ProvController@delete'
		]);
	});

	$router->group(['prefix' => 'city/area'], function () use ($router) {
		// 城市小区
		$router->get('list', [
			'as' => 'areaList', 'uses' => '\App\Http\Controllers\City\AreaController@list'
		]);
		$router->get('get', [
			'as' => 'getArea', 'uses' => '\App\Http\Controllers\City\AreaController@get'
		]);
		$router->post('create', [
			'as' => 'createArea', 'uses' => '\App\Http\Controllers\City\AreaController@create'
		]);
		$router->post('update', [
			'as' => 'updateArea', 'uses' => '\App\Http\Controllers\City\AreaController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteArea', 'uses' => '\App\Http\Controllers\City\AreaController@delete'
		]);
	});

	$router->group(['prefix' => 'city/district'], function () use ($router) {
		// 城市小区
		$router->get('list', [
			'as' => 'DistrictList', 'uses' => '\App\Http\Controllers\City\DistrictController@list'
		]);
		$router->get('ofCity', [
			'as' => 'districtOfCity', 'uses' => '\App\Http\Controllers\City\DistrictController@ofCity'
		]);
		$router->get('get', [
			'as' => 'getDistrict', 'uses' => '\App\Http\Controllers\City\DistrictController@get'
		]);
		$router->post('create', [
			'as' => 'createDistrict', 'uses' => '\App\Http\Controllers\City\DistrictController@create'
		]);
		$router->post('update', [
			'as' => 'updateDistrict', 'uses' => '\App\Http\Controllers\City\DistrictController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteDistrict', 'uses' => '\App\Http\Controllers\City\DistrictController@delete'
		]);
	});

});

<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2023-10-20 14:46:38
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	$router->group(['prefix' => 'clerk'], function () use ($router) {
		$router->get('get', [
			'as' => 'getClerk', 'uses' => '\App\Http\Controllers\Clerk\ClerkController@get'
		]);
		$router->get('list', [
			'as' => 'clerkList', 'uses' => '\App\Http\Controllers\Clerk\ClerkController@list'
		]);
		$router->post('update', [
			'as' => 'updateClerk', 'uses' => '\App\Http\Controllers\Clerk\ClerkController@update'
		]);
		$router->post('bindUser', [
			'as' => 'bindUserClerk', 'uses' => '\App\Http\Controllers\Clerk\ClerkController@bindUser'
		]);
		$router->post('cancelBindUser', [
			'as' => 'cancelBindUserClerk', 'uses' => '\App\Http\Controllers\Clerk\ClerkController@cancelBindUser'
		]);
		$router->post('create', [
			'as' => 'createClerk', 'uses' => '\App\Http\Controllers\Clerk\ClerkController@create'
		]);
		$router->post('delete', [
			'as' => 'deleteClerk', 'uses' => '\App\Http\Controllers\Clerk\ClerkController@delete'
		]);
	});
	$router->group(['prefix' => 'clerk', 'middleware' => 'auth'], function () use ($router) {
		// clerk
		$router->get('ofMe', [
			'as' => 'clerkOfMe', 'uses' => '\App\Http\Controllers\Clerk\ClerkController@ofMe'
		]);
	});

	$router->group(['prefix' => 'clerk/role'], function () use ($router) {
		// clerk role
		$router->get('get', [
			'as' => 'getClerkRole', 'uses' => '\App\Http\Controllers\Clerk\RoleController@get'
		]);
		$router->get('ofPcid', [
			'as' => 'ofPcidClerkRole', 'uses' => '\App\Http\Controllers\Clerk\RoleController@ofPcid'
		]);
		$router->post('update', [
			'as' => 'updateClerkRole', 'uses' => '\App\Http\Controllers\Clerk\RoleController@update'
		]);
	});

	$router->group(['prefix' => 'clerk/username'], function () use ($router) {
		// clerk username
		$router->get('get', [
			'as' => 'getClerkUsername', 'uses' => '\App\Http\Controllers\Clerk\UsernameController@get'
		]);
		$router->post('update', [
			'as' => 'updateClerkUsername', 'uses' => '\App\Http\Controllers\Clerk\UsernameController@update'
		]);
		$router->post('updateCid', [
			'as' => 'updateCidClerkUsername', 'uses' => '\App\Http\Controllers\Clerk\UsernameController@updateCid'
		]);
	});
	$router->group(['prefix' => 'clerk/password'], function () use ($router) {
		// clerk Password
		$router->post('update', [
			'as' => 'updateClerkPassword', 'uses' => '\App\Http\Controllers\Clerk\PasswordController@update'
		]);
		$router->post('updateCid', [
			'as' => 'updateCidClerkPassword', 'uses' => '\App\Http\Controllers\Clerk\PasswordController@updateCid'
		]);
	});
	
	// 平台管理员
	$router->group(['prefix' => 'clerk/role/admin'], function () use ($router) {
		// clerk role
		$router->get('get', [
			'as' => 'getClerkAdminRole', 'uses' => '\App\Http\Controllers\Clerk\Role\AdminController@get'
		]);
		$router->post('update', [
			'as' => 'updateClerkAdminRole', 'uses' => '\App\Http\Controllers\Clerk\Role\AdminController@update'
		]);
	});

	// 门店管理员
	$router->group(['prefix' => 'clerk/role/store'], function () use ($router) {
		// clerk role
		$router->get('get', [
			'as' => 'getClerkRole', 'uses' => '\App\Http\Controllers\Clerk\Role\StoreController@get'
		]);
		$router->post('update', [
			'as' => 'updateClerkRole', 'uses' => '\App\Http\Controllers\Clerk\Role\StoreController@update'
		]);
	});

});

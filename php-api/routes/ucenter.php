<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2023-12-30 22:08:46
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	/**用户中心 */
	// 粉丝
	$router->group(['prefix' => 'fans'], function () use ($router) {
		// 获取粉丝列表
		$router->get('list', [
			'as' => 'fansList', 'uses' => '\App\Http\Controllers\Ucenter\Fans\FansController@list'
		]);

		$router->get('ofUser', [
			'as' => 'fansOfUser', 'uses' => '\App\Http\Controllers\Ucenter\Fans\FansController@ofUser'
		]);
		
		$router->post('bindUser', [
			'as' => 'fansBindUser', 'uses' => '\App\Http\Controllers\Ucenter\Fans\FansController@bindUser'
		]);
		$router->post('delete', [
			'as' => 'fansDelete', 'uses' => '\App\Http\Controllers\Ucenter\Fans\FansController@delete'
		]);

		// 重新绑定用户
		$router->post('rebindUser', [
			'as' => 'rebindUser', 'uses' => '\App\Http\Controllers\Ucenter\Fans\FansController@rebindUser'
		]);
		
	});
	// 用户
	$router->group(['prefix' => 'user'], function () use ($router) {
		// 用户列表
		$router->get('list', [
			'as' => 'userList', 'uses' => '\App\Http\Controllers\Ucenter\User\UserController@list'
		]);
		$router->get('get', [
			'as' => 'getUser', 'uses' => '\App\Http\Controllers\Ucenter\User\UserController@get'
		]);
		$router->get('getByMobile', [
			'as' => 'getUserByMobile', 'uses' => '\App\Http\Controllers\Ucenter\User\UserController@getByMobile'
		]);
		$router->post('update', [
			'as' => 'updateUser', 'uses' => '\App\Http\Controllers\Ucenter\User\UserController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteUser', 'uses' => '\App\Http\Controllers\Ucenter\User\UserController@delete'
		]);
		
		// 登入用户名
		$router->post('username/update', [
			'as' => 'updateUsername', 'uses' => '\App\Http\Controllers\Ucenter\User\UsernameController@update'
		]);
		$router->post('password/update', [
			'as' => 'updatePassword', 'uses' => '\App\Http\Controllers\Ucenter\User\PasswordController@update'
		]);
		
	});
	$router->group(['prefix' => 'user', 'middleware' => 'auth'], function () use ($router) {
		$router->post('register', [
			'as' => 'register', 'uses' => '\App\Http\Controllers\Ucenter\User\UserController@register'
		]);
		// 修改用户信息
		$router->post('updateOfMe', [
			'as' => 'updateOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\UserController@updateOfMe'
		]);
		// 修改用户姓名
		$router->post('name/updateOfMe', [
			'as' => 'updateNameOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\NameController@updateOfMe'
		]);

		
		// 登入用户名
		$router->get('username/queryOfMe', [
			'as' => 'queryUsernameOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\UsernameController@queryOfMe'
		]);
		$router->post('username/updateOfMe', [
			'as' => 'updateUsernameOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\UsernameController@updateOfMe'
		]);
		
		
		// 登入密码
		$router->get('password/ofMe', [
			'as' => 'queryPasswordOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\PasswordController@ofMe'
		]);
		$router->post('password/set', [
			'as' => 'setPasswordOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\PasswordController@set'
		]);
		$router->post('password/updateOfMe', [
			'as' => 'updatePasswordOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\PasswordController@updateOfMe'
		]);
		
		// 查询用户信息
		$router->get('info/ofMe', [
			'as' => 'userInfoOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\InfoController@ofMe'
		]);
		
		// 查询微信小程序用户信息
		$router->get('info/ofMiniapp', [
			'as' => 'userInfoOfMiniapp', 'uses' => '\App\Http\Controllers\Ucenter\User\InfoController@ofMiniapp'
		]);
		
		// 查询微信公众号用户信息
		$router->get('info/ofMp', [
			'as' => 'userInfoOfMp', 'uses' => '\App\Http\Controllers\Ucenter\User\InfoController@ofMp'
		]);
		
		// 查询企业微信用户信息
		$router->get('info/ofQyapp', [
			'as' => 'userInfoOfQyapp', 'uses' => '\App\Http\Controllers\Ucenter\User\InfoController@ofQyapp'
		]);
		
		// 查询支付宝小程序用户信息
		$router->get('info/ofAlipay', [
			'as' => 'userInfoOfAlipay', 'uses' => '\App\Http\Controllers\Ucenter\User\InfoController@ofAlipay'
		]);
		
		// 查询抖音小程序用户信息
		$router->get('info/ofDouyin', [
			'as' => 'userInfoOfDouyin', 'uses' => '\App\Http\Controllers\Ucenter\User\InfoController@ofDouyin'
		]);
		
	});

	// 职业
	$router->group(['prefix' => 'user/career', 'middleware' => 'auth'], function () use ($router) {
		$router->get('ofMe', [
			'as' => 'ofMyCareer', 'uses' => '\App\Http\Controllers\Ucenter\User\CareerController@ofMe'
		]);
		$router->post('updateOfMe', [
			'as' => 'updateOfMyCareer', 'uses' => '\App\Http\Controllers\Ucenter\User\CareerController@updateOfMe'
		]);
		
	});

	// 区县
	$router->group(['prefix' => 'user/district', 'middleware' => 'auth'], function () use ($router) {
		$router->get('ofMe', [
			'as' => 'ofMyDistrict', 'uses' => '\App\Http\Controllers\Ucenter\User\DistrictController@ofMe'
		]);
		$router->post('updateOfMe', [
			'as' => 'updateOfMyDistrict', 'uses' => '\App\Http\Controllers\Ucenter\User\DistrictController@updateOfMe'
		]);
		
	});

	// 手机号
	$router->group(['prefix' => 'mobile'], function () use ($router) {
		// 查询微信小程序手机号信息
		$router->post('getWxBind', [
			'as' => 'getWxBindMobile', 'uses' => '\App\Http\Controllers\Ucenter\Mobile\MobileController@getWxBind'
		]);

		// 查询支付宝小程序手机号信息
		$router->post('getAliBind', [
			'as' => 'getAliBindMobile', 'uses' => '\App\Http\Controllers\Ucenter\Mobile\MobileController@getAliBind'
		]);
		
	});
	$router->group(['prefix' => 'mobile', 'middleware' => 'auth'], function () use ($router) {
		// 查询微信小程序手机号信息
		$router->post('updateOfMe', [
			'as' => 'updateOfMyMobile', 'uses' => '\App\Http\Controllers\Ucenter\Mobile\MobileController@updateOfMe'
		]);
		
	});
	
	$router->group(['prefix' => 'user/idcard', 'middleware' => 'auth'], function () use ($router) {
		// 查询微信小程序手机号信息
		$router->get('ofMe', [
			'as' => 'idcardOfMe', 'uses' => '\App\Http\Controllers\Ucenter\User\IdcardController@ofMe'
		]);
		$router->post('updateOfMe', [
			'as' => 'updateOfMyIdcard', 'uses' => '\App\Http\Controllers\Ucenter\User\IdcardController@updateOfMe'
		]);
		
	});
	
	$router->group(['prefix' => 'user/idcard'], function () use ($router) {
		// 查询微信小程序手机号信息
		$router->get('get', [
			'as' => 'getIdcard', 'uses' => '\App\Http\Controllers\Ucenter\User\IdcardController@get'
		]);
		
	});
	
	$router->group(['prefix' => 'user/bank', 'middleware' => 'auth'], function () use ($router) {
		// 银行信息
		$router->get('ofMe', [
			'as' => 'getBankOfMe', 'uses' => '\App\Http\Controllers\Ucenter\Bank\BankController@ofMe'
		]);
		
	});
	
	// 收藏
	$router->group(['prefix' => 'collect', 'middleware' => 'auth'], function () use ($router) {
		$router->get('ofMy', [
			'as' => 'ofMy', 'uses' => '\App\Http\Controllers\Ucenter\Collect\CollectController@ofMy'
		]);
		$router->get('get', [
			'as' => 'get', 'uses' => '\App\Http\Controllers\Ucenter\Collect\CollectController@get'
		]);
		$router->get('ofStore', [
			'as' => 'ofStore', 'uses' => '\App\Http\Controllers\Ucenter\Collect\CollectController@ofStore'
		]);
		$router->post('add', [
			'as' => 'add', 'uses' => '\App\Http\Controllers\Ucenter\Collect\CollectController@add'
		]);
		$router->post('del', [
			'as' => 'del', 'uses' => '\App\Http\Controllers\Ucenter\Collect\CollectController@del'
		]);
	});
	
});

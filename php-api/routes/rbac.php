<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-31 12:01:36
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	
	/**RBAC */
	$router->group(['prefix' => 'auth'], function () use ($router) {

		// 用户退出
		$router->post('register', [
			'as' => 'register', 'uses' => '\App\Http\Controllers\Rbac\Auth\AuthController@register'
		]);
		$router->post('login', [
			'as' => 'login', 'uses' => '\App\Http\Controllers\Rbac\Auth\AuthController@login'
		]);
		$router->post('logout', [
			'as' => 'logout', 'uses' => '\App\Http\Controllers\Rbac\Auth\AuthController@logout'
		]);
		
		// 后台用户登入
		$router->post('admin/login', [
			'as' => 'adminLogin', 'uses' => '\App\Http\Controllers\Rbac\Auth\AdminController@login'
		]);
		$router->get('admin/role', [
			'as' => 'adminRole', 'uses' => '\App\Http\Controllers\Rbac\Auth\AdminController@role'
		]);
		$router->post('admin/logout', [
			'as' => 'adminLogout', 'uses' => '\App\Http\Controllers\Rbac\Auth\AdminController@logout'
		]);
		
		// 微信小程序用户登入
		$router->post('miniapp/login', [
			'as' => 'miniappLogin', 'uses' => '\App\Http\Controllers\Rbac\Auth\MiniappController@login'
		]);
	
		// 微信公众号用户登入
		$router->post('mp/code', [
			'as' => 'mpCodeLogin', 'uses' => '\App\Http\Controllers\Rbac\Auth\MpController@code'
		]);
		
		// 企业微信用户登入
		$router->post('qw/code', [
			'as' => 'qwCodeLogin', 'uses' => '\App\Http\Controllers\Rbac\Auth\QwController@code'
		]);
		
		// 支付宝小程序用户登入
		$router->post('alipay/login', [
			'as' => 'alipayLogin', 'uses' => '\App\Http\Controllers\Rbac\Auth\AlipayController@login'
		]);
	
		// 头条小程序用户登入
		$router->post('douyin/login', [
			'as' => 'douyinLogin', 'uses' => '\App\Http\Controllers\Rbac\Auth\DouyinController@login'
		]);
	
		// 短信登入
		$router->post('mobile/login', [
			'as' => 'mobileLogin', 'uses' => '\App\Http\Controllers\Rbac\Auth\MobileController@login'
		]);
	
	});

    
	/**RBAC */
	$router->group(['prefix' => 'rbac', 'middleware' => 'auth'], function () use ($router) {

        
		// 登入用户名
		$router->get('username/ofMe', [
			'as' => 'queryUsernameOfMe', 'uses' => '\App\Http\Controllers\Rbac\UsernameController@ofMe'
		]);
		$router->post('username/updateOfMe', [
			'as' => 'updateUsernameOfMe', 'uses' => '\App\Http\Controllers\Rbac\UsernameController@updateOfMe'
		]);
		
		// 登入密码
		$router->get('password/ofMe', [
			'as' => 'queryPasswordOfMe', 'uses' => '\App\Http\Controllers\Rbac\PasswordController@ofMe'
		]);
		$router->post('password/updateOfMe', [
			'as' => 'updatePasswordOfMe', 'uses' => '\App\Http\Controllers\Rbac\PasswordController@updateOfMe'
		]);
    });
	
	
	$router->group(['prefix' => 'rabc/conf/login'], function () use ($router) {
		$router->get('get', [
			'as' => 'get', 'uses' => '\App\Http\Controllers\Rbac\Conf\LoginController@get'
		]);
		$router->post('set', [
			'as' => 'set', 'uses' => '\App\Http\Controllers\Rbac\Conf\LoginController@set'
		]);
	});
	$router->group(['prefix' => 'rabc/conf/miniapp'], function () use ($router) {
		$router->get('get', [
			'as' => 'get', 'uses' => '\App\Http\Controllers\Rbac\Conf\MiniappController@get'
		]);
		$router->post('set', [
			'as' => 'set', 'uses' => '\App\Http\Controllers\Rbac\Conf\MiniappController@set'
		]);
	});
});

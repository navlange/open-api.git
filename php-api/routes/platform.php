<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2024-09-16 03:34:56
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	$router->group(['prefix' => 'conf/init'], function () use ($router) {
		// 获取初始化设置
		$router->get('get', [
			'as' => 'getInitConf', 'uses' => '\App\Http\Controllers\Platform\Conf\InitController@get'
		]);
	});
	
	/**平台接口 */
	// 设置
	$router->group(['prefix' => 'conf'], function () use ($router) {
		// conf
		$router->get('get', [
			'as' => 'getConf', 'uses' => '\App\Http\Controllers\Platform\Conf\CommonController@get'
		]);
		$router->post('update', [
			'as' => 'updateConf', 'uses' => '\App\Http\Controllers\Platform\Conf\CommonController@update'
		]);
		$router->post('commercial/update', [
			'as' => 'updateCommercialConf', 'uses' => '\App\Http\Controllers\Platform\Conf\CommercialController@update'
		]);
		
		// 微信公众号设置
		$router->get('mp/get', [
			'as' => 'getConf', 'uses' => '\App\Http\Controllers\Platform\Conf\MpController@get'
		]);
		$router->post('mp/update', [
			'as' => 'updateConf', 'uses' => '\App\Http\Controllers\Platform\Conf\MpController@update'
		]);
		
		// miniapp conf
		$router->get('miniapp/get', [
			'as' => 'getConf', 'uses' => '\App\Http\Controllers\Platform\Conf\MiniappController@get'
		]);
		$router->post('miniapp/update', [
			'as' => 'updateConf', 'uses' => '\App\Http\Controllers\Platform\Conf\MiniappController@update'
		]);
		
		// qiwei conf
		$router->get('qiwei/get', [
			'as' => 'getConf', 'uses' => '\App\Http\Controllers\Platform\Conf\QiweiController@get'
		]);
		$router->post('qiwei/update', [
			'as' => 'updateConf', 'uses' => '\App\Http\Controllers\Platform\Conf\QiweiController@update'
		]);
		
		// alipay conf
		$router->get('alipay/get', [
			'as' => 'getConf', 'uses' => '\App\Http\Controllers\Platform\Conf\AlipayController@get'
		]);
		$router->post('alipay/update', [
			'as' => 'updateConf', 'uses' => '\App\Http\Controllers\Platform\Conf\AlipayController@update'
		]);
		
	});

	$router->group(['prefix' => 'conf/about'], function () use ($router) {
		// conf
		$router->get('get', [
			'as' => 'getConfAbout', 'uses' => '\App\Http\Controllers\Platform\Conf\AboutController@get'
		]);
	});

	$router->group(['prefix' => 'conf/admin'], function () use ($router) {
		// conf
		$router->get('get', [
			'as' => 'getConfAdmin', 'uses' => '\App\Http\Controllers\Platform\Conf\AdminController@get'
		]);
	});

	
	$router->group(['prefix' => 'platform/contacts'], function () use ($router) {

		// 联系设置
		$router->get('get', [
			'as' => 'getContactsConf', 'uses' => '\App\Http\Controllers\Platform\Conf\ContactsController@get'
		]);
		$router->post('update', [
			'as' => 'updateContactsConf', 'uses' => '\App\Http\Controllers\Platform\Conf\ContactsController@update'
		]);
		
	});

	$router->group(['prefix' => 'platform/icp'], function () use ($router) {

		// ICP设置
		$router->get('get', [
			'as' => 'getConf', 'uses' => '\App\Http\Controllers\Platform\Conf\IcpController@get'
		]);
		$router->post('update', [
			'as' => 'updateConf', 'uses' => '\App\Http\Controllers\Platform\Conf\IcpController@update'
		]);
		
	});

	$router->group(['prefix' => 'platform/info'], function () use ($router) {
		// 获取初始化设置
		$router->get('get', [
			'as' => 'getPlatformInfo', 'uses' => '\App\Http\Controllers\Platform\Info\InfoController@get'
		]);
	});
	$router->group(['prefix' => 'platform/os'], function () use ($router) {
		// 获取初始化设置
		$router->get('get', [
			'as' => 'getPlatformOs', 'uses' => '\App\Http\Controllers\Platform\Info\OsController@get'
		]);
	});

	// 微信公众号
	$router->group(['prefix' => 'platform/mp'], function () use ($router) {
		// 获取初始化设置
		$router->get('jssdk/getSignPackage', [
			'as' => 'getMpSignPackage', 'uses' => '\App\Http\Controllers\Platform\Mp\JssdkController@getSignPackage'
		]);
	});
	
	// 系统
	$router->group(['prefix' => 'platform/system'], function () use ($router) {

		// 系统设置
		$router->get('status/get', [
			'as' => 'getSystemStatus', 'uses' => '\App\Http\Controllers\Platform\System\StatusController@get'
		]);
		$router->post('status/set', [
			'as' => 'setSystemStatus', 'uses' => '\App\Http\Controllers\Platform\System\StatusController@set'
		]);
		
	});

	// 职业
	$router->group(['prefix' => 'career'], function () use ($router) {
		// Career
		$router->get('list', [
			'as' => 'careerList', 'uses' => '\App\Http\Controllers\Platform\Career\CareerController@list'
		]);
		$router->post('create', [
			'as' => 'createCareer', 'uses' => '\App\Http\Controllers\Platform\Career\CareerController@create'
		]);
		$router->post('update', [
			'as' => 'updateCareer', 'uses' => '\App\Http\Controllers\Platform\Career\CareerController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteCareer', 'uses' => '\App\Http\Controllers\Platform\Career\CareerController@delete'
		]);
		
	});

	// 抖音
	$router->group(['prefix' => 'dy/life'], function () use ($router) {
		// dy
		$router->get('conf/get', [
			'as' => 'getDyLifeConf', 'uses' => '\App\Http\Controllers\Platform\Dy\Life\ConfController@get'
		]);
		$router->post('conf/update', [
			'as' => 'updateDyLifeConf', 'uses' => '\App\Http\Controllers\Platform\Dy\Life\ConfController@update'
		]);
		$router->post('prepare', [
			'as' => 'dyVerifyPrepare', 'uses' => '\App\Http\Controllers\Platform\Dy\Life\VerifyController@prepare'
		]);
		$router->post('verify', [
			'as' => 'dyVerifySubmit', 'uses' => '\App\Http\Controllers\Platform\Dy\Life\VerifyController@submit'
		]);
	});
	
	// 上传
	$router->group(['prefix' => 'upload'], function () use ($router) {
		// upload
		$router->post('file', [
			'as' => 'uploadFile', 'uses' => '\App\Http\Controllers\Platform\Upload\FileController@upload'
		]);
		$router->post('image', [
			'as' => 'uploadImage', 'uses' => '\App\Http\Controllers\Platform\Upload\ImageController@upload'
		]);
		
	});

	
	// 职务
	$router->group(['prefix' => 'position'], function () use ($router) {
		// Position
		$router->get('list', [
			'as' => 'positionList', 'uses' => '\App\Http\Controllers\Platform\Position\PositionController@list'
		]);
		$router->post('create', [
			'as' => 'createPosition', 'uses' => '\App\Http\Controllers\Platform\Position\PositionController@create'
		]);
		$router->post('update', [
			'as' => 'updatePosition', 'uses' => '\App\Http\Controllers\Platform\Position\PositionController@update'
		]);
		$router->post('delete', [
			'as' => 'deletePosition', 'uses' => '\App\Http\Controllers\Platform\Position\PositionController@delete'
		]);
		
	});

	
	// 技能
	$router->group(['prefix' => 'skill'], function () use ($router) {
		// Skill
		$router->get('list', [
			'as' => 'skillList', 'uses' => '\App\Http\Controllers\Platform\Skill\SkillController@list'
		]);
		$router->post('create', [
			'as' => 'createSkill', 'uses' => '\App\Http\Controllers\Platform\Skill\SkillController@create'
		]);
		$router->post('update', [
			'as' => 'updateSkill', 'uses' => '\App\Http\Controllers\Platform\Skill\SkillController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteSkill', 'uses' => '\App\Http\Controllers\Platform\Skill\SkillController@delete'
		]);
		
	});


	// 标签
	$router->group(['prefix' => 'tag'], function () use ($router) {
		// tag
		$router->get('list', [
			'as' => 'tagList', 'uses' => '\App\Http\Controllers\Platform\Tag\TagController@list'
		]);
		$router->get('ofScene', [
			'as' => 'tagofScene', 'uses' => '\App\Http\Controllers\Platform\Tag\TagController@ofScene'
		]);
		$router->get('ofIds', [
			'as' => 'tagOfIds', 'uses' => '\App\Http\Controllers\Platform\Tag\TagController@ofIds'
		]);
		$router->post('create', [
			'as' => 'createTag', 'uses' => '\App\Http\Controllers\Platform\Tag\TagController@create'
		]);
		$router->post('update', [
			'as' => 'updateTag', 'uses' => '\App\Http\Controllers\Platform\Tag\TagController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteTag', 'uses' => '\App\Http\Controllers\Platform\Tag\TagController@delete'
		]);
		
	});
	// 分类
	$router->group(['prefix' => 'type'], function () use ($router) {
		// type
		$router->get('get', [
			'as' => 'typeget', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@get'
		]);
		$router->get('list', [
			'as' => 'typeList', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@list'
		]);
		$router->get('ofMode', [
			'as' => 'typeOfMode', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@ofMode'
		]);
		$router->get('ofModeRoot', [
			'as' => 'typeOfModeRoot', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@ofModeRoot'
		]);
		$router->get('ofScene', [
			'as' => 'typeOfScene', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@ofScene'
		]);
		$router->get('ofSceneRoot', [
			'as' => 'typeOfSceneRoot', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@ofSceneRoot'
		]);
		$router->get('ofPre', [
			'as' => 'typeOfPre', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@ofPre'
		]);
		$router->get('ofIndexArticleRecommend', [
			'as' => 'typeofIndexArticleRecommend', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@ofIndexArticleRecommend'
		]);
		$router->post('create', [
			'as' => 'createType', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@create'
		]);
		$router->post('update', [
			'as' => 'updateType', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteType', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@delete'
		]);
		$router->post('setOrder', [
			'as' => 'setOrderStore', 'uses' => '\App\Http\Controllers\Platform\Type\TypeController@setOrder'
		]);
		
	});

	// 打印
	$router->group(['prefix' => 'printer'], function () use ($router) {
		// printer
		$router->get('list', [
			'as' => 'printerlist', 'uses' => '\App\Http\Controllers\Platform\Print\PrinterController@list'
		]);
		$router->get('ofStore', [
			'as' => 'printerOfStore', 'uses' => '\App\Http\Controllers\Platform\Print\PrinterController@ofStore'
		]);
		$router->post('create', [
			'as' => 'createPrinter', 'uses' => '\App\Http\Controllers\Platform\Print\PrinterController@create'
		]);
		$router->post('update', [
			'as' => 'updatePrinter', 'uses' => '\App\Http\Controllers\Platform\Print\PrinterController@update'
		]);
		$router->post('delete', [
			'as' => 'deletePrinter', 'uses' => '\App\Http\Controllers\Platform\Print\PrinterController@delete'
		]);
		
	});
	$router->post('print', [
		'as' => 'print', 'uses' => '\App\Http\Controllers\Platform\Print\PrintController@print'
	]);
	

	// 统计
	$router->group(['prefix' => 'statistic'], function () use ($router) {
		// statistic
		$router->get('platform/get', [
			'as' => 'getStatistic', 'uses' => '\App\Http\Controllers\Platform\Statistic\PlatformController@get'
		]);
		$router->get('platform/all', [
			'as' => 'allStatistic', 'uses' => '\App\Http\Controllers\Platform\Statistic\PlatformController@all'
		]);
		$router->post('platform/incPv', [
			'as' => 'incPvStatistic', 'uses' => '\App\Http\Controllers\Platform\Statistic\PlatformController@incPv'
		]);
		$router->post('endapp/get', [
			'as' => 'getEndappStatistic', 'uses' => '\App\Http\Controllers\Platform\Statistic\EndappController@get'
		]);
	});

	// 建议
	$router->group(['prefix' => 'suggest', 'middleware' => 'auth'], function () use ($router) {
		// suggest
		$router->post('submit', [
			'as' => 'submitSuggest', 'uses' => '\App\Http\Controllers\Platform\Suggest\SuggestController@submit'
		]);
		$router->get('ofMy', [
			'as' => 'ofMySuggest', 'uses' => '\App\Http\Controllers\Platform\Suggest\SuggestController@ofMy'
		]);
	});
	$router->group(['prefix' => 'suggest'], function () use ($router) {
		// suggest
		$router->get('list', [
			'as' => 'suggestList', 'uses' => '\App\Http\Controllers\Platform\Suggest\SuggestController@list'
		]);
		$router->get('get', [
			'as' => 'getSuggest', 'uses' => '\App\Http\Controllers\Platform\Suggest\SuggestController@get'
		]);
		$router->post('finish', [
			'as' => 'finishSuggest', 'uses' => '\App\Http\Controllers\Platform\Suggest\SuggestController@finish'
		]);
		$router->post('delete', [
			'as' => 'deleteSuggest', 'uses' => '\App\Http\Controllers\Platform\Suggest\SuggestController@delete'
		]);
	});


	// 别名
	$router->group(['prefix' => 'alias'], function () use ($router) {
		// alias
		$router->get('get', [
			'as' => 'getAlias', 'uses' => '\App\Http\Controllers\Platform\Alias\AliasController@get'
		]);
		$router->post('update', [
			'as' => 'updateAlias', 'uses' => '\App\Http\Controllers\Platform\Alias\AliasController@update'
		]);
	});

	
	$router->group(['prefix' => 'platform/agreement'], function () use ($router) {
		// conf
		$router->get('get', [
			'as' => 'getAgreement', 'uses' => '\App\Http\Controllers\Platform\Agreement\AgreementController@get'
		]);
		$router->post('update', [
			'as' => 'updateAgreement', 'uses' => '\App\Http\Controllers\Platform\Agreement\AgreementController@update'
		]);
		$router->get('master/get', [
			'as' => 'getMasterAgreement', 'uses' => '\App\Http\Controllers\Platform\Agreement\AgreementController@getMaster'
		]);
		$router->get('user/get', [
			'as' => 'getUserAgreement', 'uses' => '\App\Http\Controllers\Platform\Agreement\AgreementController@getUser'
		]);
		$router->get('privacy/get', [
			'as' => 'getPrivacyAgreement', 'uses' => '\App\Http\Controllers\Platform\Agreement\AgreementController@getPrivacy'
		]);
	});


	// 平台
	$router->group(['prefix' => 'platform/miniapp'], function () use ($router) {
		// 小程序
		$router->get('version/ofProj', [
			'as' => 'getMiniappVersionOfProj', 'uses' => '\App\Http\Controllers\Platform\Miniapp\VersionController@ofProj'
		]);
		$router->get('version/get', [
			'as' => 'getMiniappVersion', 'uses' => '\App\Http\Controllers\Platform\Miniapp\VersionController@get'
		]);
		$router->post('version/create', [
			'as' => 'createMiniappVersion', 'uses' => '\App\Http\Controllers\Platform\Miniapp\VersionController@create'
		]);
		$router->post('version/update', [
			'as' => 'updateMiniappVersion', 'uses' => '\App\Http\Controllers\Platform\Miniapp\VersionController@update'
		]);
		$router->post('version/updateAuditId', [
			'as' => 'updateAuditIdMiniappVersion', 'uses' => '\App\Http\Controllers\Platform\Miniapp\VersionController@updateAuditId'
		]);
		$router->post('version/releaseSuccess', [
			'as' => 'releaseSuccessMiniappVersion', 'uses' => '\App\Http\Controllers\Platform\Miniapp\VersionController@releaseSuccess'
		]);
		$router->post('version/undoCodeAuditSuccess', [
			'as' => 'undoCodeAuditSuccessMiniappVersion', 'uses' => '\App\Http\Controllers\Platform\Miniapp\VersionController@undoCodeAuditSuccess'
		]);
		$router->post('version/delete', [
			'as' => 'deleteMiniappVersion', 'uses' => '\App\Http\Controllers\Platform\Miniapp\VersionController@delete'
		]);
	});
	
	// 支付宝小程序
	$router->group(['prefix' => 'platform/alipay'], function () use ($router) {
		// 版本
		$router->get('version/ofProj', [
			'as' => 'getAlipayVersionOfProj', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@ofProj'
		]);
		$router->get('version/get', [
			'as' => 'getAlipayVersion', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@get'
		]);
		$router->post('version/create', [
			'as' => 'createAlipayVersion', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@create'
		]);
		$router->post('version/update', [
			'as' => 'updateAlipayVersion', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@update'
		]);
		$router->post('version/updateAuditId', [
			'as' => 'updateAuditIdAlipayVersion', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@updateAuditId'
		]);
		$router->post('version/releaseSuccess', [
			'as' => 'releaseSuccessAlipayVersion', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@releaseSuccess'
		]);
		$router->post('version/undoCodeAuditSuccess', [
			'as' => 'undoCodeAuditSuccessAlipayVersion', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@undoCodeAuditSuccess'
		]);
		$router->post('version/auditedCancelSuccess', [
			'as' => 'auditedCancelSuccessAlipayVersion', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@auditedCancelSuccess'
		]);
		$router->post('version/delete', [
			'as' => 'deleteAlipayVersion', 'uses' => '\App\Http\Controllers\Platform\Alipay\VersionController@delete'
		]);

		// domain
		$router->get('domain/list', [
			'as' => 'getAlipayDomainList', 'uses' => '\App\Http\Controllers\Platform\Alipay\DomainController@list'
		]);
		$router->post('domain/addDomainLocal', [
			'as' => 'addAlipayDomainLocal', 'uses' => '\App\Http\Controllers\Platform\Alipay\DomainController@addDomainLocal'
		]);
		$router->post('domain/deleteDomainLocal', [
			'as' => 'deleteAlipayDomainLocal', 'uses' => '\App\Http\Controllers\Platform\Alipay\DomainController@deleteDomainLocal'
		]);
	});
	
	// 日志
	$router->group(['prefix' => 'platform/log'], function () use ($router) {
		// 日志
		$router->post('loginSuccess', [
			'as' => 'loginSuccess', 'uses' => '\App\Http\Controllers\Platform\Log\LogController@loginSuccess'
		]);
		// 日志
		$router->get('list', [
			'as' => 'list', 'uses' => '\App\Http\Controllers\Platform\Log\LogController@list'
		]);
	});

	// 搜索
	$router->group(['prefix' => 'platform/search'], function () use ($router) {
		// 搜索
		$router->post('add', [
			'as' => 'addSearch', 'uses' => '\App\Http\Controllers\Platform\Search\SearchController@add'
		]);
		$router->get('list', [
			'as' => 'listSearch', 'uses' => '\App\Http\Controllers\Platform\Search\SearchController@list'
		]);
	});

	// 品牌
	$router->group(['prefix' => 'brand'], function () use ($router) {
		// 品牌
		$router->get('/', [
			'as' => 'getBrand', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@get'
		]);
		$router->get('list', [
			'as' => 'brandList', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@list'
		]);
		$router->get('ofType0', [
			'as' => 'brandOfType0', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@ofType0'
		]);
		$router->post('create', [
			'as' => 'createBrand', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@create'
		]);
		$router->post('update', [
			'as' => 'updateBrand', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteBrand', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@delete'
		]);
		$router->post('setOrder', [
			'as' => 'setOrderStore', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@setOrder'
		]);
        // acceptAudit
        $router->post('acceptAudit', [
            'as' => 'acceptAuditBrand', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@acceptAudit'
        ]);
        // rejectAudit
        $router->post('rejectAudit', [
            'as' => 'rejectAuditBrand', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@rejectAudit'
        ]);
		
	});

    
	$router->group(['prefix' => 'brand', 'middleware' => 'auth'], function () use ($router) {
		// 品牌
		$router->get('/ofMe', [
			'as' => 'getBrandOfMe', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@ofMe'
		]);
        $router->post('register', [
            'as' => 'registerBrand', 'uses' => '\App\Http\Controllers\Platform\Brand\BrandController@register'
        ]);
    });

});

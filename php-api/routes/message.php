<?php
/*
 * @Author: lokei
 * @Date: 2022-08-02 18:52:31
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-03 13:43:55
 * @Description: 
 */

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {

	
	// 消息
	$router->group(['prefix' => 'message'], function () use ($router) {
		$router->get('get', [
			'as' => 'get', 'uses' => '\App\Http\Controllers\Message\MessageController@get'
		]);
		$router->get('list', [
			'as' => 'list', 'uses' => '\App\Http\Controllers\Message\MessageController@list'
		]);
		$router->get('ofType', [
			'as' => 'ofType', 'uses' => '\App\Http\Controllers\Message\MessageController@ofType'
		]);
		$router->post('create', [
			'as' => 'create', 'uses' => '\App\Http\Controllers\Message\MessageController@create'
		]);
		$router->post('update', [
			'as' => 'update', 'uses' => '\App\Http\Controllers\Message\MessageController@update'
		]);
		$router->post('delete', [
			'as' => 'delete', 'uses' => '\App\Http\Controllers\Message\MessageController@delete'
		]);
		
	});
	$router->group(['prefix' => 'message', 'middleware' => 'auth'], function () use ($router) {
		$router->get('ofMy', [
			'as' => 'ofMy', 'uses' => '\App\Http\Controllers\Message\MessageController@ofMy'
		]);
		$router->post('createByMe', [
			'as' => 'createByMe', 'uses' => '\App\Http\Controllers\Message\MessageController@createByMe'
		]);
		$router->get('newCount', [
			'as' => 'newCount', 'uses' => '\App\Http\Controllers\Message\MessageController@newCount'
		]);
		$router->post('read', [
			'as' => 'read', 'uses' => '\App\Http\Controllers\Message\MessageController@read'
		]);
	});
	
	/**消息中心 */
	// 公众号模板消息
	$router->group(['prefix' => 'message/template'], function () use ($router) {
		// 模板列表
		$router->get('list', [
			'as' => 'list', 'uses' => '\App\Http\Controllers\Message\Template\TemplateController@list'
		]);
		$router->get('get', [
			'as' => 'get', 'uses' => '\App\Http\Controllers\Message\Template\TemplateController@get'
		]);
		$router->post('create', [
			'as' => 'create', 'uses' => '\App\Http\Controllers\Message\Template\TemplateController@create'
		]);
		$router->post('update', [
			'as' => 'update', 'uses' => '\App\Http\Controllers\Message\Template\TemplateController@update'
		]);
		$router->post('delete', [
			'as' => 'delete', 'uses' => '\App\Http\Controllers\Message\Template\TemplateController@delete'
		]);
		
	});
	// 小程序订阅消息
	$router->group(['prefix' => 'message/template/miniapp'], function () use ($router) {
		// 模板列表
		$router->get('list', [
			'as' => 'list', 'uses' => '\App\Http\Controllers\Message\Template\MiniappController@list'
		]);
		$router->get('get', [
			'as' => 'get', 'uses' => '\App\Http\Controllers\Message\Template\MiniappController@get'
		]);
		$router->post('create', [
			'as' => 'create', 'uses' => '\App\Http\Controllers\Message\Template\MiniappController@create'
		]);
		$router->post('update', [
			'as' => 'update', 'uses' => '\App\Http\Controllers\Message\Template\MiniappController@update'
		]);
		$router->post('delete', [
			'as' => 'delete', 'uses' => '\App\Http\Controllers\Message\Template\MiniappController@delete'
		]);
		$router->get('ofScene', [
			'as' => 'ofScene', 'uses' => '\App\Http\Controllers\Message\Template\MiniappController@ofScene'
		]);
		
	});
	
});

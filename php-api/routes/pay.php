<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2024-09-05 22:04:37
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	//账户
	$router->group(['prefix' => 'pay/account'], function () use ($router) {
		$router->get('bank/ofUser', [
			'as' => 'bankAccountOfUser', 'uses' => '\App\Http\Controllers\Pay\Account\BankController@ofUser'
		]);
	});
	$router->group(['prefix' => 'pay/account', 'middleware' => 'auth'], function () use ($router) {
		// wx
		$router->get('ofMe', [
			'as' => 'accountOfMe', 'uses' => '\App\Http\Controllers\Pay\Account\AccountController@ofMe'
		]);
		$router->get('bank/ofMe', [
			'as' => 'bankAccountOfMe', 'uses' => '\App\Http\Controllers\Pay\Account\BankController@ofMe'
		]);
		$router->post('bank/updateOfMe', [
			'as' => 'bankAccountupdateOfMe', 'uses' => '\App\Http\Controllers\Pay\Account\BankController@updateOfMe'
		]);
		
	});

	//抖音担保支付
	$router->group(['prefix' => 'pay/douyin', 'middleware' => 'auth'], function () use ($router) {
		// douyin
		$router->post('ecpay', [
			'as' => 'ecPay', 'uses' => '\App\Http\Controllers\Pay\Douyin\PayController@ecpay'
		]);
		
	});
	$router->group(['prefix' => 'pay/douyin'], function () use ($router) {
		// 回调通知
		$router->post('notify/{pid}/{store_id}', [
			'as' => 'ecpayNotify', 'uses' => '\App\Http\Controllers\Pay\Douyin\NotifyController@callback'
		]);
	});

	$router->group(['prefix' => 'pay/wx/transfer'], function () use ($router) {
		// 微信转账
		$router->post('batches', [
			'as' => 'wxpayTransferBatches', 'uses' => '\App\Http\Controllers\Pay\Weixin\TransferController@batches'
		]);
	});

	// 兑换
	$router->group(['prefix' => 'pay/exchange/code'], function () use ($router) {
		// 兑换
		$router->get('list', [
			'as' => 'exchangeCodeList', 'uses' => '\App\Http\Controllers\Pay\Exchange\CodeController@list'
		]);
		$router->post('create', [
			'as' => 'createExchangeCode', 'uses' => '\App\Http\Controllers\Pay\Exchange\CodeController@create'
		]);
		$router->post('delete', [
			'as' => 'deleteExchangeCode', 'uses' => '\App\Http\Controllers\Pay\Exchange\CodeController@delete'
		]);
	});
	
	// 收入
	$router->group(['prefix' => 'pay/income'], function () use ($router) {
		// 收入
		$router->get('list', [
			'as' => 'incomeList', 'uses' => '\App\Http\Controllers\Pay\Income\IncomeController@list'
		]);
		$router->post('delete', [
			'as' => 'deleteIncome', 'uses' => '\App\Http\Controllers\Pay\Income\IncomeController@delete'
		]);
	});
	
	$router->group(['prefix' => 'pay/income/record', 'middleware' => 'auth'], function () use ($router) {
		// 收入
		$router->get('ofMe', [
			'as' => 'incomeListOfMe', 'uses' => '\App\Http\Controllers\Pay\Income\RecordController@ofMe'
		]);
		$router->get('ofMyFx', [
			'as' => 'incomeListofMyFx', 'uses' => '\App\Http\Controllers\Pay\Income\RecordController@ofMyFx'
		]);
	});
	
	$router->group(['prefix' => 'pay/income/record'], function () use ($router) {
		// 收入
		$router->get('ofOrder', [
			'as' => 'incomeListOfOrder', 'uses' => '\App\Http\Controllers\Pay\Income\RecordController@ofOrder'
		]);
	});
	
	// 提现
	$router->group(['prefix' => 'pay/withdraw'], function () use ($router) {
		$router->post('submit', [
			'as' => 'withdrawSubmit', 'uses' => '\App\Http\Controllers\Pay\Withdraw\WithdrawController@submit'
		]);
		$router->get('ofStore', [
			'as' => 'withdrawListOfMe', 'uses' => '\App\Http\Controllers\Pay\Withdraw\RecordController@ofStore'
		]);
		$router->get('list', [
			'as' => 'withdrawList', 'uses' => '\App\Http\Controllers\Pay\Withdraw\RecordController@list'
		]);
		$router->post('accept', [
			'as' => 'withdrawAccept', 'uses' => '\App\Http\Controllers\Pay\Withdraw\RecordController@accept'
		]);
	});
	
	// 储值
	$router->group(['prefix' => 'pay/card'], function () use ($router) {
		$router->get('get', [
			'as' => 'getCard', 'uses' => '\App\Http\Controllers\Pay\Card\CardController@get'
		]);
		$router->get('list', [
			'as' => 'cardList', 'uses' => '\App\Http\Controllers\Pay\Card\CardController@list'
		]);
		$router->post('create', [
			'as' => 'createCard', 'uses' => '\App\Http\Controllers\Pay\Card\CardController@create'
		]);
		$router->post('update', [
			'as' => 'updateCard', 'uses' => '\App\Http\Controllers\Pay\Card\CardController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteCard', 'uses' => '\App\Http\Controllers\Pay\Card\CardController@delete'
		]);
	});
	$router->group(['prefix' => 'pay/card/record', 'middleware' => 'auth'], function () use ($router) {
		$router->get('ofMe', [
			'as' => 'ofMeCard', 'uses' => '\App\Http\Controllers\Pay\Card\RecordController@ofMe'
		]);
		$router->get('ofMy', [
			'as' => 'ofMyCard', 'uses' => '\App\Http\Controllers\Pay\Card\RecordController@ofMy'
		]);
		$router->post('register', [
			'as' => 'registerCard', 'uses' => '\App\Http\Controllers\Pay\Card\RecordController@register'
		]);
	});
	$router->group(['prefix' => 'pay/card/record'], function () use ($router) {
		$router->get('get', [
			'as' => 'getCardRecord', 'uses' => '\App\Http\Controllers\Pay\Card\RecordController@get'
		]);
		$router->get('list', [
			'as' => 'listCardRecord', 'uses' => '\App\Http\Controllers\Pay\Card\RecordController@list'
		]);
		$router->post('delete', [
			'as' => 'deleteCardRecord', 'uses' => '\App\Http\Controllers\Pay\Card\RecordController@delete'
		]);
		$router->post('verify', [
			'as' => 'verifyCardRecord', 'uses' => '\App\Http\Controllers\Pay\Card\RecordController@verify'
		]);
		$router->post('recharge', [
			'as' => 'rechargeCardRecord', 'uses' => '\App\Http\Controllers\Pay\Card\RecordController@recharge'
		]);
	});

	
	// 收银
	$router->group(['prefix' => 'pay/cash'], function () use ($router) {
		$router->get('conf/get', [
			'as' => 'getCashConf', 'uses' => '\App\Http\Controllers\Pay\Cash\ConfController@get'
		]);
		$router->post('conf/update', [
			'as' => 'updateCashConf', 'uses' => '\App\Http\Controllers\Pay\Cash\ConfController@update'
		]);
		
		$router->get('get', [
			'as' => 'getCash', 'uses' => '\App\Http\Controllers\Pay\Cash\CashController@get'
		]);
		$router->get('list', [
			'as' => 'cashList', 'uses' => '\App\Http\Controllers\Pay\Cash\CashController@list'
		]);
		$router->get('ofOrder', [
			'as' => 'cashOfOrder', 'uses' => '\App\Http\Controllers\Pay\Cash\CashController@ofOrder'
		]);
		$router->post('create', [
			'as' => 'createCash', 'uses' => '\App\Http\Controllers\Pay\Cash\CashController@create'
		]);
		$router->post('update', [
			'as' => 'updateCash', 'uses' => '\App\Http\Controllers\Pay\Cash\CashController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteCash', 'uses' => '\App\Http\Controllers\Pay\Cash\CashController@delete'
		]);
	});
	
	// 消费记录
	$router->group(['prefix' => 'pay/record', 'middleware' => 'auth'], function () use ($router) {
		$router->get('ofMe', [
			'as' => 'payRecordOfMe', 'uses' => '\App\Http\Controllers\Pay\Record\RecordController@ofMe'
		]);
	});

	// 协议
	$router->group(['prefix' => 'pay/agreement'], function () use ($router) {
		$router->get('get', [
			'as' => 'getPayAgreement', 'uses' => '\App\Http\Controllers\Pay\AgreementController@get'
		]);
	});

	// 充值
	$router->group(['prefix' => 'pay/recharge'], function () use ($router) {
		// 充值
		$router->get('list', [
			'as' => 'getRechargeList', 'uses' => '\App\Http\Controllers\Pay\Recharge\RechargeController@list'
		]);
		$router->get('conf/get', [
			'as' => 'getRechargeConf', 'uses' => '\App\Http\Controllers\Pay\Recharge\ConfController@get'
		]);
		$router->post('conf/set', [
			'as' => 'setRechargeConf', 'uses' => '\App\Http\Controllers\Pay\Recharge\ConfController@set'
		]);
	});
});

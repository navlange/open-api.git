<?php
/*
 * @Author: lokei
 * @Date: 2022-08-02 18:52:31
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-29 16:23:13
 * @Description: 
 */

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	/**快递公司 */
	$router->group(['prefix' => 'delivery/company'], function () use ($router) {
		$router->get('list', [
			'as' => 'list', 'uses' => '\App\Http\Controllers\Delivery\CompanyController@list'
		]);
		$router->get('get', [
			'as' => 'get', 'uses' => '\App\Http\Controllers\Delivery\CompanyController@get'
		]);
		$router->post('create', [
			'as' => 'create', 'uses' => '\App\Http\Controllers\Delivery\CompanyController@create'
		]);
		$router->post('update', [
			'as' => 'update', 'uses' => '\App\Http\Controllers\Delivery\CompanyController@update'
		]);
		$router->post('delete', [
			'as' => 'delete', 'uses' => '\App\Http\Controllers\Delivery\CompanyController@delete'
		]);
		
	});
	
});

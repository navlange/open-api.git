<?php
/*
 * @Author: lokei
 * @Date: 2023-11-26 20:20:48
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-07 00:40:51
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	$router->group(['prefix' => 'email'], function () use ($router) {
		$router->post('sendCode', [
			'as' => 'sendCode', 'uses' => '\App\Http\Controllers\Email\VerifyController@sendCode'
		]);
    });

});

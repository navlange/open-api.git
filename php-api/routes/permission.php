<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2023-01-28 11:22:34
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	$router->group(['prefix' => 'role'], function () use ($router) {
		// role
		$router->get('list', [
			'as' => 'listRole', 'uses' => '\App\Http\Controllers\Rbac\Permission\RoleController@list'
		]);
		$router->get('get', [
			'as' => 'getRole', 'uses' => '\App\Http\Controllers\Rbac\Permission\RoleController@get'
		]);
		$router->post('create', [
			'as' => 'createRole', 'uses' => '\App\Http\Controllers\Rbac\Permission\RoleController@create'
		]);
		$router->post('update', [
			'as' => 'updateRole', 'uses' => '\App\Http\Controllers\Rbac\Permission\RoleController@update'
		]);
		$router->post('delete', [
			'as' => 'deleteRole', 'uses' => '\App\Http\Controllers\Rbac\Permission\RoleController@delete'
		]);
	});

	$router->group(['prefix' => 'permission/clerk'], function () use ($router) {
		// join
		$router->get('get', [
			'as' => 'getClerkPermission', 'uses' => '\App\Http\Controllers\Permission\ClerkController@get'
		]);
		$router->post('update', [
			'as' => 'updateClerkPermission', 'uses' => '\App\Http\Controllers\Permission\ClerkController@update'
		]);
	});

});

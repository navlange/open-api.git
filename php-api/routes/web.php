<?php
/*
 * @Author: lokei
 * @Date: 2022-08-02 18:52:31
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-29 09:39:53
 * @Description: 
 */

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	$router->get('/', function () use ($router) {
		return $router->app->version();
	});

	$router->get('/phpinfo', function () use ($router) {
		return phpinfo();
	});
	
	// ueditor
	$router->get('/ueditor', function () use ($router) {
		include("../app/Common/UEditor/controller.php");
	});
	$router->post('/ueditor', function () use ($router) {
		include("../app/Common/UEditor/controller.php");
	});

});

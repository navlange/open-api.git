<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-29 16:58:11
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	
	/**SCRM */
	$router->group(['prefix' => 'department'], function () use ($router) {
		// 获取部门列表
		$router->get('list', [
			'as' => 'department/list', 'uses' => '\App\Http\Controllers\Scrm\Department\DepartmentController@list'
		]);
		// 同步企业微信部门
		$router->post('qiwei/sync', [
			'as' => 'syncQiweiDepartment', 'uses' => '\App\Http\Controllers\Scrm\Qiwei\DepartmentController@sync'
		]);
		// 编辑部门
		$router->post('update', [
			'as' => 'updateDepartment', 'uses' => '\App\Http\Controllers\Scrm\Department\DepartmentController@update'
		]);
	});
	$router->group(['prefix' => 'clerk'], function () use ($router) {
		///企业微信
		// 获取部门成员列表
		$router->get('ofDepartment', [
			'as' => 'clerk/ofDepartment', 'uses' => '\App\Http\Controllers\Scrm\Clerk\ClerkController@ofDepartment'
		]);
		// 同步企业微信成员
		$router->post('qiwei/sync', [
			'as' => 'syncQiweiClerk', 'uses' => '\App\Http\Controllers\Scrm\Qiwei\ClerkController@sync'
		]);
		// 编辑企微成员
		$router->post('qiwei/update', [
			'as' => 'updateClerk', 'uses' => '\App\Http\Controllers\Scrm\Qiwei\ClerkController@update'
		]);
	});

	
	$router->group(['prefix' => 'customer'], function () use ($router) {
		
		$router->get('get', [
			'as' => 'getCustomer', 'uses' => '\App\Http\Controllers\Scrm\Customer\CustomerController@get'
		]);
		$router->get('list', [
			'as' => 'CustomerList', 'uses' => '\App\Http\Controllers\Scrm\Customer\CustomerController@list'
		]);
		$router->post('update', [
			'as' => 'updateCustomer', 'uses' => '\App\Http\Controllers\Scrm\Customer\CustomerController@update'
		]);
		$router->post('create', [
			'as' => 'createCustomer', 'uses' => '\App\Http\Controllers\Scrm\Customer\CustomerController@create'
		]);
		$router->post('delete', [
			'as' => 'deleteCustomer', 'uses' => '\App\Http\Controllers\Scrm\Customer\CustomerController@delete'
		]);
	});

	$router->group(['prefix' => 'customer', 'middleware' => 'auth'], function () use ($router) {
		
		$router->get('ofMe', [
			'as' => 'ofMeCustomer', 'uses' => '\App\Http\Controllers\Scrm\Customer\CustomerController@ofMe'
		]);
		$router->post('bindForMe', [
			'as' => 'bindForMeCustomer', 'uses' => '\App\Http\Controllers\Scrm\Customer\CustomerController@bindForMe'
		]);
	});

});

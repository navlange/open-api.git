<?php
/*
 * @Author: lokei
 * @Date: 2023-11-26 20:20:48
 * @LastEditors: lokei
 * @LastEditTime: 2023-11-26 20:20:51
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'api'], function () use ($router) {
	
	$router->group(['prefix' => 'sms'], function () use ($router) {
		$router->post('mobileCheck', [
			'as' => 'mobileCheck', 'uses' => '\App\Http\Controllers\Sms\MobileController@check'
		]);
    });

});

<?php
/*
 * @Author: lokei
 * @Date: 2022-12-31 11:37:18
 * @LastEditors: lokei
 * @LastEditTime: 2023-02-28 10:58:15
 * @Description: 
 */
namespace App\Middle;

use App\Models\Widget\NavModel;

class NavMiddle {
    public static function reset($mode) {
        if($mode == 'index') {
            NavModel::where('mode', '=', 'index')->delete();
            $nav = new NavModel();
			$nav->mode = $mode;
			$nav->name = "门店";
			$nav->wxapp_url = '"/store/pages/store/list"';
			$nav->display = "1";
			$nav->img = '""';
            $nav->save();
			$nav = new NavModel();
			$nav->mode = $mode;
			$nav->name ="商城";
			$nav->wxapp_url = '/things/pages/goods/list';
			$nav->display = "1";
			$nav->img = "";
            $nav->save();
			$nav = new NavModel();
			$nav->mode = $mode;
			$nav->name = "优惠券";
			$nav->wxapp_url = "/member/pages/coupon/list";
			$nav->display = "1";
			$nav->img = "";
            $nav->save();
			$nav = new NavModel();
			$nav->mode = $mode;
			$nav->name = "会员";
			$nav->wxapp_url = "/member/pages/person/index";
			$nav->display = "1";
			$nav->img = "";
            $nav->save();
        } else if ($mode == 'person') {
            NavModel::where('mode', '=', 'index')->delete();
            $nav = new NavModel();
			$nav->mode = $mode;
			$nav->name = "优惠券";
			$nav->wxapp_url = "/member/pages/coupon/my";
			$nav->display = "1";
			$nav->img = "";
			$nav->save();
			$nav = new NavModel();
			$nav->mode = $mode;
			$nav->name = "订单列表";
			$nav->wxapp_url = "/order/pages/order/list";
			$nav->display = "1";
			$nav->img = "";
			$nav->save();
			$nav = new NavModel();
			$nav->mode = $mode;
			$nav->name = "呼叫服务";
			$nav->nav_option = 'online_customer';
			$nav->wxapp_url = "";
			$nav->display = "1";
			$nav->img = "";
			$nav->save();
			$nav = new NavModel();
			$nav->mode = $mode;
			$nav->name = "充值记录";
			$nav->wxapp_url = "/pay/pages/recharge/list";
			$nav->display = "1";
			$nav->img = "";
			$nav->save();
        }
    }
}
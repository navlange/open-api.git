<?php
/*
 * @Author: lokei
 * @Date: 2023-05-22 23:22:41
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-22 23:29:20
 * @Description: 
 */
namespace App\Middle;

use App\Models\Platform\Statistic\StatisticModel;

class StatisticMiddle {
    public static function incStoreAmount($count = 1) {
        $statistic = StatisticModel::first();
        if(is_null($statistic)) {
            $statistic = new StatisticModel();
            $statistic->store_amount = $count;
            $statistic->save();
        } else {
            $statistic->store_amount = $statistic->store_amount + $count;
            $statistic->save();
        }
    }

    public static function incPv() {
        $statistic = StatisticModel::first();
        if(is_null($statistic)) {
            $statistic = new StatisticModel();
            $statistic->pv = 1;
            $statistic->save();
        } else {
            $statistic->pv = $statistic->pv + 1;
            $statistic->save();
        }
    }
}
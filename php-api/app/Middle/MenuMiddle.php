<?php
/*
 * @Author: lokei
 * @Date: 2022-12-31 21:24:55
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-20 20:17:33
 * @Description: 
 */
namespace App\Middle;

use App\Models\Widget\MenuModel;

class MenuMiddle {
    public static function reset() {
        $menu = new MenuModel();
        $menu->name = '首页';
        $menu->customer_name = '首页';
        $menu->name_en = 'index';
        $menu->theme = '0';
        $menu->action = '0';
        $menu->wxapp_url = '/pages/index/index';
        if (config('app.open_mode') != null && (config('app.open_mode') == '2' || config('app.open_mode') == '2-1')) {
            $menu->wxapp_url = '/hdb/pages/index';
        }
        $menu->display = '1';
        $menu->icon_name = 'shouye';
        $menu->save();
        $menu = new MenuModel();
        $menu->name = '发现';
        $menu->customer_name = '发现';
        $menu->name_en = 'store_list';
        $menu->theme = '0';
        $menu->action = '0';
        $menu->wxapp_url = '/shop/pages/list/index';
        $menu->display = '0';
        $menu->icon_name = 'faxian';
        $menu->save();
        $menu = new MenuModel();
        $menu->name = '电子商城';
        $menu->customer_name = '电子商城';
        $menu->name_en = 'shop';
        $menu->theme = '0';
        $menu->action = '0';
        $menu->wxapp_url = '/things/pages/goods/list';
        $menu->display = '0';
        $menu->icon_name = '31quanbushangpin';
        $menu->save();
        $menu = new MenuModel();
        $menu->name = '待用';
        $menu->customer_name = '待用';
        $menu->name_en = 'custom';
        $menu->theme = '0';
        $menu->action = '0';
        $menu->wxapp_url = '';
        $menu->display = '0';
        $menu->icon_name = 'faxian';
        $menu->save();
        $menu = new MenuModel();
        $menu->name = '我的订单';
        $menu->customer_name = '我的订单';
        $menu->name_en = 'order';
        $menu->theme = '0';
        $menu->action = '0';
        $menu->wxapp_url = '/order/pages/order/list';
        $menu->display = '0';
        $menu->icon_name = 'dingdan3';
        $menu->save();
        $menu = new MenuModel();
        $menu->name = '个人中心';
        $menu->customer_name = '个人中心';
        $menu->name_en = 'person';
        $menu->theme = '0';
        $menu->action = '0';
        $menu->wxapp_url = '/ucenter/pages/person/index';
        $menu->display = '1';
        $menu->icon_name = 'gerenzhongxinxia';
        $menu->save();
    }
}
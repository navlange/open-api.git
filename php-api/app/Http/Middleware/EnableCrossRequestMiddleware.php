<?php
/*
 * @Author: lokei
 * @Date: 2022-11-28 10:16:55
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-14 14:04:27
 * @Description: 
 */
namespace App\Http\Middleware;

use Closure;
class EnableCrossRequestMiddleware{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->method() == 'OPTIONS') {
            $response = response('success');
        } else {
            $response = $next($request);
        }
        $origin = $request->server('HTTP_ORIGIN') ? $request->server('HTTP_ORIGIN') : '';
        
        $allow_origin = explode(';', env('CROS_ORIGIN'));
        if (in_array($origin, $allow_origin)) {
            $response->header('Access-Control-Allow-Origin', $origin);
            $response->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, proj-token, app-mode, authorization');
            $response->header('Access-Control-Expose-Headers', 'Authorization, authenticated');
            $response->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, OPTIONS');
            $response->header('Access-Control-Allow-Credentials', 'true');
        }
        return $response;
    }
}

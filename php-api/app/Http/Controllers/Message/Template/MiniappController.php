<?php
/*
 * @Author: lokei
 * @Date: 2022-08-25 17:18:09
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-13 19:39:39
 * @Description: 
 */
namespace App\Http\Controllers\Message\Template;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Message\Template\TemplateModel;
use Illuminate\Http\Request;

class MiniappController extends Controller {
    public function list() {
        $items = TemplateModel::where('type', '=', 'miniapp')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function get(Request $request) {
        $template = TemplateModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $template;
        return $res;
    }

    public function create(Request $request) {
        $template = new TemplateModel();
        $template->type = 'miniapp';
        $template->identity = $request->input('identity');
        $template->name = $request->input('name');
        $template->template_id = $request->input('template_id');
        $template->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $template = TemplateModel::where('id', '=', $request->input('id'))->first();
        $template->identity = $request->input('identity');
        $template->name = $request->input('name');
        $template->template_id = $request->input('template_id');
        $template->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        TemplateModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
    
    public function ofScene(Request $request) {
        $template = TemplateModel::where('type', '=', 'miniapp')->where('identity', '=', $request->input('identity'))->first();
        $res = ResultTool::success();
        $res['data'] = $template;
        return $res;
    }

}
<?php
/*
 * @Author: lokei
 * @Date: 2022-08-25 17:18:09
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-13 10:41:32
 * @Description: 
 */
namespace App\Http\Controllers\Message\Template;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Message\Template\TemplateModel;
use Illuminate\Http\Request;

class TemplateController extends Controller {
    public function list() {
        $items = TemplateModel::where('type', '=', 'mp')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function get(Request $request) {
        $template = TemplateModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $template;
        return $res;
    }

    public function create(Request $request) {
        $template = new TemplateModel();
        $template->type = 'mp';
        $template->identity = $request->input('identity');
        $template->name = $request->input('name');
        $template->template_id = $request->input('template_id');
        $template->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $template = TemplateModel::where('id', '=', $request->input('id'))->first();
        $template->identity = $request->input('identity');
        $template->name = $request->input('name');
        $template->template_id = $request->input('template_id');
        $template->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        TemplateModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
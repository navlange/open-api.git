<?php
/*
 * @Author: lokei
 * @Date: 2023-05-19 18:48:52
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-03 13:46:12
 * @Description: 
 */

namespace App\Http\Controllers\Message;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Message\MessageModel;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;
use LDAP\Result;

class MessageController extends Controller
{

    public function get(Request $request)
    {
        $message = MessageModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $message;
        return $res;
    }

    public function list()
    {
        $items = MessageModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofType(Request $request)
    {
        $items = MessageModel::where('type', '=', $request->input('type'))->orderBy('created_at', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofMy()
    {
        $user = app('auth')->user();
        $items = MessageModel::where('uid', '=', $user->uid)->orderBy('created_at', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function newCount()
    {
        $user = app('auth')->user();
        $res = ResultTool::success();
        if (!is_null($user)) {
            $res['data']['count'] = $user->message_count;
        } else {
            $res['data']['count'] = 0;
        }
        return $res;
    }

    public function create(Request $request)
    {
        if ($request->type == '1') {
            $user = UserModel::where('uid', '=', intval($request->input('user_identity')))->first();
            if (is_null($user)) {
                $user = UserModel::where('mobile', '=', $request->input('user_identity'))->first();
            }
            if (is_null($user)) {
                $res = ResultTool::fail();
                $res['errorMsg'] = '非有效用户';
                return $res;
            } else {
                $user->message_count = $user->message_count + 1;
                $user->save();
            }
        }
        $message = new MessageModel();
        $message->type = $request->input('type');
        $message->content = $request->input('content');
        $message->created_at = date('Y-m-d H:i:s', time());
        if ($message->type == '1') {
            $message->uid = $user->uid;
        }
        $message->readed = '0';
        $message->save();
        return ResultTool::success();
    }

    public function createByMe(Request $request)
    {
        $user = app('auth')->user();
        $message = new MessageModel();
        $message->type = '0';
        $message->content = $request->input('content');
        $message->created_at = date('Y-m-d H:i:s', time());
        $message->uid = $user->uid;
        $message->readed = '0';
        $message->save();
        return ResultTool::success();
    }

    public function update(Request $request)
    {
        $message = MessageModel::where('id', '=', $request->input('id'))->first();
        $message->type = $request->input('type');
        $message->content = $request->input('content');
        $message->created_at = date('Y-m-d H:i:s', time());
        $message->save();
        return ResultTool::success();
    }

    public function delete(Request $request)
    {
        $message = MessageModel::where('id', '=', $request->input('id'))->first();
        if ($message->type == '1' && $message->readed == '0') {
            $user = UserModel::where('uid', '=', $message->uid)->first();
            if (!is_null($user)) {
                if ($user->message_count > 0) {
                    $user->message_count = $user->message_count - 1;
                } else {
                    $user->message_count = 0;
                }
                $user->save();
            }
        }
        MessageModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }

    public function read(Request $request) {
        $message = MessageModel::where('id', '=', $request->input('id'))->first();
        if($message->readed == '0') {
            $message->readed = '1';
            $message->save();
            if($message->type == '1') {
                $user = UserModel::where('uid', '=', $message->uid)->first();
                if(!is_null($user)) {
                    if($user->message_count > 0) {
                        $user->message_count = $user->message_count - 1;
                    } else {
                        $user->message_count = 0;
                    }
                    $user->save();
                }
            }
        }
        return ResultTool::success();
    }
}

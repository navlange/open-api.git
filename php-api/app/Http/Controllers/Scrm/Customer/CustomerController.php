<?php
/*
 * @Author: lokei
 * @Date: 2023-02-08 00:00:48
 * @LastEditors: lokei
 * @LastEditTime: 2023-09-09 16:59:50
 * @Description: 
 */
namespace App\Http\Controllers\Scrm\Customer;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Scrm\Customer\CustomerModel;
use Illuminate\Http\Request;

class CustomerController extends Controller {

    public function get(Request $request) {
        $customer = CustomerModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $customer;
        return $res;
    }

    public function ofMe() {
        $user = app('auth')->user();
        $customer = CustomerModel::where('uid', '=', $user->uid)->first();
        $res = ResultTool::success();
        $res['data'] = $customer;
        return $res;
    }

    public function bindForMe(Request $request) {
        $user = app('auth')->user();
        $customer = CustomerModel::where('id', '=', $request->input('customer_id'))->first();
        $customer->uid = $user->uid;
        $customer->save();
        return ResultTool::success();
    }

    public function list() {
        $items = CustomerModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $customer = new CustomerModel();
        $customer->sn = $request->input('sn');
        $customer->name = $request->input('name');
        $customer->mobile = $request->input('mobile');
        $customer->consignee_name = $request->input('consignee_name');
        $customer->save();
        return ResultTool::success();
    }
    
    public function update(Request $request) {
        $customer = CustomerModel::where('id', '=', $request->input('id'))->first();
        $customer->sn = $request->input('sn');
        $customer->name = $request->input('name');
        $customer->mobile = $request->input('mobile');
        $customer->consignee_name = $request->input('consignee_name');
        $customer->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        CustomerModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
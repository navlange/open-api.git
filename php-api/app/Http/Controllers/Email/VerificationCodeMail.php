<?php
/*
 * @Author: lokei
 * @Date: 2024-03-07 01:00:38
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-07 11:37:51
 * @Description: 
 */

namespace App\Http\Controllers\Email;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationCodeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.verification')
                    ->with([
                        'code' => $this->code,
                    ]);
    }
}
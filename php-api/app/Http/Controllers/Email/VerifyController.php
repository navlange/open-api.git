<?php
/*
 * @Author: lokei
 * @Date: 2024-03-07 00:47:52
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-07 12:11:01
 * @Description: 
 */
namespace App\Http\Controllers\Email;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;

class VerifyController extends Controller
{
    public function sendCode(Request $request)
    {
        $code = rand(100000, 999999); // Generate a random verification code
        Mail::to($request->input('email'))->send(new VerificationCodeMail($code));
        Redis::setex('email_verification_code:' . $request->input('email'), 10 * 60, $code);
        return ResultTool::success();
    }
}
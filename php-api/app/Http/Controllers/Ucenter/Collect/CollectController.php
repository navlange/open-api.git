<?php
/*
 * @Author: lokei
 * @Date: 2023-03-30 17:22:34
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-02 09:57:41
 * @Description: 
 */

namespace App\Http\Controllers\Ucenter\Collect;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Store\StoreModel;
use App\Models\Ucenter\Collect\CollectModel;
use Illuminate\Http\Request;

class CollectController extends Controller
{

    public function ofMy(Request $request)
    {
        $user = app('auth')->user();
        $model = CollectModel::where('uid', '=', $user->uid);
        if ($request->input('mode') != null && $request->input('mode') != '') {
            $model->where('mode', '=', $request->input('mode'));
        }

        if ($request->input('mode') != null && $request->input('mode') == 'store') {
            $model->with('store');
        }
        $items = $model->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }

    public function get(Request $request)
    {
        $user = app('auth')->user();
        $model = CollectModel::where('uid', '=', $user->uid)->where('target_id', '=', $request->input('target_id'));
        if ($request->input('mode') != null && $request->input('mode') != '') {
            $model->where('mode', '=', $request->input('mode'));
        }
        $collect = $model->first();
        $is_collected = '0';
        if (!is_null($collect)) {
            $is_collected = '1';
        }
        $res = ResultTool::success();
        $res['data']['collected'] = $is_collected;
        return $res;
    }

    public function ofStore(Request $request)
    {
        $user = app('auth')->user();
        $model = CollectModel::where('uid', '=', $user->uid)->where('store_id', '=', $request->input('store_id'));
        $collect = $model->first();
        $is_collected = '0';
        if (!is_null($collect)) {
            $is_collected = '1';
        }

        $store = StoreModel::where('id', '=', $request->input('store_id'))->first();
        $store->collect_count = $store->collect_count + 1;
        $store->save();

        $res = ResultTool::success();
        $res['data']['collected'] = $is_collected;
        return $res;
    }

    public function add(Request $request)
    {
        $user = app('auth')->user();
        $model = CollectModel::where('uid', '=', $user->uid);
        $model->target_id = $request->input('target_id');
        if ($request->input('mode') != null && $request->input('mode') != '') {
            $model = $model->where('mode', '=', $request->input('mode'));
        }
        $is_exist = $model->first();
        if (is_null($is_exist)) {
            $collect = new CollectModel();
            $collect->uid = $user->uid;
            $collect->mode = $request->input('mode');
            $collect->target_id = $request->input('target_id');
            $collect->createtime = date('Y-m-d H:i:s', time());
            $collect->save();
        }
        return ResultTool::success();
    }

    public function del(Request $request)
    {
        $user = app('auth')->user();
        $conditions = [
            'uid' => $user->uid
        ];
        $conditions['target_id'] = $request->input('target_id');
        $conditions['mode'] = $request->input('mode');
        CollectModel::where($conditions)->delete();
        return ResultTool::success();
    }
}

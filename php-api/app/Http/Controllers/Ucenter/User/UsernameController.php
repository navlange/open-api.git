<?php
/*
 * @Author: lokei
 * @Date: 2022-09-26 15:03:48
 * @LastEditors: lokei
 * @LastEditTime: 2023-03-23 09:13:26
 * @Description: 
 */
namespace App\Http\Controllers\Ucenter\User;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;

class UsernameController extends Controller {
	public function queryOfMe() {
		$user = app('auth')->user();
		$res = ResultTool::success();
		$res['data']['username'] = $user->username;
		return $res;
	}
    public function updateOfMe(Request $request) {
		$user = app('auth')->user();
		$exist_username = UserModel::where('username', '=', $request->input('username'))->where('uid', '!=', $user->uid)->first();
		if(!is_null($exist_username)) {
			$res = ResultTool::fail();
			$res['errorMsg'] = '用户名已存在！';
			return $res;
		}
        $user->username = $request->input('username');
        $user->save();
        return ResultTool::success();
    }

	public function update(Request $request) {
		$exist_username = UserModel::where('username', '=', $request->input('username'))->where('uid', '!=', $request->input('uid'))->first();
		if(!is_null($exist_username)) {
			$res = ResultTool::fail();
			$res['errorMsg'] = '用户名已存在！';
			return $res;
		}
		$user = UserModel::where('uid', '=', $request->input('uid'))->first();
        $user->username = $request->input('username');
        $user->save();
        return ResultTool::success();
	}
}
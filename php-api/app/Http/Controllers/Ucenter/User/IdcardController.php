<?php
/*
 * @Author: lokei
 * @Date: 2022-09-26 15:03:48
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-15 22:02:23
 * @Description: 
 */
namespace App\Http\Controllers\Ucenter\User;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;

class IdcardController extends Controller {

    public function get(Request $request) {
        $user = UserModel::where('uid', '=', $request->input('uid'))->first();
        $res = ResultTool::success();
        $res['data']['idcard'] = $user->idcard;
        $res['data']['idcard_img'] = $user->idcard_img;
        $res['data']['idcard_bg_img'] = $user->idcard_bg_img;
        return $res;
    }

    public function ofMe() {
        $user = app('auth')->user();
        $res = ResultTool::success();
        $res['data'] = [
            'idcard' => $user->idcard,
            'idcard_img' => $user->idcard_img,
            'idcard_bg_img' => $user->idcard_bg_img
        ];
        return $res;
    }
    public function updateOfMe(Request $request) {
		$user = app('auth')->user();
        $user->idcard = $request->input('idcard');
        $user->idcard_img = $request->input('idcard_img');
        $user->idcard_bg_img = $request->input('idcard_bg_img');
        $user->save();
        return ResultTool::success();
    }
}
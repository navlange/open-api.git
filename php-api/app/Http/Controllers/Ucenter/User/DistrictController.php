<?php
/*
 * @Author: lokei
 * @Date: 2022-09-26 15:03:48
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-12 11:14:28
 * @Description: 
 */
namespace App\Http\Controllers\Ucenter\User;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DistrictController extends Controller {
    public function ofMe() {
		$user = app('auth')->user();
        $res = ResultTool::success();
        $res['data']['district'] = $user->district;
        return $res;
    }

    public function updateOfMe(Request $request) {
		$user = app('auth')->user();
        $user->district = $request->input('district');
        $user->save();
        return ResultTool::success();
    }
}
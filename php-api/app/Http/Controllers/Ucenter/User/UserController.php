<?php
/*
 * @Author: lokei
 * @Date: 2022-09-26 10:42:08
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-24 19:36:05
 * @Description: 
 */

namespace App\Http\Controllers\Ucenter\User;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function register(Request $request)
    {
        $user = app('auth')->user();
        $user->realname = $request->input('name');
        $user->mobile = $request->input('mobile');
        $user->district = $request->input('district');
        $user->career = $request->input('career');
        $user->has_registered = '1';
        $user->save();
        return ResultTool::success();
    }

    public function list(Request $request)
    {
        $model = UserModel::where('uid', '>', 0);
        $total_model = UserModel::where('uid', '>', 0);
        if ($request->input('mobile') != null && $request->input('mobile') != '') {
            $model = $model->where('mobile', '=', $request->input('mobile'));
            $total_model = $total_model->where('mobile', '=', $request->input('mobile'));
        }
        if ($request->input('name') != null && $request->input('name') != '') {
            $model->where('realname', 'LIKE', "%{$request->input('name')}%")->orWhere('nickname', 'LIKE', "%{$request->input('name')}%");
            $total_model->where('realname', 'LIKE', "%{$request->input('name')}%")->orWhere('nickname', 'LIKE', "%{$request->input('name')}%");
        }
        if ($request->input('limit') != null && $request->input('limit') != '') {
            $model->limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')));
        }
        $items = $model->orderBy('createtime', 'desc')->get();
        $data = array();
        foreach ($items as $item) {
            array_push($data, [
                'uid' => $item->uid,
                'avatar' => $item->avatar,
                'realname' => $item->realname,
                'nickname' => $item->nickname,
                'mobile' => $item->mobile,
                'balance' => $item->balance,
                'integral' => $item->integral,
                'district' => $item->district,
                'career' => $item->career,
                'createtime' => $item->createtime
            ]);
        }
        $res = ResultTool::success();
        $res['data']['items'] = $data;
        $res['data']['total'] = $total_model->count();
        return $res;
    }

    public function get(Request $request)
    {
        $user = UserModel::where('uid', '=', $request->input('uid'))->first();
        if (!is_null($user)) {
            $res = ResultTool::success();
            $data = [
                'username' => $user->username,
                'mobile' => $user->mobile
            ];
            $res['data'] = $data;
            return $res;
        } else {
            $res = ResultTool::fail();
            $res['errorMsg'] = '用户不存在';
            return $res;
        }
    }

    public function getByMobile(Request $request)
    {
        $user = UserModel::where('mobile', '=', $request->input('mobile'))->first();
        if (!is_null($user)) {
            $res = ResultTool::success();
            $data = [
                'uid' => $user->uid,
                'username' => $user->username,
                'mobile' => $user->mobile
            ];
            $res['data'] = $data;
            return $res;
        } else {
            $res = ResultTool::fail();
            $res['errorMsg'] = '手机号用户不存在';
            return $res;
        }
    }

    public function update(Request $request)
    {
        $is_other = UserModel::where('mobile', '=', $request->input('mobile'))->where('uid', '!=', $request->input('uid'))->first();
        if (!is_null($is_other)) {
            $res = ResultTool::fail();
            $res['errorMsg'] = '手机号已经绑定其它用户！';
            return $res;
        }
        $user = UserModel::where('uid', '=', $request->input('uid'))->first();
        $user->mobile = $request->input('mobile');
        $user->save();
        return ResultTool::success();
    }

    public function updateOfMe(Request $request)
    {
        $user = app('auth')->user();
        $user->nickname = $request->input('nickname');
        $user->avatar = $request->input('avatar');
        $user->save();
        return ResultTool::success();
    }

    public function delete(Request $request)
    {
        UserModel::where('uid', '=', $request->input('uid'))->delete();
        return ResultTool::success();
    }
}

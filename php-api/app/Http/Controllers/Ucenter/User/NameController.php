<?php
/*
 * @Author: lokei
 * @Date: 2022-09-26 15:03:48
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-08 17:09:41
 * @Description: 
 */
namespace App\Http\Controllers\Ucenter\User;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NameController extends Controller {
    public function updateOfMe(Request $request) {
		$user = app('auth')->user();
        $user->realname = $request->input('name');
        $user->save();
        return ResultTool::success();
    }
}
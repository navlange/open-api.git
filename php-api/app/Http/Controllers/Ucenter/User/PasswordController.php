<?php
/*
 * @Author: lokei
 * @Date: 2022-09-26 15:03:48
 * @LastEditors: lokei
 * @LastEditTime: 2023-03-23 09:31:02
 * @Description: 
 */

namespace App\Http\Controllers\Ucenter\User;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
	public function ofMe()
	{
		$user = app('auth')->user();
		$res = ResultTool::success();
		$res['data']['is_set'] = (is_null($user->password) || $user->password == '') ? '0' : '1';
		return $res;
	}

	public function updateOfMe(Request $request)
	{
		$user = app('auth')->user();
		if (is_null($user->password) || $user->password == '') {
			$user->password = sha1($request->input('password') . '-' . $user->salt . '-' . env('AUTH_KEY'));
			$user->save();
		} else {
			if ($user->password != sha1($request->input('old_password') . '-' . $user->salt . '-' . env('AUTH_KEY'))) {
				$res = ResultTool::fail();
				$res['errorMsg'] = '当前密码错误！';
				return $res;
			} else {
				$user->password = sha1($request->input('password') . '-' . $user->salt . '-' . env('AUTH_KEY'));
				$user->save();
			}
		}
		return ResultTool::success();
	}

	public function update(Request $request)
	{
		$user = UserModel::where('uid', '=', $request->input('uid'))->first();
		$user->password = sha1($request->input('password') . '-' . $user->salt . '-' . env('AUTH_KEY'));
		$user->save();
		return ResultTool::success();
	}
}

<?php
/*
 * @Author: lokei
 * @Date: 2022-09-26 15:03:48
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-12 10:26:29
 * @Description: 
 */
namespace App\Http\Controllers\Ucenter\User;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CareerController extends Controller {
    public function ofMe() {
		$user = app('auth')->user();
        $res = ResultTool::success();
        $res['data']['career'] = $user->career;
        return $res;
    }

    public function updateOfMe(Request $request) {
		$user = app('auth')->user();
        $user->career = $request->input('career');
        $user->save();
        return ResultTool::success();
    }
}
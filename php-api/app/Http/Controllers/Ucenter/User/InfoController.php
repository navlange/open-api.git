<?php
/*
 * @Author: lokei
 * @Date: 2022-08-02 21:19:38
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-29 23:30:29
 * @Description: 
 */

namespace App\Http\Controllers\Ucenter\User;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Clerk\ClerkModel;
// use App\Models\Store\StoreModel;
use App\Models\Ucenter\FansModel;
use Illuminate\Support\Facades\Schema;

class InfoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function ofMe()
    {
        $user = auth('api')->user();
        $res = ResultTool::success();

        $res['data'] = [
            'realname' => $user->realname,
            'nickname' => $user->nickname,
            'avatar' => $user->avatar,
            'mobile' => $user->mobile,
            'has_registered' => $user->has_registered
        ];

        // if (Schema::hasTable('store')) {
        //     $store = StoreModel::where('uid', '=', $user->uid)->first();
        //     if (!is_null($store)) {
        //         $res['data']['store_info'] = [
        //             'store_id' => $store->id,
        //             'roles' => 'owner'
        //         ];
        //     }
        // }

        if (Schema::hasTable('clerk')) {
            $clerk = ClerkModel::where('uid', '=', $user->uid)->first();
            if (!is_null($clerk)) {
                $res['data']['clerk_info'] = [
                    'store_id' => $clerk->store_id,
                    'roles' => $clerk->role
                ];
            }
        }

        return $res;
    }

    public function ofMiniapp()
    {
        $user = auth('api')->user();
        $fans = FansModel::where('uid', '=', $user->uid)->where('platform', '=', 'miniapp')->first();
        $res = ResultTool::success();

        $res['data'] = [
            'openid' => $fans->openid,
            'realname' => $user->realname,
            'nickname' => $user->nickname,
            'avatar' => $user->avatar,
            'mobile' => $user->mobile,
            'has_registered' => $user->has_registered
        ];

        // if (Schema::hasTable('store')) {
        //     $store = StoreModel::where('uid', '=', $user->uid)->first();
        //     if (!is_null($store)) {
        //         $res['data']['store_info'] = [
        //             'store_id' => $store->id,
        //             'roles' => 'owner'
        //         ];
        //     }
        // }

        if (Schema::hasTable('clerk')) {
            $clerk = ClerkModel::where('uid', '=', $user->uid)->first();
            if (!is_null($clerk)) {
                $res['data']['clerk_info'] = [
                    'store_id' => $clerk->store_id,
                    'roles' => $clerk->role
                ];
            }
        }

        return $res;
    }

    public function ofMp()
    {
        $user = auth('api')->user();
        $fans = FansModel::where('uid', '=', $user->uid)->where('platform', '=', 'mp')->first();
        $res = ResultTool::success();

        $res['data'] = [
            'openid' => $fans->openid,
            'realname' => $user->realname,
            'nickname' => $user->nickname,
            'avatar' => $user->avatar,
            'mobile' => $user->mobile,
            'has_registered' => $user->has_registered
        ];

        // if (Schema::hasTable('store')) {
        //     $store = StoreModel::where('uid', '=', $user->uid)->first();
        //     if (!is_null($store)) {
        //         $res['data']['store_info'] = [
        //             'store_id' => $store->id,
        //             'roles' => 'owner'
        //         ];
        //     }
        // }

        if (Schema::hasTable('clerk')) {
            $clerk = ClerkModel::where('uid', '=', $user->uid)->first();
            if (!is_null($clerk)) {
                $res['data']['clerk_info'] = [
                    'store_id' => $clerk->store_id,
                    'roles' => $clerk->role
                ];
            }
        }
        
        return $res;
    }

    public function ofQyapp()
    {
        $user = auth('api')->user();
        $fans = FansModel::where('uid', '=', $user->uid)->where('platform', '=', 'qy')->first();
        $res = ResultTool::success();

        $res['data'] = [
            'openid' => $fans->openid,
            'realname' => $user->realname,
            'nickname' => $user->nickname,
            'avatar' => $user->avatar,
            'mobile' => $user->mobile
        ];

        // $clerk = ClerkModel::where('uid', '=', $user->uid)->first();
        // if (!is_null($clerk)) {
        //     $res['data']['role'] = $clerk->role;
        // }

        // if (config('app.open_mode') != null && config('app.open_mode') == '1') {
        //     $roles = '';
        //     $store = StoreModel::where('uid', '=', $user->uid)->first();
        //     if (!is_null($store)) {
        //         $roles = 'owner';
        //         $store_id = $store->id;
        //     }
        //     $clerk = ClerkModel::where('uid', '=', $user->uid)->first();
        //     if (!is_null($clerk)) {
        //         $clerk_id = $clerk->id;
        //         if (is_null($store)) {
        //             $store_id = $clerk->store_id;
        //         }
        //         if (!($roles == '')) {
        //             $roles .= '|';
        //         }
        //         $roles .= $clerk->role;
        //     }
        //     if (isset($store_id) && $store_id > 0) {
        //         $res['data']['store_info'] = [
        //             'store_id' => $store_id,
        //             'roles' => $roles
        //         ];
        //         if (isset($clerk_id)) {
        //             $res['data']['store_info']['clerk_id'] = $clerk_id;
        //         }
        //     }

        //     $res['data']['master_info'] = [];
        //     $master_items = MasterModel::where('uid', '=', $user->uid)->get();
        //     if (!is_null($master_items) && count($master_items) > 0) {
        //         foreach ($master_items as $master) {
        //             array_push($res['data']['master_info'], [
        //                 'app_mode' => $master->app_mode,
        //                 'status' => $master->status
        //             ]);
        //         }
        //     }
        //     if (Schema::hasTable('driver')) {
        //         $driver = DriverModel::where('uid', '=', $user->uid)->first();
        //         if (!is_null($driver)) {
        //             array_push($res['data']['master_info'], [
        //                 'app_mode' => '11',
        //                 'status' => $driver->status
        //             ]);
        //         }
        //     }
        // }

        return $res;
    }

    public function ofAlipay()
    {
        $user = auth('api')->user();
        $fans = FansModel::where('uid', '=', $user->uid)->where('platform', '=', 'alipay')->first();
        $res = ResultTool::success();

        $res['data'] = [
            'openid' => $fans->openid,
            'mobile' => $user->mobile
        ];

        $roles = '';
        // $store = StoreModel::where('uid', '=', $user->uid)->first();
        // if (!is_null($store)) {
        //     $roles = 'owner';
        //     $store_id = $store->id;
        // }
        $clerk = ClerkModel::where('uid', '=', $user->uid)->first();
        if (!is_null($clerk)) {
            // if (is_null($store)) {
            //     $store_id = $clerk->store_id;
            // }
            // if (!($roles == '')) {
            //     $roles .= '|';
            // }
            $roles .= $clerk->role;
        }
        if (isset($store_id) && $store_id > 0) {
            $res['data']['store_info'] = [
                'store_id' => $store_id,
                'roles' => $roles
            ];
        }

        return $res;
    }
    
    public function ofDouyin()
    {
        $user = auth('api')->user();
        $fans = FansModel::where('uid', '=', $user->uid)->where('platform', '=', 'douyin')->first();
        $res = ResultTool::success();

        $res['data'] = [
            'openid' => $fans->openid,
            'mobile' => $user->mobile
        ];

        $roles = '';
        // $store = StoreModel::where('uid', '=', $user->uid)->first();
        // if (!is_null($store)) {
        //     $roles = 'owner';
        //     $store_id = $store->id;
        // }
        $clerk = ClerkModel::where('uid', '=', $user->uid)->first();
        if (!is_null($clerk)) {
            // if (is_null($store)) {
            //     $store_id = $clerk->store_id;
            // }
            // if (!($roles == '')) {
            //     $roles .= '|';
            // }
            $roles .= $clerk->role;
        }
        if (isset($store_id) && $store_id > 0) {
            $res['data']['store_info'] = [
                'store_id' => $store_id,
                'roles' => $roles
            ];
        }

        return $res;
    }
}

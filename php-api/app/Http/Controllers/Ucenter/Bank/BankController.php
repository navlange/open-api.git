<?php
/*
 * @Author: lokei
 * @Date: 2023-03-20 21:12:13
 * @LastEditors: lokei
 * @LastEditTime: 2023-03-20 21:15:04
 * @Description: 
 */
namespace App\Http\Controllers\Ucenter\Bank;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Ucenter\BankModel;

class BankController extends Controller {
    public function ofMe() {
        $user = app('auth')->user();
        $bank = BankModel::where('uid', '=', $user->uid)->first();
        $res = ResultTool::success();
        $res['data'] = $bank;
        return $res;
    }
}
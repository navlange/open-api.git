<?php
/*
 * @Author: lokei
 * @Date: 2022-11-14 10:05:04
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-29 16:21:07
 * @Description: 
 */
namespace App\Http\Controllers\Delivery;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Delivery\CompanyModel;
use Illuminate\Http\Request;

class CompanyController extends Controller {
    public function get(Request $request) {
        $company = CompanyModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $company;
        return $res;
    }

    public function list() {
        $items = CompanyModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $company = new CompanyModel();
        $company->name = $request->input('name');
        $company->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $company = CompanyModel::where('id', '=', $request->input('id'))->first();
        $company->name = $request->input('name');
        $company->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        CompanyModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
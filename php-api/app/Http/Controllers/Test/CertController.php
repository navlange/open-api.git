<?php
/*
 * @Author: lokei
 * @Date: 2023-02-27 20:02:43
 * @LastEditors: lokei
 * @LastEditTime: 2023-02-27 20:20:56
 * @Description: 
 */
namespace App\Http\Controllers\Test;

use Alipay\EasySDK\Kernel\Util\AntCertificationUtil;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CertController extends Controller {
    public function getPublicKey(Request $request) {
        $util = new AntCertificationUtil();
        $key = $util->getPublicKey('/www/wwwroot/attachment/SY767352217928753152/files/2023/02/07cc6d949f6fc881ba19223383402e29.crt');
        echo $key;
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 00:02:07
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:12:35
 * @Description: 
 */

namespace App\Http\Controllers\City;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\City\DistrictModel;
use Illuminate\Http\Request;


class DistrictController extends Controller
{
    public function list(Request $request)
    {
        $res = ResultTool::success();
        $items = DistrictModel::get();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofCity(Request $request) {
        $items = DistrictModel::where('prov', '=', $request->input('prov'))->where('city', '=', $request->input('city'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }

    public function get(Request $request)
    {
        
    }

    
    public function update(Request $request)
    {
        $district = DistrictModel::where('id', '=', $request->input('id'))->first();
        $district->prov = $request->input('prov');
        $district->city = $request->input('city');
        $district->district = $request->input('district');
        $district->save();
        return ResultTool::success();
    }

    public function create(Request $request)
    {
        $district = new DistrictModel();
        $district->prov = $request->input('prov');
        $district->city = $request->input('city');
        $district->district = $request->input('district');
        $district->save();
        return ResultTool::success();
    }

    public function delete(Request $request)
    {
        DistrictModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}

<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 00:02:07
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:12:17
 * @Description: 
 */

namespace App\Http\Controllers\City;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\City\AreaModel;
use Illuminate\Http\Request;


class AreaController extends Controller
{
    public function list(Request $request)
    {
        $res = ResultTool::success();
        $items = AreaModel::get();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function get(Request $request)
    {
        
    }

    
    public function update(Request $request)
    {
        $area = AreaModel::where('id', '=', $request->input('id'))->first();
        $area->prov = $request->input('prov');
        $area->city = $request->input('city');
        $area->area = $request->input('area');
        $area->save();
        return ResultTool::success();
    }

    public function create(Request $request)
    {
        $area = new AreaModel();
        $area->prov = $request->input('prov');
        $area->city = $request->input('city');
        $area->area = $request->input('area');
        $area->save();
        return ResultTool::success();
    }

    public function delete(Request $request)
    {
        AreaModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}

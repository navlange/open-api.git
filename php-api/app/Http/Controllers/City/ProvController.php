<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 00:02:07
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:12:42
 * @Description: 
 */

namespace App\Http\Controllers\City;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\City\ProvModel;
use Illuminate\Http\Request;


class ProvController extends Controller
{
    public function list(Request $request)
    {
        $res = ResultTool::success();
        $items = ProvModel::get();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function get(Request $request)
    {
        
    }

    
    public function update(Request $request)
    {
        
    }

    public function create(Request $request)
    {
        $prov = new ProvModel();
        $prov->prov = $request->input('prov');
        $prov->save();
        return ResultTool::success();
    }

    public function delete(Request $request)
    {
        ProvModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}

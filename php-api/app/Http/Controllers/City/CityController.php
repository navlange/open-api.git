<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 00:02:07
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:12:27
 * @Description: 
 */

namespace App\Http\Controllers\City;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\City\CityModel;
use Illuminate\Http\Request;


class CityController extends Controller
{
    public function list(Request $request)
    {
        $res = ResultTool::success();
        $items = CityModel::get();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function get(Request $request)
    {
        
    }

    
    public function update(Request $request)
    {
        
    }

    public function create(Request $request)
    {
        $city = new CityModel();
        $city->alphabetical_index = $request->input('alphabetical_index');
        $city->prov = $request->input('prov');
        $city->city = $request->input('city');
        $city->save();
        return ResultTool::success();
    }

    public function delete(Request $request)
    {
        CityModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}

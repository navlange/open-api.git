<?php
/*
 * @Author: lokei
 * @Date: 2023-07-03 21:39:11
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-18 09:42:16
 * @Description: 
 */

namespace App\Http\Controllers\App;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\App\AppModel;
use Illuminate\Http\Request;

use GuzzleHttp;

class InstallController extends Controller
{

    public function query(Request $request) {
        $app = AppModel::where('identity', '=', $request->input('identity'))->first();
        if(!is_null($app) && $app->status == '1') {
            return ResultTool::success();
        } else {
            return ResultTool::fail();
        }
    }

    public function list() {
        $items = AppModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }

    public function check(Request $request)
    {
        $scheme = $_SERVER['REQUEST_SCHEME']; //协议
        $domain = $_SERVER['HTTP_HOST']; //域名/主机
        $identity = $request->input('identity');
        if ($identity == 'open-cms') {
            $app = AppModel::where('identity', '=', $identity)->first();
            if(is_null($app)) {
                $app = new AppModel();
                $app->identity = $identity;
                $app->created_at = date('Y-m-d H:i:s', time());
                $app->status = '0';
                $app->save();
            }
            $url = $scheme . "://" . $domain . '/cms-api/';
            $http = new GuzzleHttp\Client;
            try {
                $response = $http->get($url, [
                    'query' => []
                ]);
                if ($response->getStatusCode() == 200) {
                    $app->status = '1';
                    $app->save();
                    $res = ResultTool::success();
                    return $res;
                } else {
                    $app->status = '0';
                    $app->save();
                    $res = ResultTool::fail();
                    $res['errorMsg'] = '未安装';
                    return $res;
                }
            } catch (\Exception $e) {
                $app->status = '0';
                $app->save();
                $res = ResultTool::fail();
                $res['errorMsg'] = '未安装';
                return $res;
            }
        } else if ($identity == 'open-shop') {
            $app = AppModel::where('identity', '=', $identity)->first();
            if(is_null($app)) {
                $app = new AppModel();
                $app->identity = $identity;
                $app->created_at = date('Y-m-d H:i:s', time());
                $app->status = '0';
                $app->save();
            }
            $url = $scheme . "://" . $domain . '/shop-api/';
            $http = new GuzzleHttp\Client;
            try {
                $response = $http->get($url, [
                    'query' => []
                ]);
                if ($response->getStatusCode() == 200) {
                    $app->status = '1';
                    $app->save();
                    $res = ResultTool::success();
                    return $res;
                } else {
                    $app->status = '0';
                    $app->save();
                    $res = ResultTool::fail();
                    $res['errorMsg'] = '未安装';
                    return $res;
                }
            } catch (\Exception $e) {
                $app->status = '0';
                $app->save();
                $res = ResultTool::fail();
                $res['errorMsg'] = '未安装';
                return $res;
            }
        } else if ($identity == 'open-erp') {
            $app = AppModel::where('identity', '=', $identity)->first();
            if(is_null($app)) {
                $app = new AppModel();
                $app->identity = $identity;
                $app->created_at = date('Y-m-d H:i:s', time());
                $app->status = '0';
                $app->save();
            }
            $url = $scheme . "://" . $domain . '/erp-api/';
            $http = new GuzzleHttp\Client;
            try {
                $response = $http->get($url, [
                    'query' => []
                ]);
                if ($response->getStatusCode() == 200) {
                    $app->status = '1';
                    $app->save();
                    $res = ResultTool::success();
                    return $res;
                } else {
                    $app->status = '0';
                    $app->save();
                    $res = ResultTool::fail();
                    $res['errorMsg'] = '未安装';
                    return $res;
                }
            } catch (\Exception $e) {
                $app->status = '0';
                $app->save();
                $res = ResultTool::fail();
                $res['errorMsg'] = '未安装';
                return $res;
            }
        } else if ($identity == 'open-iot') {
            $app = AppModel::where('identity', '=', $identity)->first();
            if(is_null($app)) {
                $app = new AppModel();
                $app->identity = $identity;
                $app->created_at = date('Y-m-d H:i:s', time());
                $app->status = '0';
                $app->save();
            }
            $url = $scheme . "://" . $domain . '/iot-api/';
            $http = new GuzzleHttp\Client;
            try {
                $response = $http->get($url, [
                    'query' => []
                ]);
                if ($response->getStatusCode() == 200) {
                    $app->status = '1';
                    $app->save();
                    $res = ResultTool::success();
                    return $res;
                } else {
                    $app->status = '0';
                    $app->save();
                    $res = ResultTool::fail();
                    $res['errorMsg'] = '未安装';
                    return $res;
                }
            } catch (\Exception $e) {
                $app->status = '0';
                $app->save();
                $res = ResultTool::fail();
                $res['errorMsg'] = '未安装';
                return $res;
            }
        } else {
            $res = ResultTool::fail();
            $res['errorMsg'] = '未知应用';
            return $res;
        }
    }
}

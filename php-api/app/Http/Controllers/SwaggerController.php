<?php
/*
 * @Author: lokei
 * @Date: 2022-10-02 22:18:03
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-03 16:20:28
 * @Description: 
 */
namespace App\Http\Controllers;

use OpenApi\Annotations\Contact;
use OpenApi\Annotations\Info;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;
use OpenApi\Annotations\Server;

/**
 *
 * @Info(
 *     version="1.0.0",
 *     title="奇辰Open-API",
 *     description="构建软件服务SaaS的开源生态，提供前后端分离的基础软件框架，开放微服务接口API，接入主流第三方微信小程序、微信支付平台、企业微信、物联网软硬件平台等。",
 *     @Contact(
 *         email="15727013996@163.com",
 *         name="奇辰"
 *     )
 * )
 *
 * @Server(
 *     url=SWAGGER_LUME_CONST_HOST,
 *     description="Open-api接口",
 * )
 *
 * @Schema(
 *     schema="ApiResponse",
 *     type="object",
 *     description="响应实体，响应结果统一使用该结构",
 *     title="响应实体",
 *     @Property(
 *         property="code",
 *         type="string",
 *         description="响应代码"
 *     ),
 *     @Property(property="message", type="string", description="响应结果提示")
 * )
 *
 *
 * @package App\Http\Controllers
 */
class SwaggerController
{}
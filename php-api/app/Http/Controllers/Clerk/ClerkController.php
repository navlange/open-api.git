<?php
/*
 * @Author: lokei
 * @Date: 2022-08-20 17:03:15
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-01 20:13:36
 * @Description: 
 */

namespace App\Http\Controllers\Clerk;

use App\Common\Tools\IpTool;
use App\Common\Tools\RandomTool;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Clerk\ClerkModel;
use App\Models\Platform\Log\LogModel;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ClerkController extends Controller
{
	public function get(Request $request)
	{
		$clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
		$res = ResultTool::success();
		$res['data']['clerk'] = $clerk;
		return $res;
	}
	public function ofMe()
	{
		$user = app('auth')->user();
		$clerk = ClerkModel::where('uid', '=', $user->uid)->first();
		$res = ResultTool::success();
		$res['data']['clerk'] = $clerk;
		return $res;
	}
	public function list(Request $request)
	{
		if ($request->has('ids')) {
			$ids = explode(',', ($request->input('ids')));
			$items = ClerkModel::whereIn('id', $ids)->get();
		} else {
			if (config('app.token.cid') != null && intval(config('app.token.cid')) > 0) {
				$login_clerk = ClerkModel::where('id', '=', config('app.token.cid'))->first();
				if ($login_clerk->role == 'admin') {
					$items = ClerkModel::where('role', '=', 'store_admin')->orWhere('role', '=', 'store_clerk')->get();
				} else if ($login_clerk->role == 'store_admin') {
					$items = ClerkModel::where('role', '=', 'store_clerk')->where('store_id', '=', $login_clerk->store_id)->get();
				}
			} else {
				$items = ClerkModel::get();
			}
		}
		$res = ResultTool::success();
		$res['data']['items'] = $items;
		$res['data']['count'] = count($items);
		return $res;
	}

	public function bindUser(Request $request)
	{
		$user = UserModel::where('mobile', '=', $request->input('mobile'))->first();
		if (is_null($user)) {
			$res = ResultTool::fail();
			$res['errorMsg'] = '手机号用户不存在';
			return $res;
		}
		$is_binded = ClerkModel::where('uid', '=', $user->uid)->first();
		if (!is_null($is_binded)) {
			$res = ResultTool::fail();
			$res['errorMsg'] = '手机号用户已绑定其它成员';
			return $res;
		}
		$clerk = ClerkModel::where('id', '=', $request->input('clerk_id'))->first();
		$clerk->uid = $user->uid;
		$clerk->mobile = $request->input('mobile');
		$clerk->save();
		$res = ResultTool::success();
		$res['data']['uid'] = $user->uid;
		return $res;
	}

	public function cancelBindUser(Request $request) {
		$clerk = ClerkModel::where('id', '=', $request->input('clerk_id'))->first();
		$clerk->uid = null;
		$clerk->save();
		return ResultTool::success();
	}

	public function update(Request $request)
	{
		// $user = UserModel::where('mobile', '=', $request->input('mobile'))->first();
		// if (is_null($user)) {
		// 	$res = ResultTool::fail();
		// 	$res['errorMsg'] = '用户不存在';
		// 	return $res;
		// } else {

		$is_exist = ClerkModel::where('name', '=', $request->input('name'))->where('id', '<>', $request->input('id'))->first();
		if (!is_null($is_exist)) {
			$res = ResultTool::fail();
			$res['errorMsg'] = '姓名成员已存在！';
			return $res;
		}
		$clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
		$clerk->role = $request->input('role');
		if (($request->input('role') == 'store_admin' || $request->input('role') == 'store_clerk') && $request->input('store_id') != null && intval($request->input('store_id')) > 0) {
			// $store = StoreModel::where('id', '=', $request->input('store_id'))->first();
			$clerk->store_id = $request->input('store_id');
			$clerk->store_name = $request->input('store_name');
		} else {
			$clerk->store_id = 0;
			$clerk->store_name = '平台';
		}
		// $clerk->uid = $user->uid;
		$clerk->name = $request->input('name');
		// $clerk->mobile = $request->input('mobile');
		$clerk->save();
		return ResultTool::success();
		// }
	}
	public function create(Request $request)
	{
		// $user = UserModel::where('mobile', '=', $request->input('mobile'))->first();
		// if (is_null($user)) {
		// 	$res = ResultTool::fail();
		// 	$res['errorMsg'] = '手机号尚未在手机端绑定用户！';
		// 	return $res;
		// } else {

		$is_exist = ClerkModel::where('name', '=', $request->input('name'))->first();
		if (!is_null($is_exist)) {
			$res = ResultTool::fail();
			$res['errorMsg'] = '姓名成员已存在！';
			return $res;
		}
		$clerk = new ClerkModel();
		$clerk->salt = RandomTool::GetRandStr(8);
		$clerk->role = $request->input('role');
		if (($request->input('role') == 'store_admin' || $request->input('role') == 'store_clerk') && $request->input('store_id') != null && intval($request->input('store_id')) > 0) {
			// $store = StoreModel::where('id', '=', $request->input('store_id'))->first();
			$clerk->store_id = $request->input('store_id');
			$clerk->store_name = $request->input('store_name');
		} else {
			$clerk->store_id = 0;
			$clerk->store_name = '平台';
		}
		// $clerk->uid = $user->uid;
		$clerk->name = $request->input('name');
		// $clerk->mobile = $request->input('mobile');
		$clerk->createtime = date('Y-m-d H:i:s', time());
		$clerk->save();

		// 日志
		$log = new LogModel();
		$log->ipAddress = IpTool::getRealIp();
		$token = $request->input('token');
		$operation = '添加子账户';
		try {
			$user = JWTAuth::setToken($token)->authenticate();
			if (!is_null($user)) {
				$operation = $user->username . '添加子账户';
			}
		} catch (\Exception $e) {
		}
		$log->operation = $operation;
		$log->createtime = date('Y-m-d H:i:s', time());
		$log->save();

		return ResultTool::success();
		// }
	}
	public function delete(Request $request)
	{
		ClerkModel::where('id', '=', $request->input('id'))->delete();

		// 日志
		$log = new LogModel();
		$log->ipAddress = IpTool::getRealIp();
		$token = $request->input('token');
		$operation = '删除子账户';
		try {
			$user = JWTAuth::setToken($token)->authenticate();
			if (!is_null($user)) {
				$operation = $user->username . '删除子账户';
			}
		} catch (\Exception $e) {
		}
		$log->operation = $operation;
		$log->createtime = date('Y-m-d H:i:s', time());
		$log->save();

		return ResultTool::success();
	}
}

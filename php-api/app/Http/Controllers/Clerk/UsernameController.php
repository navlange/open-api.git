<?php
/*
 * @Author: lokei
 * @Date: 2023-06-28 13:35:58
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-29 09:28:14
 * @Description: 
 */
namespace App\Http\Controllers\Clerk;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Clerk\ClerkModel;
use Illuminate\Http\Request;

class UsernameController extends Controller {
    public function get(Request $request) {
        $clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data']['username'] = $clerk->username;
        return $res;
    }

    public function update(Request $request) {
		$exist_username = ClerkModel::where('username', '=', $request->input('username'))->where('id', '!=', $request->input('id'))->first();
		if(!is_null($exist_username)) {
			$res = ResultTool::fail();
			$res['errorMsg'] = '用户名已存在！';
			return $res;
		}
		$clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
        $clerk->username = $request->input('username');
        $clerk->save();
        return ResultTool::success();
	}

    public function updateCid(Request $request)
    {
        if (config('app.token.cid') != null && intval(config('app.token.cid')) > 0) {
            $exist_username = ClerkModel::where('username', '=', $request->input('username'))->where('id', '!=', intval(config('app.token.cid')))->first();
            if (!is_null($exist_username)) {
                $res = ResultTool::fail();
                $res['errorMsg'] = '用户名已存在！';
                return $res;
            }
            $clerk = ClerkModel::where('id', '=', intval(config('app.token.cid')))->first();
            $clerk->username = $request->input('username');
            $clerk->save();
            return ResultTool::success();
        } else {
            $res = ResultTool::fail();
            $res['errorMsg'] = '成员不存在！';
            return $res;
        }
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-01-28 20:19:59
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-28 11:50:41
 * @Description: 
 */
namespace App\Http\Controllers\Clerk\Role;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Clerk\ClerkModel;
use Illuminate\Http\Request;

class StoreController extends Controller {
    public function get(Request $request) {
        $clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data']['roles_store'] = $clerk->roles_store;
        return $res;
    }

    public function update(Request $request) {
        $clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
        $clerk->roles_store = $request->input('roles_store');
        $clerk->save();
        return ResultTool::success();
    }
}
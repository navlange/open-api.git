<?php
/*
 * @Author: lokei
 * @Date: 2023-01-28 20:19:59
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-27 12:59:33
 * @Description: 
 */
namespace App\Http\Controllers\Clerk\Role;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Clerk\ClerkModel;
use Illuminate\Http\Request;

class AdminController extends Controller {
    public function get(Request $request) {
        $clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data']['roles_admin'] = $clerk->roles_admin;
        return $res;
    }

    public function update(Request $request) {
        $clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
        $clerk->roles_admin = $request->input('roles_admin');
        $clerk->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-01-28 20:19:59
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-27 13:25:14
 * @Description: 
 */
namespace App\Http\Controllers\Clerk;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Clerk\ClerkModel;
use Illuminate\Http\Request;

class RoleController extends Controller {
    public function get(Request $request) {
        $clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data']['role'] = $clerk->role;
        return $res;
    }

    public function ofPcid() {
        $clerk = ClerkModel::where('id', '=', config('app.token.cid'))->first();
        $res = ResultTool::success();
        $res['data']['role'] = $clerk->role;
        $res['data']['name'] = $clerk->username;
        if($clerk->role == 'admin') {
            $res['data']['roles_admin'] = $clerk->roles_admin;
        }
        if($clerk->role == 'store_admin') {
            $res['data']['store_id'] = $clerk->store_id;
        }
        if($clerk->role == 'store_admin' || $clerk->role == 'store_clerk') {
            $res['data']['roles_store'] = $clerk->roles_store;
        }
        return $res;
    }

    public function update(Request $request) {
        $clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
        $clerk->role = $request->input('role');
        $clerk->save();
        return ResultTool::success();
    }
}
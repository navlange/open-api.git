<?php
/*
 * @Author: lokei
 * @Date: 2023-06-28 15:12:21
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-29 09:45:44
 * @Description: 
 */

namespace App\Http\Controllers\Clerk;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Clerk\ClerkModel;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
	public function update(Request $request)
	{
		$clerk = ClerkModel::where('id', '=', $request->input('id'))->first();
		$clerk->password = sha1($request->input('password') . '-' . $clerk->salt . '-' . env('AUTH_KEY'));
		$clerk->save();
		return ResultTool::success();
	}

	public function updateCid(Request $request)
	{
		if (config('app.token.cid') != null && intval(config('app.token.cid')) > 0) {
			$clerk = ClerkModel::where('id', '=', intval(config('app.token.cid')))->first();
			$clerk->password = sha1($request->input('password') . '-' . $clerk->salt . '-' . env('AUTH_KEY'));
			$clerk->save();
			return ResultTool::success();
		} else {
			$res = ResultTool::fail();
			$res['errorMsg'] = '成员不存在！';
			return $res;
		}
	}
}

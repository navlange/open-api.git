<?php
/*
 * @Author: lokei
 * @Date: 2022-08-09 11:15:50
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-07 12:35:40
 * @Description: 
 */

namespace App\Http\Controllers\Rbac\Auth;

use App\Common\Tools\RandomTool;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Rbac\Conf\LoginModel;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request)
    {
        $is_exist = UserModel::where('username', '=', $request->input('username'))->first();
        if (!is_null($is_exist)) {
            $res = ResultTool::fail();
            $res['errorMsg'] = '用户名已存在！';
            return $res;
        }
        if ($request->input('mobile') != '') {
            $is_exist = UserModel::where('mobile', '=', $request->input('mobile'))->first();
            if (!is_null($is_exist)) {
                $res = ResultTool::fail();
                $res['errorMsg'] = '手机号已存在！';
                return $res;
            }
            $login_conf = LoginModel::first();
            if (!is_null($login_conf) && $login_conf->mobile_check_on == 1) {
                $verification_code = Redis::get('mobile_verification_code:' . $request->input('mobile'));
                if ($verification_code == null || $verification_code == '' || $request->input('verification_code') == null || $request->input('verification_code') == '' || ($verification_code != $request->input('verification_code'))) {
                    $res = ResultTool::fail();
                    $res['errorMsg'] = '验证码错误！';
                    return $res;
                } else {
                    Redis::del('mobile_verification_code:' . $request->input('mobile'));
                }
            }
        }
        if ($request->input('email') != '') {
            $is_exist = UserModel::where('email', '=', $request->input('email'))->first();
            if (!is_null($is_exist)) {
                $res = ResultTool::fail();
                $res['errorMsg'] = '邮箱已存在！';
                return $res;
            }
            $login_conf = LoginModel::first();
            if (!is_null($login_conf) && $login_conf->email_check_on == 1) {
                $verification_code = Redis::get('email_verification_code:' . $request->input('email'));
                if ($verification_code == null || $verification_code == '' || $request->input('email_verification_code') == null || $request->input('email_verification_code') == '' || ($verification_code != $request->input('email_verification_code'))) {
                    $res = ResultTool::fail();
                    $res['errorMsg'] = '邮箱验证码错误！';
                    return $res;
                } else {
                    Redis::del('email_verification_code:' . $request->input('email'));
                }
            }
        }
        $user = new UserModel();
        $user->username = $request->input('username');
        if ($request->input('mobile') != '') {
            $user->mobile = $request->input('mobile');
        }
        if ($request->input('email') != '') {
            $user->email = $request->input('email');
        }
        $user->salt = RandomTool::GetRandStr(8);
        $user->createtime = date('Y-m-d H:i:s', time());
        $user->password = sha1($request->input('password') . '-' . $user->salt . '-' . env('AUTH_KEY'));
        $user->save();
        $res = ResultTool::success();
        $token = Auth::login($user);
        $res['data']['token'] = $token;
        return $res;
    }

    public function login(Request $request)
    {
        $user = UserModel::where('username', '=', $request->input('username'))->first();
        if (is_null($user)) {
            $user = UserModel::where('mobile', '=', $request->input('username'))->first();
            if (is_null($user)) {
                $res = ResultTool::fail();
                $res['errorCode'] = 1000;
                $res['errorMsg'] = '用户不存在！';
                return $res;
            }
        }
        $password = sha1($request->input('password') . '-' . $user->salt . '-' . env('AUTH_KEY'));
        if ($user->password != $password) {
            $res = ResultTool::fail();
            $res['errorCode'] = 1001;
            $res['errorMsg'] = '密码错误！';
            return $res;
        }
        $res = ResultTool::success();
        $token = Auth::login($user);
        $res['data']['token'] = $token;
        return $res;
    }

    //
    public function logout(Request $request)
    {
        return ResultTool::success();
    }
}

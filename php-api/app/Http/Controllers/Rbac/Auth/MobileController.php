<?php
/*
 * @Author: lokei
 * @Date: 2022-08-09 11:15:50
 * @LastEditors: lokei
 * @LastEditTime: 2023-12-30 22:25:02
 * @Description: 
 */

namespace App\Http\Controllers\Rbac\Auth;

use App\Common\Tools\RandomTool;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Tymon\JWTAuth\Facades\JWTAuth;

class MobileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        $verification_code = Redis::get('mobile_verification_code:' . $request->input('mobile'));
        if ($verification_code != $request->input('verification_code')) {
            $res = ResultTool::fail();
            $res['errorMsg'] = '验证码错误！';
            return $res;
        } else {
            Redis::del('mobile_verification_code:' . $request->input('mobile'));
        }
        $user = UserModel::where('mobile', '=', $request->input('mobile'))->first();
        if (is_null($user)) {
            $user = new UserModel();
            $user->mobile = $request->input('mobile');
            $user->salt = RandomTool::GetRandStr(8);
            $user->createtime = date('Y-m-d H:i:s', time());
            $user->save();
        }
        $res = ResultTool::success();
        $token = Auth::login($user);
        $res['data']['token'] = $token;
        return $res;
    }
}

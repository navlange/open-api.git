<?php
/*
 * @Author: lokei
 * @Date: 2022-08-08 22:55:21
 * @LastEditors: lokei
 * @LastEditTime: 2024-02-26 14:20:51
 * @Description: 
 */

namespace App\Http\Controllers\Rbac\Auth;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Conf\ConfMiniappModel;
use App\Models\Ucenter\FansModel;
use App\Models\Ucenter\UserModel;
use App\Services\Ucenter\FansService;
use App\Services\Ucenter\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use GuzzleHttp;
use Illuminate\Support\Facades\Redis;

class MiniappController extends Controller
{

    private UserService $user_service;

    private FansService $fans_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->user_service = new UserService();
        $this->fans_service = new FansService();
    }

    //

    public function login(Request $request)
    {
        $http = new GuzzleHttp\Client;
        if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
            if (config('app.proj_type') != null && config('app.proj_type') == '6') {
                if(config('app.secret')) {
                    $secret = config('app.secret');
                } else {
                    $conf = ConfMiniappModel::first();
                    if(!is_null($conf) && $conf->secret != null && $conf->secret != '') {
                        $secret = $conf->secret;
                    } else {
                        app('log')->info('小程序配置错误：' . config('app.proj_identity'));
                        $res = ResultTool::fail();
                        $res['errorMsg'] = '小程序配置错误！';
                        return $res;
                    }
                }
                $conf = ConfMiniappModel::first();
                $params = [
                    'appid' => $request->header('appid'),
                    'secret' => $secret,
                    'js_code' => $request->code,
                    'grant_type' => 'authorization_code'
                ];
            } else {
                $endapp = Redis::get('proj:endapp:' . $request->header('appid'));
                $endapp_info = json_decode($endapp);
                $params = [
                    'appid' => $request->header('appid'),
                    'secret' => $endapp_info->secret,
                    'js_code' => $request->code,
                    'grant_type' => 'authorization_code'
                ];
            }
        } else {
            $conf = ConfMiniappModel::first();
            $params = [
                'appid' => $conf->appid,
                'secret' => $conf->secret,
                'js_code' => $request->code,
                'grant_type' => 'authorization_code'
            ];
        }
        $response = $http->get('https://api.weixin.qq.com/sns/jscode2session', [
            'query' => $params
        ]);
        $result = json_decode($response->getBody(), true);
        if (array_key_exists('errcode', $result) && $result['errcode'] != 0) {
            app('log')->info($result);
            $res = ResultTool::fail();
            $res['errorCode'] = $result['errcode'];
            $res['errorMsg'] = $result['errmsg'];
            return $res;
        }
        if (array_key_exists('openid', $result) && $result['openid'] != '') {
            $fans = FansModel::where('openid', '=', $result['openid'])->where('platform', '=', 'miniapp')->first();
            if (!is_null($fans)) {
                if (!($fans->uid > 0)) {
                    $uid = $this->user_service->createUser();
                    $fans->uid = $uid;
                    $fans->save();
                } else {
                    $user = UserModel::where('uid', '=', $fans->uid)->first();
                    if (is_null($user)) {
                        $uid = $this->user_service->createUser();
                        $fans->uid = $uid;
                        $fans->save();
                    }
                }
            } else {
                $fid = $this->fans_service->registerOpenid('miniapp', $result['openid']);
                $fans = FansModel::where('openid', '=', $result['openid'])->first();
                $uid = $this->user_service->createUser();
                $fans->uid = $uid;
                $fans->save();
            }
            if (!isset($user)) {
                $user = UserModel::where('uid', '=', $fans->uid)->first();
            }
            $token = Auth::login($user);
            $res = ResultTool::success();
            $res['data']['token'] = $token;
            return $res;
        }
        $res = ResultTool::fail();
        $res['errorMsg'] = '小程序用户登入失败！';
        return $res;
    }
}

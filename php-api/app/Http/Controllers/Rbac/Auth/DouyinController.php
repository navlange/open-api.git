<?php
/*
 * @Author: lokei
 * @Date: 2022-11-16 09:03:59
 * @LastEditors: lokei
 * @LastEditTime: 2023-10-22 18:50:55
 * @Description: 
 */
namespace App\Http\Controllers\Rbac\Auth;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Conf\ConfDouyinModel;
use Illuminate\Http\Request;

use App\Models\Ucenter\FansModel;
use App\Models\Ucenter\UserModel;
use App\Services\Ucenter\FansService;
use App\Services\Ucenter\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

use GuzzleHttp;

class DouyinController extends Controller {
    private UserService $user_service;

    private FansService $fans_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->user_service = new UserService();
        $this->fans_service = new FansService();
    }

    public function login(Request $request) {

        if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
            $endapp = Redis::get('proj:' . config('app.proj_identity') . ':douyin');
            $endapp_info = json_decode($endapp);
            $params = [
                'appid' => $endapp_info->appid,
                'secret' => $endapp_info->secret,
                'anonymous_code' => '',
                'code' => $request->input('code')
            ];
        } else {
            $conf = ConfDouyinModel::first();
            $params = [
                'appid' => $conf->appid,
                'secret' => $conf->secret,
                'anonymous_code' => '',
                'code' => $request->input('code')
            ];
        }
        $http = new GuzzleHttp\Client;
        $response = $http->post('https://developer.toutiao.com/api/apps/v2/jscode2session', [
            'json' => $params
        ]);
        $result = json_decode($response->getBody(), true);
        if (array_key_exists('err_no', $result) && $result['err_no'] != 0) {
            $res = ResultTool::fail();
            $res['errorCode'] = $result['err_no'];
            $res['errorMsg'] = $result['err_tips'];
            return $res;
        }
        if (array_key_exists('openid', $result['data']) && $result['data']['openid'] != '') {
            $fans = FansModel::where('openid', '=', $result['data']['openid'])->where('platform', '=', 'douyin')->first();
            if (!is_null($fans)) {
                if (!($fans->uid > 0)) {
                    $uid = $this->user_service->createUser();
                    $fans->uid = $uid;
                    $fans->save();
                }
            } else {
                $this->fans_service->registerOpenid('douyin', $result['data']['openid']);
                $fans = FansModel::where('openid', '=', $result['data']['openid'])->first();
                $uid = $this->user_service->createUser();
                $fans->uid = $uid;
                $fans->save();
            }
            $user = UserModel::where('uid', '=', $fans->uid)->first();
            $token = Auth::login($user);
            $res = ResultTool::success();
            $res['data']['token'] = $token;
            return $res;
        }
        $res = ResultTool::fail();
        $res['errorMsg'] = '微信公众号用户登入失败！';
        return $res;
    }
}

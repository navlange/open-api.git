<?php
/*
 * @Author: lokei
 * @Date: 2023-01-28 11:14:42
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-24 19:54:30
 * @Description: 
 */
namespace App\Http\Controllers\Rbac\Permission;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Rbac\Permission\RoleModel;
use Illuminate\Http\Request;

class RoleController extends Controller {
    public function list() {
        $items = RoleModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $role = new RoleModel();
        $role->name = $request->input('name');
        $role->identity = $request->input('identity');
        $role->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $role = RoleModel::where('id', '=', $request->input('id'))->first();
        $role->name = $request->input('name');
        $role->identity = $request->input('identity');
        $role->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        RoleModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
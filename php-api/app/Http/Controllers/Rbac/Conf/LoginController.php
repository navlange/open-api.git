<?php
/*
 * @Author: lokei
 * @Date: 2024-01-01 18:30:33
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-06 22:36:12
 * @Description: 
 */
namespace App\Http\Controllers\Rbac\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Rbac\Conf\LoginModel;
use Illuminate\Http\Request;

class LoginController extends Controller
{
	/**
	 * 获取H5配置
	 * @return [type] [description]
	 */
	public function get()
	{
		$conf = LoginModel::first();
        if(is_null($conf)) {
            $conf = new LoginModel;
            $conf->login_mode = '0';
            $conf->save();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'login_mode' => $conf->login_mode,
            'mobile_check_on' => $conf->mobile_check_on,
            'email_check_on' => $conf->email_check_on
        ];
        return $res;
    }

    public function set(Request $request) {
        if($request->has('login_mode')) {
            $conf = LoginModel::first();
            $conf->login_mode = $request->login_mode;
            $conf->mobile_check_on = $request->mobile_check_on;
            $conf->email_check_on = $request->email_check_on;
            $conf->save();
            return ResultTool::success();
        } else {
            return ResultTool::fail();
        }
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2024-01-01 18:30:33
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-31 12:02:25
 * @Description: 
 */

namespace App\Http\Controllers\Rbac\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Rbac\Conf\MiniappModel;
use Illuminate\Http\Request;

class MiniappController extends Controller
{
    /**
     * 获取H5配置
     * @return [type] [description]
     */
    public function get()
    {
        $conf = MiniappModel::first();
        if (is_null($conf)) {
            $conf = new MiniappModel;
            $conf->mobile_check_on = '0';
            $conf->save();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'mobile_check_on' => $conf->mobile_check_on
        ];
        return $res;
    }

    public function set(Request $request)
    {
        $conf = MiniappModel::first();
        if (is_null($conf)) {
            $conf = new MiniappModel;
            $conf->mobile_check_on = '0';
            $conf->save();
        }
        $conf->mobile_check_on = $request->mobile_check_on;
        $conf->save();
        return ResultTool::success();
    }
}

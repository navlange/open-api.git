<?php
/*
 * @Author: lokei
 * @Date: 2022-11-08 08:27:51
 * @LastEditors: lokei
 * @LastEditTime: 2023-09-28 11:50:35
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Record;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Card\CardRecordModel;
use App\Models\Pay\Record\PayRecordModel;
use Illuminate\Http\Request;

class RecordController extends Controller {
    
    public function list(Request $request) {
        $model = PayRecordModel::with('user');
        $total = $model->count();
        if($request->input('limit') != null && $request->input('limit') != '') {
            $model->limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')));
        }
        $items = $model->orderBy('createtime', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = $total;
        return $res;
    }
    
    public function ofMe(Request $request) {
        $user = app('auth')->user();
        $model = PayRecordModel::where('uid', '=', $user->uid);
        if($request->input('pay_mode') == '4') {
            $model->where('pay_card_record_id', '=', $request->input('card_record_id'));
        }
        $items = $model->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }
}
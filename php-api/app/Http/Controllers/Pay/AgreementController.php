<?php
/*
 * @Author: lokei
 * @Date: 2023-02-07 23:23:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-02-07 23:24:55
 * @Description: 
 */
namespace App\Http\Controllers\Pay;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Conf\ConfModel;

class AgreementController extends Controller {
    public function get() {
        $conf = ConfModel::first();
        $res = ResultTool::success();
        $res['data']['agreement'] = $conf->pay_agreement;
        return $res;
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-06-09 17:28:34
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-09 17:32:02
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Recharge;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Recharge\RechargeModel;
use Illuminate\Http\Request;

class RechargeController extends Controller {
    public function list(Request $request) {
        if($request->input('limit') != null && $request->input('limit') != '') {
            if(isset($model)) {
                $model->limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')));
            } else {
                $model = RechargeModel::limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')));
            }
        }
        $items = $model->orderBy('createtime', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = RechargeModel::count();
        return $res;
    }
}
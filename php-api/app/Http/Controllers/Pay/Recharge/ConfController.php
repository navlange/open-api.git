<?php
/*
 * @Author: lokei
 * @Date: 2023-02-21 23:47:51
 * @LastEditors: lokei
 * @LastEditTime: 2023-02-21 23:49:24
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Recharge;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Conf\ConfModel;
use Illuminate\Http\Request;

class ConfController extends Controller {
    public function get() {
        $conf = ConfModel::first();
        $res = ResultTool::success();
        $res['data'] = [
            'recharge_input_on' => $conf ? $conf->recharge_input_on : '0'
        ];
        return $res;
    }

    public function set(Request $request) {
        $conf = ConfModel::first();
        $conf->recharge_input_on = $request->input('recharge_input_on');
        $conf->save();
        return ResultTool::success();
    }
}
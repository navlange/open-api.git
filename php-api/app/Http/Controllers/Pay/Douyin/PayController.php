<?php
/*
 * @Author: lokei
 * @Date: 2023-10-24 16:34:47
 * @LastEditors: lokei
 * @LastEditTime: 2023-10-24 19:52:38
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Douyin;

use App\Common\Douyin\Sign;
use App\Common\Tools\ResultTool;
use App\Common\Tools\Sn;
use App\Http\Controllers\Controller;
use App\Models\Pay\Record\PayRecordModel;
use App\Models\Platform\Conf\ConfDouyinModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

use GuzzleHttp;

class PayController extends Controller {
    public function ecpay(Request $request) {
        $user = app('auth')->user();
        $store_id = 0;
        $appid = '';
        if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
            $endapp = Redis::get('proj:' . config('app.proj_identity') . ':douyin');
            $endapp_info = json_decode($endapp);
            $appid = $endapp_info->appid;
        } else {
            $conf = ConfDouyinModel::first();
            $appid = $conf->appid;
        }

        $sn = Sn::build_order_no();
        $pay_record = new PayRecordModel();
        $pay_record->store_id = $store_id > 0 ? $store_id : (intval($request->input('store_id')) > 0 ? intval($request->input('store_id')) : 0);
        $pay_record->uid = $user->uid;
        $pay_record->mode = $request->input('mode');
        $pay_record->pay_mode = '7';
        $pay_record->tid = $sn;
        $pay_record->order_id = $request->input('order_id');
        $pay_record->money = $request->input('money');
        $pay_record->status = '0';
        $pay_record->createtime = date('Y-m-d H:i:s', time());
        $pay_record->save();

        $notify_url = env('HOST_URL') . ((env('HOST_PORT') != null && intval(env('HOST_PORT')) > 0) ? (':' . env('HOST_PORT')) : '') . '/api/pay/douyin/notify';
        if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
            $notify_url .= '/' . config('app.proj_identity');
        } else {
            $notify_url .= '/0';
        }
        if ($store_id > 0) {
            $notify_url .= '/' . $store_id;
        } else {
            $notify_url .= '/0';
        }
        
        $params = [
            'app_id' => $appid,
            'out_order_no' => $sn,
            'total_amount' => intval(floatVal($request->input('money')) * 100),
            'subject' => '抖音小程序商品',
            'body' => '抖音小程序商品',
            'valid_time' => 60*60,
            'notify_url' => $notify_url
        ];
        $sign = Sign::sign($params);
        $params['sign'] = $sign;
        
        $http = new GuzzleHttp\Client;
        $response = $http->post('https://developer.toutiao.com/api/apps/ecpay/v1/create_order', [
            'json' => $params
        ]);
        $result = json_decode($response->getBody(), true);
        if (array_key_exists('err_no', $result) && $result['err_no'] != 0) {
            $res = ResultTool::fail();
            $res['errorCode'] = $result['err_no'];
            $res['errorMsg'] = $result['err_tips'];
            return $res;
        } else {
            $res = ResultTool::success();
            $res['data'] = $result['data'];
            return $res;
        }
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-08-18 21:15:24
 * @LastEditors: lokei
 * @LastEditTime: 2023-10-24 19:38:57
 * @Description: 
 */

namespace App\Http\Controllers\Pay\Douyin;

use App\Http\Controllers\Controller;
use App\Models\Pay\Record\PayRecordModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class NotifyController extends Controller
{
    public function callback($pid, $store_id, Request $request)
    {
        // $inBody = file_get_contents('php://input');
        
        // $inBodyArray = (array)json_decode($inBody, true);
        // app('log')->info($inBodyArray);
        // app('log')->info($request);
        // // 使用PHP7的数据解构语法，从Array中解构并赋值变量
        // ['resource' => [
        //     'ciphertext'      => $ciphertext,
        //     'nonce'           => $nonce,
        //     'associated_data' => $aad
        // ]] = $inBodyArray;
        // // 加密文本消息解密
        // $inBodyResource = AesGcm::decrypt($ciphertext, $apiv3Key, $nonce, $aad);
        // 把解密后的文本转换为PHP Array数组
        // $inBodyResourceArray = (array)json_decode($inBodyArray, true);
        // $fields = array();
        $msg = json_decode($request->input('msg'), true);
        $fields['sn'] = $msg['cp_orderno'];
        if ($pid != null && $pid != '') {
            $fields['pid'] = $pid;
        }
        Redis::xAdd('pay_success', '*', $fields);

        $pay_record = PayRecordModel::where('tid', '=', $msg['cp_orderno'])->first();
        $pay_record->transaction_id = $msg['channel_no'];
        $pay_record->status = '1';
        $pay_record->save();

        return [
            'err_no' => 0,
            'err_tips' => 'success'
        ];
    }
}

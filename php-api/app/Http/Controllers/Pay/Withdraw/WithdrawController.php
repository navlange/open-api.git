<?php
/*
 * @Author: lokei
 * @Date: 2022-10-12 17:22:59
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-12 17:34:25
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Withdraw;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Withdraw\RecordModel;
use App\Models\Store\StoreModel;
use Illuminate\Http\Request;

class WithdrawController extends Controller {
    public function submit(Request $request) {
        if($request->input('type') == 'store') {
            $store = StoreModel::where('id', '=', $request->input('store_id'))->first();
            $money = floatval($request->input('money'));
            if($store->income - $store->withdrawed - $store->withdrawing - $money < 0) {
                $res = ResultTool::fail();
                $res['errorMsg'] = '提现金额超过可提现余额！';
                return $res;
            }
            $store->withdrawing = $store->withdrawing + $money;
            $store->save();
            $withdraw = new RecordModel();
            $withdraw->type = $request->input('type');
            $withdraw->bank_id = $store->bank_id;
            $withdraw->bank_name = $store->bank_name;
            $withdraw->store_id = $request->input('store_id');
            $withdraw->store_name = $store->name;
            $withdraw->store_mobile = $store->contacts_mobile;
            $withdraw->money = $money;
            $withdraw->createtime = date('Y-m-d H:i:s', time());
            $withdraw->status = '0';
            $withdraw->save();
            return ResultTool::success();
        }
        
    }
}
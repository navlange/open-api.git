<?php
/*
 * @Author: lokei
 * @Date: 2022-10-07 16:17:35
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-17 09:17:29
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Withdraw;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Message\Template\TemplateModel;
use App\Models\Pay\Withdraw\RecordModel;
use App\Models\Scrm\Fx\MemberModel;
use App\Models\Store\StoreModel;
use App\Models\Ucenter\FansModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RecordController extends Controller {
    public function ofStore(Request $request) {
        $items = RecordModel::where('store_id', '=', $request->input('store_id'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function list(Request $request) {
        if($request->input('type') != null && $request->input('type') != '') {
            $model = RecordModel::where('type', '=', $request->input('type'));
        }
        if ($request->input('start_day') != null && $request->input('start_day') != '') {
            if(isset($model)) {
                $model->where('createtime', '>=', $request->input('start_day'));
            } else {
                $model = RecordModel::where('createtime', '>=', $request->input('start_day'));
            }
        }
        if ($request->input('finish_day') != null && $request->input('finish_day') != '') {
            if(isset($model)) {
                $model->where('createtime', '<=', $request->input('finish_day'));
            } else {
                $model = RecordModel::where('createtime', '<=', $request->input('finish_day'));
            }
        }
        if($request->input('store_id') != null && intval($request->input('store_id')) > 0) {
            if(isset($model)) {
                $model = $model->where('store_id', '=', $request->input('store_id'));
            } else {
                $model = RecordModel::where('store_id', '=', $request->input('store_id'));
            }
        }
        if($request->input('type') != null && $request->input('type') == 'fx' && $request->input('is_downloading') != null && $request->input('is_downloading') == '1') {
            $with_array = array('bank' => function($query) {
                $query->select('uid', 'bank_id', 'bank_name', 'bank_username');
            }, 'user' => function($query) {
                $query->select('uid', 'realname', 'nickname', 'mobile');
            });
        } else {
            $with_array = array('user' => function($query) {
                $query->select('uid', 'realname', 'nickname', 'mobile');
            });
        }
        
        if(isset($model)) {
            $items = $model->with($with_array)->get();
        } else {
            $items = RecordModel::with($with_array)->get();
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function accept(Request $request) {
        $record = RecordModel::where('id', '=', $request->input('id'))->first();
        if($record->type == 'store') {
            $store = StoreModel::where('id', '=', $record->store_id)->first();
            $store->withdrawed = $store->withdrawed + $record->money;
            $store->withdrawing = $store->withdrawing  - $record->money;
            $store->save();

            if ($store->uid > 0) {
                $template = TemplateModel::where('identity', '=', 'pay_account_change')->first();
                if (!is_null($template)) {
                    $fans = FansModel::where('uid', '=', $store->uid)->where('platform', '=', 'mp')->first();
                    if (!is_null($fans)) {
                        $fields = array();
                        $fields['type'] = 'mp';
                        $fields['pid'] = config('app.proj_identity');
                        $fields['touser'] = $fans->openid;
                        $fields['template_id'] = $template->template_id;
                        $fields['keyword_amount'] = 4;
                        $fields['first'] = '账户资金变动提醒';

                        $keyword1 = [
                            'name' => 'keyword1',
                            'value' => '提现完成'
                        ];
                        $fields['keyword1'] = json_encode($keyword1);

                        $keyword2 = [
                            'name' => 'keyword2',
                            'value' => $record->money
                        ];
                        $fields['keyword2'] = json_encode($keyword2);
                        
                        $keyword3 = [
                            'name' => 'keyword3',
                            'value' => date('Y-m-d H:i:s', time())
                        ];
                        $fields['keyword3'] = json_encode($keyword3);
                        
                        $keyword4 = [
                            'name' => 'keyword4',
                            'value' => number_format($store->income - $store->withdrawed - $store->withdrawing, 2)
                        ];
                        $fields['keyword4'] = json_encode($keyword4);
                        
                        $fields['remark'] = '感谢使用, 如有问题请及时联系我们!';
                        Redis::xAdd('send_message', '*', $fields);
                    }
                }
            }
        } else if ($record->type == 'fx') {
            $fx_member = MemberModel::where('uid', '=', $record->uid)->first();
            $fx_member->withdrawed_money = $fx_member->withdrawed_money + $record->money;
            $fx_member->save();
        }
        $record->status = '1';
        $record->save();

        return ResultTool::success();
    }
}
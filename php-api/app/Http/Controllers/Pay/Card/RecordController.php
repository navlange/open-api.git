<?php
/*
 * @Author: lokei
 * @Date: 2022-10-29 16:04:13
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-15 11:53:38
 * @Description: 
 */

namespace App\Http\Controllers\Pay\Card;

use App\Common\Tools\ResultTool;
use App\Common\Tools\Sn;
use App\Http\Controllers\Controller;
use App\Models\Pay\Card\CardRecordModel;
use App\Models\Pay\Card\PayCardModel;
use App\Models\Pay\Log\PayLogModel;
use App\Models\Pay\Recharge\PackageModel;
use App\Models\Pay\Recharge\RechargeModel;
use App\Models\Platform\Print\PrinterModel;
use App\Models\Ucenter\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RecordController extends Controller
{

    public function get(Request $request)
    {
        $record = CardRecordModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $record;
        return $res;
    }

    public function list(Request $request)
    {
        if ($request->input('card_id') != null && intval($request->input('card_id')) > 0) {
            $model = CardRecordModel::where('card_id', '=', $request->input('card_id'));
        }
        if (isset($model)) {
            $model = $model->orderBy('created_at', 'desc');
        } else {
            $model = CardRecordModel::orderBy('created_at', 'desc');
        }
        $items = $model->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofMe(Request $request)
    {
        $res = ResultTool::success();
        $user = app('auth')->user();
        if ($request->input('card_id') != null && $request->input('card_id') > 0) {
            $record = CardRecordModel::where('uid', '=', $user->uid)->where('card_id', '=', $request->input('card_id'))->first();
            $res['data'] = $record;
            return $res;
        } else {
            $items = CardRecordModel::where('uid', '=', $user->uid)->with('card')->get();
            $res['data']['items'] = $items;
            return $res;
        }
    }

    public function ofMy(Request $request)
    {
        $user = app('auth')->user();
        $items = CardRecordModel::where('uid', '=', $user->uid)->with('card')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }

    public function register(Request $request)
    {
        $user = app('auth')->user();
        $is_exist = CardRecordModel::where('uid', '=', $user->uid)->where('card_id', '=', $request->input('card_id'))->where('status', '=', '1')->first();
        if (!is_null($is_exist)) {
            $res = ResultTool::fail();
            if ($is_exist->status == '0') {
                $res['errorMsg'] = '信息已提交';
            } else {
                $res['errorMsg'] = '已开通';
            }
            return $res;
        }
        $record = new CardRecordModel();
        $record->sn = Sn::build_order_no();
        $record->uid = $user->uid;
        $record->card_id = $request->input('card_id');
        $card = PayCardModel::where('id', '=', $request->input('card_id'))->first();
        $record->card_name = $card->name;
        $record->name = $request->input('name');
        $record->mobile = $request->input('mobile');
        $record->company = $request->input('company');
        $record->id_number = $request->input('id_number');
        $record->img = $request->input('img');
        $record->money = 0;
        if ($card->need_verify == '0' && !($card->price > 0)) {
            $record->status = '1';
        } else {
            $record->status = '0';
        }
        $record->payed = '0';
        $record->created_at = date('Y-m-d H:i:s', time());
        $record->save();
        if ($record->status == '1') {
            $user = UserModel::where('uid', '=', $record->uid)->first();
            if ($user->pay_card_amount != null) {
                $user->pay_card_amount = $user->pay_card_amount + 1;
            } else {
                $user->pay_card_amount = 1;
            }
            $user->save();
        }
        $res = ResultTool::success();
        $res['data']['record_id'] = $record->id;
        return $res;
    }

    public function delete(Request $request)
    {
        $record = CardRecordModel::where('id', '=', $request->input('id'))->first();
        $user = UserModel::where('uid', '=', $record->uid)->first();
        if ($user->pay_card_amount != null && $user->pay_card_amount > 0) {
            $user->pay_card_amount = $user->pay_card_amount - 1;
        } else {
            $user->pay_card_amount = 0;
        }
        $user->save();
        CardRecordModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }

    public function verify(Request $request)
    {
        $record = CardRecordModel::where('id', '=', $request->input('id'))->first();
        $record->status = '1';
        $record->save();
        $user = UserModel::where('uid', '=', $record->uid)->first();
        if ($user->pay_card_amount != null) {
            $user->pay_card_amount = $user->pay_card_amount + 1;
        } else {
            $user->pay_card_amount = 1;
        }
        $user->save();
        return ResultTool::success();
    }

    public function recharge(Request $request)
    {
        $record = CardRecordModel::where('id', '=', $request->input('id'))->first();
        if ($request->input('package_id') != null && intval($request->input('package_id')) > 0) {
            $package = PackageModel::where('id', '=', $request->input('package_id'))->first();
            $record->money = $record->money + $package->recharge_money + $package->gift_money;
            $record->save();
        } else {
            $record->money = $record->money + $request->input('money');
            $record->save();
        }
        $pay_log = new PayLogModel();
        $pay_log->uid = $record->uid;
        $pay_log->type = '4';
        if ($request->input('package_id') != null && intval($request->input('package_id')) > 0) {
            $money = $package->recharge_money;
        } else {
            $money = $request->input('money');
        }
        $pay_log->balance = $money;
        $description = $request->input('description');
        if ($request->input('package_id') != null && intval($request->input('package_id')) > 0) {
            $description = $description . '; 充值套餐：' . $package->name;
        }
        $pay_log->description = $description;
        $pay_log->createtime = date('Y-m-d H:i:s', time());
        $pay_log->save();

        $recharge = new RechargeModel();
        $recharge->card_record_id = $request->input('id');
        $recharge->uid = $record->uid;
        $recharge->name = $record->name;
        $recharge->mobile = $record->mobile;
        if(isset($package)) {
            $recharge->package_id = $package->id;
            $recharge->money = $package->recharge_money;
            $recharge->gift_money = $package->gift_money;
        } else {
            $recharge->money = $request->input('money');
            $recharge->gift_money = 0;
        }
        $recharge->createtime = date('Y-m-d H:i:s', time());
        $recharge->status = '1';
        $recharge->note = $request->input('description');
        $recharge->save();
        

        $card = PayCardModel::where('id', '=', $record->card_id)->first();
        if (!is_null($card) && $card->store_id > 0) {
            $printer = PrinterModel::where('store_id', '=', $card->store_id)->first();
            if (!is_null($printer)) {
                $content = '<CB>储值卡充值</CB><BR>';
                $content .= '充值时间：' . $pay_log->createtime . '<BR>';
                $content .= '储值卡：' . $card->name . '<BR>';
                $content .= '用户名：' . $record->name . '<BR>';
                $content .= '手机号：' . $record->mobile . '<BR>';
                if($request->input('package_id') != null && intval($request->input('package_id')) > 0) {
                    $recharge_money = '充值' . $package->recharge_money . '; 赠送' . $package->gift_money;
                } else {
                    $recharge_money = $request->input('money');
                }
                $content .='充值金额：' . $recharge_money . '<BR>';
                $content .= '余额：' . $record->money . '<BR>';
                $content .= '操作时间：' . $pay_log->createtime . '<BR>';
                $content .= '备注：' . ($request->input('description') ? $request->input('description') : '') . '<BR>';
                if ($request->input('package_id') != null && intval($request->input('package_id')) > 0) {
                    $content .= '充值套餐：' . $package->name . '<BR>';
                }
                $fields = array();
                $fields['content'] = $content;

                $fields['printer'] = json_encode([
                    'type' => $printer->type,
                    'user' => $printer->user,
                    'ukey' => $printer->ukey,
                    'sn' => $printer->sn
                ]);
                Redis::xAdd('smt_print', '*', $fields);
            }
        }

        return ResultTool::success();
    }
}

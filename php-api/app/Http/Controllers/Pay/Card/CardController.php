<?php
/*
 * @Author: lokei
 * @Date: 2022-10-29 16:04:13
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-08 09:54:56
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Card;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Card\PayCardModel;
use App\Models\Store\StoreModel;
use Illuminate\Http\Request;

class CardController extends Controller {

    public function get(Request $request) {
        $card = PayCardModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $card;
        return $res;
    }

    public function list(Request $request) {
        if(!is_null($request->input('store_id')) && intval($request->input('store_id')) > 0) {
            $items = PayCardModel::where('store_id', '=', $request->input('store_id'))->get();
        } else {
            $items = PayCardModel::get();
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $card = new PayCardModel();
        $card->name = $request->input('name');
        $card->store_id = $request->input('store_id');
        if($request->input('store_id') > 0) {
            $store = StoreModel::where('id', '=', $request->input('store_id'))->first();
            $card->store_name = $store->name;
        }
        $card->img_display = $request->input('img_display');
        $card->price = $request->input('price');
        $card->need_verify = $request->input('need_verify');
        $card->detail = $request->input('detail');
        $card->save();
        return ResultTool::success();
    }
    public function update(Request $request) {
        $card = PayCardModel::where('id', '=', $request->input('id'))->first();
        $card->name = $request->input('name');
        $card->store_id = $request->input('store_id');
        if($request->input('store_id') > 0) {
            $store = StoreModel::where('id', '=', $request->input('store_id'))->first();
            $card->store_name = $store->name;
        }
        $card->img_display = $request->input('img_display');
        $card->price = $request->input('price');
        $card->need_verify = $request->input('need_verify');
        $card->detail = $request->input('detail');
        $card->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        PayCardModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-11-14 08:33:21
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-14 09:09:53
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Cash;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Cash\CashModel;
use Illuminate\Http\Request;

class CashController extends Controller {
    public function ofOrder(Request $request) {
        $items = CashModel::where('order_id', '=', $request->input('order_id'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $cash = new CashModel();
        $cash->order_id = $request->input('order_id');
        $cash->money = $request->input('money');
        $cash->mode = $request->input('mode');
        $cash->createtime = date('Y-m-d H:i:s', time());
        $cash->save();
        return ResultTool::success();
    }
}
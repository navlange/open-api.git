<?php
/*
 * @Author: lokei
 * @Date: 2022-12-01 10:00:06
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-01 10:29:34
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Cash;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Conf\ConfModel;
use Illuminate\Http\Request;

class ConfController extends Controller {
    public function get() {
        $res = ResultTool::success();
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $res['data']['cash_balance_on'] = '0';
            $res['data']['cash_pay_card_on'] = '0';
        } else {
            $res['data']['cash_balance_on'] = $conf->cash_balance_on;
            $res['data']['cash_pay_card_on'] = $conf->cash_pay_card_on;
        }
        return $res;
    }

    public function update(Request $request) {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
        }
        $conf->cash_balance_on = $request->input('cash_balance_on');
        $conf->cash_pay_card_on = $request->input('cash_pay_card_on');
        $conf->save();
        return ResultTool::success();
    }
}
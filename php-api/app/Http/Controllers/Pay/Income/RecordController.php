<?php
/*
 * @Author: lokei
 * @Date: 2022-10-07 16:17:35
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-22 14:07:07
 * @Description: 
 */

namespace App\Http\Controllers\Pay\Income;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Income\IncomeModel;
use Illuminate\Http\Request;

class RecordController extends Controller
{
    public function ofMe(Request $request)
    {
        if ($request->input('type') == 'store') {
            $model = IncomeModel::where('store_id', '=', $request->input('store_id'));
            if ($request->input('start_day') != null && $request->input('start_day') != '') {
                $model->where('createtime', '>=', $request->input('start_day'));
            }
            if ($request->input('finish_day') != null && $request->input('finish_day') != '') {
                $model->where('createtime', '<=', $request->input('finish_day'));
            }
            $items = $model->get();
            $res = ResultTool::success();
            $res['data']['items'] = $items;
            $res['data']['total'] = count($items);
            return $res;
        } else {
            $user = app('auth')->user();
            $items = IncomeModel::where('uid', '=', $user->uid)->get();
            $res = ResultTool::success();
            $res['data']['items'] = $items;
            $res['data']['total'] = count($items);
            return $res;
        }
    }

    public function ofMyFx(Request $request)
    {
        $user = app('auth')->user();
        $items = IncomeModel::where('type', '=', 'fx')->where('uid', '=', $user->uid)->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofOrder(Request $request)
    {
        $items = IncomeModel::where('order_id', '=', $request->input('order_id'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }
}

<?php
/*
 * @Author: lokei
 * @Date: 2022-10-07 16:17:35
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-18 10:55:00
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Income;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Income\IncomeModel;
use Illuminate\Http\Request;

class IncomeController extends Controller {
    public function list(Request $request) {
        if($request->input('store_id') != null && intval($request->input('store_id')) > 0) {
            $model = IncomeModel::where('store_id', '=', $request->input('store_id'));
        }
        if ($request->input('start_day') != null && $request->input('start_day') != '') {
            if(isset($model)) {
                $model = $model->where('createtime', '>=', $request->input('start_day'));
            } else {
                $model = IncomeModel::where('createtime', '>=', $request->input('start_day'));
            }
        }
        if ($request->input('finish_day') != null && $request->input('finish_day') != '') {
            if(isset($model)) {
                $model = $model->where('createtime', '<=', $request->input('finish_day'));
            } else {
                $model = IncomeModel::where('createtime', '<=', $request->input('finish_day'));
            }
        }
        if(isset($model)) {
            $model = $model->orderBy('createtime', 'desc');
        } else {
            $model = IncomeModel::orderBy('createtime', 'desc');
        }
        if($request->input('limit') != null && $request->input('limit') != '' && $request->input('is_downloading') == null) {
            $page_model = $model;
            $page_model = $page_model->limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')));
        }
        if(isset($page_model)) {
            $items = $page_model->get();
        } else {
            $items = $model->get();
        }
        $total = $model->count();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = $total;
        return $res;
    }

    public function delete(Request $request) {
        IncomeModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
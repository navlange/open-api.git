<?php
/*
 * @Author: lokei
 * @Date: 2022-11-08 08:27:51
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-09 09:06:38
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Account;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Card\CardRecordModel;

class AccountController extends Controller {
    public function ofMe() {
        $user = app('auth')->user();
        $balance = $user->balance;
        $record_list = CardRecordModel::where('uid', '=', $user->uid)->where('status', '=', '1')->get();
        foreach($record_list as $record) {
            $balance += $record->money;
        }
        $pay_account = [
            'balance' => $balance,
            'pay_card_amount' => $user->pay_card_amount
        ];
        $res = ResultTool::success();
        $res['data'] = $pay_account;
        return $res;
    }
}
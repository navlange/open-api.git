<?php
/*
 * @Author: lokei
 * @Date: 2022-11-08 08:27:51
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-16 23:20:07
 * @Description: 
 */
namespace App\Http\Controllers\Pay\Account;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Account\AccountModel;
use App\Models\Pay\Card\CardRecordModel;
use Illuminate\Http\Request;

class BankController extends Controller {
    public function ofMe() {
        $user = app('auth')->user();
        $account = AccountModel::where('uid', '=', $user->uid)->first();
        if(is_null($account)) {
            $account = new AccountModel();
            $account->uid = $user->uid;
            $account->save();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'bank_id' => $account->bank_id,
            'bank_name' => $account->bank_name,
            'bank_username' => $account->bank_username
        ];
        return $res;
    }

    public function ofUser(Request $request) {
        $account = AccountModel::where('uid', '=', $request->input('uid'))->first();
        if(is_null($account)) {
            $account = new AccountModel();
            $account->uid = $request->input('uid');
            $account->save();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'bank_id' => $account->bank_id,
            'bank_name' => $account->bank_name,
            'bank_username' => $account->bank_username
        ];
        return $res;
    }

    public function updateOfMe(Request $request) {
        $user = app('auth')->user();
        $account = AccountModel::where('uid', '=', $user->uid)->first();
        if(is_null($account)) {
            $account = new AccountModel();
            $account->uid = $user->uid;
            $account->save();
        }
        $account->bank_id = $request->input('bank_id');
        $account->bank_name = $request->input('bank_name');
        $account->bank_username = $request->input('bank_username');
        $account->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-11-27 20:45:08
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-24 19:53:49
 * @Description: 
 */
namespace App\Http\Controllers\Permission;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Rbac\Permission\ClerkModel;
use Illuminate\Http\Request;

class ClerkController extends Controller {
    public function get() {
        $conf = ClerkModel::first();
        if(is_null($conf)) {
            $conf = new ClerkModel();
            $conf->online_goods_on = '0';
            $conf->save();
            $conf = ClerkModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = $conf;
        return $res;
    }

    public function update(Request $request) {
        $conf = ClerkModel::first();
        $conf->online_goods_on = $request->input('online_goods_on');
        $conf->save();
        return ResultTool::success();
    }
}
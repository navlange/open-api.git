<?php
/*
 * @Author: lokei
 * @Date: 2022-09-18 20:27:37
 * @LastEditors: lokei
 * @LastEditTime: 2023-10-14 22:08:00
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Print;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Print\PrinterModel;
use Illuminate\Http\Request;

class PrinterController extends Controller {

    public function list() {
        $items = PrinterModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofStore(Request $request) {
        $items = PrinterModel::where('store_id', '=', $request->input('store_id'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $printer = new PrinterModel();
        $printer->name = $request->input('name');
        $printer->store_id = $request->input('store_id');
        $printer->type = $request->input('type');
        $printer->user = $request->input('user');
        $printer->ukey = $request->input('ukey');
        $printer->sn = $request->input('sn');
        $printer->print_count = $request->input('print_count');
        $is_on_str = $request->input('is_on');
        $printer->is_on = eval("return $is_on_str;") ? '1' : '0';
        $printer->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $printer = PrinterModel::where('id', '=', $request->input('id'))->first();
        $printer->type = $request->input('type');
        $printer->user = $request->input('user');
        $printer->ukey = $request->input('ukey');
        $printer->sn = $request->input('sn');
        $printer->print_count = $request->input('print_count');
        $is_on_str = $request->input('is_on');
        $printer->is_on = eval("return $is_on_str;") ? '1' : '0';
        $printer->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        PrinterModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
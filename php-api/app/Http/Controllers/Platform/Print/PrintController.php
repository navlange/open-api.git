<?php
/*
 * @Author: lokei
 * @Date: 2022-10-15 23:07:55
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-30 18:03:18
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Print;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Order\OrderModel;
use App\Models\Platform\Print\PrinterModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class PrintController extends Controller
{
    public function print(Request $request)
    {
        $fields = array();
        if ($request->input('store_id') != null && intval($request->input('store_id')) > 0) {
            $content = '<CB>订单打印</CB><BR>';
            // $content .= '名称　　　　　 单价  数量 金额<BR>';
            // $content .= '--------------------------------<BR>';
            // $content .= '饭　　　　　　 1.0    1   1.0<BR>';
            // $content .= '炒饭　　　　　 10.0   10  10.0<BR>';
            // $content .= '蛋炒饭　　　　 10.0   10  100.0<BR>';
            // $content .= '鸡蛋炒饭　　　 100.0  1   100.0<BR>';
            // $content .= '番茄蛋炒饭　　 1000.0 1   100.0<BR>';
            // $content .= '西红柿蛋炒饭　 1000.0 1   100.0<BR>';
            // $content .= '西红柿鸡蛋炒饭 100.0  10  100.0<BR>';
            // $content .= '备注：加辣<BR>';
            $goods_info = json_decode($request->input('goods_info'));
            $content .= '名称　　　　　 数量  单价  小计<BR>';
            $content .= '--------------------------------<BR>';
            foreach ($goods_info as $goods) {
                $content .= $goods->name;
                $name_len = mb_strlen($goods->name,'utf-8');
                if($name_len < 9) {
                    for($i=0; $i<(9-$name_len); $i++) {
                        $content .= '　';
                    }
                }
                $content .= $goods->amount . '  ' . $goods->price . '  ' . (intval($goods->amount) * floatval($goods->price)) . '<BR>';
                $content .= '--------------------------------<BR>';
            }
            $content .= '合计：' . $request->input('money') . '元<BR>';
            // $content .= '送货地点：广州市南沙区xx路xx号<BR>';
            $content .= '店名：' . $request->input('store_info') . '<BR>';
            $content .= '单号：' . $request->input('sn') . '<BR>';
            $content .= '联系电话：' . $request->input('consignee_mobile') . '<BR>';
            $content .= '下单时间：' . $request->input('createtime') . '<BR>';
            $content .= '支付方式：' . $request->input('pay_mode') . '<BR>';
            $content .= '备注信息：' . ($request->input('note') ? $request->input('note') : '') . '<BR>';
            if($request->input('custom_content') != null) {
                $content .= $request->input('custom_content');
            }
            // $content .= '订餐时间：2016-08-08 08:08:08<BR>';
            // $content .= '<QR>http://www.dzist.com</QR>';
            $fields['content'] = $content;
            $printer = PrinterModel::where('store_id', '=', $request->input('store_id'))->first();
            if (!is_null($printer)) {
                $fields['printer'] = json_encode([
                    'type' => $printer->type,
                    'user' => $printer->user,
                    'ukey' => $printer->ukey,
                    'sn' => $printer->sn
                ]);
                Redis::xAdd('smt_print', '*', $fields);
            }
        }
        return ResultTool::success();
    }

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-08-08 09:23:25
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-31 12:32:03
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Pay\Conf\ConfModel;
use App\Models\Platform\Alias\AliasModel;
use App\Models\Platform\ConfModel as PlatformConfModel;
use App\Models\Rbac\Conf\LoginModel;
use App\Models\Rbac\Conf\MiniappModel;
use App\Models\Widget\ConfModel as WidgetConfModel;
use App\Models\Widget\ThemePersonModel;
use Illuminate\Support\Facades\Schema;

class InitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function get()
    {
        $res = ResultTool::success();

        $platform_conf = PlatformConfModel::first();
        if (is_null($platform_conf)) {
            $platform_conf = new PlatformConfModel();
            $platform_conf->save();
        }
        
		$rbac_login_conf = LoginModel::first();
        if(is_null($rbac_login_conf)) {
            $rbac_login_conf = new LoginModel;
            $rbac_login_conf->login_mode = '0';
            $rbac_login_conf->save();
        }

        $rbac_miniapp_conf = MiniappModel::first();
        if(is_null($rbac_miniapp_conf)) {
            $rbac_miniapp_conf = new MiniappModel;
            $rbac_miniapp_conf->mobile_check_on = '0';
            $rbac_miniapp_conf->save();
        }

        $res['data']['platform_conf'] = !is_null($platform_conf) ? [
            'logo' => $platform_conf->logo,
            'name' => $platform_conf->name,
            'tel' => $platform_conf->tel
        ] : null;
        $res['data']['rbac_login_conf'] = [
            'login_mode' => $rbac_login_conf->login_mode,
            'mobile_check_on' => $rbac_login_conf->mobile_check_on,
            'email_check_on' => $rbac_login_conf->email_check_on
        ];
        $res['data']['rbac_miniapp_conf'] = [
            'mobile_check_on' => $rbac_miniapp_conf->mobile_check_on
        ];

        if (Schema::hasTable('alias')) {
            $alias = AliasModel::first();
            if (is_null($alias)) {
                $alias = new AliasModel();
                $alias->store = '商家门店';
                $alias->save();
                $alias = AliasModel::first();
            }
            $res['data']['alias'] = $alias;
        }

        return $res;
    }
}

<?php
/*
 * @Author: lokei
 * @Date: 2023-06-19 00:19:14
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-19 00:55:27
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfModel;

class AdminController extends Controller {
    public function get() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->admin_delete_on = '1';
            $conf->save();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'admin_delete_on' => $conf->admin_delete_on
        ];
        return $res;
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-08-08 09:23:25
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-28 17:07:03
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfModel;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function get() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->name = 'open-api';
            $conf->save();
            $conf = ConfModel::first();
        }
        $res = ResultTool::success();
        $res['data']['conf'] = $conf;
        return $res;
    }

    public function update(Request $request) {
        $conf = ConfModel::first();
        $conf->name = $request->input('name');
        $conf->tel = $request->input('tel');
        $conf->logo = $request->input('logo');
        $conf->about = $request->input('about');
        $conf->save();
        return ResultTool::success();
    }
    
}

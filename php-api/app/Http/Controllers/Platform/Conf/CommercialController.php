<?php
/*
 * @Author: lokei
 * @Date: 2022-08-08 09:23:25
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-05 11:04:21
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfModel;
use Illuminate\Http\Request;

class CommercialController extends Controller
{
    public function update(Request $request) {
        $conf = ConfModel::first();
        $conf->commercial_store_fee = $request->input('commercial_store_fee');
        $conf->save();
        return ResultTool::success();
    }
    
}

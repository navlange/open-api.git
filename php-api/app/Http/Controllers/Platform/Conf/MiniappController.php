<?php
/*
 * @Author: lokei
 * @Date: 2022-08-08 13:07:43
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-16 23:16:14
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Conf\ConfMiniappModel;
use Illuminate\Http\Request;

class MiniappController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    function resData($origin) {
        $data = array();
        $data['miniapp_name'] = !is_null($origin) ? $origin->name : '';
        $data['miniapp_appid'] = !is_null($origin) ? $origin->appid : '';
        $data['miniapp_secret'] = !is_null($origin) ? $origin->secret : '';
        return $data;
    }

    public function get() {
        $conf = ConfMiniappModel::first();
        if(is_null($conf)) {
            $conf = new ConfMiniappModel();
            $conf->save();
            $conf = ConfMiniappModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = $this->resData($conf);
        return $res;
    }

    public function update(Request $request) {
        $conf = ConfMiniappModel::first();
        if(is_null($conf)) {
            $conf = new ConfMiniappModel();
        }
        $conf->name = $request->input('miniapp_name');
        $conf->appid = $request->input('miniapp_appid');
        $conf->secret = $request->input('miniapp_secret');
        $conf->save();
        return ResultTool::success();
    }
    
}

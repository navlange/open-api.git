<?php
/*
 * @Author: lokei
 * @Date: 2022-08-08 09:23:25
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-28 17:07:03
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfModel;

class AboutController extends Controller {
    public function get() {
        $conf = ConfModel::first();
        $res = ResultTool::success();
        $res['data'] = array(
            'about' => $conf->about
        );
        return $res;
    }
}
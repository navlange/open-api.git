<?php
/*
 * @Author: lokei
 * @Date: 2023-07-03 08:25:08
 * @LastEditors: lokei
 * @LastEditTime: 2023-10-13 19:10:58
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfModel;
use Illuminate\Http\Request;

use OpenApi\Annotations\Get;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;
use App\Http\Responses\CommResp;

class ContactsController extends Controller {
    /**
	 * @Get(
	 *     path="/api/platform/contacts/get",
	 *     tags={"【平台】平台管理"},
	 *     summary="平台联系信息",
	 *     @Response(
	 *         response="200",
	 *         description="正常操作响应",
	 *         @MediaType(
	 *             mediaType="application/json",
	 *             @Schema(
	 *                 allOf={
	 *                     @Schema(ref="#/components/schemas/CommResp")
	 *                 }
	 *             )
	 *         )
	 *     )
	 * )
	 *
	 * @param Request $request
	 *
	 * @return CommResp
	 */
    public function get() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->save();
            $conf = ConfModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = [
			'contacts_mobile' => env('PLATFORM_CONTACTS_MOBILE') ? env('PLATFORM_CONTACTS_MOBILE') : $conf->contacts_mobile,
			'contacts_email' => env('PLATFORM_CONTACTS_EMAIL') ? env('PLATFORM_CONTACTS_EMAIL') : $conf->contacts_email,
			'contacts_qq' => env('PLATFORM_CONTACTS_QQ') ? env('PLATFORM_CONTACTS_QQ') : $conf->contacts_qq,
			'contacts_qw' => env('PLATFORM_CONTACTS_QW') ? env('PLATFORM_CONTACTS_QW') : $conf->contacts_qw,
			'contacts_mp' => env('PLATFORM_CONTACTS_MP') ? env('PLATFORM_CONTACTS_MP') : $conf->contacts_mp,
            'contacts_name' => $conf->contacts_name,
            'contacts_address' => $conf->contacts_address
        ];
        return $res;
    }

    /**
	 * @Post(
	 *     path="/api/platform/contacts/update",
	 *     tags={"【平台】平台管理"},
	 *     summary="设置平台联系信息",
	 *	   @OA\Parameter(
	 *         name="contacts_mobile",
	 *         in="query",
	 *         required=true,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *	   @OA\Parameter(
	 *         name="contacts_qq",
	 *         in="query",
	 *         required=true,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *	   @OA\Parameter(
	 *         name="contacts_qw",
	 *         in="query",
	 *         required=true,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *	   @OA\Parameter(
	 *         name="contacts_mp",
	 *         in="query",
	 *         required=true,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *     @Response(
	 *         response="200",
	 *         description="正常操作响应",
	 *         @MediaType(
	 *             mediaType="application/json",
	 *             @Schema(
	 *                 allOf={
	 *                     @Schema(ref="#/components/schemas/CommResp")
	 *                 }
	 *             )
	 *         )
	 *     )
	 * )
	 *
	 * @param Request $request
	 *
	 * @return CommResp
	 */
    public function update(Request $request) {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->save();
            $conf = ConfModel::first();
        }
        $conf->contacts_name = $request->input('contacts_name');
        $conf->contacts_mobile = $request->input('contacts_mobile');
        $conf->contacts_email = $request->input('contacts_email');
        $conf->contacts_address = $request->input('contacts_address');
        $conf->contacts_qq = $request->input('contacts_qq');
        $conf->contacts_qw = $request->input('contacts_qw');
        $conf->contacts_mp = $request->input('contacts_mp');
        $conf->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-08-20 18:55:58
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-25 13:34:10
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfQiweiModel;
use Illuminate\Http\Request;

class QiweiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    function resData($origin) {
        $data = array();
        $data['corpid'] = !is_null($origin) ?  $origin->corpid : '';
        $data['corpsecret'] = !is_null($origin) ?  $origin->corpsecret : '';
        return $data;
    }

    public function get() {
        $conf = ConfQiweiModel::first();
        if(is_null($conf)) {
            $conf = new ConfQiweiModel();
            $conf->save();
            $conf = ConfQiweiModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = $this->resData($conf);
        return $res;
    }

    public function update(Request $request) {
        $conf = ConfQiweiModel::first();
        $conf->corpid = $request->input('corpid');
        $conf->corpsecret = $request->input('corpsecret');
        $conf->save();
        return ResultTool::success();
    }
    
}

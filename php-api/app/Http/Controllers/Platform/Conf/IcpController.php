<?php
/*
 * @Author: lokei
 * @Date: 2023-07-03 08:25:08
 * @LastEditors: lokei
 * @LastEditTime: 2023-12-18 15:01:53
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfModel;
use Illuminate\Http\Request;

use OpenApi\Annotations\Get;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;
use App\Http\Responses\CommResp;

class IcpController extends Controller {
    /**
	 * @Get(
	 *     path="/api/platform/icp/get",
	 *     tags={"【平台】平台管理"},
	 *     summary="平台ICP信息",
	 *     @Response(
	 *         response="200",
	 *         description="正常操作响应",
	 *         @MediaType(
	 *             mediaType="application/json",
	 *             @Schema(
	 *                 allOf={
	 *                     @Schema(ref="#/components/schemas/CommResp")
	 *                 }
	 *             )
	 *         )
	 *     )
	 * )
	 *
	 * @param Request $request
	 *
	 * @return CommResp
	 */
    public function get() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->save();
            $conf = ConfModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'icp' => $conf ? $conf->icp : '',
            'police_icp' => $conf ? $conf->police_icp : ''
        ];
        return $res;
    }

    /**
	 * @Post(
	 *     path="/api/platform/icp/update",
	 *     tags={"【平台】平台管理"},
	 *     summary="设置平台ICP信息",
	 *	   @OA\Parameter(
	 *         name="icp",
	 *         in="query",
	 *         required=true,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *	   @OA\Parameter(
	 *         name="police_icp",
	 *         in="query",
	 *         required=true,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *     @Response(
	 *         response="200",
	 *         description="正常操作响应",
	 *         @MediaType(
	 *             mediaType="application/json",
	 *             @Schema(
	 *                 allOf={
	 *                     @Schema(ref="#/components/schemas/CommResp")
	 *                 }
	 *             )
	 *         )
	 *     )
	 * )
	 *
	 * @param Request $request
	 *
	 * @return CommResp
	 */
    public function update(Request $request) {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->save();
            $conf = ConfModel::first();
        }
        $conf->icp = $request->input('icp');
        $conf->police_icp = $request->input('police_icp');
        $conf->save();
        return ResultTool::success();
    }
}
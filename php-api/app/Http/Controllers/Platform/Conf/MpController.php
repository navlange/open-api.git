<?php
/*
 * @Author: lokei
 * @Date: 2022-08-25 15:21:45
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-02 15:09:44
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfMpModel;
use Illuminate\Http\Request;

class MpController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    function resData($origin) {
        $data = array();
        $data['name'] = !is_null($origin) ? $origin->name : '';
        $data['appid'] = !is_null($origin) ? $origin->appid : '';
        $data['secret'] = !is_null($origin) ? $origin->secret : '';
        $data['qrcode'] = !is_null($origin) ? $origin->qrcode : '';
        return $data;
    }

    public function get() {
        $conf = ConfMpModel::first();
        if(is_null($conf)) {
            $conf = new ConfMpModel();
            $conf->save();
            $conf = ConfMpModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = $this->resData($conf);
        return $res;
    }

    public function update(Request $request) {
        $conf = ConfMpModel::first();
        if(is_null($conf)) {
            $conf = new ConfMpModel();
        }
        $conf->name = $request->input('name');
        $conf->appid = $request->input('appid');
        $conf->secret = $request->input('secret');
        $conf->qrcode = $request->input('qrcode');
        $conf->save();
        return ResultTool::success();
    }
    
}

<?php
/*
 * @Author: lokei
 * @Date: 2022-08-08 13:07:43
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-28 11:57:57
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Conf\ConfAlipayModel;
use Illuminate\Http\Request;
use GuzzleHttp;

class AlipayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    function resData($origin) {
        $data = array();
        // $data['merchantPrivateKey'] = $origin->merchantPrivateKey;
        // $data['alipayCertPath'] = $origin->alipayCertPath;
        // $data['alipayRootCertPath'] = $origin->alipayRootCertPath;
        // $data['merchantCertPath'] = $origin->merchantCertPath;
        $data['secret_key'] = $origin->secret_key;
        return $data;
    }

    public function get() {
        $conf = ConfAlipayModel::first();
        if(is_null($conf)) {
            $conf = new ConfAlipayModel();
            $conf->save();
            $conf = ConfAlipayModel::first();
        }
        if(($conf == null || $conf->secret_key == null || $conf->secret_key == '') && config('app.proj_identity') != null && config('app.proj_identity') != '') {
            $http = new GuzzleHttp\Client;
            $response = $http->get(env('QC_SAAS_HOST') . '/saas/alipay/aes/get', [
                'query' => [
                    'proj_identity' => config('app.proj_identity')
                ]
            ]);
            $result = json_decode($response->getBody(), true);
            if($result['errorCode'] == 200) {
                if(array_key_exists('aes_key', $result['data']) && $result['data']['aes_key'] != null && $result['data']['aes_key'] != '') {
                    if(is_null($conf)) {
                        $conf = new ConfAlipayModel();
                    }
                    $conf->secret_key = $result['data']['aes_key'];
                    $conf->save();
                }
            }
        }
        $res = ResultTool::success();
        $res['data'] = !is_null($conf) ? $this->resData($conf) : null;
        return $res;
    }

    public function update(Request $params) {
        $conf = ConfAlipayModel::first();
        if(is_null($conf)) {
            $conf = new ConfAlipayModel();
        }
        // $conf->merchantPrivateKey = $request->input('merchantPrivateKey');
        // $conf->alipayCertPath = $request->input('alipayCertPath');
        // $conf->alipayRootCertPath = $request->input('alipayRootCertPath');
        // $conf->merchantCertPath = $request->input('merchantCertPath');

        if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
            $http = new GuzzleHttp\Client;
            $response = $http->post(env('QC_SAAS_HOST') . '/saas/alipay/aes/set', [
                'query' => [
                    'proj_identity' => config('app.proj_identity')
                ]
            ]);
            $result = json_decode($response->getBody(), true);
            if($result['errorCode'] == 200) {
                if(array_key_exists('aes_key', $result['data']) && $result['data']['aes_key'] != null && $result['data']['aes_key'] != '') {
                    $conf->secret_key = $result['data']['aes_key'];
                }
            }
        } else {
            $conf->secret_key = $params->input('secret_key');
        }
        
        $conf->save();
        $res = ResultTool::success();
        $res['data']['secret_key'] = $conf->secret_key;
        return $res;
    }
    
}

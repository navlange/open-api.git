<?php
/*
 * @Author: lokei
 * @Date: 2022-08-08 09:23:25
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-12 10:57:47
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Alias;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Alias\AliasModel;
use Illuminate\Http\Request;

class AliasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function get() {
        $alias = AliasModel::first();
        if(is_null($alias)) {
            $alias = new AliasModel();
            $alias->store = '门店';
            $alias->cart = '购物车';
            $alias->goods = '商品';
            $alias->balance = '余额';
            $alias->consignee_info = '点单信息';
            $alias->order_note = '备注';
            $alias->save();
            $alias = AliasModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = $alias;
        return $res;
    }

    public function update(Request $request) {
        $alias = AliasModel::first();
        $alias->store = $request->input('store');
        $alias->cart = $request->input('cart');
        $alias->goods = $request->input('goods');
        $alias->balance = $request->input('balance');
        $alias->consignee_info = $request->input('consignee_info');
        $alias->order_note = $request->input('order_note');
        $alias->save();
        return ResultTool::success();
    }
    
}

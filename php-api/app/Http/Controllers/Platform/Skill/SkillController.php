<?php
/*
 * @Author: lokei
 * @Date: 2022-09-17 16:25:29
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-15 23:57:16
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Skill;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Skill\SkillModel;
use Illuminate\Http\Request;

class SkillController extends Controller {
    public function list() {
        $items = SkillModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $tag = new SkillModel();
        // $tag->scene = $request->input('scene');
        $tag->name = $request->input('name');
        $tag->img_thumb = $request->input('img_thumb');
        $tag->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $tag = SkillModel::where('id', '=', $request->input('id'))->first();
        // $tag->scene = $request->input('scene');
        $tag->name = $request->input('name');
        $tag->img_thumb = $request->input('img_thumb');
        $tag->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        SkillModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
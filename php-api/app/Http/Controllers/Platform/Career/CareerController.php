<?php
/*
 * @Author: lokei
 * @Date: 2022-09-17 16:25:29
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-12 09:02:43
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Career;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Career\CareerModel;
use App\Models\Widget\ThemeIndexModel;
use Illuminate\Http\Request;

class CareerController extends Controller {
    public function list() {
        $items = CareerModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $career = new CareerModel();
        $career->name = $request->input('name');
        $career->img_thumb = $request->input('img_thumb');
        $career->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $career = CareerModel::where('id', '=', $request->input('id'))->first();
        $career->name = $request->input('name');
        $career->img_thumb = $request->input('img_thumb');
        $career->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        CareerModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-07-23 21:05:54
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-23 21:07:17
 * @Description: 
 */
namespace App\Http\Controllers\Platform\System;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfModel;
use Illuminate\Http\Request;

class StatusController extends Controller {
    public function get(Request $request) {
        $status = ConfModel::first();
        if(is_null($status)) {
            $status = new ConfModel();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'system_status' => $status->system_status,
            'system_status_tips' => $status->system_status_tips
        ];
        return $res;
    }

    public function set(Request $request) {
        $status = ConfModel::first();
        if(is_null($status)) {
            $status = new ConfModel();
        }
        $status->system_status = $request->input('system_status');
        $status->system_status_tips = $request->input('system_status_tips');
        $status->save();
        return ResultTool::success();
    }
}
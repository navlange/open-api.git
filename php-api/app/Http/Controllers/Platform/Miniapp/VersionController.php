<?php
/*
 * @Author: lokei
 * @Date: 2022-12-30 12:08:01
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-30 18:06:12
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Miniapp;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Miniapp\VersionModel;
use Illuminate\Http\Request;

class VersionController extends Controller {
    public function ofProj() {
        $items = VersionModel::orderBy('createtime', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function get(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data']['version'] = $version;
        return $res;
    }

    public function create(Request $request) {
        $version = new VersionModel();
        $version->version = $request->input('version');
        $version->description = $request->input('description');
        $version->status = '6';
        $version->createtime = date('Y-m-d H:i:s', time());
        $version->save();
        return ResultTool::success();
    }
    
    public function update(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $version->status = $request->input('status');
        if($request->input('status') == '1') {
            $version->reject_reason = $request->input('reason');
            $version->reject_screen_shot = $request->input('reject_screen_shot');
        }
        $version->save();
        return ResultTool::success();
    }
    
    public function updateAuditId(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('version_id'))->first();
        $version->status = '2';
        $version->audit_id = $request->input('audit_id');
        $version->save();
        return ResultTool::success();
    }
    
    public function releaseSuccess(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $version->status = '5';
        $version->save();
        return ResultTool::success();
    }
    
    public function undoCodeAuditSuccess(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $version->status = '3';
        $version->save();
        return ResultTool::success();
    }
    
    public function delete(Request $request) {
        VersionModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
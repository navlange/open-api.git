<?php
/*
 * @Author: lokei
 * @Date: 2022-09-17 16:25:29
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-18 11:41:26
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Type;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Type\TypeModel;
use App\Models\Widget\ThemeIndexModel;
use Illuminate\Http\Request;

class TypeController extends Controller {

    public function get(Request $request) {
        $type = TypeModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $type;
        return $res;
    }

    public function list(Request $request) {
        if($request->input('scene') != null && $request->input('scene') != '') {
            $items = TypeModel::where('scene', '=', $request->input('scene'))->orderBy('order_index', 'desc')->get();
        } else {
            $items = TypeModel::orderBy('order_index', 'desc')->get();
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofMode(Request $request) {
        $items = TypeModel::where('app_mode', '=', $request->input('app_mode'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofScene(Request $request) {
        $model = TypeModel::where('scene', '=', $request->input('scene'));
        if($request->input('app_mode') !== null && $request->input('app_mode') !== '') {
            $model->where('app_mode', '=', $request->input('app_mode'));
        }
        if($request->input('scene') == 'product_feature') {
            $model->where('target_id', '=', $request->input('product_id'));
        }
        $items = $model->orderBy('order_index', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofSceneRoot(Request $request) {
        $model = TypeModel::where('scene', '=', $request->input('scene'))->where('pre_id', '=', 0);
        if($request->input('app_mode') !== null && $request->input('app_mode') !== '') {
            $model->where('app_mode', '=', $request->input('app_mode'));
        }
        $items = $model->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofModeRoot(Request $request) {
        $items = TypeModel::where('app_mode', '=', $request->input('app_mode'))->where('pre_id', '=', 0)->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofPre(Request $request) {
        $model = TypeModel::where('pre_id', '=', $request->input('pre_id'));
        if($request->input('scene') != null && $request->input('scene') != '') {
            $model->where('scene', '=', $request->input('scene'));
        }
        $items = $model->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $type = new TypeModel();
        if($request->input('app_mode') != null && $request->input('app_mode') != '') {
            $type->app_mode = $request->input('app_mode');
        } else {
            $type->app_mode = '';
        }
        if($request->input('scene') != null && $request->input('scene') != '') {
            $type->scene = $request->input('scene');
        }
        $type->name = $request->input('name');
        $type->img_thumb = $request->input('img_thumb');
        $type->img_display = $request->input('img_display');
		if(!is_null($request->input('pre_id'))) {
			$type->pre_id = $request->input('pre_id');
			// $pre_type = TypeModel::where('id', '=', $request->input('pre_id'))->first();
			// $type->full_name = $pre_type->name . '|' . $request->input('name');
		}
        $type->intro = $request->input('intro');
        $type->main_color = $request->input('main_color');
        $type->main_color_subtle = $request->input('main_color_subtle');

        if($request->input('scene') == 'product_feature') {
            $type->target_id = $request->input('product_id');
        }
        
        $type->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $type = TypeModel::where('id', '=', $request->input('id'))->first();
        if($request->input('app_mode') != null && $request->input('app_mode') != '') {
            $type->app_mode = $request->input('app_mode');
        } else {
            $type->app_mode = '';
        }
        $type->name = $request->input('name');
        $type->img_thumb = $request->input('img_thumb');
        $type->img_display = $request->input('img_display');
		if(!is_null($request->input('pre_id'))) {
			$type->pre_id = $request->input('pre_id');
			// $pre_type = TypeModel::where('id', '=', $request->input('pre_id'))->first();
			// $type->full_name = $pre_type->name . '|' . $request->input('name');
		}
        $type->intro = $request->input('intro');
        $type->main_color = $request->input('main_color');
        $type->main_color_subtle = $request->input('main_color_subtle');
        $type->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        TypeModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
    
    public function setOrder(Request $request) {
        $article = TypeModel::where('id', '=', $request->input('id'))->first();
        $article->order_index = $request->input('order_index');
        $article->save();
        return ResultTool::success();
    }
}
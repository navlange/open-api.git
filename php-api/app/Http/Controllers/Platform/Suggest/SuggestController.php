<?php
/*
 * @Author: lokei
 * @Date: 2022-10-24 23:37:22
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-27 21:32:41
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Suggest;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Suggest\SuggestModel;
use Illuminate\Http\Request;
use Symfony\Component\Console\Completion\Suggestion;

class SuggestController extends Controller {
    public function list() {
        $items = SuggestModel::orderBy('created_at', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }
    public function ofMy() {
        $user = app('auth')->user();
        $items = SuggestModel::where('uid', '=', $user->uid)->orderBy('created_at', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }
    public function submit(Request $request) {
        $user = app('auth')->user();
        $suggest = new SuggestModel();
        $suggest->uid = $user->uid;
        $suggest->content = $request->input('content');
        $suggest->image = $request->input('image');
        $suggest->mobile = $request->input('mobile');
        $suggest->created_at = date('Y-m-d H:i:s', time());
        $suggest->status = '0';
        $suggest->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        SuggestModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }

    
	public function get(Request $request) {
		$suggest = SuggestModel::where('id', '=', $request->input('id'))->first();
		$res = ResultTool::success();
		$res['data'] = $suggest;
		return $res;
	}

	public function finish(Request $request) {
		$suggest = SuggestModel::where('id', '=', $request->input('id'))->first();
		$suggest->status = '1';
        $suggest->reply = $request->input('reply');
		$suggest->save();
		return ResultTool::success();
	}

}
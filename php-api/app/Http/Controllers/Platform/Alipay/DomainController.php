<?php
/*
 * @Author: lokei
 * @Date: 2023-04-28 17:31:51
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-28 19:17:49
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Alipay;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Conf\ConfAlipayModel;
use Illuminate\Http\Request;

class DomainController extends Controller {
    public function list() {
        $conf = ConfAlipayModel::first();
        if(is_null($conf)) {
            $conf = new ConfAlipayModel();
            $conf->domain = '';
            $conf->save();
        }
        $res = ResultTool::success();
        $res['data']['domain'] = $conf->domain;
        return $res;
    }

    public function addDomainLocal(Request $request) {
        $conf = ConfAlipayModel::first();
        if(is_null($conf)) {
            $conf = new ConfAlipayModel();
            $conf->domain = '';
            $conf->save();
        }
        if($conf->domain == null || $conf->domain == '') {
            $conf->domain = $request->input('domain');
        } else {
            $conf->domain = $conf->domain . ';' . $request->input('domain');
        }
        $conf->save();
        return ResultTool::success();
    }

    public function deleteDomainLocal(Request $request) {
        $conf = ConfAlipayModel::first();
        $domain_list = explode(';', $conf->domain);
        for($i = 0; $i < count($domain_list); $i++) {
            if($domain_list[$i] == $request->input('domain')) {
                array_splice($domain_list, $i, 1);
                break;
            }
        }
        if(count($domain_list) > 0) {
            $conf->domain = join(';', $domain_list);
        } else {
            $conf->domain = '';
        }
        $conf->save();
        return ResultTool::success();
    }
}
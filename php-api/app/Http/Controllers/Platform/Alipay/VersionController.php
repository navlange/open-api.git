<?php
/*
 * @Author: lokei
 * @Date: 2023-04-28 00:35:14
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-19 16:01:58
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Alipay;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Alipay\VersionModel;
use Illuminate\Http\Request;

class VersionController extends Controller {
    public function ofProj() {
        $items = VersionModel::orderBy('createtime', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }
    
    public function get(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data']['version'] = $version;
        return $res;
    }

    public function create(Request $request) {
        $version = new VersionModel();
        $version->version = $request->input('version');
        $version->description = $request->input('description');
        $version->template_id = $request->input('template_id');
        $version->status = '6';
        $version->createtime = date('Y-m-d H:i:s', time());
        $version->save();
        return ResultTool::success();
    }
    
    public function update(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $version->status = $request->input('status');
        if($request->input('status') == 'AUDIT_REJECT') {
            $version->reject_reason = $request->input('reason');
            $version->reject_screen_shot = $request->input('reject_screen_shot');
        }
        $version->save();
        return ResultTool::success();
    }
    
    public function updateAuditId(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('version_id'))->first();
        $version->status = 'AUDITING';
        // $version->audit_id = $request->input('audit_id');
        $version->save();
        return ResultTool::success();
    }
    
    public function releaseSuccess(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $version->status = 'RELEASE';
        $version->save();
        return ResultTool::success();
    }
    
    public function undoCodeAuditSuccess(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $version->status = '3';
        $version->save();
        return ResultTool::success();
    }
    
    public function auditedCancelSuccess(Request $request) {
        $version = VersionModel::where('id', '=', $request->input('id'))->first();
        $version->status = 'AUDITED_CANCEL';
        $version->save();
        return ResultTool::success();
    }
    
    public function delete(Request $request) {
        VersionModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-04-29 00:48:29
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-29 01:25:51
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Search;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Search\SearchModel;
use Illuminate\Http\Request;

class SearchController extends Controller {
    public function add(Request $request) {
        if($request->input('value') != null && $request->input('value') != '') {
            $search = SearchModel::where('value', '=', $request->input('value'))->first();
            if(!is_null($search)) {
                $search->count = $search->count + 1;
                $search->save();
            } else {
                $search = new SearchModel();
                $search->value = $request->input('value');
                $search->created_at = date('Y-m-d H:i:s', time());
                $search->count = 1;
                $search->save();
            }
        }
        return ResultTool::success();
    }

    public function list(Request $request) {
        if($request->input('limit') != null && intval($request->input('limit')) > 0) {
            $items = SearchModel::orderBy('count', 'desc')->limit(100)->offset(0)->get();
        } else {
            $items = SearchModel::orderBy('count', 'desc')->get();
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }
}
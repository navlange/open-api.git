<?php
/*
 * @Author: lokei
 * @Date: 2023-05-09 23:47:00
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-20 11:33:50
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Agreement;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfModel;
use Illuminate\Http\Request;

class AgreementController extends Controller {
    public function get() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->name = 'open-api';
            $conf->save();
            $conf = ConfModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'user_agreement' => $conf->user_agreement,
            'privacy_agreement' => $conf->privacy_agreement,
            'master_agreement' => $conf->master_agreement
        ];
        return $res;
    }

    public function update(Request $request) {
        $conf = ConfModel::first();
        $conf->user_agreement = $request->input('user_agreement');
        $conf->privacy_agreement = $request->input('privacy_agreement');
        $conf->master_agreement = $request->input('master_agreement');
        $conf->save();
        return ResultTool::success();
    }

    public function getMaster() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->name = 'open-api';
            $conf->save();
            $conf = ConfModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'agreement' => $conf->master_agreement
        ];
        return $res;
    }
    
    public function getUser() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->name = 'open-api';
            $conf->save();
            $conf = ConfModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'agreement' => $conf->user_agreement
        ];
        return $res;
    }
    
    public function getPrivacy() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->name = 'open-api';
            $conf->save();
            $conf = ConfModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'agreement' => $conf->privacy_agreement
        ];
        return $res;
    }
}
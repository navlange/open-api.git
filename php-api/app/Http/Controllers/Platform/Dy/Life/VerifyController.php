<?php
/*
 * @Author: lokei
 * @Date: 2023-09-05 09:08:15
 * @LastEditors: lokei
 * @LastEditTime: 2023-11-25 21:09:52
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Dy\Life;

use App\Common\Douyin\Common;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Store\StoreModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class VerifyController extends Controller
{
    public function prepare(Request $request)
    {
        $access_token = Common::getAccessToken();
        if ($access_token == null) {
            $res = ResultTool::fail();
            $res['errorMsg'] = '获取access_token失败';
            return $res;
        }

        $encrypted_data = null;

        if (strlen($request->input('dy_code')) > 8 && substr($request->input('dy_code'), 0, 20) == 'https://v.douyin.com') {
            $shortUrl = $request->input('dy_code');
            // 初始化 cURL  
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $shortUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // 发送请求并获取响应  
            $response = curl_exec($ch);
            curl_close($ch);

            // 解析响应，获取长链  
            if ($response !== false) {
                $res_info = explode('"', $response);
                if(count($res_info) >= 3) {
                    $longUrl = $res_info[1];
                    $params = parse_url($longUrl);

                    $queryData = explode('&amp;', $params['query']);
                    //定义接收数组
                    $qqData = array();
                    //循环参数
                    foreach ($queryData as $k => $v) {
                        //将参数再次分割
                        $str = explode('=', $v);
                        //参数赋值
                        $qqData[$str[0]] = $str[1];
                        if ($str[0] == 'object_id') {
                            $encrypted_data = $str[1];
                            break;
                        }
                    }
                } else {
                    $res = ResultTool::fail();
                    $res['errorMsg'] = '无法获取长链！';
                    return $res;
                }
            } else {
                $res = ResultTool::fail();
                $res['errorMsg'] = '获取长链接失败！';
                return $res;
            }
        }

        $prepare_result = Common::prepareCertificate($access_token, $encrypted_data, $request->input('dy_code'));
        if ($prepare_result['data']['error_code'] == 0) {
            foreach($prepare_result['data']['certificates'] as $k => $v) {
                $prepare_result['data']['certificates'][$k]['certificate_id'] = '' . $prepare_result['data']['certificates'][$k]['certificate_id'];
            }
            $res = ResultTool::success();
            $res['data']['certificates'] = $prepare_result['data']['certificates'];
            return $res;
        } else {
            app()->make('log')->info('提交优惠券失败', ['encrypted_data' => $encrypted_data, 'error_code' => $prepare_result['data']['error_code'], 'description' => $prepare_result['data']['description']]);
            $res = ResultTool::fail();
            $res['errorMsg'] = $prepare_result['data']['description'];
            return $res;
        }
    }

    public function submit(Request $request)
    {
        $access_token = Common::getAccessToken();
        $store = StoreModel::where('id', '=', $request->input('store_id'))->first();
        if ($store->poi_id != null && $store->poi_id != '') {
            $prepare_result = Common::prepareCertificate($access_token, null, $request->input('dy_code'));
            if ($prepare_result['data']['error_code'] == 0) {
                $verify_result = Common::verifyCoupon($prepare_result['data']['verify_token'], $access_token, $store->poi_id, null);
                if ($verify_result['data']['error_code'] == 0) {
                    $fields = array();
                    $fields['order_id'] = $request->input('order_id');
                    $fields['pid'] = config('app.proj_identity');
                    $fields['pay_mode'] = '3-2';
                    Redis::xAdd('pay_success', '*', $fields);
                } else {
                    $res = ResultTool::fail();
                    $res['errorMsg'] = $verify_result['data']['description'];
                    return $res;
                    return $res;
                }
            } else {
                $res = ResultTool::fail();
                $res['errorMsg'] = $prepare_result['data']['description'];
                return $res;
            }
        } else {
            $res = ResultTool::fail();
            $res['errorMsg'] = '未绑定抖店！';
            return $res;
        }
    }
}

<?php
/*
 * @Author: lokei
 * @Date: 2023-09-05 11:28:50
 * @LastEditors: lokei
 * @LastEditTime: 2023-09-05 11:38:35
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Dy\Life;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Dy\Life\ConfModel;
use Illuminate\Http\Request;

class ConfController extends Controller {
    public function get(Request $request) {
        $conf = ConfModel::first();
        $res = ResultTool::success();
        $res['data'] = $conf;
        return $res;
    }

    public function update(Request $request) {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
        }
        $conf->name = $request->input('name');
        $conf->appid = $request->input('appid');
        $conf->secret = $request->input('secret');
        $conf->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-10-19 20:54:35
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-19 07:35:49
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Statistic;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\StatisticMiddle;
use App\Models\Platform\Statistic\StatisticModel;
use Illuminate\Support\Facades\Redis;

class PlatformController extends Controller {

    public function get() {
        $res = ResultTool::success();
        $statistic = StatisticModel::first();
        if(is_null($statistic)) {
            $statistic = new StatisticModel();
            $statistic->pv = 0;
            $statistic->save();
            $statistic = StatisticModel::first();
        }
        $res['data']['pv'] = $statistic->pv;
        // $res['data']['store_amount'] = $statistic->store_amount;
        return $res;
    }

    public function incPv() {
        StatisticMiddle::incPv();
    }

    public function all() {
        $keys = Redis::keys('statistic:platform:*');
        $items = [];
        if(count($keys) > 0) {
            foreach($keys as $key) {
                array_push($items, [
                    'day' => substr($key, 19),
                    'statistic' => Redis::hGetAll($key)
                ]);
            }
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }
}
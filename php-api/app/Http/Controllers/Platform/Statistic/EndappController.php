<?php
/*
 * @Author: lokei
 * @Date: 2023-05-23 23:35:42
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-24 08:26:51
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Statistic;

use App\Common\Miniapp\Common;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;

class EndappController extends Controller
{
    public function get()
    {
        $access_token = Common::getAccessToken();

        $statistic = [];
        $cur_day = date('Ymd', time());
        for($i = 0; $i < 30; $i ++) {
            $cur_day = date('Ymd', strtotime($cur_day) - 24*60*60);
            $params = [
                'begin_date' => $cur_day,
                'end_date' => $cur_day
            ];
    
            // 这里的参数需要使用raw的方式传参（！！！！这里需要注意）
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/datacube/getweanalysisappiddailyvisittrend?access_token=$access_token");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params)); // 必须为字符串
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain')); // 必须声明请求头
            // 如果没有错误的话返回的就是小程序码的文件流
            $response = curl_exec($ch);
            if (isset($response['errcode'])) { //查看文档错误代码
                $res = ResultTool::fail();
                $res['errorMsg'] = "获取小程序统计信息出错";
                return $res;
            }
            array_push($statistic, json_decode($response));
        }
        
        // $result = json_decode($response->getBody(), true);
        $res = ResultTool::success();
        $res['data'] = $statistic;
        return $res;
    }
}

<?php
/*
 * @Author: lokei
 * @Date: 2022-08-16 21:24:58
 * @LastEditors: lokei
 * @LastEditTime: 2024-09-16 02:08:03
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Upload;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Material\ImageModel;
use Illuminate\Http\Request;

class ImageController extends Controller {
    public function upload(Request $request) {
        // $setting = $_W['setting']['upload']['image'];
        $result = array(
            'jsonrpc' => '2.0',
            'id' => 'id',
            'error' => array('code' => 1, 'message'=>''),
        );

        $file = $request->file('file');
		if($file -> isValid()) {
			$size = $file->getSize();
			$clientName = $file -> getClientOriginalName();    //客户端文件名称..
			// $tmpName = $file ->getFileName();   //缓存在tmp文件夹中的文件名例如php8933.tmp 这种类型的.
			// $realPath = $file -> getRealPath();     //这个表示的是缓存在tmp文件夹下的文件的绝对路径
			$entension = $file -> getClientOriginalExtension();   //上传文件的后缀.
			// $mimeTye = $file -> getMimeType();    //也就是该资源的媒体类型
			$newName = $newName = md5(date('ymdhis').$clientName).".".$entension;    //定义上传文件的新名称
			// $path = $file -> move(env('ATTACHMENT_ROOT') . (config('app.proj_identity') ? (config('app.proj_identity') . '/') : '') . "images/" . date('Y/m'),$newName);    //把缓存文件移动到制定文件夹
			// $data['file_name'] = (config('app.proj_identity') ? (config('app.proj_identity') . '/') : '') . "images/" . date('Y/m') . '/' . $newName;
			
            $attachmentRoot = env('ATTACHMENT_ROOT');
            $projIdentity = config('app.proj_identity') ? (config('app.proj_identity') . '/') : '';
            $projIdentity_os = str_replace('/', DIRECTORY_SEPARATOR, $projIdentity);
            $datePath = 'images' . '/' . date('Y/m');
            $datePath_os = str_replace('/', DIRECTORY_SEPARATOR, $datePath);
            $destinationPath_os = $attachmentRoot . $projIdentity_os . $datePath_os;

            $path = $file->move($destinationPath_os, $newName);    // 把缓存文件移动到指定文件夹

            $data['file_name'] = $projIdentity . $datePath . '/' . $newName;
            
            $data['status'] = '0';
			$res = ResultTool::success();

			$image = new ImageModel();
			$image->path = $data['file_name'];
			$image->size = $size;
			$image->created_at = date('Y-m-d H:i:s', time());
			$image->save();
		} else {
			$data['status'] = '1';
			$res = ResultTool::fail();
		}
		$res['data'] = $data;
		return $res;
	}
}

<?php
/*
 * @Author: lokei
 * @Date: 2022-08-16 21:24:58
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-12 16:11:00
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Upload;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileController extends Controller {
    public function upload(Request $request) {
        // $setting = $_W['setting']['upload']['image'];
        $result = array(
            'jsonrpc' => '2.0',
            'id' => 'id',
            'error' => array('code' => 1, 'message'=>''),
        );

        $file = $request->file('file');
		if($file -> isValid()) {
			$clientName = $file -> getClientOriginalName();    //客户端文件名称..
			// $tmpName = $file ->getFileName();   //缓存在tmp文件夹中的文件名例如php8933.tmp 这种类型的.
			// $realPath = $file -> getRealPath();     //这个表示的是缓存在tmp文件夹下的文件的绝对路径
			$entension = $file -> getClientOriginalExtension();   //上传文件的后缀.
			// $mimeTye = $file -> getMimeType();    //也就是该资源的媒体类型
			$newName = $newName = md5(date('ymdhis').$clientName).".".$entension;    //定义上传文件的新名称
			$path = $file -> move(env('ATTACHMENT_ROOT') . (config('app.proj_identity') ? (config('app.proj_identity') . '/') : '') . "files/" . date('Y/m'),$newName);    //把缓存文件移动到制定文件夹
			$data['file_name'] = (config('app.proj_identity') ? (config('app.proj_identity') . '/') : '') . "files/" . date('Y/m') . '/' . $newName;
			$data['status'] = '0';
			$res = ResultTool::success();
		} else {
			$data['status'] = '1';
			$res = ResultTool::fail();
		}
		$res['data'] = $data;
		return $res;
	}
}
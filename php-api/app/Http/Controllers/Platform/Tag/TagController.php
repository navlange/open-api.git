<?php
/*
 * @Author: lokei
 * @Date: 2022-09-17 16:25:29
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-15 17:14:28
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Tag;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Tag\TagModel;
use App\Models\Widget\ThemeIndexModel;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function list()
    {
        $items = TagModel::get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofScene(Request $request)
    {
        $items = TagModel::where('scene', '=', $request->input('scene'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function ofIds(Request $request)
    {
        $res = ResultTool::success();
        $ids = explode('|', $request->input('ids'));
        $items = TagModel::whereIn('id', $ids)->get();
        $res['data']['items'] = $items;
        return $res;
    }

    public function create(Request $request)
    {
        $tag = new TagModel();
        $tag->scene = $request->input('scene');
        $tag->name = $request->input('name');
        $tag->img_thumb = $request->input('img_thumb');
        $tag->save();
        return ResultTool::success();
    }

    public function update(Request $request)
    {
        $tag = TagModel::where('id', '=', $request->input('id'))->first();
        $tag->scene = $request->input('scene');
        $tag->name = $request->input('name');
        $tag->img_thumb = $request->input('img_thumb');
        $tag->save();
        return ResultTool::success();
    }

    public function delete(Request $request)
    {
        TagModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
}

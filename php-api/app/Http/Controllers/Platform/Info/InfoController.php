<?php
/*
 * @Author: lokei
 * @Date: 2023-07-01 10:09:05
 * @LastEditors: lokei
 * @LastEditTime: 2023-10-04 10:16:39
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Info;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;

use OpenApi\Annotations\Get;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;
use App\Http\Responses\CommResp;
use App\Models\Platform\ConfModel;

class InfoController extends Controller
{
	/**
	 * @Get(
	 *     path="/api/platform/info/get",
	 *     tags={"【平台】平台管理"},
	 *     summary="平台信息",
	 *     @Response(
	 *         response="200",
	 *         description="正常操作响应",
	 *         @MediaType(
	 *             mediaType="application/json",
	 *             @Schema(
	 *                 allOf={
	 *                     @Schema(ref="#/components/schemas/CommResp")
	 *                 }
	 *             )
	 *         )
	 *     )
	 * )
	 *
	 * @param Request $request
	 *
	 * @return CommResp
	 */
	public function get()
	{
		$res = ResultTool::success();

		$conf = ConfModel::first();
		if(is_null($conf)) {
			$conf = new ConfModel();
		}
		$res['data']['platform'] = [
			'logo' => env('PLATFORM_LOGO') ? env('PLATFORM_LOGO') : $conf->logo,
			'name' => $conf->name
		];
		$res['data']['icp'] = [
			'icp' => env('PLATFORM_ICP') ? env('PLATFORM_ICP') : $conf->icp,
			'police_icp' => env('PLATFORM_POLICE_ICP') ? env('PLATFORM_POLICE_ICP') : $conf->police_icp,
		];

		$res['data']['contacts'] = [
			'contacts_mobile' => env('PLATFORM_CONTACTS_MOBILE') ? env('PLATFORM_CONTACTS_MOBILE') : $conf->contacts_mobile,
			'contacts_email' => env('PLATFORM_CONTACTS_EMAIL') ? env('PLATFORM_CONTACTS_EMAIL') : $conf->contacts_email,
			'contacts_qq' => env('PLATFORM_CONTACTS_QQ') ? env('PLATFORM_CONTACTS_QQ') : $conf->contacts_qq,
			'contacts_qw' => env('PLATFORM_CONTACTS_QW') ? env('PLATFORM_CONTACTS_QW') : $conf->contacts_qw,
			'contacts_mp' => env('PLATFORM_CONTACTS_MP') ? env('PLATFORM_CONTACTS_MP') : $conf->contacts_mp
		];
		$res['data']['server'] = [
			'os' => php_uname(),
			'server' => $_SERVER["SERVER_SOFTWARE"],
			'php' => PHP_VERSION,
			'lummen' => app()->version()
		];
		return $res;
	}

	function readable_size($length)
	{
		$units = array('B', 'kB', 'MB', 'GB', 'TB');
		foreach ($units as $unit) {
			if ($length > 1024)
				$length = round($length / 1024, 1);
			else
				break;
		}
		return $length . ' ' . $unit;
	}
}

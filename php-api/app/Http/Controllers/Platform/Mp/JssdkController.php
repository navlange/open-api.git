<?php
/*
 * @Author: lokei
 * @Date: 2023-07-11 17:06:38
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-11 18:02:46
 * @Description: 
 */

namespace App\Http\Controllers\Platform\Mp;

use App\Common\Mp\Common;
use App\Common\Tools\RandomTool;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\ConfMpModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

use GuzzleHttp;

class JssdkController extends Controller
{
    function ascll($params = array())
    {
        if (!empty($params)) {
            $p =  ksort($params);
            if ($p) {
                $str = '';
                foreach ($params as $k => $val) {
                    $str .= $k . '=' . $val . '&';
                }
                $strs = rtrim($str, '&');
                return $strs;
            }
        }
        return '参数错误';
    }
    
    public function getSignPackage(Request $request)
    {
        $appid = '';
        $secret = '';
        if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
            $endapp = Redis::get('proj:' . config('app.proj_identity') . ':mp');
            $endapp_info = json_decode($endapp);
            $appid = $endapp_info->appid;
            $secret = $endapp_info->secret;
        } else {
            $conf = ConfMpModel::first();
            $appid = $conf->appid;
            $secret = $conf->secret;
        }

        if ($appid != '') {
            $redis_key = 'mp_access_token_' . $appid . '_jsapi_ticket';
            $jsapi_ticket = '';
            if (Redis::exists($redis_key)) {
                $jsapi_ticket =  Redis::get($redis_key);
            } else {
                $http = new GuzzleHttp\Client;
                $params = [
                    'access_token' => Common::getAccessToken(),
                    'type' => 'jsapi'
                ];
                $response = $http->get('https://api.weixin.qq.com/cgi-bin/ticket/getticket', [
                    'query' => $params
                ]);
                $result = json_decode($response->getBody(), true);
                if(array_key_exists('errcode', $result) && $result['errcode'] != 0) {
                    app('log')->info('获取公众号jssdk：' . $result['errmsg']);
                    return ResultTool::fail();
                } else {
                    Redis::setex($redis_key, intval($result['expires_in']) - 300, $result['ticket']);
                    $jsapi_ticket = $result['ticket'];
                }
            }
            $res = ResultTool::success();
            $timestamp = time();
            $nonceStr = RandomTool::GetRandStr(16);

            $arr = [
                "noncestr" => $nonceStr,
                "timestamp" => $timestamp,
                "jsapi_ticket" => $jsapi_ticket,
                "url" => $request->input('url')
            ];
            $sign = sha1($this->ascll($arr));
            
            $res['data'] = [
                'appId' => $appid,
                'timestamp' => $timestamp,
                'nonceStr' => $nonceStr,
                'signature' => $sign
            ];
            return $res;
        } else {
            return ResultTool::fail();
        }
    }
}

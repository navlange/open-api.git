<?php
/*
 * @Author: lokei
 * @Date: 2023-03-23 22:53:31
 * @LastEditors: lokei
 * @LastEditTime: 2024-10-09 21:15:40
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Brand;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Brand\BrandModel;
use Illuminate\Http\Request;

class BrandController extends Controller {

    public function ofMe(Request $request) {
        $user = app('auth')->user();
        $brand = BrandModel::where('uid', '=', $user->uid)->first();
        $res = ResultTool::success();
        $res['data'] = $brand;
        return $res;
    }

    public function register(Request $request) {
        $user = app('auth')->user();
        $brand = BrandModel::where('uid', '=', $user->uid)->first();
        if($brand != null) {
            return ResultTool::fail('已经注册过品牌');
        }
        $brand = new BrandModel();
        $brand->uid = $user->uid;
        $brand->name = $request->input('name');
        $brand->logo = $request->input('logo');
        $brand->detail = $request->input('detail');
        $brand->regNumber = $request->input('regNumber');
        $brand->regFile = $request->input('regFile');
        $brand->holder = $request->input('holder');
        $brand->businessLicense = $request->input('businessLicense');
        $brand->intro = $request->input('intro');
        $brand->additionalInfo = $request->input('additionalInfo');
        $brand->status = '0';
        $brand->save();
        return ResultTool::success();
    }

    public function get(Request $request) {
        $brand = BrandModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $brand;
        return $res;
    }

    public function list(Request $request) {
        if($request->input('limit') != null && intval($request->input('limit')) > 0) {
            // $items = BrandModel::limit($request->input('limit'))->orderBy('id', 'desc')->get();
            // 分页查询limit, page为input参数
            $items = BrandModel::orderBy('id', 'desc')->limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')))->get();
            $total = BrandModel::count();
        } else {
            $items = BrandModel::orderBy('id', 'desc')->get();
            $total = count($items);
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = $total;
        return $res;
    }

    public function ofType0(Request $request) {
        $items = BrandModel::where('type_0_id', '=', $request->input('type_0_id'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }

    public function create(Request $request) {
        $brand = new BrandModel();
        $brand->type_0_id = $request->input('type_0_id');
        $brand->name = $request->input('name');
        $brand->logo = $request->input('logo');
        $brand->intro = $request->input('intro');
        $brand->img_display = $request->input('img_display');
        $brand->detail = $request->input('detail');
        $brand->save();
        return ResultTool::success();
    }
    
    public function update(Request $request) {
        $brand = BrandModel::where('id', '=', $request->input('id'))->first();
        // $request有type_0_id字段
        if($request->has('type_0_id')) {
            $brand->type_0_id = $request->input('type_0_id');
        }

        $brand->name = $request->input('name');
        $brand->logo = $request->input('logo');
        $brand->intro = $request->input('intro');
        $brand->img_display = $request->input('img_display');
        $brand->detail = $request->input('detail');

        // $request有regNumber字段
        if($request->has('regNumber')) {
            $brand->regNumber = $request->input('regNumber');
        }
        // $request有regFile字段
        if($request->has('regFile')) {
            $brand->regFile = $request->input('regFile');
        }
        // $request有holder字段
        if($request->has('holder')) {
            $brand->holder = $request->input('holder');
        }
        // $request有businessLicense字段
        if($request->has('businessLicense')) {
            $brand->businessLicense = $request->input('businessLicense');
        }
        // $request有intro字段
        if($request->has('intro')) {
            $brand->intro = $request->input('intro');
        }
        // $request有additionalInfo字段
        if($request->has('additionalInfo')) {
            $brand->additionalInfo = $request->input('additionalInfo');
        }
        
        $brand->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        BrandModel::where('id', '=', $request->input('id'))->delete();
        return ResultTool::success();
    }
    
    public function setOrder(Request $request) {
        $article = BrandModel::where('id', '=', $request->input('id'))->first();
        $article->order_index = $request->input('order_index');
        $article->save();
        return ResultTool::success();
    }

    public function acceptAudit(Request $request) {
        $brand = BrandModel::where('id', '=', $request->input('id'))->first();
        $brand->status = '1';
        $brand->save();
        return ResultTool::success();
    }

    public function rejectAudit(Request $request) {
        $brand = BrandModel::where('id', '=', $request->input('id'))->first();
        $brand->status = '0';
        $brand->save();
        return ResultTool::success();
    }
}
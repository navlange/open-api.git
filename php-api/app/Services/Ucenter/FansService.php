<?php
/*
 * @Author: lokei
 * @Date: 2022-08-09 07:48:10
 * @LastEditors: lokei
 * @LastEditTime: 2022-09-23 21:31:16
 * @Description: 
 */

namespace App\Services\Ucenter;

use App\Models\Ucenter\FansModel;

class FansService
{
	public function registerOpenid($platform, $openid)
	{
		$fans = new FansModel();
        $fans->platform = $platform;
		$fans->openid = $openid;
		$fans->followtime = date('Y-m-d H:i:s', time());
		$fans->save();
		return $fans->fid;
	}

}
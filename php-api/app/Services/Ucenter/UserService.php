<?php
/*
 * @Author: lokei
 * @Date: 2022-08-09 07:50:02
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-08 12:34:11
 * @Description: 
 */

namespace App\Services\Ucenter;

use App\Common\Tools\RandomTool;
use App\Models\Ucenter\UserModel;

class UserService
{
	public function createUser()
	{
		$user = new UserModel();
		$user->mobile = '';
		$user->salt = RandomTool::GetRandStr(8);
		$user->createtime = date('Y-m-d H:i:s', time());
		$user->save();
		return $user->uid;
	}
}
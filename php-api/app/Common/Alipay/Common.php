<?php
/*
 * @Author: lokei
 * @Date: 2022-11-16 09:20:25
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-04 17:45:13
 * @Description: 
 */

namespace App\Common\Alipay;

use Alipay\EasySDK\Kernel\Config;
use App\Models\Platform\Conf\ConfAlipayModel;
use Illuminate\Support\Facades\Request;

class Common
{
    public static function getOptions()
    {
        $options = new Config();
        $options->protocol = 'https';
        $options->gatewayHost = 'openapi.alipay.com';
        $options->signType = 'RSA2';

        if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
            $options->appId = config('app.suit_appid');
            
            $options->merchantPrivateKey = config('app.suit_merchantPrivateKey');
    
            $options->alipayPublicKey = config('app.suit_alipayPublicKey');
            $options->alipayRootCertSN = config('app.suit_alipayRootCertSN');
            $options->merchantCertSN = config('app.suit_merchantCertSN');
        } else {
            $options->appId = Request::header('appid');
            
            $conf_alipay = ConfAlipayModel::first();

            // 为避免私钥随源码泄露，推荐从文件中读取私钥字符串而不是写入源码中
            // $options->merchantPrivateKey = '<-- 请填写您的应用私钥，例如：MIIEvQIBADANB ... ... -->';
            $options->merchantPrivateKey = $conf_alipay->merchantPrivateKey;
    
            // $options->alipayCertPath = '<-- 请填写您的支付宝公钥证书文件路径，例如：/foo/alipayCertPublicKey_RSA2.crt -->';
            $options->alipayCertPath = env('ATTACHMENT_ROOT') . $conf_alipay->alipayCertPath;
            // $options->alipayRootCertPath = '<-- 请填写您的支付宝根证书文件路径，例如：/foo/alipayRootCert.crt" -->';
            $options->alipayRootCertPath = env('ATTACHMENT_ROOT') . $conf_alipay->alipayRootCertPath;
            // $options->merchantCertPath = '<-- 请填写您的应用公钥证书文件路径，例如：/foo/appCertPublicKey_2019051064521003.crt -->';
            $options->merchantCertPath = env('ATTACHMENT_ROOT') . $conf_alipay->merchantCertPath;
        }

        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
        // $options->alipayPublicKey = '<-- 请填写您的支付宝公钥，例如：MIIBIjANBg... -->';

        //可设置异步通知接收服务地址（可选）
        $options->notifyUrl = "<-- 请填写您的支付类接口异步通知接收服务地址，例如：https://www.test.com/callback -->";

        //可设置AES密钥，调用AES加解密相关接口时需要（可选）
        $options->encryptKey = "<-- 请填写您的AES密钥，例如：aa4BtZ4tspm2wnXLb1ThQA== -->";



        return $options;
    }
}

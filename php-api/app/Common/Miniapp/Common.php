<?php
/*
 * @Author: lokei
 * @Date: 2022-09-24 09:03:54
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-01 15:04:06
 * @Description: 
 */

namespace App\Common\Miniapp;

use App\Models\Platform\Conf\ConfMiniappModel;
use GuzzleHttp;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;

class Common
{
    public static function getAccessToken()
    {
        $redis_key = 'miniapp_access_token';
        if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
            $redis_key = $redis_key . '_' . config('app.proj_identity');
        }
        if (Redis::exists($redis_key)) {
            return Redis::get($redis_key);
        } else {
            $http = new GuzzleHttp\Client;
            if (config('app.proj_identity') != null && config('app.proj_identity') != '') {
                if (config('app.proj_type') != null && config('app.proj_type') == '6') {
                    $conf = ConfMiniappModel::first();
                    $params = [
                        'appid' => Request::header('appid'),
                        'secret' => config('app.secret') ? config('app.secret') : $conf->secret,
                        'grant_type' => 'client_credential'
                    ];
                } else {
                    if (Request::header('appid') != '') {
                        $endapp = Redis::get('proj:endapp:' . Request::header('appid'));
                        $endapp_info = json_decode($endapp);
                        $params = [
                            'appid' => Request::header('appid'),
                            'secret' => $endapp_info->secret,
                            'grant_type' => 'client_credential'
                        ];
                    } else {
                        $endapp = Redis::get('proj:' . config('app.proj_identity') . ':miniapp');
                        $endapp_info = json_decode($endapp);
                        $params = [
                            'appid' => $endapp_info->appid,
                            'secret' => $endapp_info->secret,
                            'grant_type' => 'client_credential'
                        ];
                    }
                }
            } else {
                $conf = ConfMiniappModel::first();
                $params = [
                    'appid' => $conf->appid,
                    'secret' => $conf->secret,
                    'grant_type' => 'client_credential'
                ];
            }
            $response = $http->get('https://api.weixin.qq.com/cgi-bin/token', [
                'query' => $params
            ]);
            $result = json_decode($response->getBody(), true);
            if(array_key_exists('errcode', $result)) {
                app('log')->info('获取小程序access_token失败：'. $result['errcode'] . ':' . $result['errmsg'] . ', pid: ' . config('app.proj_identity') . ', params: ' . json_encode($params));
                return '';
            }
            Redis::setex($redis_key, intval($result['expires_in']) - 300, $result['access_token']);
            return $result['access_token'];
        }
    }
}

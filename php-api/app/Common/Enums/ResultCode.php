<?php
/*
 * @Descripttion: 
 * @version: 
 * @Author: 奇辰科技
 * @Date: 2021-10-15 11:44:45
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-01 11:30:29
 */
namespace App\Common\Enums;

use MyCLabs\Enum\Enum;

class ResultCode extends Enum
{
    const SUCCESS = ['code' => 200, 'msg' => '成功'];
    const FAIL = ['code' => 999, 'msg' => '失败'];
	const USER_NOT_LOGIN = ['code' => 2001, 'msg' => '用户未登录'];
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-04-26 08:10:40
 * @LastEditors: lokei
 * @LastEditTime: 2022-04-26 08:11:54
 * @Description: 
 */
namespace App\Common\Tools;

use App\Common\Enums\ResultCode;

class ResultTool {
	public static function success() {
		return [
			'errorCode' => (ResultCode::SUCCESS)['code'],
			'errorMsg' => (ResultCode::SUCCESS)['msg'],
			'data' => []
		];
	}

	public static function fail() {
		return [
			'errorCode' => (ResultCode::FAIL)['code'],
			'errorMsg' => (ResultCode::FAIL)['msg'],
			'data' => []
		];
	}
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-08-09 07:54:05
 * @LastEditors: lokei
 * @LastEditTime: 2022-08-09 07:54:09
 * @Description: 
 */

namespace App\Common\Tools;

class RandomTool
{
	public static function GetRandStr($length)
	{
		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$len = strlen($str) - 1;
		$randstr = '';
		for ($i = 0; $i < $length; $i++) {
			$num = mt_rand(0, $len);
			$randstr .= $str[$num];
		}
		return $randstr;
	}
}

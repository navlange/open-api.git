<?php
/*
 * @Author: lokei
 * @Date: 2022-08-18 17:09:40
 * @LastEditors: lokei
 * @LastEditTime: 2022-09-21 16:33:47
 * @Description: 
 */

namespace App\Common\Tools;

class Sn
{
    public static function build_order_no()
    {
        list($t1, $t2) = explode(' ', microtime());
        return date('YmdHis', $t2) . $t1 * 1000000 . substr(implode(array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 6);
    }
}

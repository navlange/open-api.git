<?php
/*
 * @Author: lokei
 * @Date: 2022-09-24 09:03:54
 * @LastEditors: lokei
 * @LastEditTime: 2023-02-21 13:50:54
 * @Description: 
 */

namespace App\Common\Qyapi;

use GuzzleHttp;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;

class Common
{
    public static function getSuiteAccessToken($open_mode)
    {
        $redis_key = 'qy_suite_access_token';
        if (config('app.open_mode') != null && config('app.open_mode') != '') {
            $redis_key = $redis_key . '_' . config('app.open_mode');
        }
        if (Redis::exists($redis_key)) {
            return Redis::get($redis_key);
        } else {
            $http = new GuzzleHttp\Client;
            $response = $http->get(env('QC_SAAS_HOST') . '/saas/qyapi/getSuiteAccessToken', [
                'query' => [
                    'open_mode' => $open_mode
                ]
            ]);
            $result = json_decode($response->getBody(), true);
            Redis::setex($redis_key, intval($result['data']['expires_in']), $result['data']['access_token']);
            return $result['data']['access_token'];
        }
    }
}

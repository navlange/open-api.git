<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-03 23:31:43
 * @Description: 
 */
namespace App\Models\App;

use Illuminate\Database\Eloquent\Model;

class AppModel extends Model {
    protected $table = 'app';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
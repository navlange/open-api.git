<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-28 14:04:42
 * @Description: 
 */

namespace App\Models\Platform\Alias;

use Illuminate\Database\Eloquent\Model;

class AliasModel extends Model
{
	protected $table = 'alias';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-12 18:56:21
 * @Description: 
 */

namespace App\Models\Platform\Qrcode;

use Illuminate\Database\Eloquent\Model;

class QrcodeModel extends Model
{
	protected $table = 'qrcode';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

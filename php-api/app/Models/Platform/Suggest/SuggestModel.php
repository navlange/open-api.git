<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-31 13:06:38
 * @Description: 
 */

namespace App\Models\Platform\Suggest;

use App\Models\Common\SaasModel;

class SuggestModel extends SaasModel
{
	protected $table = 'suggest';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-09-18 11:54:22
 * @Description: 
 */

namespace App\Models\Platform\Type;

use Illuminate\Database\Eloquent\Model;

class TypeModel extends Model
{
	protected $table = 'type';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

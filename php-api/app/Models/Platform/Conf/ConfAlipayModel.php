<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-16 10:37:52
 * @Description: 
 */

namespace App\Models\Platform\Conf;

use Illuminate\Database\Eloquent\Model;

class ConfAlipayModel extends Model
{
	protected $table = 'conf_alipay';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-21 22:23:21
 * @Description: 
 */

namespace App\Models\Platform\Conf;

use Illuminate\Database\Eloquent\Model;

class ConfMiniappModel extends Model
{
	protected $table = 'conf_miniapp';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-10-22 18:39:24
 * @Description: 
 */

namespace App\Models\Platform\Conf;

use Illuminate\Database\Eloquent\Model;

class ConfDouyinModel extends Model
{
	protected $table = 'conf_douyin';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

}

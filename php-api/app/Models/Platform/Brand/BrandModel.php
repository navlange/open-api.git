<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-03-23 22:54:31
 * @Description: 
 */

namespace App\Models\Platform\Brand;

use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
	protected $table = 'brand';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

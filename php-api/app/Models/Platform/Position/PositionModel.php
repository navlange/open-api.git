<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-15 23:11:34
 * @Description: 
 */

namespace App\Models\Platform\Position;

use App\Models\Common\SaasModel;

class PositionModel extends SaasModel
{
	protected $table = 'position';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        self::updating(
            function ($model) {
                if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
                    $model->primaryKey = 'pid';
                }
            }
        );
    }

}

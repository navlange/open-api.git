<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-09-05 11:30:28
 * @Description: 
 */

namespace App\Models\Platform\Dy\Life;

use Illuminate\Database\Eloquent\Model;

class ConfModel extends Model
{
	protected $table = 'conf_dy_life';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-29 00:54:16
 * @Description: 
 */

namespace App\Models\Platform\Search;

use App\Models\Common\SaasModel;

class SearchModel extends SaasModel
{
	protected $table = 'search';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        self::updating(
            function ($model) {
                if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
                    $model->primaryKey = 'pid';
                }
            }
        );
    }

}

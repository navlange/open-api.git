<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-09-18 20:27:13
 * @Description: 
 */

namespace App\Models\Platform\Print;

use Illuminate\Database\Eloquent\Model;

class PrinterModel extends Model
{
	protected $table = 'printer';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

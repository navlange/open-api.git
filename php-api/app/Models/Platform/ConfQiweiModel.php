<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-08-20 18:57:33
 * @Description: 
 */

namespace App\Models\Platform;

use Illuminate\Database\Eloquent\Model;

class ConfQiweiModel extends Model
{
	protected $table = 'conf_qiwei';

    protected $primaryKey = null;

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-30 16:08:56
 * @Description: 
 */

namespace App\Models\Platform;

use App\Models\Common\SaasModel;

class ConfModel extends SaasModel
{
	protected $table = 'conf';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        self::updating(
            function ($model) {
                if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
                    $model->primaryKey = 'pid';
                }
            }
        );
    }

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-30 11:51:12
 * @Description: 
 */

namespace App\Models\Platform;

use App\Models\Common\SaasModel;

class ConfMpModel extends SaasModel
{
	protected $table = 'conf_mp';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

}

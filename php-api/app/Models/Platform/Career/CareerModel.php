<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-12 08:53:05
 * @Description: 
 */

namespace App\Models\Platform\Career;

use App\Models\Common\SaasModel;

class CareerModel extends SaasModel
{
	protected $table = 'career';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        self::updating(
            function ($model) {
                if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
                    $model->primaryKey = 'pid';
                }
            }
        );
    }

}

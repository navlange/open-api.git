<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-28 00:40:29
 * @Description: 
 */

namespace App\Models\Platform\Alipay;

use App\Models\Common\SaasModel;

class VersionModel extends SaasModel
{
	protected $table = 'alipay_version';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

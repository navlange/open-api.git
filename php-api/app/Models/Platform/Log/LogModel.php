<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-26 11:30:44
 * @Description: 
 */

namespace App\Models\Platform\Log;

use App\Models\Common\SaasModel;

class LogModel extends SaasModel
{
	protected $table = 'log';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        self::updating(
            function ($model) {
                if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
                    $model->primaryKey = 'pid';
                }
            }
        );
    }

}

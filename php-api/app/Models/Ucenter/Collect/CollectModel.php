<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-19 00:18:32
 * @Description: 
 */

namespace App\Models\Ucenter\Collect;

use App\Models\Common\SaasModel;

class CollectModel extends SaasModel
{
	protected $table = 'collect';

	protected $primaryKey = 'id';
    //

    public $timestamps = false;

    public function store()
    {
        return $this->hasOne('App\Models\Store\StoreModel', 'id', 'store_id');
    }

}
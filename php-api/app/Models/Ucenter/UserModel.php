<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-22 13:09:20
 * @Description: 
 */

namespace App\Models\Ucenter;

use App\Models\Common\SaasModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Lumen\Auth\Authorizable;

use Tymon\JWTAuth\Contracts\JWTSubject;

class UserModel extends SaasModel implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, HasFactory;

	protected $table = 'user';

	protected $primaryKey = 'uid';
    //

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

	// 获取用户标识（即：用户的id，对应于生成的token的payload中的sub字段）
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
 
    // payload中附加的自定义数据
    public function getJWTCustomClaims()
    {
        return [
            'data'=>$this->data
        ];
    }
	
}

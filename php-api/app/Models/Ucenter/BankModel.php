<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-03-20 21:14:23
 * @Description: 
 */

namespace App\Models\Ucenter;

use App\Models\Common\SaasModel;

class BankModel extends SaasModel
{
	protected $table = 'bank';

	protected $primaryKey = 'id';
    //

    public $timestamps = false;

}
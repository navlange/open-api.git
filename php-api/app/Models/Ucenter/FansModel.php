<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-22 13:09:30
 * @Description: 
 */

namespace App\Models\Ucenter;

use App\Models\Common\SaasModel;

class FansModel extends SaasModel
{
	protected $table = 'fans';

	protected $primaryKey = 'fid';
    //

    public $timestamps = false;

}
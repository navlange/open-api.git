<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-29 16:21:42
 * @Description: 
 */
namespace App\Models\Delivery;

use Illuminate\Database\Eloquent\Model;

class CompanyModel extends Model {
    protected $table = 'delivery_company';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
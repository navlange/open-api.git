<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-30 23:10:45
 * @Description: 
 */
namespace App\Models\Material;

use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model {
    protected $table = 'image';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
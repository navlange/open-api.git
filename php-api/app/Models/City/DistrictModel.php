<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 11:04:36
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:11:04
 * @Description: 
 */
namespace App\Models\City;

use App\Models\Common\SaasModel;

class DistrictModel extends SaasModel {
    protected $table = 'district';

	protected $primaryKey = 'id';
    //

    public $timestamps = false;
    
    protected static function boot()
    {
        parent::boot();

        self::updating(
            function ($model) {
                if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
                    $model->primaryKey = 'pid';
                }
            }
        );
    }

}
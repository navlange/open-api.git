<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 11:04:36
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:10:54
 * @Description: 
 */
namespace App\Models\City;

use Illuminate\Database\Eloquent\Model;

class AreaModel extends Model {
    protected $table = 'area';

	protected $primaryKey = 'id';
    //

    public $timestamps = false;
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 11:04:36
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:11:15
 * @Description: 
 */
namespace App\Models\City;

use Illuminate\Database\Eloquent\Model;

class ProvModel extends Model {
    protected $table = 'prov';

	protected $primaryKey = 'id';
    //

    public $timestamps = false;
}
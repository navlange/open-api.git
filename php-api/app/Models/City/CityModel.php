<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 11:04:36
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-20 01:10:59
 * @Description: 
 */
namespace App\Models\City;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model {
    protected $table = 'city';

	protected $primaryKey = 'id';
    //

    public $timestamps = false;
}
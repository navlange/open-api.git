<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-19 18:46:32
 * @Description: 
 */
namespace App\Models\Message;

use Illuminate\Database\Eloquent\Model;

class MessageModel extends Model {
    protected $table = 'message';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
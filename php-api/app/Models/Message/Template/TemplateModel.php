<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2022-08-25 17:21:00
 * @Description: 
 */
namespace App\Models\Message\Template;

use Illuminate\Database\Eloquent\Model;

class TemplateModel extends Model {
    protected $table = 'message_template';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
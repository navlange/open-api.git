<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-11-14 08:34:45
 * @Description: 
 */

namespace App\Models\Pay\Cash;

use Illuminate\Database\Eloquent\Model;

class CashModel extends Model
{
	protected $table = 'cash';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

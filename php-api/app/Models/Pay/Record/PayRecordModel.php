<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-09-28 11:29:03
 * @Description: 
 */

namespace App\Models\Pay\Record;

use Illuminate\Database\Eloquent\Model;

class PayRecordModel extends Model
{
	protected $table = 'pay';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

    public function user() {
        return $this->belongsTo('App\Models\Ucenter\UserModel','uid','uid');
    }
}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-14 17:30:21
 * @Description: 
 */

namespace App\Models\Pay\Account;

use Illuminate\Database\Eloquent\Model;

class AccountModel extends Model
{
	protected $table = 'pay_account';

    protected $primaryKey = 'uid';

    //

    public $timestamps = false;

}

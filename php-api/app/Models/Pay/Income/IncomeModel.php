<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-07 16:18:33
 * @Description: 
 */

namespace App\Models\Pay\Income;

use Illuminate\Database\Eloquent\Model;

class IncomeModel extends Model
{
	protected $table = 'income_record';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-24 17:01:05
 * @Description: 
 */

namespace App\Models\Pay\Log;

use Illuminate\Database\Eloquent\Model;

class PayLogModel extends Model
{
	protected $table = 'pay_log';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

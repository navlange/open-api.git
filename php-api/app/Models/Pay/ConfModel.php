<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-21 22:27:05
 * @Description: 
 */

namespace App\Models\Pay;

use Illuminate\Database\Eloquent\Model;

class ConfModel extends Model
{
	protected $table = 'pay_conf';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

}

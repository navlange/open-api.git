<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-29 16:02:41
 * @Description: 
 */

namespace App\Models\Pay\Card;

use Illuminate\Database\Eloquent\Model;

class PayCardModel extends Model
{
	protected $table = 'pay_card';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-12 17:32:34
 * @Description: 
 */

namespace App\Models\Pay\Exchange;

use Illuminate\Database\Eloquent\Model;

class CodeModel extends Model
{
	protected $table = 'exchange_code';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

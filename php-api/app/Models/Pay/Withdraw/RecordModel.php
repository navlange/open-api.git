<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-17 08:31:16
 * @Description: 
 */

namespace App\Models\Pay\Withdraw;

use Illuminate\Database\Eloquent\Model;

class RecordModel extends Model
{
	protected $table = 'withdraw_record';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

	public function user()
    {
        return $this->hasOne('App\Models\Ucenter\UserModel', 'uid', 'uid');
    }

    public function bank()
    {
        return $this->hasOne('App\Models\Pay\Account\AccountModel', 'uid', 'uid');
    }
}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-05-27 15:30:26
 * @Description: 
 */

namespace App\Models\Pay\Recharge;

use Illuminate\Database\Eloquent\Model;

class PackageModel extends Model
{
	protected $table = 'package';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

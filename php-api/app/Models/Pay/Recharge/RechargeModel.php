<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-09 08:27:08
 * @Description: 
 */

namespace App\Models\Pay\Recharge;

use Illuminate\Database\Eloquent\Model;

class RechargeModel extends Model
{
	protected $table = 'recharge';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

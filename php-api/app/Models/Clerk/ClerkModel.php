<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-28 11:46:23
 * @Description: 
 */
namespace App\Models\Clerk;

use Illuminate\Database\Eloquent\Model;

class ClerkModel extends Model {
    protected $table = 'clerk';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
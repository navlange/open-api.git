<?php
/*
 * @Author: lokei
 * @Date: 2024-01-01 18:34:02
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-31 12:09:35
 * @Description: 
 */
namespace App\Models\Rbac\Conf;

use Illuminate\Database\Eloquent\Model;

class MiniappModel extends Model {
    protected $table = 'rbac_conf_miniapp';
    protected $primaryKey = null;
    
    public $timestamps = false;

    public $incrementing = false;

}
<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-24 19:53:23
 * @Description: 
 */

namespace App\Models\Rbac\Permission;

use Illuminate\Database\Eloquent\Model;

class ClerkModel extends Model
{
	protected $table = 'permission_clerk';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

}

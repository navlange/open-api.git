<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-24 19:53:17
 * @Description: 
 */

namespace App\Models\Rbac\Permission;

use Illuminate\Database\Eloquent\Model;

class RoleModel extends Model
{
	protected $table = 'role';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

    public $incrementing = false;

}

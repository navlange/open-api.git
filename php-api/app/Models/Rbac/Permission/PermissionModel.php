<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-24 19:47:54
 * @Description: 
 */

namespace App\Models\Rbac\Permission;

use Illuminate\Database\Eloquent\Model;

class PermissionModel extends Model
{
	protected $table = 'permission';

    protected $primaryKey = 'uid';

    //

    public $timestamps = false;

    public $incrementing = false;

}

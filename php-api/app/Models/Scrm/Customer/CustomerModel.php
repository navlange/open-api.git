<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2023-02-08 00:02:13
 * @Description: 
 */
namespace App\Models\Scrm\Customer;

use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model {
    protected $table = 'customer';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
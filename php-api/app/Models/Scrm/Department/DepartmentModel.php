<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2022-08-21 09:10:52
 * @Description: 
 */
namespace App\Models\Scrm\Department;

use Illuminate\Database\Eloquent\Model;

class DepartmentModel extends Model {
    protected $table = 'department';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
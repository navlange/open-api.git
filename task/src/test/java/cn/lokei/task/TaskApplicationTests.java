/*
 * @Author: lokei
 * @Date: 2022-08-25 10:51:02
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-16 08:37:16
 * @Description: 
 */
package cn.lokei.task;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class TaskApplicationTests {

	@Autowired
	RedisTemplate<String, Object> redisTemplate;

	@Test
	void contextLoads() {
		// JSONObject message = new JSONObject();
		// message.put("openid", "omOR006jDhebN2x7sX7Dhnq2qevI");
		// redisTemplate.opsForList().leftPush("message:queue", JSON.toJSONString(message));
	}

}

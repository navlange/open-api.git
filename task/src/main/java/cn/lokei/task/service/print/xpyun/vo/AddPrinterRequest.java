/*
 * @Author: lokei
 * @Date: 2022-10-16 08:09:57
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-16 08:22:09
 * @Description: 
 */
package cn.lokei.task.service.print.xpyun.vo;

/**
 * 添加打印机请求参数
 *
 * @author RabyGao
 * @date Aug 7, 2019
 */
public class AddPrinterRequest extends RestRequest {

    public cn.lokei.task.service.print.xpyun.vo.AddPrinterRequestItem[] getItems() {
        return items;
    }

    public void setItems(cn.lokei.task.service.print.xpyun.vo.AddPrinterRequestItem[] items) {
        this.items = items;
    }

    /**
     * 请求项集合
     */
    private AddPrinterRequestItem[] items;

}

/*
 * @Author: lokei
 * @Date: 2022-10-16 08:09:57
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-16 08:21:36
 * @Description: 
 */
package cn.lokei.task.service.print.xpyun.vo;

/**
 * 查询订单状态请求参数
 *
 * @author RabyGao
 * @date Aug 7, 2019
 */
public class QueryOrderStateRequest extends RestRequest {

    /**
     * 订单编号
     */
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}

/*
 * @Author: lokei
 * @Date: 2022-10-16 08:09:57
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-16 08:20:46
 * @Description: 
 */
package cn.lokei.task.service.print.xpyun.vo;

/**
 * 添加打印机请求项
 *
 * @author RabyGao
 * @date Aug 7, 2019
 */
public class AddPrinterRequestItem {

    /**
     * 打印机编号
     */
    private String sn;
    /**
     * 打印机名称
     */
    private String name;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

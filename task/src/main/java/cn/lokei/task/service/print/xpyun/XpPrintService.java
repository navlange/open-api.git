package cn.lokei.task.service.print.xpyun;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import org.springframework.stereotype.Service;

import cn.lokei.task.entity.printer.Printer;
import cn.lokei.task.service.print.xpyun.util.Config;
import cn.lokei.task.service.print.xpyun.util.HttpClientUtil;
import cn.lokei.task.service.print.xpyun.vo.*;

import java.util.List;

/**
 * 云打印相关接口封装类
 * @author JavaLyl
 */
@Service("xpPrintService")
public class XpPrintService {

    private static String BASE_URL = "https://open.xpyun.net/api/openapi";
    
    /**
     * 1.批量添加打印机
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<PrinterResult> addPrinters(AddPrinterRequest restRequest) {
        String url = BASE_URL + "/xprinter/addPrinters";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<PrinterResult> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<PrinterResult>>(){});
        return result;
    }

    /**
     * 2.设置打印机语音类型
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<Boolean> setPrinterVoiceType(SetVoiceTypeRequest restRequest) {
        String url = BASE_URL + "/xprinter/setVoiceType";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<Boolean> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<Boolean>>(){});
        return result;
    }

    /**
     * 3.打印小票订单
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<String> print(PrintRequest restRequest) {
        String url = BASE_URL + "/xprinter/print";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<String> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<String>>(){});
        return result;
    }

    public void printFontAlign(Printer printer, String content) {
        /**
         * <BR>：换行符（同一行有闭合标签(如 </C>)则应放到闭合标签前面, 连续两个换行符<BR><BR>可以表示加一空行）
         *  <L></L>：左对齐
         *  <C></C>：居中对齐
         *  <R></R>：右对齐
         *  注意：同一行内容不能使用多种对齐方式，可通过补空格方式自定义对齐样式。
         *       58mm的机器，一行打印16个汉字，32个字母
         *       80mm的机器，一行打印24个汉字，48个字母
         *
         *  <N></N>：字体正常大小
         *  <HB></HB>：字体变高一倍
         *  <WB></WB>：字体变宽一倍
         *  <B></B>：字体放大一倍
         *  <CB></CB>：字体放大一倍居中
         *  <HB2></HB2>：字体变高二倍
         *  <WB2></WB2>：字体变宽二倍
         *  <B2></B2>：字体放大二倍
         *  <BOLD></BOLD>：字体加粗
         *  <IMG></IMG>：打印LOGO图片，需登录开放平台在【打印机管理=》设备管理】下通过设置LOGO功能进行上传。此处直接写入
         *             空标签, 如 <IMG></IMG> 即可, 具体可参考样例。
         *             图片宽度设置：可以通过 <IMG> 标签名称自定义，如 <IMG60> 表示宽度为60，相应的闭合标签 </IMG>
         *             不需要指定高度。<IMG> 标签不指定宽度默认为40，最小值为20，最大值为100
         *  <QR></QR>：二维码（标签内容是二维码值, 最大不能超过256个字符, 单个订单最多只能打印一个二维码）。
         *             二维码宽度设置：可以通过 <QR> 标签名称自定义，如 <QR180> 表示宽度为180，相应的闭合标签 </QR>
         *             不需要指定宽度。<QR> 标签不指定宽度默认为110，最小值为90，最大值为180
         *  <BARCODE></BARCODE>：条形码（标签内容是条形码值）
         */
        // StringBuilder printContent = new StringBuilder();
        // printContent.append("不加标签：").append("默认字体大小<BR>");

        // printContent.append("<BR>");
        // printContent.append("L标签：").append("<L>左对齐<BR></L>");
        // printContent.append("<BR>");
        // printContent.append("R标签：").append("<R>右对齐<BR></R>");
        // printContent.append("<BR>");
        // printContent.append("C标签：").append("<C>居中对齐<BR></C>");

        // printContent.append("<BR>");
        // printContent.append("N标签：").append("<N>字体正常大小<BR></N>");
        // printContent.append("<BR>");
        // printContent.append("HB标签：").append("<HB>字体变高一倍<BR></HB>");
        // printContent.append("<BR>");
        // printContent.append("WB标签：").append("<WB>字体变宽一倍<BR></WB>");
        // printContent.append("<BR>");
        // printContent.append("B标签：").append("<B>字体放大一倍<BR></B>");
        // printContent.append("<BR>");
        // printContent.append("HB2标签：").append("<HB2>字体变高二倍<BR></HB2>");
        // printContent.append("<BR>");
        // printContent.append("WB2标签：").append("<WB2>字体变宽二倍<BR></WB2>");
        // printContent.append("<BR>");
        // printContent.append("B2标签：").append("<B2>字体放大二倍<BR></B2>");
        // printContent.append("<BR>");
        // printContent.append("BOLD标签：").append("<BOLD>字体加粗<BR></BOLD>");

        // // 嵌套使用对齐和字体
        // printContent.append("<BR>");
        // printContent.append("<C>嵌套使用：").append("<BOLD>居中加粗</BOLD>").append("<BR></C>");

        // // 打印条形码和二维码
        // printContent.append("<BR>");
        // printContent.append("<C><BARCODE>9884822189</BARCODE></C>");
        // printContent.append("<C><QR>https://www.xpyun.net</QR></C>");


        PrintRequest request = new PrintRequest();
        request.setUser(printer.getUser());
        request.setUkey(printer.getUkey());
        Config.createRequestHeader(request);
        //*必填*：打印机编号
        request.setSn(printer.getSn());
        //*必填*：打印内容,不能超过12K
        request.setContent(content);
        //打印份数，默认为1
        request.setCopies(1);
        //声音播放模式，0 为取消订单模式，1 为静音模式，2 为来单播放模式，3为有用户申请退单了。默认为 2 来单播放模式
        request.setVoice(2);
        //打印模式：
        //值为 0 或不指定则会检查打印机是否在线，如果不在线 则不生成打印订单，直接返回设备不在线状态码；如果在线则生成打印订单，并返回打印订单号。
        //值为 1不检查打印机是否在线，直接生成打印订单，并返回打印订单号。如果打印机不在线，订单将缓存在打印队列中，打印机正常在线时会自动打印。
        request.setMode(0);
        this.print(request);
        // ObjectRestResponse<String> resp = this.print(request);
        //resp.data:正确返回订单编号
        // System.out.println(resp);
    }

    /**
     * 4.打印标签订单
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<String> printLabel(PrintRequest restRequest) {
        String url = BASE_URL + "/xprinter/printLabel";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<String> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<String>>(){});
        return result;
    }

    /**
     * 5.批量删除打印机
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<PrinterResult> delPrinters(DelPrinterRequest restRequest) {
        String url = BASE_URL + "/xprinter/delPrinters";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<PrinterResult> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<PrinterResult>>(){});
        return result;
    }

    /**
     * 6.修改打印机信息
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<Boolean> updPrinter(UpdPrinterRequest restRequest) {
        String url = BASE_URL + "/xprinter/updPrinter";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<Boolean> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<Boolean>>(){});
        return result;
    }

    /**
     * 7.清空待打印队列
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<Boolean> delPrinterQueue(PrinterRequest restRequest) {
        String url = BASE_URL + "/xprinter/delPrinterQueue";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<Boolean> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<Boolean>>(){});
        return result;
    }

    /**
     * 8.查询订单是否打印成功
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<Boolean> queryOrderState(QueryOrderStateRequest restRequest) {
        String url = BASE_URL + "/xprinter/queryOrderState";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<Boolean> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<Boolean>>(){});
        return result;
    }

    /**
     * 9.查询打印机某天的订单统计数
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<OrderStatisResult> queryOrderStatis(QueryOrderStatisRequest restRequest) {
        String url = BASE_URL + "/xprinter/queryOrderStatis";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<OrderStatisResult> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<OrderStatisResult>>(){});
        return result;
    }

    /**
     * 10.查询打印机状态
     *
     * 0、离线 1、在线正常 2、在线不正常
     * 备注：异常一般是无纸，离线的判断是打印机与服务器失去联系超过30秒
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<Integer> queryPrinterStatus(PrinterRequest restRequest) {
        String url = BASE_URL + "/xprinter/queryPrinterStatus";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<Integer> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<Integer>>(){});
        return result;
    }

    /**
     * 10.批量查询打印机状态
     *
     * 0、离线 1、在线正常 2、在线不正常
     * 备注：异常一般是无纸，离线的判断是打印机与服务器失去联系超过30秒
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<List<Integer>> queryPrintersStatus(PrintersRequest restRequest) {
        String url = BASE_URL + "/xprinter/queryPrintersStatus";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<List<Integer>> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<List<Integer>>>(){});
        return result;
    }

    /**
     * 11.云喇叭播放语音
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<String> playVoice(VoiceRequest restRequest) {
        String url = BASE_URL + "/xprinter/playVoice";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<String> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<String>>(){});
        return result;
    }

    /**
     * 12.POS指令
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<String> pos(PrintRequest restRequest) {
        String url = BASE_URL + "/xprinter/pos";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<String> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<String>>(){});
        return result;
    }
    /**
     * 13.钱箱控制
     * @param restRequest
     * @return
     */
    public ObjectRestResponse<String> controlBox(PrintRequest restRequest) {
        String url = BASE_URL + "/xprinter/controlBox";
        String jsonRequest = JSON.toJSONString(restRequest);
        String resp = HttpClientUtil.doPostJSON(url, jsonRequest);
        ObjectRestResponse<String> result = JSON.parseObject(resp, new TypeReference<ObjectRestResponse<String>>(){});
        return result;
    }

}

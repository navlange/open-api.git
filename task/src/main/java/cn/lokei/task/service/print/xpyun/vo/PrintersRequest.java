/*
 * @Author: lokei
 * @Date: 2022-10-16 08:09:57
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-16 08:21:22
 * @Description: 
 */
package cn.lokei.task.service.print.xpyun.vo;

import java.util.List;

/**
 * @author LylJavas
 * @create 2021/4/14 18:01
 * @description 批量打印机请求参数
 */
public class PrintersRequest extends RestRequest {

    /**
     * 打印机编号列表
     */
    private List<String> snlist;

    public List<String> getSnlist() {
        return snlist;
    }

    public void setSnlist(List<String> snlist) {
        this.snlist = snlist;
    }
}

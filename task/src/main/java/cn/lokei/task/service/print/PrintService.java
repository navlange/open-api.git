/*
 * @Author: lokei
 * @Date: 2022-10-16 08:09:57
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-16 08:36:23
 * @Description: 
 */
package cn.lokei.task.service.print;

import javax.annotation.Resource;

import cn.lokei.task.entity.printer.Printer;
import cn.lokei.task.service.print.xpyun.XpPrintService;

import org.springframework.stereotype.Component;

@Component("printService")
public class PrintService {

    @Resource
    FeiePrintService feiePrintService;

    @Resource
    XpPrintService xpPrintService;

    public void print(Printer printer, String content) {
        if (printer.getType().equals("feie")) {
            feiePrintService.print(printer, content);
        } else if (printer.getType().equals("xinyue")) {
            // StringBuilder printContent = new StringBuilder();
            // if (app_mode.equals(AppMode.ROOM.getIndex()) && printer.getReserve_order_on().equals("1")) {
                // printContent.append("<CB>" + jsonObject.get("store") + "</CB>");
                // printContent.append("<BR><BR>");
                // printContent.append("订单编号：").append(jsonObject.get("sn"));
                // printContent.append("<BR>");
                // printContent.append("下单时间：").append(jsonObject.get("createtime"));
                // printContent.append("<BR>");
                // printContent.append("预订金额：").append(jsonObject.get("money"));
                // printContent.append("<BR><BR>");
                // printContent.append("预订信息：").append("<B>" + jsonObject.get("room") + "</B>");
                // if(jsonObject.get("arrival_time") != null && !jsonObject.get("arrival_time").equals("")) {
                //     printContent.append("到店时间：").append("<B>" + jsonObject.get("arrival_time") + "</B>");
                // }
                // printContent.append("<BR><BR>");
                // printContent.append("营销员：").append("<B>" + (jsonObject.get("clerk") != null ? jsonObject.get("clerk") : "") + "</B>");
                // printContent.append("<BR><BR>");
                // printContent.append("客户姓名：").append("<B>" + jsonObject.get("consignee_name") + "</B>");
                // printContent.append("<BR><BR>");
                // printContent.append("客户电话：").append("<B>" + jsonObject.get("consignee_mobile") + "</B>");
                // printContent.append("<BR>");
                // printContent.append("预订日期：").append(jsonObject.get("day"));
                // printContent.append("<BR>");
                // printContent.append("<C><QR>" + jsonObject.get("url") + "</QR></C>");
                xpPrintService.printFontAlign(printer, content);
            // } else if (app_mode.equals(AppMode.SHOP.getIndex()) && printer.getGoods_order_on().equals("1")) {
            //     printContent.append("<CB>" + jsonObject.get("store") + "</CB>");
            //     printContent.append("<BR><BR>");
            //     printContent.append("订单编号：").append(jsonObject.get("sn"));
            //     printContent.append("<BR>");
            //     printContent.append("下单时间：").append(jsonObject.get("createtime"));
            //     printContent.append("<BR>");
            //     printContent.append("订单金额：").append(jsonObject.get("money"));
            //     printContent.append("<BR><BR>");
            //     printContent.append("预订号码：").append("<B>" + jsonObject.get("consignee_info") + "</B>");
            //     printContent.append("<BR><BR>");
            //     printContent.append("商品：");
            //     JSONObject goods_info = JSONObject.parseObject(jsonObject.get("goods_info").toString());
            //     Iterator iter = goods_info.entrySet().iterator();
            //     int count = 0;
            //     while (iter.hasNext()) {
            //         if(count > 0) {
            //             printContent.append("      ");
            //         }
            //         Map.Entry entry = (Map.Entry) iter.next();
            //         JSONObject g_info = JSONObject.parseObject(entry.getValue().toString());
            //         printContent.append("<B>" + g_info.get("name") + " *" + g_info.get("amount") + "</B>");
            //         printContent.append("<BR><BR>");
            //         count += 1;
            //     }
            //     printContent.append("<BR>");
            //     // printContent.append("<BR><BR>");
            //     // printContent.append("客户姓名：").append("<B>"+jsonObject.get("consignee_name")+"</B>");
            //     // printContent.append("<BR><BR>");
            //     // printContent.append("客户电话：").append("<B>"+jsonObject.get("consignee_mobile")+"</B>");
            //     // printContent.append("<BR>");
            //     // printContent.append("预订日期：").append(jsonObject.get("day"));
            //     xpPrintService.printFontAlign(printer, printContent.toString());
            // }
        }
    }
}

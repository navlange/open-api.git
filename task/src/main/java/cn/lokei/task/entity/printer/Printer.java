/*
 * @Descripttiis_: 
 * @versiis_: 
 * @Author: 奇辰科技
 * @Date: 2021-10-13 01:51:47
 * @LastEditors: lokei
 * @LastEditTime: 2023-02-23 11:07:11
 */
package cn.lokei.task.entity.printer;

import lombok.Data;

@Data
public class Printer {
	private Integer id;
	private Integer pid;
	private Integer store_id;
	private String type;
	private String user;
	private String ukey;
	private String sn;
	private String is_on;
	private String reserve_order_on;
	private String goods_order_on;
	private Integer print_number;

	public Printer() {

	}

	public Printer(String type, String user, String ukey, String sn) {
		this.type = type;
		this.user = user;
		this.ukey = ukey;
		this.sn = sn;
	}
}

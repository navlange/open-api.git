/*
 * @Author: lokei
 * @Date: 2022-08-27 12:33:11
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-16 08:03:38
 * @Description: 
 */
package cn.lokei.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskApplication.class, args);
	}

}

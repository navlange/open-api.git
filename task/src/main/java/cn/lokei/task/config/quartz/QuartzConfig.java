/*
 * @Author: lokei
 * @Date: 2022-10-27 09:15:33
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-12 20:26:37
 * @Description: 
 */
package cn.lokei.task.config.quartz;

// import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

// import cn.lokei.task.config.iot.GatewayConfig;

@Configuration
// @AutoConfigureAfter(GatewayConfig.class)
public class QuartzConfig {

    @Bean(name = "quartzScheduler")
    public SchedulerFactoryBean schedulerFactory() {

        SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();

        // 用于quartz集群,QuartzScheduler 启动时更新己存在的Job
        factoryBean.setOverwriteExistingJobs(true);

        // 延时启动，应用启动1秒后
        factoryBean.setStartupDelay(1);

        return factoryBean;
    }
}

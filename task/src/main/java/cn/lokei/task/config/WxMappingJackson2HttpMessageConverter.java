/*
 * @Author: lokei
 * @Date: 2022-08-27 12:33:11
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-15 21:26:41
 * @Description: 
 */
package cn.lokei.task.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

public class WxMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {
    public WxMappingJackson2HttpMessageConverter(){
        List<MediaType> mediaTypes;
        mediaTypes = new ArrayList<>();
        //设置消息转换类的响应类型
        mediaTypes.add(MediaType.APPLICATION_JSON);
        setSupportedMediaTypes(mediaTypes);
    }
}

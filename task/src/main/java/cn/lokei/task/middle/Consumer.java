/*
 * @Author: lokei
 * @Date: 2022-08-25 10:54:43
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-15 21:28:14
 * @Description: 
 */
package cn.lokei.task.middle;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;


@Component
@EnableScheduling
public class Consumer {

    // @Autowired
    // private RedisTemplate<String, Object> redisTemplate;

    // @Autowired
    // ConfMpDao confMpDao;

    // @Autowired
    // TemplateDao templateDao;

    // @PostConstruct
    // public void brPop() {
    //     new Thread(() -> {
    //         List<Template> template_list = templateDao.queryAll();
    //         for (Template message_template : template_list) {
    //             if(message_template.getIdentity() != null && !message_template.getIdentity().equals("")) {
    //                 while (true) {
    //                     try {
    //                         String message = (String) redisTemplate.opsForList().rightPop(message_template.getIdentity(), 10,
    //                                 TimeUnit.SECONDS);
    //                         if (message != null) {
    //                             RestTemplate template = new RestTemplate();
    //                             String access_token = (String) redisTemplate.opsForValue().get("mp_access_token");
    //                             if (access_token == null) {
    //                                 ConfMp conf = confMpDao.query();
    //                                 if (conf != null) {
    //                                     try {
    //                                         String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type={grant_type}&appid={appid}&secret={secret}";
    //                                         Map<String, Object> requestMap = new HashMap<>();
    //                                         // JSONObject requestMap = new JSONObject();
    //                                         requestMap.put("grant_type", "client_credential");
    //                                         requestMap.put("appid", conf.getAppid());
    //                                         requestMap.put("secret", conf.getSecret());
    
    //                                         ResponseEntity<JSONObject> responseEntity = template
    //                                                 .getForEntity(url, JSONObject.class, requestMap);
    //                                         HttpStatus statusCode = responseEntity.getStatusCode(); // 获取响应码
    //                                         if (statusCode == HttpStatus.OK) {
    //                                             JSONObject body = responseEntity.getBody();
    //                                             if (body != null && body.get("access_token") != null) {
    //                                                 access_token = body.getString("access_token");
    //                                                 Integer expires_in = body.getInteger("expires_in") - 5 * 60;
    //                                                 redisTemplate.opsForValue().set("mp_access_token", access_token,
    //                                                         expires_in,
    //                                                         TimeUnit.SECONDS);
    //                                             }
    //                                         } else {
    //                                             log.error("saas 服务访问失败！");
    //                                             // throw new RuntimeException("saas 服务访问失败！");
    //                                         }
    //                                     } catch (Exception e) {
    //                                         log.error(e.toString());
    //                                         // throw new RuntimeException(e.toString());
    //                                     }
    //                                 }
    //                             }
    //                             if (access_token != null) {
    //                                 // Template template = templateDao.queryAll()
    //                                 String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token;
    //                                 Map<String, Object> requestMap = new HashMap<>();
    //                                 // JSONObject requestMap = new JSONObject();
    //                                 JSONObject message_json = JSON.parseObject(message);
    //                                 requestMap.put("touser", message_json.getString("openid"));
    //                                 requestMap.put("template_id", message_template.getMp_template_id());
    //                                 Map<String, Object> data = new HashMap<>();
    //                                 Map<String, Object> first = new HashMap<>();
    //                                 first.put("value", "first");
    //                                 first.put("color", "#173177");
    //                                 data.put("first", first);
    //                                 Map<String, Object> keyword1 = new HashMap<>();
    //                                 keyword1.put("value", "keyword1");
    //                                 keyword1.put("color", "#173177");
    //                                 data.put("keyword1", first);
    //                                 Map<String, Object> keyword2 = new HashMap<>();
    //                                 keyword2.put("value", "keyword2");
    //                                 keyword2.put("color", "#173177");
    //                                 data.put("keyword2", first);
    //                                 Map<String, Object> keyword3 = new HashMap<>();
    //                                 keyword3.put("value", "keyword3");
    //                                 keyword3.put("color", "#173177");
    //                                 data.put("keyword3", first);
    //                                 Map<String, Object> keyword4 = new HashMap<>();
    //                                 keyword4.put("value", "keyword4");
    //                                 keyword4.put("color", "#173177");
    //                                 data.put("keyword4", first);
    //                                 Map<String, Object> remark = new HashMap<>();
    //                                 remark.put("value", "remark");
    //                                 remark.put("color", "#173177");
    //                                 data.put("remark", remark);
    //                                 requestMap.put("data", data);
    
    //                                 template.postForEntity(url, requestMap, JSONObject.class);
    //                             }
    //                         }
    //                     } catch (Exception e) {
    //                         log.info(e.toString());
    //                     }
    //                 }
    //             }
    //         }
    //     }).start();
    // }
}

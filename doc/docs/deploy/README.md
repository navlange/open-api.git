# 环境部署

奇辰Open-API支持多语言、多框架共同实现一套统一云原生API。

## 环境需求

|  |要求 |
|  ----  | ----  |
| 操作系统 | Linux系统 |
| PHP | 8.0版本以上 |
| Web服务 | 推荐Nginx |

## 安装部署

### 1、nginx服务配置
```
root /path_to_root;  #网站根路径

location /api {
    try_files $uri $uri/ /open-api/php-api/public/index.php?$query_string;
}
```
'/open-api/php-api/public/index.php?$query_string'表示到后端api接口由网站根目录‘/path_to_root’下面的/open-api/php-api/public目录内index.php作为请求处理的入口文件。

### 2、部署后端api

* 下载源码

在网站根路径/path_to_root下执行：git clone https://gitee.com/navlange/open-api.git，下载open-api源码。

* 安装第三方库

进入源码目录下php-api目录，执行composer install命令安装第三方库。

* 初始化

拷贝一份php-api目录下的.env.example到php-api目录下面，取名为.env。修改.env数据库配置：

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=******   #设置数据库名称
DB_USERNAME=******   #设置数据库用户名
DB_PASSWORD=******   #设置数据库密码
```

* 导入数据库

建立数据库，数据库配置需要和.env配置文件一致。将源码install目录下origin.sql数据库导入。

### 3、部署前端

将install目录下admin.zip解压至网站根目录/path_to_root下的html/admin目录。

### 4、登入后台

通过"http://域名/html/admin"或"https://域名/html/admin"访问后台。
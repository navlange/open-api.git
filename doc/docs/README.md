# 系统简介

为软件服务提供一套开源框架，快速落地需求解决方案；设计实现规范化标准接口，避免工程技术实现重复劳动；采用主流技术路线接入各大平台，构建开放生态。努力达到采用奇辰Open-API进行软件服务的简单化目标。

## 业务流程

<img :src="$withBase('/img/business_flow.jpg')" alt="业务流程">


奇辰Open-API面向的业务参与者包括：
* 用户：使用应用功能的人；
* 应用：由服务商提供或者自研具备用户所需功能的应用，包括H5、小程序、APP等；
* 服务商：具备开发能力的服务商；
* 第三方平台：微信小程序、微信支付、支付宝等第三方平台。

其中，**服务商**是奇辰Open-API面向的主要使用者。

奇辰Open-API业务流程主要为服务商提供开源框架为各种场地提供应用软件服务。业务所需要的后端服务由Open-API提供，包括第三方服务也通过Open-API统一调用，避免服务商重复开发第三方接口；业务前端功能可以基于奇辰Open-API提供的通用基础前端框架实现。

## 代码结构
```
admin----------------------后台，基于vue-element-admin实现前后端分离
doc------------------------项目文档，基于vuepress实现
iot------------------------物联网
|-gateway------------------物联网网关，基于Java Springboot框架实现
|-coap---------------------物联网CoAP协议，基于Java Springboot框架实现
|-sim----------------------物联网模拟平台，基于Java Springboot框架实现
task-----------------------任务中心，支持队列的任务管理调度中心
mobile---------------------前端，采用uniapp实现多端支持
php-api--------------------后端业务，采用Lumen框架实现的PHP版本后端
```
整个代码包含多个独立子项目，php-api的代码是采用Lumen(php轻型MVC框架：[https://lumen.laravel-china.org/](https://lumen.laravel-china.org/)，[https://lumen.laravel.com/](https://lumen.laravel.com/))实现的前后端分离PHP后端业务；admin目录是基于vue-element-admin实现前后端分离的后台业务，mobile目录是采用uniapp实现的支持多端（H5、小程序、APP）前端业务；task目录是采用Springboot框架实现的支持队列的任务管理调度子项目；doc目录是基于vuepress实现的全系统文档；iot包含了物联网相关子项目，其中gateway是基于Java Springboot框架实现的物联网网关。

## 业务板块

### 1、RBAC权限管理
* 创始人
* 管理员
* 普通用户
* 权限管理

### 2、用户中心
* 用户管理
* 粉丝管理

### 3、CMS内容管理
* 表单
* 公告
* 文章
* 海报
* 二维码
* 投诉建议
* 评价体系

### 4、SCRM客户关系管理
* 部门管理
* 成员管理
* 分销系统

### 5、会员系统
* 会员管理
* 等级管理

### 6、公共平台
* 系统设置
* 标签分类

### 7、城市商家
* 商家门店管理
* 商家门店Dashboard
* 城市小区管理

### 8、支付系统

#### 微信支付
* 微信支付平台证书管理
* JSAPI支付
* 小程序支付
* 支付回调

#### 支付宝支付
* 支付宝支付

#### 储值系统
*储值卡

#### 收银系统


### 9、消息中心

* 微信模板消息
* 小程序订阅模板消息

### 10、物联网
* 物联网网关

### 11、行业应用
* 从业人员管理
* 调度中心

### 12、物流系统
* 快递公司
* 快递配送

### 13、社区论坛
* 帖子动态

## 技术路线

### 体系结构
<img :src="$withBase('/img/architecture.jpg')" alt="体系结构">

奇辰Open-API体系机构如图所示，自顶向下端层最靠近用户，提供PC、Web、微信公众号和小程序等多种接入方式；业务层和端层分别构成前后端分离架构的后端和前端，后端业务层提供RBAC权限管理、用户中心、SCRM客户关系管理、支付系统、消息中心、物联网业务和AI业务等；为了支撑业务层高效运行需要服务层实现队列服务、元数据服务、内容管理、配置服务和监控服务等；往下的数据库层提供数据持久化功能；为了更高效数据服务设计数据缓存、数据对账、数据同步和数据事务的数据层。

### 技术路线
框架采用的相关技术如下表所示：
|  模块 |  技术 |
|  ----  | ----  |
| 端层  | Vue：开发语言<br>vue-element-admin：后台框架<br>uniapp：前端多端开发框架  |
| 后端  | lumen：php微服务后端开发框架  |
| 消息中心  | Java Springboot：开发框架<br>Redis：消息队列  |
| 物联网网关  | MQTT：物联网网关协议<br>CoAP：物联网CoAP协议  |
| 数据缓存  | Redis：数据缓存  |
| 数据库  | Mysql：关系数据库  |

## 第三方平台

### 腾讯

<br>
<div style="display: flex">
    <div style="width: 150px;text-align: center">
        <div style="width: 150px;height: 100px;line-height: 100px">
            <img width="80" :src="$withBase('/img/weixinmp.png')" alt="foo">
        </div>
        <div>
            微信公众号
        </div>
    </div>
    <div style="width: 150px;text-align: center">
        <div style="width: 150px;height: 100px;line-height: 100px">
            <img width="80" :src="$withBase('/img/miniapp.png')" alt="foo">
        </div>
        <div>
            微信小程序
        </div>
    </div>
    <div style="width: 150px;text-align: center">
        <div style="width: 150px;height: 100px;line-height: 100px">
            <img width="100" :src="$withBase('/img/wxpay.png')" alt="foo">
        </div>
        <div>
            微信支付
        </div>
    </div>
    <div style="width: 150px;text-align: center">
        <div style="width: 150px;height: 100px;line-height: 100px">
            <img width="100" :src="$withBase('/img/qiwei.jpg')" alt="foo">
        </div>
        <div>
            企业微信
        </div>
    </div>
</div>
<div style="display: flex">
    <div style="width: 150px;text-align: center">
        <div style="width: 150px;height: 100px;line-height: 100px">
            <img width="80" :src="$withBase('/img/alipay.png')" alt="foo">
        </div>
        <div>
            支付宝
        </div>
    </div>
</div>



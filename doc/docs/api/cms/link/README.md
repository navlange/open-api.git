# 链接
链接管理接口。


## 获取所有链接
* 描述

获取所有链接。

* 接口

调用方式：HTTP、HTTPS

api: link/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 链接列表 |
| total  | int32  | 链接数量 |

## 添加链接
* 描述

添加链接。

* 接口

调用方式：HTTP、HTTPS

api: link/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 链接名称 |
| path  | string  | 链接路径 |
| query  | string  | 链接参数 |

## 获取链接信息
* 描述

获取链接信息。

* 接口

调用方式：HTTP、HTTPS

api: link/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 链接id |

## 获取小程序url链接
* 描述

获取小程序url链接。

* 接口

调用方式：HTTP、HTTPS

api: link/getTarget

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 链接id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| url_link  | array  | 打开小程序url链接 |

## 修改链接信息
* 描述

修改链接信息。

* 接口

调用方式：HTTP、HTTPS

api: link/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 链接id |
| name  | string  | 链接名称 |
| path  | string  | 链接路径 |
| query  | string  | 链接参数 |

## 删除链接
* 描述

删除链接。

* 接口

调用方式：HTTP、HTTPS

api: link/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 链接id |

# 评价体系
评价管理接口。

## 提交评价
* 描述

提交评价。

* 接口

调用方式：HTTP、HTTPS

api: comment/submit

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mobile  | string  | 评价联系方式 |
| name  | string  | 评价人称呼 |
| avatar  | string  | 评价人头像 |
| content  | string  | 评价内容 |
| image  | string  | 评价附件图片 |
| order_id  | int  | 订单ID |
| goods_id  | int  | 商品ID |


## 获取商品所有评价
* 描述

获取所有评价。

* 接口

调用方式：HTTP、HTTPS

api: comment/ofGoods

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| goods_id  | int  | 商品ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 评价列表 |
| total  | int32  | 评价数量 |

## 获取订单评价信息
* 描述

获取评价信息。

* 接口

调用方式：HTTP、HTTPS

api: comment/OfOrder

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_id  | int32  | 订单id |

## 获取发帖评价
* 描述

获取发帖评价。

* 接口

调用方式：HTTP、HTTPS

api: comment/OfTopic

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| topic_id  | int32  | 帖子id |

## 删除评价
* 描述

删除评价。

* 接口

调用方式：HTTP、HTTPS

api: comment/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 评价id |

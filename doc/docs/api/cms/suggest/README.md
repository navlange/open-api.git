# 投诉建议
投诉建议管理接口。


## 获取所有投诉建议
* 描述

获取所有投诉建议。

* 接口

调用方式：HTTP、HTTPS

api: suggest/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 投诉建议列表 |
| total  | int32  | 投诉建议数量 |

## 获取登入用户提交的投诉建议
* 描述

获取登入用户提交的投诉建议。

* 接口

调用方式：HTTP、HTTPS

api: suggest/ofMy

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 投诉建议列表 |
| total  | int32  | 投诉建议数量 |

## 提交投诉建议
* 描述

提交投诉建议。

* 接口

调用方式：HTTP、HTTPS

api: suggest/submit

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mobile  | string  | 投诉建议联系方式 |
| content  | string  | 投诉建议内容 |
| image  | string  | 投诉建议附件图片 |

## 获取投诉建议信息
* 描述

获取投诉建议信息。

* 接口

调用方式：HTTP、HTTPS

api: suggest/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 投诉建议id |

## 删除投诉建议
* 描述

删除投诉建议。

* 接口

调用方式：HTTP、HTTPS

api: suggest/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 投诉建议id |

## 投诉建议处理完成
* 描述

投诉建议处理完成。

* 接口

调用方式：HTTP、HTTPS

api: suggest/finish

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 投诉建议id |
| reply  | string  | 处理意见 |

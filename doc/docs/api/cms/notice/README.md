# 公告
公告管理接口。


## 获取所有公告
* 描述

获取所有公告。

* 接口

调用方式：HTTP、HTTPS

api: notice/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 必填 | 描述  |
|  ----  | ----  |  ----  |  ----  |
| status  | string  | 否  | 公告状态 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 公告列表 |
| total  | int32  | 公告数量 |

## 添加公告
* 描述

添加公告。

* 接口

调用方式：HTTP、HTTPS

api: notice/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 所属门店ID |
| title  | string  | 公告标题 |
| content  | string  | 公告内容 |
| status  | string  | 公告状态 |

## 获取公告信息
* 描述

获取公告信息。

* 接口

调用方式：HTTP、HTTPS

api: notice/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 公告id |

## 修改公告信息
* 描述

修改公告信息。

* 接口

调用方式：HTTP、HTTPS

api: notice/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 公告id |
| store_id  | int32  | 所属门店ID |
| title  | string  | 公告标题 |
| content  | string  | 公告内容 |
| status  | string  | 公告状态 |

## 删除公告
* 描述

删除公告。

* 接口

调用方式：HTTP、HTTPS

api: notice/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 公告id |

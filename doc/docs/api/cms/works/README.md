# 作品


## 获取作品列表
* 描述

获取作品列表。

* 接口

调用方式：HTTP、HTTPS

api: works/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 作品列表 |
| total  | int32  | 作品数量 |


## 获取作品
* 描述

获取作品信息。

* 接口

调用方式：HTTP、HTTPS

api: works/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 作品id |

## 获取登入用户作品列表
* 描述

获取登入用户作品列表。

* 接口

调用方式：HTTP、HTTPS

api: works/ofMy

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 作品列表 |
| total  | int32  | 作品数量 |

## 发布作品
* 描述

发布作品。

* 接口

调用方式：HTTP、HTTPS

api: works/add

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| content  | string  | 作品内容 |
| images  | string  | 作品图片 |

## 删除作品
* 描述

删除作品。

* 接口

调用方式：HTTP、HTTPS

api: works/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 作品id |

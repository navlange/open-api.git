# 二维码

## 创建小程序二维码
* 描述

创建小程序二维码。

* 接口

调用方式：HTTP、HTTPS

api: qrcode/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 二维码模式 |
| page  | string  | 打开小程序页面 |
| scene  | string  | 携带参数 |
| name  | string  | 二维码名称 |

## 查询并创建小程序二维码
* 描述

查询是否存在二维码，存在即返回；如果不存在则先创建小程序二维码再返回。

* 接口

调用方式：HTTP、HTTPS

api: qrcode/getOrCreate

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 二维码模式 |
| page  | string  | 打开小程序页面 |
| scene  | string  | 携带参数 |
| name  | string  | 二维码名称 |


## 获取二维码信息
* 描述

获取二维码信息。

* 接口

调用方式：HTTP、HTTPS

api: qrcode/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 二维码ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| qrcode  | string  | 二维码图片地址 |

## 获取二维码列表
* 描述

获取二维码列表。

* 接口

调用方式：HTTP、HTTPS

api: qrcode/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 二维码模式 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 二维码列表 |
| total  | int32  | 二维码数量 |

## 删除二维码

* 描述

删除二维码信息。

* 接口

调用方式：HTTP、HTTPS

api: qrcode/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 二维码ID |


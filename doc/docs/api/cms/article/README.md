# 文章
文章管理接口。


## 获取所有文章
* 描述

获取所有文章。

* 接口

调用方式：HTTP、HTTPS

api: article/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 文章列表 |
| total  | int32  | 文章数量 |

## 获取热门文章
* 描述

获取热门文章。

* 接口

调用方式：HTTP、HTTPS

api: article/hotList

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| type_id  | int32  | 文章分类 |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 文章列表 |
| total  | int32  | 文章数量 |

## 添加文章
* 描述

添加文章。

* 接口

调用方式：HTTP、HTTPS

api: article/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| title  | string  | 文章标题 |
| type_id  | int32  | 文章分类 |
| img_display  | string  | 展示图 |
| content  | string  | 文章内容 |
| status  | string  | 文章状态 |

## 获取文章信息
* 描述

获取文章信息。

* 接口

调用方式：HTTP、HTTPS

api: article/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 文章id |

## 修改文章信息
* 描述

修改文章信息。

* 接口

调用方式：HTTP、HTTPS

api: article/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 文章id |
| title  | string  | 文章标题 |
| type_id  | int32  | 文章分类 |
| img_display  | string  | 展示图 |
| content  | string  | 文章内容 |
| status  | string  | 文章状态 |

## 删除文章
* 描述

删除文章。

* 接口

调用方式：HTTP、HTTPS

api: article/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 文章id |

## 获取推荐文章
* 描述

获取推荐文章。

* 接口

调用方式：HTTP、HTTPS

api: article/recommend/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 推荐场景 |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| articles  | string  | 推荐文章id列表 |

## 设置推荐文章
* 描述

设置推荐文章。

* 接口

调用方式：HTTP、HTTPS

api: article/recommend/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 推荐场景 |
| articles  | string  | 推荐文章id列表 |

## 获取文章付费信息
* 描述

获取文章付费信息。

* 接口

调用方式：HTTP、HTTPS

api: article/charge/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| article_id  | int32  | 文章id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| charge_on  | string  | 文章存在付费信息 |
| price  | float  | 文章付费价格 |
| charge_name  | string  | 文章付费项目名称 |
| contacts_mobile  | string  | 文章付费查看联系方式 |

## 设置文章付费信息
* 描述

设置文章付费信息。

* 接口

调用方式：HTTP、HTTPS

api: article/charge/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| article_id  | int32  | 文章id |
| charge_on  | string  | 文章存在付费信息 |
| price  | float  | 文章付费价格 |
| charge_name  | string  | 文章付费项目名称 |
| contacts_mobile  | string  | 文章付费查看联系方式 |

## 设置文章排序
* 描述

设置商家文章排序。

* 接口

调用方式：HTTP、HTTPS

api: article/setOrder

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 文章ID |
| order_index  | int32  | 文章排序 |

## 增加文章访问次数
* 描述

增加文章访问次数。

* 接口

调用方式：HTTP、HTTPS

api: article/incPv

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 文章ID |

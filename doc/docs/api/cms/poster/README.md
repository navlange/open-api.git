# 海报
海报管理接口。


## 获取所有海报
* 描述

获取所有海报。

* 接口

调用方式：HTTP、HTTPS

api: poster/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 海报列表 |
| total  | int32  | 海报数量 |

## 添加海报
* 描述

添加海报。

* 接口

调用方式：HTTP、HTTPS

api: poster/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| title  | string  | 海报标题 |
| type  | string  | 海报类型 |
| store_id  | int32  | 门店ID，海报类型为门店海报时 |
| bg_img  | string  | 海报背景图 |

## 获取海报信息
* 描述

获取海报信息。

* 接口

调用方式：HTTP、HTTPS

api: poster/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 海报id |

## 获取类型海报信息
* 描述

获取类型海报信息。

* 接口

调用方式：HTTP、HTTPS

api: poster/ofType

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| type  | string  | 海报类型 |
| store_id  | int32  | 海报为门店类型是提供 |

## 修改海报信息
* 描述

修改海报信息。

* 接口

调用方式：HTTP、HTTPS

api: poster/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 海报id |
| title  | string  | 海报标题 |
| type  | string  | 海报类型 |
| store_id  | int32  | 门店ID，海报类型为门店海报时 |
| bg_img  | string  | 海报背景图 |

## 删除海报
* 描述

删除海报。

* 接口

调用方式：HTTP、HTTPS

api: poster/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 海报id |

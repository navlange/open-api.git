# 表单

## 表单记录


### 提交表单记录

* 描述

提交表单记录信息。

* 接口

调用方式：HTTP、HTTPS

api: form/submit

HTTP请求方法：POST

* 请求参数

提交表单内容由表单字段自定义功能设计。

### 获取表单记录信息
* 描述

获取表单记录信息。

* 接口

调用方式：HTTP、HTTPS

api: form/record/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 表单记录ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| content  | json  | 表单记录内容 |
| status  | status  | 表单记录状态 |

### 获取表单记录列表
* 描述

获取表单记录列表。

* 接口

调用方式：HTTP、HTTPS

api: form/record/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 表单记录列表 |
| total  | int32  | 表单记录数量 |

### 删除表单记录

* 描述

删除表单记录信息。

* 接口

调用方式：HTTP、HTTPS

api: form/record/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 表单记录ID |



## 表单字段

自定义表单字段

### 获取表单字段列表
* 描述

获取表单字段列表。

* 接口

调用方式：HTTP、HTTPS

api: form/item/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 表单字段列表 |
| total  | int32  | 表单字段数量 |


### 获取表单字段

* 描述

获取表单字段信息。

* 接口

调用方式：HTTP、HTTPS

api: form/item/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 表单字段ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 表单字段名称 |
| type  | string  | 表单字段类型 |
| is_on  | string  | 表单字段启用 |


### 创建表单字段

* 描述

创建表单字段。

* 接口

调用方式：HTTP、HTTPS

api: form/item/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 表单字段名称 |
| type  | string  | 表单字段类型 |
| is_on  | string  | 表单字段启用 |

### 更新表单字段

* 描述

更新表单字段信息。

* 接口

调用方式：HTTP、HTTPS

api: form/item/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 表单字段ID |
| name  | string  | 表单字段名称 |
| type  | string  | 表单字段类型 |
| is_on  | string  | 表单字段启用 |

### 删除表单字段

* 描述

删除表单字段信息。

* 接口

调用方式：HTTP、HTTPS

api: form/item/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 表单字段ID |

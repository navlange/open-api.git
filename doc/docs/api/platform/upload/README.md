# 上传

## 上传文件
* 描述

上传文件。

* 接口

调用方式：HTTP、HTTPS

api: upload/file

HTTP请求方法：POST

* 请求参数

后台代码/platform/components/FileUpload组件已封装，可以直接调用。


## 上传图片
* 描述

上传图片。

* 接口

调用方式：HTTP、HTTPS

api: upload/image

HTTP请求方法：POST

* 请求参数

后台代码/platform/components/ImageUpload组件已封通用图片上传，/platform/components/ImageThumbUpload组件已封装缩略图上传，可以直接调用。

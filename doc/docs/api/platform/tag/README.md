# 标签

## 获取标签列表
* 描述

获取标签列表。

* 接口

调用方式：HTTP、HTTPS

api: tag/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 标签列表 |
| total  | int32  | 标签数量 |

## 获取场景标签列表
* 描述

获取场景标签列表。

* 接口

调用方式：HTTP、HTTPS

api: tag/ofScene

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 标签使用场景 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 标签列表 |
| total  | int32  | 标签数量 |

## 获取首页推荐标签列表
* 描述

获取首页推荐标签列表。

* 接口

调用方式：HTTP、HTTPS

api: tag/ofIndexRecommend

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 标签列表 |


## 获取标签信息

* 描述

获取标签信息。

* 接口

调用方式：HTTP、HTTPS

api: tag/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 标签ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 标签使用场景 |
| name  | string  | 标签名称 |
| img_thumb  | string  | 标签缩略图 |


## 创建标签

* 描述

创建标签。

* 接口

调用方式：HTTP、HTTPS

api: tag/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 标签使用场景 |
| name  | string  | 标签名称 |
| img_thumb  | string  | 标签缩略图 |

## 更新标签

* 描述

更新标签信息。

* 接口

调用方式：HTTP、HTTPS

api: tag/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 标签ID |
| scene  | string  | 标签使用场景 |
| name  | string  | 标签名称 |
| img_thumb  | string  | 标签缩略图 |

## 删除标签

* 描述

删除标签。

* 接口

调用方式：HTTP、HTTPS

api: tag/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 标签ID |

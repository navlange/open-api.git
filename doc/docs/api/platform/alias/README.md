# 别名系统

## 获取平台别名设置
* 描述

获取平台别名设置。

* 接口

调用方式：HTTP、HTTPS

api: alias/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值


## 别名系统平台参数
* 描述

别名系统平台参数。

* 接口

调用方式：HTTP、HTTPS

api: alias/update

HTTP请求方法：POST

* 请求参数

# 品牌

## 获取品牌列表
* 描述

获取品牌列表。

* 接口

调用方式：HTTP、HTTPS

api: brand/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 品牌列表 |
| total  | int32  | 品牌数量 |

## 获取一级分类品牌列表
* 描述

获取一级分类品牌列表。

* 接口

调用方式：HTTP、HTTPS

api: brand/ofType0

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| type_0_id  | int32  | 一级分类ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 品牌列表 |
| total  | int32  | 品牌数量 |

## 获取品牌信息

* 描述

获取品牌信息。

* 接口

调用方式：HTTP、HTTPS

api: brand/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 品牌ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 品牌名称 |
| logo  | string  | 品牌logo |


## 创建品牌

* 描述

创建品牌。

* 接口

调用方式：HTTP、HTTPS

api: brand/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 品牌名称 |
| logo  | string  | 品牌logo |

## 更新品牌

* 描述

更新品牌信息。

* 接口

调用方式：HTTP、HTTPS

api: brand/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 品牌ID |
| name  | string  | 品牌名称 |
| logo  | string  | 品牌logo |

## 删除品牌

* 描述

删除品牌。

* 接口

调用方式：HTTP、HTTPS

api: brand/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 品牌ID |

## 设置品牌排序
* 描述

设置商家品牌排序。

* 接口

调用方式：HTTP、HTTPS

api: brand/setOrder

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 品牌ID |
| order_index  | int32  | 品牌排序 |

# 分类

## 获取分类列表
* 描述

获取分类列表。

* 接口

调用方式：HTTP、HTTPS

api: type/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 分类列表 |
| total  | int32  | 分类数量 |

## 获取模式分类列表
* 描述

获取模式分类列表。

* 接口

调用方式：HTTP、HTTPS

api: type/ofMode

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| app_mode  | string  | 分类模式 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 分类列表 |
| total  | int32  | 分类数量 |

## 获取场景分类列表
* 描述

获取场景分类列表。

* 接口

调用方式：HTTP、HTTPS

api: type/ofScene

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 分类场景 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 分类列表 |
| total  | int32  | 分类数量 |

## 获取首页推荐文章分类列表
* 描述

获取首页推荐文章分类列表。

* 接口

调用方式：HTTP、HTTPS

api: type/ofIndexArticleRecommend

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 分类列表 |



## 获取分类信息

* 描述

获取分类信息。

* 接口

调用方式：HTTP、HTTPS

api: type/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分类ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| app_mode  | string  | 分类使用场景 |
| name  | string  | 分类名称 |
| img_thumb  | string  | 分类缩略图 |


## 创建分类

* 描述

创建分类。

* 接口

调用方式：HTTP、HTTPS

api: type/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| app_mode  | string  | 分类使用场景 |
| name  | string  | 分类名称 |
| img_thumb  | string  | 分类缩略图 |

## 更新分类

* 描述

更新分类信息。

* 接口

调用方式：HTTP、HTTPS

api: type/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分类ID |
| app_mode  | string  | 分类使用场景 |
| name  | string  | 分类名称 |
| img_thumb  | string  | 分类缩略图 |

## 删除分类

* 描述

删除分类。

* 接口

调用方式：HTTP、HTTPS

api: type/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分类ID |

## 设置分类排序
* 描述

设置商家分类排序。

* 接口

调用方式：HTTP、HTTPS

api: type/setOrder

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分类ID |
| order_index  | int32  | 分类排序 |

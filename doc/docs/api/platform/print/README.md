# 打印

## 打印机

### 获取门店打印机列表
* 描述

获取门店打印机列表。

* 接口

调用方式：HTTP、HTTPS

api: printer/ofStore

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 打印机列表 |
| total  | int32  | 打印机数量 |


### 获取打印机信息

* 描述

获取打印机信息。

* 接口

调用方式：HTTP、HTTPS

api: printer/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 打印机ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 打印机所属门店 |
| type  | string  | 打印机类型 |
| user  | string  | 打印机接口用户名 |
| ukey  | string  | 打印机接口密钥 |
| sn  | string  | 打印机接口序列号 |
| is_on  | string  | 打印机启用 |


### 创建打印机

* 描述

创建打印机。

* 接口

调用方式：HTTP、HTTPS

api: printer/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 打印机所属门店 |
| type  | string  | 打印机类型 |
| user  | string  | 打印机接口用户名 |
| ukey  | string  | 打印机接口密钥 |
| sn  | string  | 打印机接口序列号 |
| is_on  | string  | 打印机启用 |

### 更新打印机

* 描述

更新打印机信息。

* 接口

调用方式：HTTP、HTTPS

api: printer/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 打印机ID |
| type  | string  | 打印机类型 |
| user  | string  | 打印机接口用户名 |
| ukey  | string  | 打印机接口密钥 |
| sn  | string  | 打印机接口序列号 |
| is_on  | string  | 打印机启用 |

### 删除打印机

* 描述

删除打印机。

* 接口

调用方式：HTTP、HTTPS

api: printer/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 打印机ID |


## 打印订单

### 打印选座票务

* 描述

打印选座票务。

* 接口

调用方式：HTTP、HTTPS

api: print/order/seat

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_id  | int32  | 订单ID |

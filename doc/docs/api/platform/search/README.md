# 搜索

## 添加搜索记录
* 描述

添加搜索记录。

* 接口

调用方式：HTTP、HTTPS

api: platform/search/add

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| value  | string  | 搜索内容 |

* 请求成功返回值


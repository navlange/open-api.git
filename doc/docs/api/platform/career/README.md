# 职业

## 获取职业列表
* 描述

获取职业列表。

* 接口

调用方式：HTTP、HTTPS

api: career/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 职业列表 |
| total  | int32  | 职业数量 |


## 获取职业信息

* 描述

获取职业信息。

* 接口

调用方式：HTTP、HTTPS

api: career/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 职业ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 职业名称 |
| img_thumb  | string  | 职业缩略图 |


## 创建职业

* 描述

创建职业。

* 接口

调用方式：HTTP、HTTPS

api: career/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 职业名称 |
| img_thumb  | string  | 职业缩略图 |

## 更新职业

* 描述

更新职业信息。

* 接口

调用方式：HTTP、HTTPS

api: career/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 职业ID |
| name  | string  | 职业名称 |
| img_thumb  | string  | 职业缩略图 |

## 删除职业

* 描述

删除职业。

* 接口

调用方式：HTTP、HTTPS

api: career/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 职业ID |

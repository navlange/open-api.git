# 协议

## 获取相关协议
* 描述

获取相关协议。

* 接口

调用方式：HTTP、HTTPS

api: platform/agreement/get

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| user_agreement  | string  | 用户服务协议 |
| privacy_agreement  | string  | 隐私政策 |


## 设置相关协议
* 描述

设置相关协议。

* 接口

调用方式：HTTP、HTTPS

api: platform/agreement/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| user_agreement  | string  | 用户服务协议 |
| privacy_agreement  | string  | 隐私政策 |

## 获取从业者服务协议
* 描述

获取从业者服务协议。

* 接口

调用方式：HTTP、HTTPS

api: platform/agreement/master/get

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| master_agreement  | string  | 从业者服务协议 |

## 获取用户服务协议
* 描述

获取用户服务协议。

* 接口

调用方式：HTTP、HTTPS

api: platform/agreement/user/get

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| user_agreement  | string  | 用户服务协议 |

## 获取隐私政策
* 描述

获取隐私政策。

* 接口

调用方式：HTTP、HTTPS

api: platform/agreement/privacy/get

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| privacy_agreement  | string  | 隐私政策 |

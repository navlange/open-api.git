# 职务

## 获取职务列表
* 描述

获取职务列表。

* 接口

调用方式：HTTP、HTTPS

api: position/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 职务列表 |
| total  | int32  | 职务数量 |

## 获取职务信息

* 描述

获取职务信息。

* 接口

调用方式：HTTP、HTTPS

api: position/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 职务ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| color  | string  | 职务色调 |
| name  | string  | 职务名称 |
| img_thumb  | string  | 职务缩略图 |


## 创建职务

* 描述

创建职务。

* 接口

调用方式：HTTP、HTTPS

api: position/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| color  | string  | 职务色调 |
| name  | string  | 职务名称 |
| img_thumb  | string  | 职务缩略图 |

## 更新职务

* 描述

更新职务信息。

* 接口

调用方式：HTTP、HTTPS

api: position/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 职务ID |
| color  | string  | 职务色调 |
| name  | string  | 职务名称 |
| img_thumb  | string  | 职务缩略图 |

## 删除职务

* 描述

删除职务。

* 接口

调用方式：HTTP、HTTPS

api: position/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 职务ID |

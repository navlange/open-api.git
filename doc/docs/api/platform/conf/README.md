# 设置

## 获取平台参数
* 描述

获取平台参数。

* 接口

调用方式：HTTP、HTTPS

api: conf/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 平台名称 |
| logo  | string  | 平台logo |


## 设置平台参数
* 描述

设置平台参数。

* 接口

调用方式：HTTP、HTTPS

api: conf/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 平台名称 |
| logo  | string  | 平台logo |

## 获取平台简介
* 描述

获取平台简介。

* 接口

调用方式：HTTP、HTTPS

api: conf/about/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| about  | string  | 平台简介 |

## 获取微信公众号参数
* 描述

获取微信公众号参数。

* 接口

调用方式：HTTP、HTTPS

api: conf/mp/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 公众号名称 |
| appid  | string  | 公众号appid |
| secret  | string  | 公众号secret |
| qrcode  | string  | 公众号二维码 |


## 设置微信公众号参数
* 描述

设置微信公众号参数。

* 接口

调用方式：HTTP、HTTPS

api: conf/mp/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 公众号名称 |
| appid  | string  | 公众号appid |
| secret  | string  | 公众号secret |
| qrcode  | string  | 公众号二维码 |

## 获取微信小程序参数
* 描述

获取微信小程序参数。

* 接口

调用方式：HTTP、HTTPS

api: conf/miniapp/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| miniapp_name  | string  | 小程序名称 |
| miniapp_appid  | string  | 小程序appid |
| miniapp_secret  | string  | 小程序secret |


## 设置微信小程序参数
* 描述

设置微信小程序参数。

* 接口

调用方式：HTTP、HTTPS

api: conf/miniapp/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| miniapp_name  | string  | 小程序名称 |
| miniapp_appid  | string  | 小程序appid |
| miniapp_secret  | string  | 小程序secret |

## 获取支付宝应用参数
* 描述

获取支付宝应用参数。

* 接口

调用方式：HTTP、HTTPS

api: conf/alipay/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| merchantPrivateKey  | string  | 应用私钥 |
| alipayCertPath  | string  | 支付宝公钥证书 |
| alipayRootCertPath  | string  | 支付宝根证书文件 |
| merchantCertPath  | string  | 应用公钥证书文件 |
| secret_key  | string  | 接口内容加密密钥 |


## 设置支付宝应用参数
* 描述

设置支付宝应用参数。

* 接口

调用方式：HTTP、HTTPS

api: conf/alipay/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| merchantPrivateKey  | string  | 应用私钥 |
| alipayCertPath  | string  | 支付宝公钥证书 |
| alipayRootCertPath  | string  | 支付宝根证书文件 |
| merchantCertPath  | string  | 应用公钥证书文件 |
| secret_key  | string  | 接口内容加密密钥 |

## 获取管理员设置

* 描述

获取管理员设置。

* 接口

调用方式：HTTP、HTTPS

api: conf/admin/get

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| admin_delete_on  | string  | 开启管理员删除 |

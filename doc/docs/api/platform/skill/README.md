# 技能

## 获取技能列表
* 描述

获取技能列表。

* 接口

调用方式：HTTP、HTTPS

api: skill/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 技能列表 |
| total  | int32  | 技能数量 |

## 获取技能信息

* 描述

获取技能信息。

* 接口

调用方式：HTTP、HTTPS

api: skill/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 技能ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| color  | string  | 技能色调 |
| name  | string  | 技能名称 |
| img_thumb  | string  | 技能缩略图 |


## 创建技能

* 描述

创建技能。

* 接口

调用方式：HTTP、HTTPS

api: skill/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| color  | string  | 技能色调 |
| name  | string  | 技能名称 |
| img_thumb  | string  | 技能缩略图 |

## 更新技能

* 描述

更新技能信息。

* 接口

调用方式：HTTP、HTTPS

api: skill/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 技能ID |
| color  | string  | 技能色调 |
| name  | string  | 技能名称 |
| img_thumb  | string  | 技能缩略图 |

## 删除技能

* 描述

删除技能。

* 接口

调用方式：HTTP、HTTPS

api: skill/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 技能ID |

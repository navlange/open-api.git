# 统计

## 获取平台统计信息
* 描述

获取平台统计信息。

* 接口

调用方式：HTTP、HTTPS

api: statistic/platform/get

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| pv  | int32  | 访问量 |
| store_amount  | int32  | 门店数量 |

## 增加浏览次数
* 描述

增加浏览次数。

* 接口

调用方式：HTTP、HTTPS

api: statistic/platform/incPv

HTTP请求方法：POST

* 请求参数


# 礼品管理
礼品管理接口。

## 礼品基础信息
### 获取所有礼品
* 描述

获取所有礼品。

* 接口

调用方式：HTTP、HTTPS

api: gift/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 礼品列表 |
| total  | int32  | 礼品数量 |

### 添加礼品
* 描述

添加礼品。

* 接口

调用方式：HTTP、HTTPS

api: gift/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 礼品名称 |
| img_thumb  | string  | 礼品缩略图 |
| img_display  | string  | 礼品展示图 |
| intro  | string  | 礼品介绍 |
| detail  | string  | 礼品详情 |
| amount  | int32  | 礼品库存 |
| sales  | int32  | 礼品销量 |

### 获取礼品信息
* 描述

获取礼品信息。

* 接口

调用方式：HTTP、HTTPS

api: gift/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 礼品id |

### 修改礼品信息
* 描述

修改礼品信息。

* 接口

调用方式：HTTP、HTTPS

api: gift/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 礼品id |
| name  | string  | 礼品名称 |
| img_thumb  | string  | 礼品缩略图 |
| img_display  | string  | 礼品展示图 |
| intro  | string  | 礼品介绍 |
| detail  | string  | 礼品详情 |
| amount  | int32  | 礼品库存 |
| sales  | int32  | 礼品销量 |

### 删除礼品
* 描述

删除礼品。

* 接口

调用方式：HTTP、HTTPS

api: gift/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 礼品id |

## 礼品记录
### 获取我的礼品
* 描述

获取我的礼品。

* 接口

调用方式：HTTP、HTTPS

api: gift/record/ofMy

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 礼品列表 |
| total  | int32  | 礼品数量 |

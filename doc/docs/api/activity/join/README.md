# 活动参与
活动参与接口。

## 活动参与
### 参与活动
* 描述

参与活动。

* 接口

调用方式：HTTP、HTTPS

api: activity/record/join

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 参与活动用户ID |
| activity_id  | int32  | 活动ID |
| pay_card_id  | int32  | 参与活动储值卡ID，当活动类型为赠送储值卡余额时提供 |

## 活动参与记录
### 获取参加活动记录
* 描述

获取参加活动记录。

* 接口

调用方式：HTTP、HTTPS

api: activity/record/ofMe

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| activity_id  | int32  | 活动id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 活动参加记录列表 |
| total  | int32  | 活动参加记录数量 |

## 活动参与管理
### 获取所有参加活动记录
* 描述

获取所有参加活动记录。

* 接口

调用方式：HTTP、HTTPS

api: activity/record/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 活动参加记录列表 |
| total  | int32  | 活动参加记录数量 |

### 删除活动参与记录
* 描述

删除活动参与记录。

* 接口

调用方式：HTTP、HTTPS

api: activity/record/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 活动参与记录id |

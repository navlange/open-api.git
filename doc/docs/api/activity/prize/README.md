# 奖品
奖品接口。

## 奖品管理
### 获取所有奖品
* 描述

获取所有奖品。

* 接口

调用方式：HTTP、HTTPS

api: prize/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 奖品列表 |
| total  | int32  | 奖品数量 |

### 添加奖品
* 描述

添加奖品。

* 接口

调用方式：HTTP、HTTPS

api: prize/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| type  | string  | 奖品类型 |
| name  | string  | 奖品名称 |
| ratio  | int32  | 获取概率 |

### 获取奖品信息
* 描述

获取奖品信息。

* 接口

调用方式：HTTP、HTTPS

api: prize/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 奖品id |

### 修改奖品信息
* 描述

修改奖品信息。

* 接口

调用方式：HTTP、HTTPS

api: prize/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 奖品id |
| type  | string  | 奖品类型 |
| name  | string  | 奖品名称 |
| ratio  | int32  | 获取概率 |

### 删除奖品
* 描述

删除奖品。

* 接口

调用方式：HTTP、HTTPS

api: prize/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 奖品id |

## 抽奖
### 抽奖
* 描述

抽奖。

* 接口

调用方式：HTTP、HTTPS

api: prize/draw

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| activity_id  | int32  | 抽奖活动id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| prize  | object  | 奖品 |

## 抽奖活动管理
抽奖活动管理接口。

### 获取所有抽奖活动
* 描述

获取所有抽奖活动。

* 接口

调用方式：HTTP、HTTPS

api: prize/activity/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 抽奖活动列表 |
| total  | int32  | 抽奖活动数量 |

### 添加抽奖活动
* 描述

添加抽奖活动。

* 接口

调用方式：HTTP、HTTPS

api: prize/activity/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| type  | string  | 抽奖活动类型 |
| name  | string  | 抽奖活动名称 |
| img_display  | string  | 抽奖活动展示图 |
| intro  | string  | 抽奖活动介绍 |
| detail  | string  | 抽奖活动详情 |
| status  | string  | 抽奖活动状态 |

### 获取抽奖活动信息
* 描述

获取抽奖活动信息。

* 接口

调用方式：HTTP、HTTPS

api: prize/activity/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 抽奖活动id |

### 修改抽奖活动信息
* 描述

修改抽奖活动信息。

* 接口

调用方式：HTTP、HTTPS

api: prize/activity/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 抽奖活动id |
| type  | string  | 抽奖活动类型 |
| name  | string  | 抽奖活动名称 |
| img_display  | string  | 抽奖活动展示图 |
| intro  | string  | 抽奖活动介绍 |
| detail  | string  | 抽奖活动详情 |
| status  | string  | 抽奖活动状态 |

### 删除抽奖活动
* 描述

删除抽奖活动。

* 接口

调用方式：HTTP、HTTPS

api: prize/activity/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 抽奖活动id |


## 奖品记录管理

### 获取所有奖品记录
* 描述

获取所有奖品记录。

* 接口

调用方式：HTTP、HTTPS

api: prize/record/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 奖品记录列表 |
| total  | int32  | 奖品记录数量 |

### 获取我的奖品记录
* 描述

获取我的奖品记录。

* 接口

调用方式：HTTP、HTTPS

api: prize/record/ofMy

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 奖品记录列表 |
| total  | int32  | 奖品记录数量 |

### 获取奖品记录信息
* 描述

获取奖品记录信息。

* 接口

调用方式：HTTP、HTTPS

api: prize/record/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 奖品记录id |

### 删除奖品记录
* 描述

删除奖品记录。

* 接口

调用方式：HTTP、HTTPS

api: prize/record/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 奖品记录id |

### 奖品记录核销
* 描述

奖品记录核销。

* 接口

调用方式：HTTP、HTTPS

api: prize/record/verify

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 奖品记录id |

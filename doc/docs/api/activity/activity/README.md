# 活动管理
活动管理接口。

## 活动基础信息
### 获取所有活动
* 描述

获取所有活动。

* 接口

调用方式：HTTP、HTTPS

api: activity/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 活动列表 |
| total  | int32  | 活动数量 |

### 添加活动
* 描述

添加活动。

* 接口

调用方式：HTTP、HTTPS

api: activity/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 活动名称 |
| img_display  | string  | 活动展示图 |
| intro  | string  | 活动介绍 |
| detail  | string  | 活动详情 |
| status  | string  | 活动状态 |

### 获取活动信息
* 描述

获取活动信息。

* 接口

调用方式：HTTP、HTTPS

api: activity/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 活动id |

### 修改活动信息
* 描述

修改活动信息。

* 接口

调用方式：HTTP、HTTPS

api: activity/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 活动id |
| name  | string  | 活动名称 |
| img_display  | string  | 活动展示图 |
| intro  | string  | 活动介绍 |
| detail  | string  | 活动详情 |
| status  | string  | 活动状态 |

### 删除活动
* 描述

删除活动。

* 接口

调用方式：HTTP、HTTPS

api: activity/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 活动id |

## 活动内容
### 设置活动内容
* 描述

设置活动内容。

* 接口

调用方式：HTTP、HTTPS

api: activity/content/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 活动ID |
| type  | string  | 活动类型 |
| pay_card_ids  | string  | 储值卡IDs，当活动类型为赠送储值卡余额时提供 |

# 发帖动态
发帖动态管理接口。


## 获取所有发帖动态
* 描述

获取所有发帖动态。

* 接口

调用方式：HTTP、HTTPS

api: topic/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 发帖动态列表 |
| total  | int32  | 发帖动态数量 |

## 添加发帖动态
* 描述

添加发帖动态。

* 接口

调用方式：HTTP、HTTPS

api: topic/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| content  | string  | 动态内容 |
| images  | string  | 图片 |

## 获取发帖动态信息
* 描述

获取发帖动态信息。

* 接口

调用方式：HTTP、HTTPS

api: topic/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 发帖动态id |

## 修改发帖动态信息
* 描述

修改发帖动态信息。

* 接口

调用方式：HTTP、HTTPS

api: topic/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 发帖动态id |
| content  | string  | 动态内容 |
| images  | string  | 图片 |

## 删除发帖动态
* 描述

删除发帖动态。

* 接口

调用方式：HTTP、HTTPS

api: topic/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 发帖动态id |

## 发布动态
* 描述

发布动态。

* 接口

调用方式：HTTP、HTTPS

api: topic/release

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| content  | string  | 动态内容 |
| images  | string  | 图片 |

## 评论
* 描述

评论。

* 接口

调用方式：HTTP、HTTPS

api: topic/comment/submit

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| topic_id  | string  | 动态ID |
| content  | string  | 评论内容 |

## 点赞喜欢
* 描述

点赞喜欢。

* 接口

调用方式：HTTP、HTTPS

api: topic/like/submit

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| topic_id  | string  | 动态ID |

## 点赞查询
* 描述

点赞查询。

* 接口

调用方式：HTTP、HTTPS

api: topic/like/ofTopics

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| ids  | array  | 动态ID数组 |

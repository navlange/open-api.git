# 会员等级

## 获取会员等级列表

* 描述

获取会员等级信息。

* 接口

调用方式：HTTP、HTTPS

api: member/level/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 会员等级列表 |
| total  | int32  | 会员等级数量 |

## 获取会员等级

* 描述

获取会员等级信息。

* 接口

调用方式：HTTP、HTTPS

api: member/level/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级ID |

## 添加会员等级

* 描述

添加会员等级信息。

* 接口

调用方式：HTTP、HTTPS

api: member/level/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 会员等级名称 |
| discount  | float  | 优惠折扣 |
| wxpay_discount_on  | bool  | 微信支付享受优惠 |
| balance_discount_on  | bool  | 余额支付享受优惠 |
| img_display  | string  | 展示图片 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |

## 修改会员等级

* 描述

修改会员等级信息。

* 接口

调用方式：HTTP、HTTPS

api: member/level/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级ID |
| name  | string  | 会员等级名称 |
| discount  | float  | 优惠折扣 |
| wxpay_discount_on  | bool  | 微信支付享受优惠 |
| balance_discount_on  | bool  | 余额支付享受优惠 |
| img_display  | string  | 展示图片 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |

## 删除会员等级

* 描述

删除会员等级信息。

* 接口

调用方式：HTTP、HTTPS

api: member/level/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级ID |

## 设置会员等级排序

* 描述

设置会员等级排序信息。

* 接口

调用方式：HTTP、HTTPS

api: member/level/setOrder

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级ID |
| order_index  | int32  | 排序 |

## 获取会员等级套餐列表

* 描述

获取会员等级套餐列表。

* 接口

调用方式：HTTP、HTTPS

api: member/price/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| level_id  | int32  | 会员等级ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 会员等级套餐列表 |
| total  | int32  | 会员等级套餐数量 |

## 获取会员等级套餐

* 描述

获取会员等级套餐信息。

* 接口

调用方式：HTTP、HTTPS

api: member/price/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级套餐ID |

## 添加会员等级套餐

* 描述

添加会员等级套餐信息。

* 接口

调用方式：HTTP、HTTPS

api: member/price/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 会员等级套餐名称 |
| price  | float  | 价格 |
| is_permanent  | bool  | 是否永久有效 |
| month  | int  | 有效期（月） |

## 修改会员等级套餐

* 描述

修改会员等级套餐信息。

* 接口

调用方式：HTTP、HTTPS

api: member/price/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级套餐ID |
| name  | string  | 会员等级套餐名称 |
| price  | float  | 价格 |
| is_permanent  | bool  | 是否永久有效 |
| month  | int  | 有效期（月） |

## 删除会员等级套餐

* 描述

删除会员等级套餐信息。

* 接口

调用方式：HTTP、HTTPS

api: member/price/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级套餐ID |


## 获取会员等级模式

* 描述

获取会员等级模式信息。

* 接口

调用方式：HTTP、HTTPS

api: member/level/mode/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级ID |

## 设置会员等级模式

* 描述

设置会员等级模式信息。

* 接口

调用方式：HTTP、HTTPS

api: member/level/mode/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员等级ID |
| register_mode  | string  | 会员等级注册模式 |
| need_audit  | string  | 注册需要审核 |

# 次卡

## 次卡管理

### 获取次卡列表

* 描述

获取次卡信息。

* 接口

调用方式：HTTP、HTTPS

api: level2/list

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 次卡列表 |
| total  | int32  | 次卡数量 |

### 获取推荐次卡列表

* 描述

获取推荐次卡列表。

* 接口

调用方式：HTTP、HTTPS

api: level2/ofRecommend

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 推进场景 |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 次卡列表 |
| total  | int32  | 次卡数量 |


### 获取次卡推荐

* 描述

获取次卡推荐。

* 接口

调用方式：HTTP、HTTPS

api: level2/recommend/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 推进场景 |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| level2_list  | string  | 次卡列表 |

### 设置次卡推荐

* 描述

设置次卡推荐。

* 接口

调用方式：HTTP、HTTPS

api: level2/recommend/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scene  | string  | 推进场景 |
| level2_list  | string  | 次卡列表 |


### 获取次卡

* 描述

获取次卡信息。

* 接口

调用方式：HTTP、HTTPS

api: level2/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 次卡ID |

### 添加次卡

* 描述

添加次卡信息。

* 接口

调用方式：HTTP、HTTPS

api: level2/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 次卡名称 |
| store_id  | int32  | 门店ID |
| count  | int32  | 使用次数 |
| intro  | string | 简介 |
| detail  | string  | 详情 |
| logo  | string  | logo |
| img_display  | string  | 展示图 |
| price  | string  | 价格 |
| origin_price  | string  | 原价 |
| total_amount  | int32  | 总数 |
| amount_per_person  | int32  | 每人限领张数 |
| verify_by_me_on  | string  | 自主核销 |
| expire  | int32  | 有效期 |
| expire_unit  | string  | 有效期单位 |

### 修改次卡

* 描述

修改次卡信息。

* 接口

调用方式：HTTP、HTTPS

api: level2/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 次卡ID |
| name  | string  | 次卡名称 |
| store_id  | int32  | 门店ID |
| count  | int32  | 使用次数 |
| intro  | string | 简介 |
| detail  | string  | 详情 |
| logo  | string  | logo |
| img_display  | string  | 展示图 |
| price  | string  | 价格 |
| origin_price  | string  | 原价 |
| total_amount  | int32  | 总数 |
| amount_per_person  | int32  | 每人限领张数 |
| verify_by_me_on  | string  | 自主核销 |
| expire  | int32  | 有效期 |
| expire_unit  | string  | 有效期单位 |

### 删除次卡

* 描述

删除次卡信息。

* 接口

调用方式：HTTP、HTTPS

api: level2/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 次卡ID |

### 次卡上线

* 描述

次卡上线。

* 接口

调用方式：HTTP、HTTPS

api: level2/online

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 次卡ID |

### 次卡下线

* 描述

次卡下线。

* 接口

调用方式：HTTP、HTTPS

api: level2/offline

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 次卡ID |

### 我的次卡

* 描述

我的次卡。

* 接口

调用方式：HTTP、HTTPS

api: member2/ofMy

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 次卡记录列表 |


## 次卡记录

### 获取次卡记录列表

* 描述

获取次卡记录列表。

* 接口

调用方式：HTTP、HTTPS

api: member2/list

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 次卡记录列表 |
| total  | int32  | 次卡记录数量 |

## 期间次卡

### 获取期间次卡列表

* 描述

获取期间次卡列表。

* 接口

调用方式：HTTP、HTTPS

api: member2/times/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| member2_id  | int32  | 次卡ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 次卡记录列表 |
| total  | int32  | 次卡记录数量 |

### 添加期间次卡次数

* 描述

添加期间次卡次数信息。

* 接口

调用方式：HTTP、HTTPS

api: member2/times/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| member2_id  | int32  | 次卡ID |
| start  | string | 开始时间 |
| finish  | string | 结束时间 |
| times  | int32  | 次数 |

### 修改期间次卡次数

* 描述

修改期间次卡次数信息。

* 接口

调用方式：HTTP、HTTPS

api: member2/times/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 次数ID |
| start  | string | 开始时间 |
| finish  | string | 结束时间 |
| times  | int32  | 次数 |

### 删除期间次卡次数

* 描述

删除期间次卡次数信息。

* 接口

调用方式：HTTP、HTTPS

api: member2/times/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 次数ID |

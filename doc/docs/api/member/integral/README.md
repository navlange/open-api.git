# 积分

## 获取积分规则

* 描述

获取积分规则。

* 接口

调用方式：HTTP、HTTPS

api: integral/rule/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| money_for_integral  | int32  | 获得1积分所需消费金额 |
| expire  | int32  | 积分有效期 |

## 设置积分规则

* 描述

设置积分规则。

* 接口

调用方式：HTTP、HTTPS

api: integral/rule/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| money_for_integral  | int32  | 获得1积分所需消费金额 |
| expire  | int32  | 积分有效期 |

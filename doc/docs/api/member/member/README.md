# 会员

## 添加会员

* 描述

添加会员。

* 接口

调用方式：HTTP、HTTPS

api: member/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| level_id  | int32  | 会员等级ID |
| mobile  | string  | 用户手机号 |

## 会员续期

* 描述

会员续期。

* 接口

调用方式：HTTP、HTTPS

api: member/updateExpire

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员ID |
| expire  | string  | 新的有效期 |

## 我的会员信息

* 描述

获取当前登入用户会员信息。

* 接口

调用方式：HTTP、HTTPS

api: member/ofMe

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 会员列表 |
| total  | int32  | 会员数量 |

## 获取会员列表

* 描述

获取会员列表。

* 接口

调用方式：HTTP、HTTPS

api: member/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 会员列表 |
| total  | int32  | 会员数量 |

## 删除会员

* 描述

删除会员信息。

* 接口

调用方式：HTTP、HTTPS

api: member/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 会员ID |

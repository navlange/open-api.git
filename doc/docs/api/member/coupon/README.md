# 优惠券

## 获取优惠券

* 描述

获取优惠券信息。

* 接口

调用方式：HTTP、HTTPS

api: coupon/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 优惠券ID |

## 添加优惠券

* 描述

添加优惠券信息。

* 接口

调用方式：HTTP、HTTPS

api: coupon/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 优惠券所属门店 |
| name  | string  | 优惠券名称 |
| money  | float  | 优惠金额 |
| min_consumption  | float  | 最低消费金额 |
| amount  | int32  | 总数量 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |
| expire  | int32  | 有效期 |
| expire_unit  | string  | 有效期单位 |

## 修改优惠券

* 描述

修改优惠券信息。

* 接口

调用方式：HTTP、HTTPS

api: coupon/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 优惠券ID |
| store_id  | int32  | 优惠券所属门店 |
| name  | string  | 优惠券名称 |
| money  | float  | 优惠金额 |
| min_consumption  | float  | 最低消费金额 |
| amount  | int32  | 总数量 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |
| expire  | int32  | 有效期 |
| expire_unit  | string  | 有效期单位 |

## 删除优惠券

* 描述

删除优惠券信息。

* 接口

调用方式：HTTP、HTTPS

api: coupon/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 优惠券ID |

## 获取我的优惠券

* 描述

获取我的优惠券。

* 接口

调用方式：HTTP、HTTPS

api: coupon/record/ofMy

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 我的优惠券 |
# 接口规范

以HTTP、HTTPS调用方式实现的接口返回如下统一参数。

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| errorCode  | int32  | 错误码 |
| errorMsg  |  string  | 错误信息 |
| data  |  array  | 返回值 |

如果请求成功，errorCode的值为200，data里面返回成功的返回值；否则根据errorCode错误码和errorMsg可以获取请求错误信息。


* 常用请求失败信息

|  errorCode |  errorMsg  |
|  ----  | ----  |  ----  |
| 999  | 失败  |
| 2001  | 用户未登入  |

# 订单操作

## 获取订单操作日志

* 描述

获取订单操作日志。

* 接口

调用方式：HTTP、HTTPS

api: order/op/ofOrder

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_id  | int32  | 订单ID |

# 核销

## 门店核销记录

* 描述

门店核销记录。

* 接口

调用方式：HTTP、HTTPS

api: verify/ofStore

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 核销记录 |

## 删除核销记录

* 描述

删除核销记录。

* 接口

调用方式：HTTP、HTTPS

api: verify/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 核销ID |

## 次卡核销记录

* 描述

次卡核销记录。

* 接口

调用方式：HTTP、HTTPS

api: verify/level2/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 核销记录 |

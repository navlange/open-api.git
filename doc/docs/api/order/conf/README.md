# 设置

## 订单通用

### 获取订单打印设置

* 描述

获取订单打印设置。

* 接口

调用方式：HTTP、HTTPS

api: order/conf/print/get

HTTP请求方法：POST

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| print_number  | int32  | 打印份数 |

### 设置订单打印参数

* 描述

设置订单打印参数。

* 接口

调用方式：HTTP、HTTPS

api: order/conf/print/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| print_number  | int32  | 打印份数 |

### 获取订单退款设置

* 描述

获取订单退款设置。

* 接口

调用方式：HTTP、HTTPS

api: order/conf/refund/get

HTTP请求方法：POST

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| refund_mode  | string  | 退款模式 |
| refund_time  | int32  | 退款时间 |
| admin_refund_on  | string  | 后台主动退款 |

### 设置订单退款参数

* 描述

设置订单退款参数。

* 接口

调用方式：HTTP、HTTPS

api: order/conf/refund/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| refund_mode  | string  | 退款模式 |
| refund_time  | int32  | 退款时间 |
| admin_refund_on  | string  | 后台主动退款 |

## 会员订单

### 获取会员订单设置

* 描述

获取会员订单设置。

* 接口

调用方式：HTTP、HTTPS

api: order/conf/member/get

HTTP请求方法：POST

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name_on  | string  | 需要姓名 |

### 设置会员订单参数

* 描述

设置会员订单参数。

* 接口

调用方式：HTTP、HTTPS

api: order/conf/member/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name_on  | string  | 需要姓名 |

## 选座票务订单

### 获取选座票务订单设置

* 描述

获取选座票务订单设置。

* 接口

调用方式：HTTP、HTTPS

api: order/conf/seat/get

HTTP请求方法：POST

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| release_seat_print_on  | string  | 下单自动打票 |
| admin_seat_print_on  | string  | 后台打票 |

### 设置选座票务订单参数

* 描述

设置选座票务订单参数。

* 接口

调用方式：HTTP、HTTPS

api: order/conf/seat/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| release_seat_print_on  | string  | 下单自动打票 |
| admin_seat_print_on  | string  | 后台打票 |

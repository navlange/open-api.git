# 改签

## 订单改签

* 描述

订单改签。

* 接口

调用方式：HTTP、HTTPS

api: order/change/day

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 订单ID |
| day  | string  | 改签日期 |

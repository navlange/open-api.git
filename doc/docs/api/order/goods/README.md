# 订单商品

## 订单座位

* 描述

获取订单座位。

* 接口

调用方式：HTTP、HTTPS

api: order/seat/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| seat_id  | int32  | 座位ID |

## 订单全部座位

* 描述

获取订单全部座位。

* 接口

调用方式：HTTP、HTTPS

api: order/seat/ofOrder

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 订单ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 座位列表 |
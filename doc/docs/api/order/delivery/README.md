# 配送

## 获取商品配送点

* 描述

获取商品配送点。

* 接口

调用方式：HTTP、HTTPS

api: delivery/store/ofGoods

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 配送点列表 |

## 配送

* 描述

配送。

* 接口

调用方式：HTTP、HTTPS

api: delivery/submit

HTTP请求方法：POST

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_id  | int32  | 订单ID |
| delivery_mode  | string  | 配送模式 |
| delivery_company_id  | int32  | 快递公司 |
| delivery_sn  | string  | 快递单号 |
| note  | string  | 备注 |

## 快递归还

* 描述

快递归还。

* 接口

调用方式：HTTP、HTTPS

api: delivery/sendback

HTTP请求方法：POST

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_id  | int32  | 订单ID |
| delivery_company_name  | string  | 快递公司 |
| delivery_sn  | string  | 快递单号 |

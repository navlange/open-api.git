# 快递配送

## 获取订单快递配送列表
* 描述

获取订单快递配送列表。

* 接口

调用方式：HTTP、HTTPS

api: delivery/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 快递公司列表 |
| total  | int32  | 快递公司数量 |

## 添加快递配送
* 描述

添加快递配送。

* 接口

调用方式：HTTP、HTTPS

api: delivery/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_id  | int32  | 订单id |
| delivery_company_id  | int32  | 快递公司id |
| delivery_sn  | string  | 快递单号 |
| note  | string  | 快递备注 |

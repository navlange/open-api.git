# 快递公司

## 获取快递公司列表
* 描述

获取快递公司列表。

* 接口

调用方式：HTTP、HTTPS

api: delivery/company/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 快递公司列表 |
| total  | int32  | 快递公司数量 |

## 获取快递公司
* 描述

获取快递公司信息。

* 接口

调用方式：HTTP、HTTPS

api: delivery/company/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 快递公司id |

## 添加快递公司
* 描述

添加快递公司。

* 接口

调用方式：HTTP、HTTPS

api: delivery/company/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 公司名称 |

## 修改快递公司
* 描述

修改快递公司。

* 接口

调用方式：HTTP、HTTPS

api: delivery/company/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 快递公司id |
| name  | string  | 公司名称 |

## 删除快递公司
* 描述

删除快递公司。

* 接口

调用方式：HTTP、HTTPS

api: delivery/company/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 快递公司id |

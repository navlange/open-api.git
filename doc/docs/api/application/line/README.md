# 线路管理


## 获取线路列表
* 描述

获取线路列表。

* 接口

调用方式：HTTP、HTTPS

api: travel/line/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 线路列表 |
| total  | int32  | 线路数量 |

## 获取线路
* 描述

获取线路信息。

* 接口

调用方式：HTTP、HTTPS

api: travel/line/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 线路id |

## 添加线路
* 描述

添加线路。

* 接口

调用方式：HTTP、HTTPS

api: travel/line/register

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| departure_city  | string  | 出发城市 |
| terminal_city  | string  | 目的城市 |

## 修改线路
* 描述

修改线路。

* 接口

调用方式：HTTP、HTTPS

api: travel/line/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 线路id |
| departure_city  | string  | 出发城市 |
| terminal_city  | string  | 目的城市 |

## 删除线路
* 描述

删除线路。

* 接口

调用方式：HTTP、HTTPS

api: travel/line/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 线路id |

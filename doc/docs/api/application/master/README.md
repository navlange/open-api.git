# 从业人员管理


## 获取从业人员列表
* 描述

获取从业人员列表。

* 接口

调用方式：HTTP、HTTPS

api: master/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 从业人员列表 |
| total  | int32  | 从业人员数量 |

## 获取从业人员
* 描述

获取从业人员信息。

* 接口

调用方式：HTTP、HTTPS

api: master/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 从业人员id |

## 添加从业人员
* 描述

添加从业人员。

* 接口

调用方式：HTTP、HTTPS

api: master/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |
| nickname  | string  | 昵称 |
| position  | string  | 职务 |
| skills  | string  | 技能 |
| avatar  | string  | 头像 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |

## 修改从业人员
* 描述

修改从业人员。

* 接口

调用方式：HTTP、HTTPS

api: master/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 从业人员id |
| store_id  | int32  | 门店id |
| nickname  | string  | 昵称 |
| position  | string  | 职务 |
| skills  | string  | 技能 |
| avatar  | string  | 头像 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |

## 删除从业人员
* 描述

删除从业人员。

* 接口

调用方式：HTTP、HTTPS

api: master/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 从业人员id |

## 绑定从业人员用户
* 描述

绑定从业人员用户

* 接口

调用方式：HTTP、HTTPS

api: master/bindUser

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| master_id  | int32  | 从业人员id |
| mobile  | string  | 手机号 |

# 从业人员服务管理


## 获取从业人员服务列表
* 描述

获取从业人员服务列表。

* 接口

调用方式：HTTP、HTTPS

api: master/service/ofMaster

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| master_id  | int32  | 从业人员id |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 从业人员服务列表 |
| total  | int32  | 从业人员服务数量 |

## 添加从业人员服务
* 描述

添加从业人员服务。

* 接口

调用方式：HTTP、HTTPS

api: master/service/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| master_id  | int32  | 从业人员id |
| service_id  | int32  | 服务id |
| price  | float  | 价格 |

## 修改从业人员服务
* 描述

修改从业人员服务。

* 接口

调用方式：HTTP、HTTPS

api: master/service/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 从业人员服务id |
| price  | float  | 价格 |

## 删除从业人员服务
* 描述

删除从业人员服务。

* 接口

调用方式：HTTP、HTTPS

api: master/service/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 从业人员服务id |

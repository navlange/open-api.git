# 调度中心

## 获取调度列表
* 描述

获取调度列表。

* 接口

调用方式：HTTP、HTTPS

api: schedule/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 调度列表 |
| total  | int32  | 调度数量 |

## 获取商家门店某天调度列表
* 描述

获取商家门店某天调度列表。

* 接口

调用方式：HTTP、HTTPS

api: schedule/ofStoreDay

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 调度列表 |
| total  | int32  | 调度数量 |

## 获取演出调度列表
* 描述

获取演出调度列表。

* 接口

调用方式：HTTP、HTTPS

api: schedule/ofShow

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| show_id  | int32  | 演出id |
| day  | string  | 日期 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 调度列表 |
| total  | int32  | 调度数量 |

## 获取商家门店演出调度列表
* 描述

获取商家门店演出调度列表。

* 接口

调用方式：HTTP、HTTPS

api: schedule/ofStoreShow

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |
| show_id  | int32  | 演出id |
| day  | string  | 日期 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 调度列表 |
| total  | int32  | 调度数量 |

## 获取某天演出调度列表
* 描述

获取某天演出调度列表。

* 接口

调用方式：HTTP、HTTPS

api: schedule/ofShowDay

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| show_id  | int32  | 演出id |
| day  | string  | 日期 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 调度列表 |
| total  | int32  | 调度数量 |

## 获取商家门店某天演出调度列表
* 描述

获取商家门店某天演出调度列表。

* 接口

调用方式：HTTP、HTTPS

api: schedule/ofStoreShowDay

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |
| show_id  | int32  | 演出id |
| day  | string  | 日期 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 调度列表 |
| total  | int32  | 调度数量 |

## 获取调度
* 描述

获取调度信息。

* 接口

调用方式：HTTP、HTTPS

api: schedule/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 调度id |

## 添加调度
* 描述

添加调度。

* 接口

调用方式：HTTP、HTTPS

api: schedule/create

HTTP请求方法：POST

## 修改调度
* 描述

修改调度。

* 接口

调用方式：HTTP、HTTPS

api: schedule/update

HTTP请求方法：POST

## 删除调度
* 描述

删除调度。

* 接口

调用方式：HTTP、HTTPS

api: schedule/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 调度id |


## 我的调度状态
* 描述

获取我的调度状态。

* 接口

调用方式：HTTP、HTTPS

api: schedule/slot/statusOfMe

HTTP请求方法：GET


* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| day  | string  | 日期 |
| slot_id  | int32  | 时间段ID |

## 添加调度状态
* 描述

添加调度状态。

* 接口

调用方式：HTTP、HTTPS

api: schedule/slot/add

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| day  | string  | 日期 |
| slot_id  | int32  | 时间段ID |
| mode  | string  | 模式 |

## 删除调度状态
* 描述

删除调度状态。

* 接口

调用方式：HTTP、HTTPS

api: schedule/slot/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 调度状态id |

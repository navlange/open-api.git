# 车主管理


## 获取车主列表
* 描述

获取车主列表。

* 接口

调用方式：HTTP、HTTPS

api: master/driver/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 车主列表 |
| total  | int32  | 车主数量 |

## 获取车主
* 描述

获取车主信息。

* 接口

调用方式：HTTP、HTTPS

api: master/driver/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车主id |

## 添加车主
* 描述

添加车主。

* 接口

调用方式：HTTP、HTTPS

api: master/driver/register

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 姓名 |
| mobile  | string  | 联系电话 |
| id_number  | string  | 身份证号 |
| series  | string  | 品牌车系 |
| color  | string  | 车辆颜色 |
| car_number  | string  | 车牌号 |
| driving_licence  | string  | 驾驶证 |
| car_licence  | string  | 驾驶证 |

## 修改车主
* 描述

修改车主。

* 接口

调用方式：HTTP、HTTPS

api: master/driver/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车主id |
| name  | string  | 姓名 |
| mobile  | string  | 联系电话 |
| id_number  | string  | 身份证号 |
| series  | string  | 品牌车系 |
| color  | string  | 车辆颜色 |
| car_number  | string  | 车牌号 |
| driving_licence  | string  | 驾驶证 |
| car_licence  | string  | 驾驶证 |

## 删除车主
* 描述

删除车主。

* 接口

调用方式：HTTP、HTTPS

api: master/driver/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车主id |

## 车主审核通过
* 描述

车主审核通过。

* 接口

调用方式：HTTP、HTTPS

api: master/driver/accept

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车主id |

## 车主审核不通过
* 描述

车主审核通过。

* 接口

调用方式：HTTP、HTTPS

api: master/driver/reject

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车主id |

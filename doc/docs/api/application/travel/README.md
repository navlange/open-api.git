# 出行管理


## 获取出行列表
* 描述

获取出行列表。

* 接口

调用方式：HTTP、HTTPS

api: travel/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 出行列表 |
| total  | int32  | 出行数量 |

## 获取出行车主信息
* 描述

获取出行车主信息。

* 接口

调用方式：HTTP、HTTPS

api: travel/driverInfo

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 出行id |

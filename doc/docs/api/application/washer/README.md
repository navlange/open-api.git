# 车服务技师管理


## 获取车服务技师列表
* 描述

获取车服务技师列表。

* 接口

调用方式：HTTP、HTTPS

api: washer/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 车服务技师列表 |
| total  | int32  | 车服务技师数量 |

## 获取区域车服务技师列表
* 描述

获取区域车服务技师列表。

* 接口

调用方式：HTTP、HTTPS

api: washer/ofArea

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| area_id  | int32  | 区域id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 车服务技师列表 |
| total  | int32  | 车服务技师数量 |

## 获取车服务技师
* 描述

获取车服务技师信息。

* 接口

调用方式：HTTP、HTTPS

api: washer/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车服务技师id |

## 登入用户车服务技师
* 描述

获取登入用户车服务技师信息。

* 接口

调用方式：HTTP、HTTPS

api: washer/ofMe

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车服务技师id |

## 车服务技师注册
* 描述

车服务技师注册。

* 接口

调用方式：HTTP、HTTPS

api: washer/register

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nickname  | string  | 昵称 |
| position  | string  | 职务 |
| skills  | string  | 技能 |
| avatar  | string  | 头像 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |

## 添加车服务技师
* 描述

添加车服务技师。

* 接口

调用方式：HTTP、HTTPS

api: washer/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |
| nickname  | string  | 昵称 |
| position  | string  | 职务 |
| skills  | string  | 技能 |
| avatar  | string  | 头像 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |

## 修改车服务技师
* 描述

修改车服务技师。

* 接口

调用方式：HTTP、HTTPS

api: washer/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车服务技师id |
| store_id  | int32  | 门店id |
| nickname  | string  | 昵称 |
| position  | string  | 职务 |
| skills  | string  | 技能 |
| avatar  | string  | 头像 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |

## 删除车服务技师
* 描述

删除车服务技师。

* 接口

调用方式：HTTP、HTTPS

api: washer/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车服务技师id |

## 审核


### 车服务技师审核通过
* 描述

车服务技师审核通过。

* 接口

调用方式：HTTP、HTTPS

api: washer/audit/pass

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车服务技师id |

### 车服务技师审核拒绝
* 描述

车服务技师审核拒绝。

* 接口

调用方式：HTTP、HTTPS

api: washer/audit/reject

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 车服务技师id |

# 乘客管理


## 获取乘客
* 描述

获取乘客信息。

* 接口

调用方式：HTTP、HTTPS

api: passenger/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 乘客id |

## 获取乘客列表
* 描述

获取乘客列表。

* 接口

调用方式：HTTP、HTTPS

api: passenger/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 乘客列表 |
| total  | int32  | 乘客数量 |

## 获取登入用户的乘客列表
* 描述

获取登入用户的乘客列表。

* 接口

调用方式：HTTP、HTTPS

api: passenger/ofMy

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 乘客列表 |
| total  | int32  | 乘客数量 |

## 删除乘客
* 描述

删除乘客。

* 接口

调用方式：HTTP、HTTPS

api: passenger/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 乘客id |

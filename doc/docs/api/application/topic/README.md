# 帖子管理


## 获取帖子列表
* 描述

获取帖子列表。

* 接口

调用方式：HTTP、HTTPS

api: chuxing/topic/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 帖子列表 |
| total  | int32  | 帖子数量 |


## 获取指定模式帖子列表
* 描述

获取指定模式帖子列表。

* 接口

调用方式：HTTP、HTTPS

api: chuxing/topic/ofMode

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 帖子模式 |


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 帖子列表 |
| total  | int32  | 帖子数量 |

## 获取帖子
* 描述

获取帖子信息。

* 接口

调用方式：HTTP、HTTPS

api: chuxing/topic/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 帖子id |

## 发布帖子
* 描述

发布帖子。

* 接口

调用方式：HTTP、HTTPS

api: chuxing/topic/release

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 帖子模式 |
| type  | string  | 帖子类型 |
| departure_city  | string  | 出发城市 |
| terminal_city  | string  | 目的城市 |
| departure_time  | string  | 出发时间 |
| amount  | int32  | 车位数量 |
| note  | string  | 备注 |

## 删除帖子
* 描述

删除帖子。

* 接口

调用方式：HTTP、HTTPS

api: chuxing/topic/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 帖子id |

## 获取帖子车主信息
* 描述

获取发布帖子车主信息。

* 接口

调用方式：HTTP、HTTPS

api: chuxing/topic/driverInfo

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 帖子id |

# 租赁


## 获取租赁设置
* 描述

获取租赁设置。

* 接口

调用方式：HTTP、HTTPS

api: business/zl/conf/get

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| user_release_goods_on  | string  | 用户发布商品 |
| deposit_account_on  | string  | 独立押金账户 |
| fee_detail  | string  | 计费详情 |


## 设置租赁
* 描述

设置租赁。

* 接口

调用方式：HTTP、HTTPS

api: business/zl/conf/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| user_release_goods_on  | string  | 用户发布商品 |
| deposit_account_on  | string  | 独立押金账户 |
| fee_detail  | string  | 计费详情 |

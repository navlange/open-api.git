# 账户

## 我的账户信息
* 描述

我的账户信息。


* 接口

调用方式：HTTP、HTTPS

api: pay/account/ofMe

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| balance  | float  | 我的余额 |
| pay_card_amount  | int32  | 我的储值卡数量 |

## 银行账户

### 用户银行账户
* 描述

用户银行账户。


* 接口

调用方式：HTTP、HTTPS

api: pay/account/bank/ofUser

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | string  | 用户ID |

* 请求成功返回

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bank_id  | string  | 银行卡号 |
| bank_name  | string  | 开户行 |
| bank_username  | string  | 账户姓名 |

### 我的银行账户
* 描述

我的银行账户。


* 接口

调用方式：HTTP、HTTPS

api: pay/account/bank/ofMe

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bank_id  | string  | 银行卡号 |
| bank_name  | string  | 开户行 |
| bank_username  | string  | 账户姓名 |

### 修改我的银行账户
* 描述

修改我的银行账户。


* 接口

调用方式：HTTP、HTTPS

api: pay/account/bank/updateOfMe

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bank_id  | string  | 银行卡号 |
| bank_name  | string  | 开户行 |
| bank_username  | string  | 账户姓名 |

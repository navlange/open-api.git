# 收银


## 收银列表
* 描述

获取收银列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/cash/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 收银列表 |
| total  | int32  | 收银数量 |


## 添加收银
* 描述

添加收银。


* 接口

调用方式：HTTP、HTTPS

api: pay/cash/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| money  | float  | 金额 |
| order_id  | int32  | 收银订单id |
| mode  | string  | 收银模式：微信、支付宝、现金、POS机 |


## 获取收银信息
* 描述

获取收银信息。


* 接口

调用方式：HTTP、HTTPS

api: pay/cash/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 收银ID |


## 删除收银
* 描述

删除收银。


* 接口

调用方式：HTTP、HTTPS

api: pay/cash/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 收银ID |


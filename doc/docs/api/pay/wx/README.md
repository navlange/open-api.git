# 微信支付


## 获取支付参数
* 描述

获取微信支付参数。


* 接口

调用方式：HTTP、HTTPS

api: pay/wx/conf/get

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mchid  | string  | 商户号 |
| signkey  | string  | 商户密钥 |
| key_path  | string  | 商户API证书，.p12格式 |
| cert_file  | string  | 商户API证书，.pem格式 |
| key_file  | string  | 商户API证书私钥，.pem格式 |
| wxpay_platform_certs  | string  | 微信支付平台证书，Json格式，可能有多个证书 |


## 设置支付参数
* 描述

设置微信支付参数。


* 接口

调用方式：HTTP、HTTPS

api: pay/wx/conf/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mchid  | string  | 商户号 |
| signkey  | string  | 商户密钥 |
| key_path  | string  | 商户API证书，.p12格式 |
| cert_file  | string  | 商户API证书，.pem格式 |
| key_file  | string  | 商户API证书私钥，.pem格式 |


## 获取微信支付平台证书
* 描述

获取微信支付平台证书。


* 接口

调用方式：HTTP、HTTPS

api: pay/wx/conf/gainPlatformCerts

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| wxpay_platform_certs  | string  | 微信支付平台证书，Json格式，可能有多个证书 |


## JSAPI支付
* 描述

小程序、微信公众号发起微信支付请求。


* 接口

调用方式：HTTP、HTTPS

api: pay/wx/pay

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| openid  | string  | 用户微信openid |
| money  | float  | 支付金额 |
| mode  | string  | 支付模式 |
| order_id  | int32  | 订单ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| timeStamp  | string  | 时间戳 |
| nonceStr  | string  | 随机字符串 |
| package  | string  | 订单详情扩展字符串 |
| signType  | string  | 签名方式 |
| paySign  | string  | 签名 |


## 支付回调
* 描述

微信支付完成回调处理接口。


* 接口

调用方式：HTTP、HTTPS

api: pay/wx/notify

HTTP请求方法：POST

## 退款
* 描述

微信支付退款请求。


* 接口

调用方式：HTTP、HTTPS

api: pay/wx/refund

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| pay_tid  | string  | 支付pid |
| money  | float  | 支付金额 |


## 退款回调
* 描述

微信退款完成回调处理接口。


* 接口

调用方式：HTTP、HTTPS

api: pay/wx/refund/notify

HTTP请求方法：POST

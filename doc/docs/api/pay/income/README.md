# 收入管理


## 收入统计列表
* 描述

获取收入统计列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/income/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 收入统计列表 |
| total  | int32  | 收入统计数量 |

## 当前用户收入列表
* 描述

获取当前用户收入列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/income/record/ofMe

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 当前用户收入列表 |
| total  | int32  | 当前用户收入数量 |

## 当前用户分销收入列表
* 描述

获取当前用户分销收入列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/income/record/ofMyFx

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 当前用户收入列表 |
| total  | int32  | 当前用户收入数量 |

## 订单收入列表
* 描述

获取订单收入列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/income/record/ofOrder

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_id  | int32  | 订单ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 订单收入列表 |
| total  | int32  | 订单收入数量 |

## 删除收入记录
* 描述

删除收入记录。


* 接口

调用方式：HTTP、HTTPS

api: pay/income/delete

HTTP请求方法：POST

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 收入记录ID |

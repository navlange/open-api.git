# 充值


## 充值记录列表
* 描述

获取充值记录列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/recharge/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 充值记录列表 |
| total  | int32  | 充值记录数量 |

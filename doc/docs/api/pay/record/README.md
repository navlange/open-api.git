# 消费记录


## 消费记录统计列表
* 描述

获取消费记录统计列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/record/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 消费记录统计列表 |
| total  | int32  | 消费记录统计数量 |

## 当前用户消费记录列表
* 描述

获取当前用户消费记录列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/record/ofMe

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 当前用户消费记录列表 |
| total  | int32  | 当前用户消费记录数量 |

## 订单消费记录列表
* 描述

获取订单消费记录列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/record/ofOrder

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_id  | int32  | 订单ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 订单消费记录列表 |
| total  | int32  | 订单消费记录数量 |

## 删除消费记录记录
* 描述

删除消费记录记录。


* 接口

调用方式：HTTP、HTTPS

api: pay/record/delete

HTTP请求方法：POST

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 消费记录记录ID |

# 协议

## 获取支付协议
* 描述

获取支付协议。


* 接口

调用方式：HTTP、HTTPS

api: pay/agreement/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| pay_agreement  | text | 支付协议内容 |

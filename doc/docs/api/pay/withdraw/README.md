# 提现管理


## 提现申请
* 描述

提现申请。


* 接口

调用方式：HTTP、HTTPS

api: pay/withdraw/submit

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| type  | string  | 提现类型：门店、合伙人等 |
| store_id  | int32  | 门店ID，提现类型为门店时 |
| partner_id  | int32  | 合伙人ID，提现类型为合伙人时 |
| money  | float  | 提现金额 |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 门店提现列表 |
| total  | int32  | 门店提现数量 |

## 门店提现列表
* 描述

获取门店提现列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/withdraw/ofStore

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 门店提现列表 |
| total  | int32  | 门店提现数量 |

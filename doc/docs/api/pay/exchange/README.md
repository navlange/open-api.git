# 兑换中心


## 兑换码列表
* 描述

获取兑换码列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/exchange/code/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 兑换码列表 |
| total  | int32  | 兑换码数量 |

## 添加兑换码
* 描述

添加兑换码。


* 接口

调用方式：HTTP、HTTPS

api: pay/exchange/code/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| amount  | int32  | 兑换码数量 |
| level_id  | int32  | 次卡ID |


## 删除兑换码
* 描述

删除兑换码。


* 接口

调用方式：HTTP、HTTPS

api: pay/exchange/code/delete

HTTP请求方法：POST

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 兑换码ID |

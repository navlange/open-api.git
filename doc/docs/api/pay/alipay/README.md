# 支付宝支付

## 支付宝APP支付
* 描述

支付宝小程序发起支付宝支付请求。


* 接口

调用方式：HTTP、HTTPS

api: pay/alipay/app/pay

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| openid  | string  | 用户微信openid |
| money  | float  | 支付金额 |
| mode  | string  | 支付模式 |
| order_id  | int32  | 订单ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| trade_no  | string  | 支付单号 |


## 支付回调
* 描述

支付宝支付完成回调处理接口。


* 接口

调用方式：HTTP、HTTPS

api: pay/alipay/notify

HTTP请求方法：POST

## 退款
* 描述

支付宝支付退款请求。


* 接口

调用方式：HTTP、HTTPS

api: pay/alipay/refund

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| pay_tid  | string  | 支付pid |
| money  | float  | 支付金额 |


## 退款回调
* 描述

微信退款完成回调处理接口。


* 接口

调用方式：HTTP、HTTPS

api: pay/alipay/refund/notify

HTTP请求方法：POST

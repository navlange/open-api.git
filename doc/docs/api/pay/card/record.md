# 储值记录


## 储值记录列表
* 描述

获取储值记录列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/record/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 储值记录列表 |
| total  | int32  | 储值记录数量 |

## 获取储值记录信息
* 描述

获取储值记录信息。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/record/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 储值记录ID |



## 删除储值记录
* 描述

删除储值记录。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/record/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 储值记录ID |


## 储值记录审核
* 描述

储值记录审核。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/record/verify

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 储值记录ID |


## 开通储值卡
* 描述

开通储值卡。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/record/register

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| card_id  | int32  | 储值卡ID |
| name  | string  | 姓名 |
| mobile  | string  | 联系电话 |
| company  | string  | 单位 |
| id_number  | string  | 身份证号 |
| img  | string  | 图片 |

## 我的指定储值卡
* 描述

我的指定储值卡。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/record/ofMe

HTTP请求方法：GET

* 请求参数

当前登入用户。

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| card_id  | int32  | 储值卡ID |

## 我的储值卡
* 描述

我的储值卡。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/record/ofMy

HTTP请求方法：GET

* 请求参数

当前登入用户。


## 储值记录充值
* 描述

储值记录充值。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/record/recharge

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 储值记录ID |
| money  | float  | 充值金额 |
| description  | string  | 充值说明 |

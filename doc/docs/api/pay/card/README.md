# 储值管理


## 储值卡列表
* 描述

获取储值卡列表。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 储值卡列表 |
| total  | int32  | 储值卡数量 |

## 获取储值卡信息
* 描述

获取储值卡信息。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 储值卡ID |


## 添加储值卡
* 描述

添加储值卡。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 名称 |
| store_id  | int32  | 储值卡所属门店 |
| price  | float  | 开通价格 |
| need_verify  | string  | 需要审核 |
| img_display  | string  | 展示图 |
| detail  | string  | 详情 |



## 修改储值卡信息
* 描述

修改储值卡信息。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 储值卡ID |
| name  | string  | 名称 |
| store_id  | int32  | 储值卡所属门店 |
| price  | float  | 开通价格 |
| need_verify  | string  | 需要审核 |
| img_display  | string  | 展示图 |
| detail  | string  | 详情 |



## 删除储值卡
* 描述

删除储值卡。


* 接口

调用方式：HTTP、HTTPS

api: pay/card/delete

HTTP请求方法：POST

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 储值卡ID |

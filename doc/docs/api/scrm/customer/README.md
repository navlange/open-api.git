# 客户
客户管理接口。

## 获取所有客户
* 描述

获取所有客户。

* 接口

调用方式：HTTP、HTTPS

api: customer/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 客户列表 |
| total  | int32  | 客户数量 |

## 添加客户
* 描述

添加客户。

* 接口

调用方式：HTTP、HTTPS

api: customer/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 客户名称 |
| mobile  | string  | 客户联系方式 |
| consignee_name  | string  | 客户联系人 |

## 获取客户信息
* 描述

获取客户信息。

* 接口

调用方式：HTTP、HTTPS

api: customer/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 客户id |

## 修改客户信息
* 描述

修改客户信息。

* 接口

调用方式：HTTP、HTTPS

api: customer/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 客户id |
| name  | string  | 客户名称 |
| mobile  | string  | 客户联系方式 |
| consignee_name  | string  | 客户联系人 |

## 删除客户
* 描述

删除客户。

* 接口

调用方式：HTTP、HTTPS

api: customer/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 客户id |

## 获取登入用户客户信息
* 描述

获取登入用户客户信息。

* 接口

调用方式：HTTP、HTTPS

api: customer/ofMe

HTTP请求方法：GET

* 请求参数
当前登入用户

## 绑定登入用户到客户
* 描述

绑定登入用户到客户。

* 接口

调用方式：HTTP、HTTPS

api: customer/bindForMe

HTTP请求方法：POST

* 请求参数
当前登入用户

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| customer_id  | int32  | 客户id |
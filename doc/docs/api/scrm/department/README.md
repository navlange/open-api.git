# 部门
部门管理功能。

## 同步企业微信部门

* 描述

本地部门和企业微信部门同步。

* 接口

调用方式：HTTP、HTTPS

api: department/qiwei/sync

HTTP请求方法：POST

## 获取部门列表
* 描述

获取部门列表。

* 接口

调用方式：HTTP、HTTPS

api: department/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 部门列表 |
| total  | int32  | 部门数量 |


## 更新部门

* 描述

更新部门信息。

* 接口

调用方式：HTTP、HTTPS

api: department/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 部门ID |
| name  | string  | 部门名称 |

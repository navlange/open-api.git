# 分销
分销系统管理接口。

## 分销提成规则

### 获取规则列表

* 描述

获取分销提成规则列表。

* 接口

调用方式：HTTP、HTTPS

api: fx/rule/list

HTTP请求方法：GET


### 获取分销规则
* 描述

获取分销规则信息。

* 接口

调用方式：HTTP、HTTPS

api: fx/rule/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分销规则id |

### 添加分销规则
* 描述

添加分销规则。

* 接口

调用方式：HTTP、HTTPS

api: fx/rule/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| deduct_level  | string  | 分销等级 |
| deduct_mode  | string  | 提成模式 |
| money  | float  | 提成固定金额 |
| ratio  | float  | 提成比例 |

### 修改分销规则
* 描述

修改分销规则。

* 接口

调用方式：HTTP、HTTPS

api: fx/rule/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分销规则id |
| deduct_level  | string  | 分销等级 |
| deduct_mode  | string  | 提成模式 |
| money  | float  | 提成固定金额 |
| ratio  | float  | 提成比例 |

### 删除分销规则
* 描述

删除分销规则。

* 接口

调用方式：HTTP、HTTPS

api: fx/rule/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分销规则id |

## 分销成员

### 获取分销成员列表

* 描述

获取分销成员列表。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/list

HTTP请求方法：GET

### 获取分销团长列表

* 描述

获取分销团长列表。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/root/list

HTTP请求方法：GET

### 获取分销成员下级列表

* 描述

获取分销成员下级列表。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/ofPre

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 上级成员uid |

### 添加分销员
* 描述

添加分销员。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| role  | string  | 分销员角色 |
| mobile  | string  | 分销员手机号 |
| pre_uid  | int32  | 上级uid |

### 分销成员审核通过

* 描述

同意分销成员申请。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/pass

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |

### 分销成员审核拒绝

* 描述

拒绝分销成员申请。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/reject

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |

### 删除分销成员

* 描述

删除分销成员。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |

## 我的分销

### 分销员注册

* 描述

分销员注册。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/register

HTTP请求方法：POST

* 请求参数

当前登入用户

### 我的分销

* 描述

我的分销员信息。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/ofMe

HTTP请求方法：GET

* 请求参数

当前登入用户

### 获取我的下级分销商列表

* 描述

获取当前分销员下级分销商列表。

* 接口

调用方式：HTTP、HTTPS

api: fx/member/myFollow

HTTP请求方法：GET

* 请求参数

## 下线

### 下线注册

* 描述

下线注册。

* 接口

调用方式：HTTP、HTTPS

api: fx/follow/submit

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| pre_uid  | int32  | 分销员uid |

### 获取分销成员下线列表

* 描述

获取分销成员下线列表。

* 接口

调用方式：HTTP、HTTPS

api: fx/follow/ofMember

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 上线成员uid |

### 获取我的下线列表

* 描述

获取当前分销员下线列表。

* 接口

调用方式：HTTP、HTTPS

api: fx/follow/ofMy

HTTP请求方法：GET

* 请求参数


### 删除下线

* 描述

删除下线。

* 接口

调用方式：HTTP、HTTPS

api: fx/follow/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 下线id |

## 提现

### 分销成员申请提现

* 描述

分销成员申请提现。

* 接口

调用方式：HTTP、HTTPS

api: fx/withdraw/submit

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| money  | float  | 金额 |

### 获取分销成员提现列表

* 描述

获取分销成员提现列表。

* 接口

调用方式：HTTP、HTTPS

api: fx/withdraw/ofMy

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 提现列表 |

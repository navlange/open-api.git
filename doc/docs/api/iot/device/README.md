# 设备

## 获取设备列表
* 描述

获取设备列表。

* 接口

调用方式：HTTP、HTTPS

api: iot/device/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 设备列表 |
| total  | int32  | 设备数量 |


## 获取模拟设备列表
* 描述

获取模拟设备列表。

* 接口

调用方式：HTTP、HTTPS

api: iot/device/ofSim

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 设备列表 |
| total  | int32  | 设备数量 |


## 获取设备

* 描述

获取设备信息。

* 接口

调用方式：HTTP、HTTPS

api: iot/device/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 设备ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| identity  | string  | 设备标识 |
| name  | string  | 设备名称 |
| type  | string  | 设备类型 |


## 创建设备

* 描述

创建设备。

* 接口

调用方式：HTTP、HTTPS

api: iot/device/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| identity  | string  | 设备标识 |
| name  | string  | 设备名称 |
| type  | string  | 设备类型 |

## 更新设备

* 描述

更新设备信息。

* 接口

调用方式：HTTP、HTTPS

api: iot/device/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 设备ID |
| identity  | string  | 设备标识 |
| name  | string  | 设备名称 |
| type  | string  | 设备类型 |

## 设置设备网关

* 描述

设置设备网关信息。

* 接口

调用方式：HTTP、HTTPS

api: iot/device/setGateway

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 设备ID |
| gateway_id  | int32  | 网关ID |

## 删除设备

* 描述

删除设备信息。

* 接口

调用方式：HTTP、HTTPS

api: iot/device/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 设备ID |

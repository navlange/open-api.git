# 网关

## 获取网关列表
* 描述

获取网关列表。

* 接口

调用方式：HTTP、HTTPS

api: iot/gateway/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 网关列表 |
| total  | int32  | 网关数量 |


## 获取网关

* 描述

获取网关信息。

* 接口

调用方式：HTTP、HTTPS

api: iot/gateway/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 网关ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 网关名称 |
| ip  | string  | 网关IP |


## 创建网关

* 描述

创建网关。

* 接口

调用方式：HTTP、HTTPS

api: iot/gateway/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 网关名称 |
| ip  | string  | 网关IP |

## 更新网关

* 描述

更新网关信息。

* 接口

调用方式：HTTP、HTTPS

api: iot/gateway/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 网关ID |
| name  | string  | 网关名称 |
| ip  | string  | 网关IP |

## 删除网关

* 描述

删除网关信息。

* 接口

调用方式：HTTP、HTTPS

api: iot/gateway/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 网关ID |

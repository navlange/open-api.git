# 剧场票务
剧场票务营销接口。

## 团购
### 获取所有团购
* 描述

获取所有团购。

* 接口

调用方式：HTTP、HTTPS

api: marketing/show/tuan/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 团购列表 |
| total  | int32  | 团购数量 |

### 添加团购
* 描述

添加团购。

* 接口

调用方式：HTTP、HTTPS

api: marketing/show/tuan/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| amount  | int32  | 团购数量 |
| price  | float  | 团购价格 |

### 获取团购信息
* 描述

获取团购信息。

* 接口

调用方式：HTTP、HTTPS

api: marketing/show/tuan/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 团购id |

### 修改团购信息
* 描述

修改团购信息。

* 接口

调用方式：HTTP、HTTPS

api: marketing/show/tuan/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 团购id |
| amount  | int32  | 团购数量 |
| price  | float  | 团购价格 |

### 删除团购
* 描述

删除团购。

* 接口

调用方式：HTTP、HTTPS

api: marketing/show/tuan/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 团购id |

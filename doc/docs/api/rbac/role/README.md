# 角色

## 获取角色
* 描述

获取角色。

* 接口

调用方式：HTTP、HTTPS

api: role/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 角色名称 |
| identity  | string  | 角色标识 |


## 获取角色列表
* 描述

获取角色列表。

* 接口

调用方式：HTTP、HTTPS

api: role/list

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 角色列表 |
| total  | int32  | 角色数量 |

## 添加角色
* 描述

添加角色。

* 接口

调用方式：HTTP、HTTPS

api: role/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 角色名称 |
| identity  | string  | 角色标识 |


## 修改角色
* 描述

修改角色。

* 接口

调用方式：HTTP、HTTPS

api: role/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 角色ID |
| name  | string  | 角色名称 |
| identity  | string  | 角色标识 |

## 删除角色
* 描述

删除角色。

* 接口

调用方式：HTTP、HTTPS

api: role/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 角色ID |

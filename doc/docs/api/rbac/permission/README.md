# 权限

## 获取成员权限
* 描述

获取成员权限。

* 接口

调用方式：HTTP、HTTPS

api: permission/clerk/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| online_goods_on  | string  | 上架商品权限 |


## 设置成员权限
* 描述

设置成员权限。

* 接口

调用方式：HTTP、HTTPS

api: permission/clerk/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| online_goods_on  | string  | 上架商品权限 |

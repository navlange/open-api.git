# 消息模板

## 获取消息模板列表
* 描述

获取消息模板列表。

* 接口

调用方式：HTTP、HTTPS

api: message/template/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 消息模板列表 |
| total  | int32  | 消息模板数量 |


## 获取消息模板

* 描述

获取消息模板信息。

* 接口

调用方式：HTTP、HTTPS

api: message/template/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 消息模板ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| identity  | string  | 消息模板标识 |
| name  | string  | 消息模板名称 |
| mp_template_id  | string  | 微信公众号模板消息ID |


## 创建消息模板

* 描述

创建消息模板。

* 接口

调用方式：HTTP、HTTPS

api: message/template/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| identity  | string  | 消息模板标识 |
| name  | string  | 消息模板名称 |
| mp_template_id  | string  | 微信公众号模板消息ID |

## 更新消息模板

* 描述

更新消息模板信息。

* 接口

调用方式：HTTP、HTTPS

api: message/template/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 消息模板ID |
| identity  | string  | 消息模板标识 |
| name  | string  | 消息模板名称 |
| mp_template_id  | string  | 微信公众号模板消息ID |

## 删除消息模板

* 描述

删除消息模板信息。

* 接口

调用方式：HTTP、HTTPS

api: message/template/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 消息模板ID |
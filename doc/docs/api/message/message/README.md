# 消息

## 获取消息列表
* 描述

获取消息列表。

* 接口

调用方式：HTTP、HTTPS

api: message/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 消息列表 |
| total  | int32  | 消息数量 |

## 获取类型消息列表
* 描述

获取类型消息列表。

* 接口

调用方式：HTTP、HTTPS

api: message/ofType

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 消息列表 |
| total  | int32  | 消息数量 |


## 获取我的消息列表
* 描述

获取我的消息列表。

* 接口

调用方式：HTTP、HTTPS

api: message/ofMy

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 消息列表 |
| total  | int32  | 消息数量 |


## 获取我的新消息数量
* 描述

获取我的新消息数量。

* 接口

调用方式：HTTP、HTTPS

api: message/newCount

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| message_count  | int32  | 新消息数量 |

## 消息阅读
* 描述

消息阅读。

* 接口

调用方式：HTTP、HTTPS

api: message/read

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 消息ID |


## 获取消息

* 描述

获取消息信息。

* 接口

调用方式：HTTP、HTTPS

api: message/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 消息ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| type  | string  | 消息类型 |
| content  | string  | 消息内容 |


## 创建消息

* 描述

创建消息。

* 接口

调用方式：HTTP、HTTPS

api: message/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| type  | string  | 消息类型 |
| content  | string  | 消息内容 |

## 更新消息

* 描述

更新消息信息。

* 接口

调用方式：HTTP、HTTPS

api: message/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 消息ID |
| type  | string  | 消息类型 |
| content  | string  | 消息内容 |

## 删除消息

* 描述

删除消息信息。

* 接口

调用方式：HTTP、HTTPS

api: message/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 消息ID |

# 小程序订阅消息

## 获取小程序订阅消息模板列表
* 描述

获取小程序订阅消息模板列表。

* 接口

调用方式：HTTP、HTTPS

api: message/template/miniapp/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 小程序订阅消息模板列表 |
| total  | int32  | 小程序订阅消息模板数量 |


## 获取小程序订阅消息模板

* 描述

获取小程序订阅消息模板信息。

* 接口

调用方式：HTTP、HTTPS

api: message/template/miniapp/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 小程序订阅消息模板ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| identity  | string  | 模板标识 |
| name  | string  | 模板名称 |
| template_id  | string  | 小程序订阅消息模板ID |


## 创建小程序订阅消息模板

* 描述

创建小程序订阅消息模板。

* 接口

调用方式：HTTP、HTTPS

api: message/template/miniapp/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| identity  | string  | 模板标识 |
| name  | string  | 模板名称 |
| template_id  | string  | 微信小程序订阅消息模板ID |

## 更新小程序订阅消息模板

* 描述

更新小程序订阅消息模板信息。

* 接口

调用方式：HTTP、HTTPS

api: message/template/miniapp/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 模板ID |
| identity  | string  | 模板标识 |
| name  | string  | 小模板名称 |
| template_id  | string  | 微信小程序订阅消息模板ID |

## 删除小程序订阅消息模板

* 描述

删除小程序订阅消息模板信息。

* 接口

调用方式：HTTP、HTTPS

api: message/template/miniapp/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 模板ID |


## 获取场景小程序订阅消息模板

* 描述

获取指定场景小程序订阅消息模板信息。

* 接口

调用方式：HTTP、HTTPS

api: message/template/miniapp/ofScene

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| identity  | string  | 场景标识 |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| identity  | string  | 模板标识 |
| name  | string  | 模板名称 |
| template_id  | string  | 小程序订阅消息模板ID |

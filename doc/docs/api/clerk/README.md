# 成员
成员管理接口。

## 成员管理

### 获取所有成员
* 描述

获取所有成员。

* 接口

调用方式：HTTP、HTTPS

api: clerk/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 成员列表 |
| total  | int32  | 成员数量 |

### 添加成员
* 描述

添加成员。

* 接口

调用方式：HTTP、HTTPS

api: clerk/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 所属门店ID |
| name  | string  | 成员名称 |
| mobile  | string  | 成员手机号 |

### 获取成员信息
* 描述

获取成员信息。

* 接口

调用方式：HTTP、HTTPS

api: clerk/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |

### 修改成员信息
* 描述

修改成员信息。

* 接口

调用方式：HTTP、HTTPS

api: clerk/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |
| store_id  | int32  | 所属门店ID |
| name  | string  | 成员名称 |
| mobile  | string  | 成员手机号 |

### 删除成员
* 描述

删除成员。

* 接口

调用方式：HTTP、HTTPS

api: clerk/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |

### 绑定成员用户
* 描述

绑定成员用户。

* 接口

调用方式：HTTP、HTTPS

api: clerk/bindUser

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |
| mobile  | string  | 用户mobile |

### 获取登入成员信息
* 描述

获取登入成员信息。

* 接口

调用方式：HTTP、HTTPS

api: clerk/ofMe

HTTP请求方法：GET

### 获取成员角色
* 描述

获取成员角色。

* 接口

调用方式：HTTP、HTTPS

api: clerk/role/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| role  | string  | 成员角色 |

### 设置成员角色
* 描述

设置成员角色。

* 接口

调用方式：HTTP、HTTPS

api: clerk/role/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |
| role  | string  | 成员角色 |

### 获取商家成员角色
* 描述

获取商家成员角色。

* 接口

调用方式：HTTP、HTTPS

api: clerk/role/store/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| role  | string  | 商家成员角色 |

### 设置商家成员角色
* 描述

设置商家成员角色。

* 接口

调用方式：HTTP、HTTPS

api: clerk/role/store/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |
| role  | string  | 商家成员角色 |

## 企业微信

### 同步企业微信成员

* 描述

本地成员和企业微信成员同步。

* 接口

调用方式：HTTP、HTTPS

api: clerk/qiwei/sync

HTTP请求方法：POST

### 获取部门成员列表
* 描述

获取部门成员列表。

* 接口

调用方式：HTTP、HTTPS

api: clerk/ofDepartment

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 部门ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 成员列表 |
| total  | int32  | 成员数量 |


### 更新企业微信成员

* 描述

更新企业微信成员信息。

* 接口

调用方式：HTTP、HTTPS

api: clerk/qiwei/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员ID |
| name  | string  | 成员名字 |

## 用户密码


### 获取成员用户名
* 描述

获取成员用户名。

* 接口

调用方式：HTTP、HTTPS

api: clerk/username/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| username  | string  | 用户名 |

### 设置成员用户名
* 描述

设置成员用户名。

* 接口

调用方式：HTTP、HTTPS

api: clerk/username/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |
| username  | string  | 用户名 |

### 设置登入成员用户名
* 描述

设置登入成员用户名。

* 接口

调用方式：HTTP、HTTPS

api: clerk/username/updateCid

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| username  | string  | 用户名 |

### 设置成员密码
* 描述

设置成员密码。

* 接口

调用方式：HTTP、HTTPS

api: clerk/password/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 成员id |
| password  | string  | 密码 |

### 设置登入成员密码
* 描述

设置登入成员密码。

* 接口

调用方式：HTTP、HTTPS

api: clerk/password/updateCid

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| password  | string  | 密码 |

# 场地

## 获取所有场地
* 描述

获取所有场地。

* 接口

调用方式：HTTP、HTTPS

api: court/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 场地列表 |
| total  | int32  | 场地数量 |

## 添加场地
* 描述

添加场地。

* 接口

调用方式：HTTP、HTTPS

api: court/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 场地名称 |
| type_id  | int32  | 场地分类 |
| store_id  | int32  | 所属门店 |
| number  | string  | 场地编号 |

## 修改场地信息
* 描述

修改场地信息。

* 接口

调用方式：HTTP、HTTPS

api: court/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 场地id |
| name  | string  | 场地名称 |
| type_id  | int32  | 场地分类 |
| store_id  | int32  | 所属门店 |
| number  | string  | 场地编号 |

## 删除场地
* 描述

删除场地。

* 接口

调用方式：HTTP、HTTPS

api: court/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 场地id |

## 获取前台银幕舞台名称

* 描述

获取前台银幕舞台名称。

* 接口

调用方式：HTTP、HTTPS

api: things/court/screen/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| court_id  | int32  | 场地ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| screen_name  | string  | 前台银幕舞台名称 |

## 设置前台银幕舞台名称

* 描述

设置前台银幕舞台名称。

* 接口

调用方式：HTTP、HTTPS

api: things/court/screen/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| court_id  | int32  | 场地ID |
| screen_name  | string  | 前台银幕舞台名称 |

# 服务
服务管理接口。

## 获取所有服务
* 描述

获取所有服务。

* 接口

调用方式：HTTP、HTTPS

api: service/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 服务列表 |
| total  | int32  | 服务数量 |

## 添加服务
* 描述

添加服务。

* 接口

调用方式：HTTP、HTTPS

api: service/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 服务名称 |
| img_thumb  | string  | 服务缩略图 |
| price  | string  | 服务价格 |
| origin_price  | string  | 原价 |
| intro  | string  | 服务简介 |
| detail  | string  | 服务详情 |

## 获取服务信息
* 描述

获取服务信息。

* 接口

调用方式：HTTP、HTTPS

api: service/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 服务id |

## 修改服务信息
* 描述

修改服务信息。

* 接口

调用方式：HTTP、HTTPS

api: service/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 服务id |
| name  | string  | 服务名称 |
| img_thumb  | string  | 服务缩略图 |
| price  | string  | 服务价格 |
| origin_price  | string  | 原价 |
| intro  | string  | 服务简介 |
| detail  | string  | 服务详情 |

## 删除服务
* 描述

删除服务。

* 接口

调用方式：HTTP、HTTPS

api: service/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 服务id |

## 设置服务排序
* 描述

设置服务排序。

* 接口

调用方式：HTTP、HTTPS

api: service/setOrder

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 服务id |
| order_index  | int32  | 服务排序 |


## 门店服务

### 获取门店服务
* 描述

获取门店服务。

* 接口

调用方式：HTTP、HTTPS

api: service/store/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 服务列表 |
| total  | int32  | 服务数量 |

### 添加门店服务
* 描述

添加门店服务。

* 接口

调用方式：HTTP、HTTPS

api: service/store/add

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店ID |
| service_id  | int32  | 服务ID |

### 删除门店服务
* 描述

删除门店服务。

* 接口

调用方式：HTTP、HTTPS

api: service/store/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店服务id |

## 服务推荐


### 获取服务推荐
* 描述

获取服务推荐。

* 接口

调用方式：HTTP、HTTPS

api: service/recommend/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scend  | string  | 推荐场景 |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| service_list  | string  | 推荐服务IDs |

### 设置服务推荐
* 描述

设置服务推荐。

* 接口

调用方式：HTTP、HTTPS

api: service/recommend/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| service_list  | string  | 推荐服务IDs |

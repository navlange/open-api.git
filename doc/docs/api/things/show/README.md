# 演出
演出管理接口。

## 获取所有演出
* 描述

获取所有演出。

* 接口

调用方式：HTTP、HTTPS

api: show/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 演出列表 |
| total  | int32  | 演出数量 |

## 获取门店所有演出
* 描述

获取门店所有演出。

* 接口

调用方式：HTTP、HTTPS

api: show/ofStore

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 演出列表 |
| total  | int32  | 演出数量 |

## 添加演出
* 描述

添加演出。

* 接口

调用方式：HTTP、HTTPS

api: show/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 演出名称 |
| store_id  | int32  | 所属门店 |
| img_thumb  | string  | 缩略图 |
| poster  | string  | 海报 |
| price  | string  | 价格 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |
| status  | string  | 状态 |

## 获取演出信息
* 描述

获取演出信息。

* 接口

调用方式：HTTP、HTTPS

api: show/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 演出id |

## 修改演出信息
* 描述

修改演出信息。

* 接口

调用方式：HTTP、HTTPS

api: show/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 演出id |
| name  | string  | 演出名称 |
| store_id  | int32  | 所属门店 |
| img_thumb  | string  | 缩略图 |
| poster  | string  | 海报 |
| price  | string  | 价格 |
| intro  | string  | 简介 |
| detail  | string  | 详情 |
| status  | string  | 状态 |

## 删除演出
* 描述

删除演出。

* 接口

调用方式：HTTP、HTTPS

api: show/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 演出id |

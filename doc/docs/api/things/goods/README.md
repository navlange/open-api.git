# 商品
商品管理接口。

## 获取所有商品
* 描述

获取所有商品。

* 接口

调用方式：HTTP、HTTPS

api: goods/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 商品列表 |
| total  | int32  | 商品数量 |

## 商品筛选
* 描述

商品筛选。

* 接口

调用方式：HTTP、HTTPS

api: goods/filter

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 商品列表 |
| total  | int32  | 商品数量 |

## 添加商品
* 描述

添加商品。

* 接口

调用方式：HTTP、HTTPS

api: goods/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | string  | 所属门店 |
| name  | string  | 商品名称 |
| norms  | string  | 商品规格 |
| version  | string  | 商品版本 |

## 获取商品信息
* 描述

获取商品信息。

* 接口

调用方式：HTTP、HTTPS

api: goods/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |

## 修改商品信息
* 描述

修改商品信息。

* 接口

调用方式：HTTP、HTTPS

api: goods/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |
| store_id  | string  | 所属门店 |
| name  | string  | 商品名称 |
| norms  | string  | 商品规格 |
| version  | string  | 商品版本 |

## 删除商品
* 描述

删除商品。

* 接口

调用方式：HTTP、HTTPS

api: goods/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |

## 设置商品排序
* 描述

设置商品排序。

* 接口

调用方式：HTTP、HTTPS

api: goods/setOrder

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |
| order_index  | int32  | 商品排序 |

## 获取商品营销
* 描述

获取商品营销。

* 接口

调用方式：HTTP、HTTPS

api: goods/marketing/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| member_on  | string  | 是否享受会员优惠 |

## 设置商品营销
* 描述

设置商品营销。

* 接口

调用方式：HTTP、HTTPS

api: goods/marketing/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |
| member_on  | string  | 是否享受会员优惠 |

## 获取商品推荐
* 描述

获取商品推荐。

* 接口

调用方式：HTTP、HTTPS

api: goods/recommend/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| scend  | string  | 推荐场景 |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| goods_list  | string  | 推荐商品IDs |

## 设置商品推荐
* 描述

设置商品推荐。

* 接口

调用方式：HTTP、HTTPS

api: goods/recommend/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| goods_list  | string  | 推荐商品IDs |

## 获取商品价格
* 描述

获取商品价格。

* 接口

调用方式：HTTP、HTTPS

api: goods/price/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| buyout_price_on  | string  | 是否支持售卖 |
| buyout_price  | float  | 买断价格 |
| advance_price_on  | float  | 是否支持租赁 |
| lease_mode  | string  | 租赁模式 |
| unit_mode  | string  | 租赁单位 |
| advance_price  | float  | 押金 |
| unit_price  | float  | 单位时间租金 |
| insurance_price  | float  | 保证金 |
| min_unit_amount  | int32  | 最小租赁时间 |

## 设置商品价格
* 描述

设置商品价格。

* 接口

调用方式：HTTP、HTTPS

api: goods/price/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |
| buyout_price_on  | string  | 是否支持售卖 |
| buyout_price  | float  | 买断价格 |
| advance_price_on  | float  | 是否支持租赁 |
| lease_mode  | string  | 租赁模式 |
| unit_mode  | string  | 租赁单位 |
| advance_price  | float  | 押金 |
| unit_price  | float  | 单位时间租金 |
| insurance_price  | float  | 保证金 |
| min_unit_amount  | int32  | 最小租赁时间 |

## 获取商品阶梯价格列表
* 描述

获取商品价格。

* 接口

调用方式：HTTP、HTTPS

api: goods/price/list

HTTP请求方法：GET

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 阶梯价格列表 |
| total  | float  | 阶梯价格数量 |

## 添加商品阶梯价格
* 描述

添加商品阶梯价格。

* 接口

调用方式：HTTP、HTTPS

api: goods/price/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 商品id |
| mode  | string  | 阶梯模式 |
| start  | int32  | 开始时间 |
| end  | int32  | 结束时间 |
| price  | float  | 价格 |

## 修改商品阶梯价格
* 描述

修改商品阶梯价格。

* 接口

调用方式：HTTP、HTTPS

api: goods/price/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 阶梯价格id |
| start  | int32  | 开始时间 |
| end  | int32  | 结束时间 |
| price  | float  | 价格 |

## 删除商品阶梯价格
* 描述

删除商品阶梯价格。

* 接口

调用方式：HTTP、HTTPS

api: goods/price/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 阶梯价格id |

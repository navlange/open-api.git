# 产品
产品管理接口。

## 获取所有产品
* 描述

获取所有产品。

* 接口

调用方式：HTTP、HTTPS

api: product/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 产品列表 |
| total  | int32  | 产品数量 |

## 添加产品
* 描述

添加产品。

* 接口

调用方式：HTTP、HTTPS

api: product/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| subject  | string  | 所属主体 |
| name  | string  | 产品名称 |
| norms  | string  | 产品规格 |
| version  | string  | 产品版本 |

## 获取产品信息
* 描述

获取产品信息。

* 接口

调用方式：HTTP、HTTPS

api: product/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 产品id |

## 查找产品
* 描述

查找产品。

* 接口

调用方式：HTTP、HTTPS

api: product/filterGet

HTTP请求方法：GET

* 请求参数
查找参数

## 修改产品信息
* 描述

修改产品信息。

* 接口

调用方式：HTTP、HTTPS

api: product/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 产品id |
| subject  | string  | 所属主体 |
| name  | string  | 产品名称 |
| norms  | string  | 产品规格 |
| version  | string  | 产品版本 |

## 删除产品
* 描述

删除产品。

* 接口

调用方式：HTTP、HTTPS

api: product/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 产品id |

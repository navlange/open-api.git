# 门票
门票管理接口。

## 获取所有门票
* 描述

获取所有门票。

* 接口

调用方式：HTTP、HTTPS

api: ticket/list

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 门票列表 |
| total  | int32  | 门票数量 |

## 添加门票
* 描述

添加门票。

* 接口

调用方式：HTTP、HTTPS

api: ticket/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 门票名称 |
| img_thumb  | string  | 门票缩略图 |
| price  | string  | 门票价格 |
| origin_price  | string  | 原价 |
| intro  | string  | 门票简介 |
| detail  | string  | 门票详情 |

## 获取门票信息
* 描述

获取门票信息。

* 接口

调用方式：HTTP、HTTPS

api: ticket/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门票id |

## 修改门票信息
* 描述

修改门票信息。

* 接口

调用方式：HTTP、HTTPS

api: ticket/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门票id |
| name  | string  | 门票名称 |
| img_thumb  | string  | 门票缩略图 |
| price  | string  | 门票价格 |
| origin_price  | string  | 原价 |
| intro  | string  | 门票简介 |
| detail  | string  | 门票详情 |

## 删除门票
* 描述

删除门票。

* 接口

调用方式：HTTP、HTTPS

api: ticket/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门票id |

## 获取门票设置
* 描述

获取门票设置。

* 接口

调用方式：HTTP、HTTPS

api: ticket/conf/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门票id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| start  | string  | 销售开始时间 |
| finish  | string  | 销售结束时间 |
| times  | int32  | 每票可用次数 |

## 修改门票设置
* 描述

修改门票设置。

* 接口

调用方式：HTTP、HTTPS

api: ticket/conf/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门票id |
| start  | string  | 销售开始时间 |
| finish  | string  | 销售结束时间 |
| times  | int32  | 每票可用次数 |

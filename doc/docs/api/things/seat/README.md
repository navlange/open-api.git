# 座位

## 获取场地座位号

* 描述

获取场地座位号。

* 接口

调用方式：HTTP、HTTPS

api: things/seat/mapping/ofCourt

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| court_id  | int32  | 场地ID |

## 设置车票普通座位

* 描述

设置车票普通座位。

* 接口

调用方式：HTTP、HTTPS

api: things/seat/setGeneralLine

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| line_id  | int32  | 线路ID |
| seat  | string  | 座位 |

## 设置车票空座位

* 描述

设置车票空座位。

* 接口

调用方式：HTTP、HTTPS

api: things/seat/empty/setLine

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| line_id  | int32  | 线路ID |
| seat  | string  | 座位 |

## 添加车票座位分类

* 描述

添加车票座位分类。

* 接口

调用方式：HTTP、HTTPS

api: things/seat/class/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| line_id  | int32  | 线路ID |
| name  | string  | 分类名称 |
| color  | string  | 分类颜色 |

## 修改座位分类

* 描述

修改座位分类。

* 接口

调用方式：HTTP、HTTPS

api: things/seat/class/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分类ID |
| name  | string  | 分类名称 |
| color  | string  | 分类颜色 |

## 删除座位分类

* 描述

删除座位分类。

* 接口

调用方式：HTTP、HTTPS

api: things/seat/class/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 分类ID |

## 设置车票座位分类

* 描述

设置车票座位分类。

* 接口

调用方式：HTTP、HTTPS

api: things/seat/class/record/setLine

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| line_id  | int32  | 线路ID |
| seat  | string  | 座位 |

## 设置车票座位不可选

* 描述

设置车票座位不可选。

* 接口

调用方式：HTTP、HTTPS

api: things/seat/unavailable/setLine

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| line_id  | int32  | 线路ID |
| seat  | string  | 座位 |

# 统计信息

## 获取所有门店统计信息
* 描述

获取所有商家门店统计信息。

* 接口

调用方式：HTTP、HTTPS

api: store/statistic/all

HTTP请求方法：GET

* 请求参数


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 门店统计列表 |
| total  | int32  | 门店统计数量 |

构成items的每项代表一个门店信息，具体内容为：

|  名称 |  描述  |
|  ----  | ----  |
| store_name  | 商家门店名称 |
| statistic  | order_total：今日订单总数<br>order_payed：今日支付订单<br>money_total：今日付款<br>money_wx：微信支付<br>money_balance：余额支付 |

## 增加门店访问次数
* 描述

增加商家门店访问次数。

* 接口

调用方式：HTTP、HTTPS

api: store/statistic/incPv

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店ID |

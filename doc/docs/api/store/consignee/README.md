# 配送


## 获取门店收件信息
* 描述

获取商家门店收件信息。

* 接口

调用方式：HTTP、HTTPS

api: store/consignee/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |

## 修改门店收件信息
* 描述

修改商家门店收件信息。

* 接口

调用方式：HTTP、HTTPS

api: store/consignee/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |
| consignee_prov  | string  | 省 |
| consignee_city  | string  | 市 |
| consignee_district  | string  | 区 |
| consignee_address  | string  | 详细地址 |
| consignee_name  | string  | 姓名 |
| consignee_mobile  | string  | 电话 |

# 门店信誉


## 设置门店信誉认证
* 描述

设置商家门店信誉认证。

* 接口

调用方式：HTTP、HTTPS

api: store/credit/certificate/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 门店绑定用户uid |

## 取消门店信誉认证
* 描述

取消商家门店信誉认证。

* 接口

调用方式：HTTP、HTTPS

api: store/credit/certificate/cancel

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 门店绑定用户uid |

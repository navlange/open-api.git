# 门店设置


## 获取门店设置
* 描述

获取商家门店设置。

* 接口

调用方式：HTTP、HTTPS

api: store/conf/get

HTTP请求方法：GET


## 修改门店设置
* 描述

修改商家门店设置。

* 接口

调用方式：HTTP、HTTPS

api: store/conf/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| agreement_content  | string  | 门店入驻协议 |

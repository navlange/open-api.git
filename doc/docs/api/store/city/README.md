# 城市管理


## 获取城市列表
* 描述

获取城市列表。

* 接口

调用方式：HTTP、HTTPS

api: city/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 城市列表 |
| total  | int32  | 城市数量 |

## 获取城市
* 描述

获取城市信息。

* 接口

调用方式：HTTP、HTTPS

api: city/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 城市id |

## 添加城市
* 描述

添加城市。

* 接口

调用方式：HTTP、HTTPS

api: city/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| prov  | string  | 省 |
| city  | string  | 市 |

## 修改城市
* 描述

修改城市。

* 接口

调用方式：HTTP、HTTPS

api: city/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 城市id |
| prov  | string  | 省 |
| city  | string  | 市 |

## 删除城市
* 描述

删除城市。

* 接口

调用方式：HTTP、HTTPS

api: city/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 城市id |

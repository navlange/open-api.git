# 门店管理

## 获取门店列表
* 描述

获取商家门店列表。

* 接口

调用方式：HTTP、HTTPS

api: store/list

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 获取门店列表模式 |

mode可能值：

为空：获取所有门店。

recommend：获取推荐门店。

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 门店列表 |
| total  | int32  | 门店数量 |

## 获取门店ids列表
* 描述

根据门店id数组获取商家门店列表。

* 接口

调用方式：HTTP、HTTPS

api: store/ofIds

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| ids  | array  | 门店id数组 |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 门店列表 |
| total  | int32  | 门店数量 |

## 获取推荐门店列表
* 描述

获取推荐门店列表。

* 接口

调用方式：HTTP、HTTPS

api: store/recommend/get

HTTP请求方法：GET

* 请求参数

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| stores  | string  | 推荐门店列表 |

## 设置推荐门店列表
* 描述

设置推荐门店列表。

* 接口

调用方式：HTTP、HTTPS

api: store/recommend/set

HTTP请求方法：POST

* 请求参数

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| stores  | string  | 推荐门店列表 |

## 添加门店
* 描述

添加商家门店。

* 接口

调用方式：HTTP、HTTPS

api: store/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 门店名称 |
| img_thumb  | string  | 门店缩略图 |
| img_display  | string  | 门店展示图 |
| gallery  | string  | 门店轮播图 |
| prov  | string  | 门店所在省份 |
| city  | string  | 门店所在城市 |
| district  | string  | 门店所在区县 |
| address  | string  | 门店所在详细地址 |
| contacts_mobile  | string  | 客服电话 |
| lng  | string  | 门店所在地区纬度 |
| lat  | string  | 门店所在地区经度 |
| tips  | string  | 门店提示信息 |
| detail  | string  | 门店详情 |

## 获取门店信息
* 描述

获取商家门店信息。

* 接口

调用方式：HTTP、HTTPS

api: store/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |

## 获取门店客服电话
* 描述

获取商家门店客服电话。

* 接口

调用方式：HTTP、HTTPS

api: store/contacts/getMobile

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mobile  | string  | 门店客服电话 |

## 修改门店信息
* 描述

修改商家门店信息。

* 接口

调用方式：HTTP、HTTPS

api: store/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |
| name  | string  | 门店名称 |
| img_thumb  | string  | 门店缩略图 |
| img_display  | string  | 门店展示图 |
| gallery  | string  | 门店轮播图 |
| prov  | string  | 门店所在省份 |
| city  | string  | 门店所在城市 |
| district  | string  | 门店所在区县 |
| address  | string  | 门店所在详细地址 |
| contacts_mobile  | string  | 客服电话 |
| lng  | string  | 门店所在地区纬度 |
| lat  | string  | 门店所在地区经度 |
| tips  | string  | 门店提示信息 |
| detail  | string  | 门店详情 |


## 获取门店应用模式
* 描述

获取商家门店应用模式。

* 接口

调用方式：HTTP、HTTPS

api: store/mode/app/ofStore

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| app_mode  | string  | 门店应用模式 |
| index_mode  | string  | 门店首页模式 |


## 设置门店应用模式
* 描述

设置商家门店应用模式。

* 接口

调用方式：HTTP、HTTPS

api: store/mode/app/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |
| app_mode  | string  | 门店应用模式 |
| index_mode  | string  | 门店首页模式 |


## 删除门店
* 描述

删除商家门店。

* 接口

调用方式：HTTP、HTTPS

api: store/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |


## 获取登入门店信息
* 描述

获取登入商家门店信息。

* 接口

调用方式：HTTP、HTTPS

api: store/ofMe

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |

## 获取登入门店状态
* 描述

获取登入商家门店状态。

* 接口

调用方式：HTTP、HTTPS

api: store/statusOfMe

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| status  | string  | 门店状态 |

## 获取登入门店角色权限
* 描述

获取登入商家门店角色权限。

* 接口

调用方式：HTTP、HTTPS

api: store/roleOfMe

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |
| roles  | string  | 角色 |


## 绑定门店用户
* 描述

绑定商家门店用户。

* 接口

调用方式：HTTP、HTTPS

api: store/auth/bindUser

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |
| uid  | int32  | 用户ID |
| mobile  | string  | 用户手机号 |

// 可以用用户uid或者用户手机号进行绑定，uid和mobile不需要同时提供。

## 获取门店营业执照
* 描述

获取商家门店营业执照。

* 接口

调用方式：HTTP、HTTPS

api: store/license/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店ID |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| license  | string  | 营业执照 |


## 修改门店银行卡
* 描述

修改商家门店银行卡。

* 接口

调用方式：HTTP、HTTPS

api: store/license/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店ID |
| license  | string  | 营业执照 |


## 获取当前门店银行卡
* 描述

获取当前商家门店银行卡。

* 接口

调用方式：HTTP、HTTPS

api: store/bank/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店ID |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bank_id  | string  | 银行卡号 |
| bank_name  | string  | 开户行 |
| bank_username  | string  | 银行开户姓名 |


## 修改门店银行卡
* 描述

修改商家门店银行卡。

* 接口

调用方式：HTTP、HTTPS

api: store/bank/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店ID |
| bank_id  | string  | 银行卡号 |
| bank_name  | string  | 开户行 |
| bank_username  | string  | 银行开户姓名 |

## 设置门店排序
* 描述

设置商家门店排序。

* 接口

调用方式：HTTP、HTTPS

api: store/setOrder

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店ID |
| order_index  | int32  | 门店排序 |

## 获取门店审核状态
* 描述

获取商家门店审核状态。

* 接口

调用方式：HTTP、HTTPS

api: store/audit/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| status  | string  | 门店状态 |

## 设置门店审核状态
* 描述

设置商家门店审核状态。

* 接口

调用方式：HTTP、HTTPS

api: store/audit/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |
| status  | string  | 门店状态 |

## 获取门店联系人信息
* 描述

获取商家门店联系人信息。

* 接口

调用方式：HTTP、HTTPS

api: store/contacts/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| contacts_name  | string  | 联系人姓名 |
| contacts_mobile  | string  | 联系人手机号 |
| contacts_email  | string  | 联系人邮箱 |

## 设置门店联系人信息
* 描述

设置商家门店联系人信息。

* 接口

调用方式：HTTP、HTTPS

api: store/contacts/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |
| contacts_name  | string  | 联系人姓名 |
| contacts_mobile  | string  | 联系人手机号 |
| contacts_email  | string  | 联系人邮箱 |

## 获取门店工商信息
* 描述

获取商家门店工商信息。

* 接口

调用方式：HTTP、HTTPS

api: store/gongshang/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| is_company_audit  | string  | 企业认证 |
| business_license  | string  | 营业执照 |
| name  | string  | 公司名称 |
| registered_capital  | int  | 注册资本 |
| company_type  | string  | 公司类型 |
| credit_code  | string  | 信用代码 |
| established_time  | string  | 成立时间 |
| operating_period  | string  | 营业期限 |
| registered_address  | string  | 注册地址 |
| legal_person  | string  | 法定代表人 |
| business_scope  | string  | 经营范围 |
| is_person_audit  | string  | 个人认证 |
| idcard_img  | string  | 身份证正面 |
| idcard_bg_img  | string  | 身份证背面 |
| idcard  | string  | 身份证号 |

## 设置门店工商信息
* 描述

设置商家门店工商信息。

* 接口

调用方式：HTTP、HTTPS

api: store/gongshang/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |
| is_company_audit  | string  | 企业认证 |
| business_license  | string  | 营业执照 |
| name  | string  | 公司名称 |
| registered_capital  | int  | 注册资本 |
| company_type  | string  | 公司类型 |
| credit_code  | string  | 信用代码 |
| established_time  | string  | 成立时间 |
| operating_period  | string  | 营业期限 |
| registered_address  | string  | 注册地址 |
| legal_person  | string  | 法定代表人 |
| business_scope  | string  | 经营范围 |
| is_person_audit  | string  | 个人认证 |
| idcard_img  | string  | 身份证正面 |
| idcard_bg_img  | string  | 身份证背面 |
| idcard  | string  | 身份证号 |

## 获取门店收银支付信息
* 描述

获取商家门店收银支付信息。

* 接口

调用方式：HTTP、HTTPS

api: store/pay/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |

* 返回参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mchid  | string  | 微信子商户号 |

## 设置门店收银支付信息
* 描述

设置商家门店收银支付信息。

* 接口

调用方式：HTTP、HTTPS

api: store/pay/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |
| mchid  | string  | 微信子商户号 |

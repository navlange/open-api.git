# 标签分类


## 获取门店支持分类
* 描述

获取商家门店支持分类。

* 接口

调用方式：HTTP、HTTPS

api: store/type/ofStore

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |

## 修改门店支持分类
* 描述

修改商家门店支持分类。

* 接口

调用方式：HTTP、HTTPS

api: store/type/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |
| type  | string  | 门店支持分类 |

## 获取门店标签
* 描述

获取商家门店标签。

* 接口

调用方式：HTTP、HTTPS

api: store/tag/ofStore

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |

## 修改门店标签
* 描述

修改商家门店标签。

* 接口

调用方式：HTTP、HTTPS

api: store/tag/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 门店id |
| tag  | string  | 门店标签 |

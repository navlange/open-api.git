# 区县管理


## 获取区县列表
* 描述

获取区县列表。

* 接口

调用方式：HTTP、HTTPS

api: city/district/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 区县列表 |
| total  | int32  | 区县数量 |

## 获取区县
* 描述

获取区县信息。

* 接口

调用方式：HTTP、HTTPS

api: city/district/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 区县id |

## 添加区县
* 描述

添加区县。

* 接口

调用方式：HTTP、HTTPS

api: city/district/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| prov  | string  | 省 |
| city  | string  | 市 |
| district  | string  | 区县 |

## 修改区县
* 描述

修改区县。

* 接口

调用方式：HTTP、HTTPS

api: city/district/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 区县id |
| prov  | string  | 省 |
| city  | string  | 市 |
| district  | string  | 区县 |

## 删除区县
* 描述

删除区县。

* 接口

调用方式：HTTP、HTTPS

api: city/district/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 区县id |

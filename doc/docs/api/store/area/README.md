# 小区管理


## 获取小区列表
* 描述

获取小区列表。

* 接口

调用方式：HTTP、HTTPS

api: city/area/list

HTTP请求方法：GET


* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 小区列表 |
| total  | int32  | 小区数量 |

## 获取小区
* 描述

获取小区信息。

* 接口

调用方式：HTTP、HTTPS

api: city/area/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 小区id |

## 添加小区
* 描述

添加小区。

* 接口

调用方式：HTTP、HTTPS

api: city/area/create

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| prov  | string  | 省 |
| city  | string  | 市 |
| area  | string  | 小区 |

## 修改小区
* 描述

修改小区。

* 接口

调用方式：HTTP、HTTPS

api: city/area/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 小区id |
| prov  | string  | 省 |
| city  | string  | 市 |
| area  | string  | 小区 |

## 删除小区
* 描述

删除小区。

* 接口

调用方式：HTTP、HTTPS

api: city/area/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 小区id |

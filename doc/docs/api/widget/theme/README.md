# 设置

## 获取样式参数
* 描述

获取样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/theme/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| color  | string  | 系统色调 |


## 设置样式参数
* 描述

设置样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/theme/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| color  | string  | 系统色调 |

# 调度

## 获取演出调度样式参数
* 描述

获取演出调度样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/schedule/show/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| show_schedule_theme  | string  | 演出调度样式 |


## 设置演出调度样式参数
* 描述

设置演出调度样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/schedule/show/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| show_schedule_theme  | string  | 演出调度样式 |

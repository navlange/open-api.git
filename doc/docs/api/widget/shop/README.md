# 电子商城

## 获取电子商城样式参数
* 描述

获取电子商城样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/shop/theme/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nav_on  | string  | 开启导航 |
| nav_amount_line  | string  | 每行导航数量 |


## 设置电子商城样式参数
* 描述

设置电子商城样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/shop/theme/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nav_on  | string  | 开启导航 |
| nav_amount_line  | string  | 每行导航数量 |

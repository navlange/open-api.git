# 引导页

## 获取引导页背景样式
* 描述

获取引导页背景样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/guide/bg/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bg_color  | string  | 背景色调 |
| bg_img  | string  | 背景图片 |

## 设置引导页背景样式
* 描述

设置引导页背景样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/guide/bg/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bg_color  | string  | 背景色调 |
| bg_img  | string  | 背景图片 |

## 获取引导页跳转样式
* 描述

获取引导页跳转样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/guide/button/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| url  | string  | 跳转链接 |
| button_img  | string  | 跳转图片 |

## 设置引导页跳转样式
* 描述

设置引导页跳转样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/guide/button/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| url  | string  | 跳转链接 |
| button_img  | string  | 跳转图片 |

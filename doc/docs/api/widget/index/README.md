# 首页

## 获取首页样式参数
* 描述

获取首页样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/get

HTTP请求方法：GET

* 请求参数
无

## 获取首页轮播图样式参数
* 描述

获取首页轮播图样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/banner/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| banner_mode  | string  | 轮播图样式 |
| banner_height  | int32  | 轮播图高度 |


## 设置首页轮播图样式参数
* 描述

设置首页轮播图样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/banner/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| banner_mode  | string  | 轮播图样式 |
| banner_height  | int32  | 轮播图高度 |

## 获取首页搜索样式
* 描述

获取首页搜索样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/search/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| search_on  | string  | 搜索开启 |
| search_mode  | string  | 搜索模式 |

## 设置首页搜索样式
* 描述

设置首页搜索样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/search/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| search_on  | string  | 搜索开启 |
| search_mode  | string  | 搜索模式 |

## 获取首页宫格样式
* 描述

获取首页宫格样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/gongge/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| gongge_on  | string  | 宫格开启 |
| gongge_theme  | string  | 宫格样式 |

## 设置首页宫格样式
* 描述

设置首页宫格样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/gongge/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| gongge_on  | string  | 宫格开启 |
| gongge_theme  | string  | 宫格样式 |

## 获取首页服务样式
* 描述

获取首页服务样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/service/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| service_on  | string  | 服务开启 |
| service_theme  | string  | 服务样式 |

## 设置首页服务样式
* 描述

设置首页服务样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/service/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| service_on  | string  | 服务开启 |
| service_theme  | string  | 服务样式 |

## 获取首页演出样式
* 描述

获取首页演出样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/show/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| show_on  | string  | 演出开启 |
| show_theme  | string  | 演出样式 |

## 设置首页演出样式
* 描述

设置首页演出样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/show/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| show_on  | string  | 演出开启 |
| show_theme  | string  | 演出样式 |

## 获取首页次卡样式
* 描述

获取首页次卡样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/level2/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| level2_on  | string  | 次卡开启 |

## 设置首页次卡样式
* 描述

设置首页次卡样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/index/level2/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| level2_on  | string  | 次卡开启 |

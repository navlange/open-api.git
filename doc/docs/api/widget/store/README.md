# 门店

## 获取门店列表样式参数
* 描述

获取门店列表样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/store/list/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| list_theme  | string  | 门店列表样式 |


## 设置门店列表样式参数
* 描述

设置门店列表样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/store/list/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| list_theme  | string  | 门店列表样式 |

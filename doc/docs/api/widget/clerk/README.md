# 门店管理

## 获取门店管理中心样式
* 描述

获取门店管理中心样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/clerk/person/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_qrcode_on  | string  | 开启门店码 |
| combo_on  | string  | 开启团购 |

## 设置门店管理中心样式
* 描述

设置门店管理中心样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/clerk/person/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_qrcode_on  | string  | 开启门店码 |
| combo_on  | string  | 开启团购 |

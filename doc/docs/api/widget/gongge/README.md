# 宫格

## 获取宫格
* 描述

获取宫格。

* 接口

调用方式：HTTP、HTTPS

api: widget/gongge/list

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | string  | 宫格列表 |

## 设置宫格
* 描述

设置宫格。

* 接口

调用方式：HTTP、HTTPS

api: widget/gongge/update

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 宫格ID |
| name  | string  | 名称 |
| img  | string  | 图片 |
| url  | string  | 跳转链接 |
| wxapp_url  | string  | 跳转小程序链接 |

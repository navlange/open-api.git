# 组态

## 获取组态参数
* 描述

获取组态参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/conf/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id_number_on  | string  | 开启身份证信息 |

## 设置组态参数
* 描述

设置组态参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/conf/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id_number_on  | string  | 开启身份证信息 |

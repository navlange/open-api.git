# 菜单

## 获取菜单样式参数
* 描述

获取菜单样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/theme/menu/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| menu_theme  | string  | 菜单样式 |


## 设置菜单样式参数
* 描述

设置菜单样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/theme/menu/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| menu_theme  | string  | 菜单样式 |

## 获取菜单列表
* 描述

获取菜单列表。

* 接口

调用方式：HTTP、HTTPS

api: widget/menu/list

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| menu_list  | array  | 菜单项列表 |

## 获取显示菜单列表
* 描述

获取显示菜单列表。

* 接口

调用方式：HTTP、HTTPS

api: widget/menu/displayList

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 显示菜单项列表 |

## 修改菜单项
* 描述

修改菜单项。

* 接口

调用方式：HTTP、HTTPS

api: widget/menu/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 菜单名称 |
| icon  | string  | 菜单图片 |
| icon_active  | string  | 菜单激活图片 |
| url  | string  | 菜单跳转链接 |
| wxapp_url  | string  | 菜单跳转小程序链接 |
| display  | string  | 是否显示 |
| active_only_icon  | string  | 菜单仅显示图片 |

## 恢复默认菜单项
* 描述

恢复默认菜单项。

* 接口

调用方式：HTTP、HTTPS

api: widget/menu/reset

HTTP请求方法：POST

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| menu_list  | array  | 菜单项列表 |

# 次卡

## 获取次卡列表样式
* 描述

获取次卡列表样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/level2/list/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| level2_list_theme  | string  | 次卡列表样式 |

## 设置次卡列表样式
* 描述

设置次卡列表样式。

* 接口

调用方式：HTTP、HTTPS

api: widget/level2/list/set

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| level2_list_theme  | string  | 次卡列表样式 |

# 个人中心

## 获取个人中心样式参数
* 描述

获取个人中心样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| theme  | Object  | 个人中心样式 |

## 获取个人中心顶部样式参数
* 描述

获取个人中心顶部样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/head/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| head_bg_color  | string  | 背景色 |
| head_bg_img  | string  | 背景图片 |
| head_bg_bottom_img  | string  | 背景图片底部 |


## 设置个人中心顶部样式参数
* 描述

设置个人中心顶部样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/head/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| head_bg_color  | string  | 背景色 |
| head_bg_img  | string  | 背景图片 |
| head_bg_bottom_img  | string  | 背景图片底部 |

## 获取个人中心导航样式参数
* 描述

获取个人中心导航样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/nav/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nav_on  | string  | 是否启用 |
| nav_amount_line  | int  | 每行数量 |


## 设置个人中心导航样式参数
* 描述

设置个人中心导航样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/nav/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nav_on  | string  | 是否启用 |
| nav_amount_line  | int  | 每行数量 |

## 获取个人中心会员样式参数
* 描述

获取个人中心会员样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/member/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| member_on  | string  | 会员板块启用 |


## 设置个人中心会员样式参数
* 描述

设置个人中心会员样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/member/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| member_on  | string  | 会员板块启用 |

## 获取个人中心支付样式参数
* 描述

获取个人中心支付样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/pay/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| pay_account_on  | string  | 支付板块启用 |


## 设置个人中心支付样式参数
* 描述

设置个人中心支付样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/pay/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| pay_account_on  | string  | 支付板块启用 |

## 获取个人中心订单样式参数
* 描述

获取个人中心订单样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/order/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_theme  | string  | 订单样式 |


## 设置个人中心订单样式参数
* 描述

设置个人中心订单样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/order/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| order_theme  | string  | 订单样式 |

## 获取个人中心列表样式参数
* 描述

获取个人中心列表样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/list/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| list_on  | string  | 是否启用 |


## 设置个人中心列表样式参数
* 描述

设置个人中心列表样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/person/list/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| list_on  | string  | 是否启用 |

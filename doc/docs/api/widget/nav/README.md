# 导航

## 获取导航列表
* 描述

获取导航列表。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/list

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nav_list  | array  | 导航项列表 |

## 获取显示导航列表
* 描述

获取显示导航列表。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/displayList

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 显示导航项列表 |

## 修改导航项
* 描述

修改导航项。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 导航名称 |
| img  | string  | 导航图片 |
| url  | string  | 导航跳转链接 |
| wxapp_url  | string  | 导航跳转小程序链接 |
| display  | string  | 是否显示 |

## 添加导航项
* 描述

添加导航项。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/add

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 导航项模式 |

## 删除导航项
* 描述

删除导航项。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 导航项ID |

## 获取导航图片参数
* 描述

获取导航图片参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/pic/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 导航图片模式 |
| img  | string  | 导航图片 |
| path  | string  | 导航路径 |
| display  | string  | 是否显示 |


## 设置导航图片参数
* 描述

设置导航图片参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/pic/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mode  | string  | 导航图片模式 |
| img  | string  | 导航图片 |
| path  | string  | 导航路径 |
| display  | string  | 是否显示 |

## 导航图热点

### 获取导航图热点列表
* 描述

获取导航图热点列表。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/pic/hot/get

HTTP请求方法：GET

* 请求参数
|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nav_pic_id  | int32  | 导航图ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 导航图热点列表 |

### 添加导航图热点项
* 描述

添加导航图热点项。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/pic/hot/add

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nav_pic_id  | int32  | 导航图ID |

### 修改导航图热点
* 描述

修改导航图热点。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/pic/hot/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nav_pic_id  | int32  | 导航图ID |
| mode  | string  | 导航图热点模式 |
| width  | int32  | 导航图热点长 |
| height  | int32  | 导航图热点高 |
| left  | int32  | 导航图热点左边距 |
| top  | int32  | 导航图热点上边距 |
| page  | string  | 导航图热点跳转链接 |

### 删除导航图热点
* 描述

删除导航图热点。

* 接口

调用方式：HTTP、HTTPS

api: widget/nav/pic/hot/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 导航图热点ID |

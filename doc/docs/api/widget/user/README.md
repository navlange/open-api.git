# 用户

## 用户列表
### 获取用户列表列表样式参数
* 描述

获取用户列表列表样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/user/list/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| district_on  | string  | 地区 |
| career_on  | string  | 职业 |


### 设置用户列表列表样式参数
* 描述

设置用户列表列表样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/user/list/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| district_on  | string  | 地区 |
| career_on  | string  | 职业 |

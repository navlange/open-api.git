# 轮播图

## 获取轮播图列表
* 描述

获取轮播图列表。

* 接口

调用方式：HTTP、HTTPS

api: widget/banner/list

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| banner_list  | array  | 轮播图列表 |

## 添加轮播图
* 描述

添加轮播图。

* 接口

调用方式：HTTP、HTTPS

api: widget/banner/add

HTTP请求方法：POST

* 请求参数

## 修改轮播图
* 描述

修改轮播图。

* 接口

调用方式：HTTP、HTTPS

api: widget/banner/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| img  | string  | 轮播图 |
| url  | string  | 轮播图跳转链接 |

## 删除轮播图
* 描述

删除轮播图。

* 接口

调用方式：HTTP、HTTPS

api: widget/banner/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 轮播图id |
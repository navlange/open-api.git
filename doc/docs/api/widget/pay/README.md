# 支付

## 获取支付页面参数
* 描述

获取支付页面参数。

* 接口

调用方式：HTTP、HTTPS

api: theme/pay/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| consignee_name_on  | string  | 是否录入姓名 |


## 设置支付页面参数
* 描述

设置支付页面参数。

* 接口

调用方式：HTTP、HTTPS

api: theme/pay/set

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| consignee_name_on  | string  | 是否录入姓名 |

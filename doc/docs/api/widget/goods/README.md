# 商品

## 获取商品详情页样式参数
* 描述

获取商品详情页样式参数。

* 接口

调用方式：HTTP、HTTPS

api: theme/goods/index/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| tag_title  | string  | 标签信息标题 |
| stock_theme  | string  | 库存数量样式 |


## 设置样式参数
* 描述

设置样式参数。

* 接口

调用方式：HTTP、HTTPS

api: theme/goods/index/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| tag_title  | string  | 标签信息标题 |
| stock_theme  | string  | 库存数量样式 |

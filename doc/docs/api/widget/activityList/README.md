# 活动列表

## 获取活动列表样式参数
* 描述

获取活动列表样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/activity/list/get

HTTP请求方法：GET

* 请求参数
无

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| list_theme  | string  | 活动列表样式 |
| client_release_on  | string  | 用户发布活动入口 |

## 设置活动列表样式参数
* 描述

设置活动列表样式参数。

* 接口

调用方式：HTTP、HTTPS

api: widget/activity/list/update

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| list_theme  | string  | 活动列表样式 |
| client_release_on  | string  | 用户发布活动入口 |

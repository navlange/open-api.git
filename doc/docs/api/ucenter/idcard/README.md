# 身份证信息
用户身份证信息管理、查看、修改接口。

## 查询用户身份证信息
* 描述

查询用户的身份证信息。


* 接口

调用方式：HTTP、HTTPS

api: user/idcard/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 用户uid |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| idcard  | string  | 用户的身份证号 |
| idcard_img  | string  | 用户的身份证正面照 |
| idcard_bg_img  | string  | 用户的身份证背面照 |

## 查询登入用户身份证信息
* 描述

查询登入用户的身份证信息。


* 接口

调用方式：HTTP、HTTPS

api: user/idcard/ofMe

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| idcard  | string  | 用户的身份证号 |
| idcard_img  | string  | 用户的身份证正面照 |
| idcard_bg_img  | string  | 用户的身份证背面照 |

## 修改登入用户身份证信息
* 描述

修改登入用户的身份证信息。


* 接口

调用方式：HTTP、HTTPS

api: user/idcard/updateOfMe

HTTP请求方法：POST

* 请求参数

当前登入用户

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| idcard  | string  | 用户的身份证号 |
| idcard_img  | string  | 用户的身份证正面照 |
| idcard_bg_img  | string  | 用户的身份证背面照 |

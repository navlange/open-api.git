# 用户登入信息
用户信息管理、查看、修改接口。

## 修改用户登入名
* 描述

修改用户登入名。


* 接口

调用方式：HTTP、HTTPS

api: user/username/update

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 用户ID |
| username  | string  | 用户登入名 |

## 修改用户登入密码
* 描述

修改用户登入密码。


* 接口

调用方式：HTTP、HTTPS

api: user/password/update

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 用户ID |
| password  | string  | 密码 |

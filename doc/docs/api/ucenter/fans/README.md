# 粉丝管理
粉丝信息管理、查看、修改接口。

## 查询粉丝列表
* 描述

获取粉丝列表。


* 接口

调用方式：HTTP、HTTPS

api: fans/list

HTTP请求方法：GET

* 请求参数

当前登入用户

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | array  | 粉丝列表 |
| total  | int32  | 粉丝数量 |

## 查询用户绑定的粉丝
* 描述

获取用户绑定的粉丝。


* 接口

调用方式：HTTP、HTTPS

api: fans/ofUser

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 用户uid |
| platform  | string  | 粉丝平台：公众号、小程序等 |


## 绑定粉丝到用户
* 描述

绑定粉丝到已添加用户。


* 接口

调用方式：HTTP、HTTPS

api: fans/bindUser

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 用户uid |
| code  | string  | 微信公众号授权code |

## 重新绑定粉丝到用户
* 描述

重新绑定粉丝到已添加用户。


* 接口

调用方式：HTTP、HTTPS

api: fans/rebindUser

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| openid  | string  | 粉丝openid |
| mobile  | string  | 用户手机号 |


## 删除粉丝
* 描述

删除粉丝。


* 接口

调用方式：HTTP、HTTPS

api: fans/delete

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| fid  | int32  | 粉丝fid |


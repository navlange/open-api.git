# 用户手机号
用户手机号管理、查看、修改接口。

## 查询用户微信绑定手机号
* 描述

查询用户微信绑定的手机号。


* 接口

调用方式：HTTP、HTTPS

api: mobile/getWxBind

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| code  | string  | 微信小程序手机号接口获取code |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| phoneNumber  | string  | 用户微信绑定的手机号 |

## 查询用户支付宝绑定手机号
* 描述

查询用户支付宝绑定的手机号。


* 接口

调用方式：HTTP、HTTPS

api: mobile/getAliBind

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| response  | string  | 支付宝小程序手机号接口获取response |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mobile  | string  | 用户支付宝绑定的手机号 |

## 修改登入用户手机号
* 描述

修改登入用户的手机号。


* 接口

调用方式：HTTP、HTTPS

api: mobile/updateOfMe

HTTP请求方法：POST

* 请求参数

当前登入用户

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mobile  | string  | 用户手机号 |

# 银行信息
用户银行信息管理、查看、修改接口。

## 查询银行卡信息
* 描述

查询的银行卡信息。


* 接口

调用方式：HTTP、HTTPS

api: user/bank/get

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| id  | int32  | 银行卡id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bank_number  | string  | 银行卡号 |
| bank_type  | string  | 银行类型 |
| bank_people  | string  | 开户人 |
| bank_branch  | string  | 开户行支行 |

## 查询登入用户银行信息
* 描述

查询登入用户的银行信息。


* 接口

调用方式：HTTP、HTTPS

api: user/bank/ofMe

HTTP请求方法：GET

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bank_number  | string  | 银行卡号 |
| bank_type  | string  | 银行类型 |
| bank_people  | string  | 开户人 |
| bank_branch  | string  | 开户行支行 |

## 修改登入用户银行信息
* 描述

修改登入用户的银行信息。


* 接口

调用方式：HTTP、HTTPS

api: user/bank/updateOfMe

HTTP请求方法：POST

* 请求参数

当前登入用户

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| bank_number  | string  | 银行卡号 |
| bank_type  | string  | 银行类型 |
| bank_people  | string  | 开户人 |
| bank_branch  | string  | 开户行支行 |

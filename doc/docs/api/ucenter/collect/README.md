# 收藏
用户收藏管理、查看、修改接口。

## 查询是否收藏对象
* 描述

查询是否收藏对象。


* 接口

调用方式：HTTP、HTTPS

api: collect/ofGoods

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| goods_id  | int32  | 商品id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| collected  | string  | 是否已收藏 |

## 查询是否收藏门店
* 描述

查询是否收藏门店。


* 接口

调用方式：HTTP、HTTPS

api: collect/ofStore

HTTP请求方法：GET

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| store_id  | int32  | 门店id |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| collected  | string  | 是否已收藏 |

## 查询我的收藏
* 描述

查询我的收藏。


* 接口

调用方式：HTTP、HTTPS

api: collect/ofMy

HTTP请求方法：GET

## 添加收藏
* 描述

添加收藏。


* 接口

调用方式：HTTP、HTTPS

api: collect/add

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| goods_id  | int32  | 商品ID |

## 删除收藏
* 描述

删除收藏。


* 接口

调用方式：HTTP、HTTPS

api: collect/del

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| goods_id  | int32  | 商品ID |

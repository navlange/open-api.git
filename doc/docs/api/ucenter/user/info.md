# 用户信息
用户信息管理、查看、修改接口。

## 查询用户列表
* 描述

查询用户列表。


* 接口

调用方式：HTTP、HTTPS

api: user/list

HTTP请求方法：GET

* 请求参数

当前登入用户

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| items  | int32  | 用户列表 |
| total  | int32  | 用户列表数量 |

## 查询用户信息
* 描述

查询用户信息。


* 接口

调用方式：HTTP、HTTPS

api: user/get

HTTP请求方法：GET

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 用户ID |

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mobile  | string  | 用户手机号 |

## 查询手机号用户信息
* 描述

查询手机号用户信息。


* 接口

调用方式：HTTP、HTTPS

api: user/getByMobile

HTTP请求方法：GET

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| mobile  | string  | 用户手机号 |


## 修改用户信息
* 描述

修改用户信息。


* 接口

调用方式：HTTP、HTTPS

api: user/update

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| uid  | int32  | 用户ID |
| mobile  | string  | 用户手机号 |

## 登入用户注册
* 描述

当前登入用户注册。


* 接口

调用方式：HTTP、HTTPS

api: user/register

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 姓名 |
| mobile  | string  | 手机号 |

## 修改登入用户信息
* 描述

修改当前登入用户信息。


* 接口

调用方式：HTTP、HTTPS

api: user/updateOfMe

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| nickname  | string  | 昵称 |
| avatar  | string  | 头像 |

## 修改登入用户姓名
* 描述

修改当前登入用户姓名。


* 接口

调用方式：HTTP、HTTPS

api: user/name/updateOfMe

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| name  | string  | 姓名 |

## 查询当前用户登入用户名
* 描述

查询当前用户登入用户名。


* 接口

调用方式：HTTP、HTTPS

api: user/username/queryOfMe

HTTP请求方法：GET

* 请求成功返回值


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| username  | string  | 姓名 |

## 修改登入用户登入用户名
* 描述

修改登入用户登入用户名。


* 接口

调用方式：HTTP、HTTPS

api: user/username/updateOfMe

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| username  | string  | 姓名 |

## 查询当前用户登入密码
* 描述

查询当前用户登入密码。


* 接口

调用方式：HTTP、HTTPS

api: user/password/ofMe

HTTP请求方法：GET

* 请求成功返回值


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| is_set  | string  | 密码是否已设置过，'1'代表已设置，'0'代表尚未设置 |

## 修改当前用户登入密码
* 描述

修改当前用户登入密码。


* 接口

调用方式：HTTP、HTTPS

api: user/password/updateOfMe

HTTP请求方法：POST

* 请求参数


|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| old_password  | string  | 当前密码，如果之前已经设置过密码需要提供old_pasword参数 |
| password  | string  | 密码 |

## 查询微信小程序用户信息
* 描述

查询微信小程序用户信息。


* 接口

调用方式：HTTP、HTTPS

api: user/info/ofMiniapp

HTTP请求方法：GET

* 请求参数

当前登入用户

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| openid  | string  | 微信小程序用户openid |
| realname  | string  | 用户姓名 |
| nickname  | string  | 用户昵称 |
| avatar  | string  | 用户头像 |
| mobile  | string  | 用户手机号 |

## 查询微信公众号用户信息
* 描述

查询微信公众号用户信息。


* 接口

调用方式：HTTP、HTTPS

api: user/info/ofMp

HTTP请求方法：GET

* 请求参数

当前登入用户

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| openid  | string  | 微信公众号用户openid |
| realname  | string  | 用户姓名 |
| nickname  | string  | 用户昵称 |
| avatar  | string  | 用户头像 |
| mobile  | string  | 用户手机号 |

## 查询企业微信用户信息
* 描述

查询企业微信用户信息。


* 接口

调用方式：HTTP、HTTPS

api: user/info/ofQyapp

HTTP请求方法：GET

* 请求参数

当前登入用户

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| openid  | string  | 微信小程序用户openid |
| realname  | string  | 用户姓名 |
| nickname  | string  | 用户昵称 |
| avatar  | string  | 用户头像 |
| mobile  | string  | 用户手机号 |

## 查询支付宝小程序用户信息
* 描述

查询支付宝小程序用户信息


* 接口

调用方式：HTTP、HTTPS

api: user/info/ofAlipay

HTTP请求方法：GET

* 请求参数

当前登入用户

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| openid  | string  | 支付宝小程序用户openid |

## 获取登入用户区县
* 描述

获取当前登入用户区县。


* 接口

调用方式：HTTP、HTTPS

api: user/district/ofMe

HTTP请求方法：POST

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| district  | string  | 区县 |


## 修改登入用户区县
* 描述

修改当前登入用户区县。


* 接口

调用方式：HTTP、HTTPS

api: user/district/updateOfMe

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| district  | string  | 区县 |

## 获取登入用户职业
* 描述

获取当前登入用户职业。


* 接口

调用方式：HTTP、HTTPS

api: user/career/ofMe

HTTP请求方法：POST

* 请求成功返回值

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| career  | string  | 职业 |


## 修改登入用户职业
* 描述

修改当前登入用户职业。


* 接口

调用方式：HTTP、HTTPS

api: user/career/updateOfMe

HTTP请求方法：POST

* 请求参数

|  名称 |  类型  | 描述  |
|  ----  | ----  |  ----  |
| career  | string  | 职业 |
# 数据字典

## Redis变量

奇辰Open-API广泛采用Redis作为缓存技术，主要变量定义如下：

### 第三方平台

|  名称 |  解释 |
|  ----  | ----  |
| qiwei_access_token  | 企业微信接口Access token  |
| mp_access_token  | 微信公众号接口Access token  |

### 商家门店

|  名称 |  解释 |
|  ----  | ----  |
| store_statistic_$project_identity_$store_identity_$day  | $store_identity唯一标识的商家门店在$day日的统计信息, $project_identity用于标识项目的唯一性  |
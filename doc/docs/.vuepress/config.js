module.exports = {
    head: [
        [
            'link', // 设置 favicon.ico，注意图片放在 public 文件夹下
            { rel: 'icon', href: '/img/logo-red.png' }
        ]
    ],
    title: '奇辰Open-API',
    base: '/doc/', //默认路径
    themeConfig: {
        logo: '/img/logo-red.png',
        nav: [
            { text: 'Open-ERP', link: 'https://www.lokei.cn/erp/doc' },
            { text: '官网', link: 'https://www.lokei.cn' },
            // 可指定链接跳转模式：默认target: '_blank'新窗口打开，_self当前窗口打开
            // { text: '百度', link: 'https://www.baidu.com' },
            // { text: 'CSDN', link: 'https://blog.csdn.net', target: '_blank' },
            // { text: '豆瓣', link: 'https://movie.douban.com', target: '_self', rel: '' },
            // // 支持嵌套,形成下拉式的导航菜单
            // {
            //     text: '语言',
            //     ariaLabel: 'Language Menu',
            //     items: [
            //         { text: '中文', link: '/language/chinese/' },
            //         { text: '英文', link: '/language/english/' }
            //     ]
            // }
        ],
        sidebar: [
            '/',
            '/deploy/',
            {
                title: '框架介绍',
                children: [
                    '/intro/def'
                ]
            },
            {
                title: 'API接口',
                children: [
                    '/api/common/specification',
                    {
                        title: 'RBAC权限',
                        children: [
                            '/api/rbac/auth/',
                            '/api/rbac/permission/',
                            '/api/rbac/role/'
                        ]
                    },
                    {
                        title: '用户中心',
                        children: [
                            '/api/ucenter/user/info',
                            '/api/ucenter/login/',
                            '/api/ucenter/fans/',
                            '/api/ucenter/mobile/',
                            '/api/ucenter/idcard/',
                            '/api/ucenter/bank/',
                            '/api/ucenter/collect/',
                            '/api/clerk/'
                        ]
                    },
                    {
                        title: 'CMS内容管理',
                        children: [
                            '/api/cms/form',
                            '/api/cms/notice/',
                            '/api/cms/article/',
                            '/api/cms/poster/',
                            '/api/cms/qrcode/',
                            '/api/cms/suggest/',
                            '/api/cms/comment/',
                            '/api/cms/link/',
                            '/api/cms/works/',
                        ]
                    },
                    {
                        title: 'SCRM客户关系',
                        children: [
                            '/api/scrm/department/',
                            '/api/scrm/fx/',
                            '/api/scrm/customer/'
                        ]
                    },
                    {
                        title: '会员系统',
                        children: [
                            '/api/member/coupon/',
                            '/api/member/integral/',
                            '/api/member/level/',
                            '/api/member/member/',
                            '/api/member/level2/'
                        ]
                    },
                    {
                        title: '城市商家',
                        children: [
                            '/api/store/statistic/',
                            '/api/store/store/',
                            '/api/store/tag/',
                            '/api/store/conf/',
                            '/api/store/city/',
                            '/api/store/area/',
                            '/api/store/district/',
                            '/api/store/consignee/',
                            '/api/store/credit/'
                        ]
                    },
                    {
                        title: 'Iot物联网',
                        children: [
                            '/api/iot/device/',
                            '/api/iot/gateway/'
                        ]
                    },
                    {
                        title: '平台接口',
                        children: [
                            '/api/platform/statistic/',
                            '/api/platform/conf/',
                            '/api/platform/log/',
                            '/api/platform/upload/',
                            '/api/platform/tag/',
                            '/api/platform/type/',
                            '/api/platform/position/',
                            '/api/platform/print/',
                            '/api/platform/alias/',
                            '/api/platform/brand/',
                            '/api/platform/search/',
                            '/api/platform/skill/',
                            '/api/platform/agreement/',
                            '/api/platform/career/',
                        ]
                    },
                    {
                        title: '模式样式',
                        children: [
                            '/api/widget/conf/',
                            '/api/widget/clerk/',
                            '/api/widget/theme/',
                            '/api/widget/banner/',
                            '/api/widget/gongge/',
                            '/api/widget/menu/',
                            '/api/widget/nav/',
                            '/api/widget/index/',
                            '/api/widget/person/',
                            '/api/widget/shop/',
                            '/api/widget/store/',
                            '/api/widget/activityList/',
                            '/api/widget/goods/',
                            '/api/widget/pay/',
                            '/api/widget/member/level2coupon',
                            '/api/widget/schedule/',
                            '/api/widget/user/',
                            '/api/widget/guide/'
                        ]
                    },
                    {
                        title: '支付收银',
                        children: [
                            '/api/pay/account/',
                            '/api/pay/agreement/',
                            '/api/pay/income/',
                            '/api/pay/withdraw/',
                            '/api/pay/wx/',
                            '/api/pay/alipay/',
                            '/api/pay/card/',
                            '/api/pay/card/record',
                            '/api/pay/cash/',
                            '/api/pay/record/',
                            '/api/pay/exchange/',
                            '/api/pay/recharge/'
                        ]
                    },
                    {
                        title: '订单中心',
                        children: [
                            '/api/order/conf/',
                            '/api/order/goods/',
                            '/api/order/change/',
                            '/api/order/op/',
                            '/api/order/delivery/',
                            '/api/order/verify/'
                        ]
                    },
                    {
                        title: '消息中心',
                        children: [
                            '/api/message/message/',
                            '/api/message/template/',
                            '/api/message/miniapp/'
                        ]
                    },
                    {
                        title: '活动中心',
                        children: [
                            '/api/activity/activity/',
                            '/api/activity/join/',
                            '/api/activity/gift/',
                            '/api/activity/prize/'
                        ]
                    },
                    {
                        title: '行业应用',
                        children: [
                            '/api/application/master/',
                            '/api/application/master/service',
                            '/api/application/schedule/',
                            '/api/application/washer/',
                            '/api/application/zl/'
                        ]
                    },
                    {
                        title: '事物对象',
                        children: [
                            '/api/things/product/',
                            '/api/things/goods/',
                            '/api/things/service/',
                            '/api/things/seat/',
                            '/api/things/court/',
                            '/api/things/show/',
                            '/api/things/ticket/'
                        ]
                    },
                    {
                        title: '出行服务',
                        children: [
                            '/api/application/driver/',
                            '/api/application/passenger/',
                            '/api/application/travel/',
                            '/api/application/line/',
                            '/api/application/topic/'
                        ]
                    },
                    {
                        title: '物流系统',
                        children: [
                            '/api/delivery/company/',
                            '/api/delivery/delivery/'
                        ]
                    },
                    {
                        title: '社区论坛',
                        children: [
                            '/api/community/topic/'
                        ]
                    },
                    {
                        title: '营销系统',
                        children: [
                            '/api/marketing/show/'
                        ]
                    }
                ]
            },
            {
                title: 'IoT物联网',
                children: [
                    '/iot/mqtt/',
                    '/iot/coap/'
                ]
            },
            {
                title: '常见问题',
                children: [
                    '/qa/'
                ]
            }
        ]
    },
    markdown: {
        lineNumbers: true
    }
}
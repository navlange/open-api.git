import { request } from '@/common/js/request';

export function ofMiniapp(query) {
	return request('user/info/ofMiniapp', query, 'GET')
}

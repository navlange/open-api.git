import { request } from '@/common/js/request';

export function checkLogin() {
	const token = uni.getStorageSync('token');
	if (token !== null && token !== '' && token !== undefined) {
		return true;
	} else {
		return false;
	}
}

export function logout() {
	let token = uni.getStorageSync('token')
	uni.removeStorageSync('token')
	uni.removeStorageSync('user_info')
	return request('auth/logout', {
		token: token
	}, 'POST')
}

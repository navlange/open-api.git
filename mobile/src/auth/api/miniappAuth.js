import { request } from '@/common/js/request';
import { ofMiniapp as userOfMiniapp } from '@/ucenter/api/user/info'

export function login() {
	try {
		return new Promise((resolve, reject) => {
			uni.login({
				provider: 'weixin',
				success: (loginRes) => {
					let data = {
						code: loginRes.code
					}
					request(
						'auth/miniapp/login', data,
						'POST'
					)
						.then((res) => {
							//成功回调
							if (res.errorCode == 200) {
								uni.setStorageSync('token', res.data.token);
								userOfMiniapp().then(res => {
									uni.setStorageSync('user_info', res.data)
								})
								resolve(res.data.token);
							} else {
								reject(res)
							}
						})
						.catch((err) => {
							//请求失败
							reject(err);
						});
				},
			});
		});
	} catch (e) {
		// error
	}
}

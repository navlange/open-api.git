import { request } from '@/common/js/request';

export function ofSim(query) {
	return request('iot/device/ofSim', query, 'GET')
}

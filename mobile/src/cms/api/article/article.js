import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function get(query) {
  return request(openapi_url('/article/get'), query, 'GET')
}

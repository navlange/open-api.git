import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function submit(query) {
  return request(openapi_url('/form/submit'), query, 'POST')
}

import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function list(query) {
  return request(openapi_url('/form/item/list'), query, 'GET')
}

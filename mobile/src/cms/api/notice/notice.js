import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function get(query) {
  return request('/cms-api/notice/get', query, 'GET')
}

export function queryOfStore(query) {
  return request('/cms-api/notice/ofStore', query, 'GET')
}

export function queryOfProj(query) {
  return request('/cms-api/notice/list', query, 'GET')
}

export function validate(val, type, rule) {
    let result = {
        status: true,
        msg: ''
    }
    switch(type) {
        case 'String':
            switch(rule) {
                case 'required':
                    if(val === '' || val === 'undefined' || val === null) {
                        result.status = false
                    }
                    break
                default:
                    break
            }
            break
        case 'Array':
            switch(rule) {
                case 'required':
                    if(val === null || val.length <= 0) {
                        result.status = false
                    }
                    break
                default:
                    break
            }
            break
        case 'Mobile':
            if(!(/^1[3456789]\d{9}$/.test(val))){
                result.status = false
                result.msg = '请输入正确格式手机号！'
            }
            break 
        default:
            break
    }
    return result
}

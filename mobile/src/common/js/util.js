export function getUrlCode() {
	// 截取url中的code方法
	var url = location.href; //获取打开的公众号的路径
	let winUrl = url;
	var theRequest = new Object();
	if (url.indexOf('?') != -1) {
		var str = url.substr(url.indexOf('?') + 1);
		var strs = str.split('&');
		for (var i = 0; i < strs.length; i++) {
			var items = strs[i].split('=');
			theRequest[strs[i].split('=')[0]] = strs[i].split('=')[1];
		}
	}
	return theRequest;
}

export function object_key_exist(obj, key) {
	if (obj instanceof Object) {
		const keys = Object.keys(obj)
		for (var i = 0; i < keys.length; i++) {
			if (keys[i] == key) {
				return true
			}
		}
	}
	return false
}

export function isJSON(str) {
	try {
		let obj = JSON.parse(str);
		return !!obj && typeof obj === 'object';
	} catch (e) { }
	return false;
}

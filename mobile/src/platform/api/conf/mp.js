import { request } from '@/common/js/request';

export function conf(query) {
	return request('conf/mp/get', query, 'GET')
}

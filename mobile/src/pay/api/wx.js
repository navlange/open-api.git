import { request } from '@/common/js/request';

export function pay(query) {
	return request('pay/wx/pay', query, 'POST')
}
